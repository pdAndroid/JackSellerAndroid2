package com.party.jackseller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.api.UserService;
import com.party.jackseller.base.MyException;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.LoginBean;
import com.party.jackseller.bean.RegisterInfo;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.User;
import com.party.jackseller.controller.VerCodeTimer;
import com.party.jackseller.event.AuthResultEvent;
import com.party.jackseller.uimy.MyShopManagerActivity;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.PhoneUtil;
import com.party.jackseller.wxapi.WXPayUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

public class RegisterActivity extends BaseActivityTitle {

    private UserService userService;
    private AppInfo mAppInfo;

    @BindView(R.id.et_account)
    EditText mPhoneEdit;

    @BindView(R.id.et_code)
    EditText mVerCodeEdit;

    @BindView(R.id.getVerCodeMessageBtn)
    Button getVerCodeMessageBtn;

    @BindView(R.id.getVerCodePhoneBtn)
    TextView getVerCodePhoneBtn;

    @BindView(R.id.phone_parent_layout)
    View phone_parent_layout;

    @BindView(R.id.invite_phone_et)
    EditText invite_phone_et;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initData();
    }


    protected void initData() {
        userService = new UserService(this);

        mAppInfo = (AppInfo) getIntent().getSerializableExtra("appInfo");
        userService = new UserService(this);
        if (mAppInfo == null) {
            getAppId();
        } else {
            getWxCode(mAppInfo);
        }
    }

    /**
     * 获取一键登录信息。
     */
    private void getAppId() {
        showAlertDialog();
        userService.getAppId(new DefaultConsumer<AppInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AppInfo> baseBean) {
                mAppInfo = baseBean.getData();
                getWxCode(mAppInfo);
                hideAlertDialog();
            }
        });
    }

    @OnClick({R.id.register_btn, R.id.login_tv_back})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.register_btn:
                register();
                break;
            case R.id.login_tv_back:
                finish();
                break;
        }
    }


    public void getWxCode(AppInfo appInfo) {
        try {
            WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
            build.getUserCode(mActivity, appInfo.getWxAppId());
        } catch (MyException e) {
            showToast(e.getMessage());
        }
    }

    @OnClick({R.id.getVerCodePhoneBtn, R.id.getVerCodeMessageBtn})
    public void clickGetVerCode(View view) {
        String type = "0";
        switch (view.getId()) {
            case R.id.getVerCodePhoneBtn:
                type = "1";
                break;
        }

        String phone = mPhoneEdit.getText().toString().trim();
        try {
            CheckUtils.checkData(!PhoneUtil.isMobileNO(phone), mPhoneEdit, "号码格式有误");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            shake(e.getView());
            return;
        }

        addDisposableIoMain(userService.getVerCode(phone, type), new DefaultConsumer<Object>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Object> baseBean) {
                VerCodeTimer mVerCodeTimer = new VerCodeTimer(60, getVerCodePhoneBtn);
                mVerCodeTimer.setChangeView(mPhoneEdit, getVerCodePhoneBtn);
                mVerCodeTimer.start();
            }
        });


    }


    /**
     * 微信授权成功,回调
     *
     * @param authResultEvent
     */
    @Subscribe(priority = 100)//011sh1Pi03TPAn1aStPi0SiFOi0sh1P9
    public void authFinish(AuthResultEvent authResultEvent) {
        EventBus.getDefault().cancelEventDelivery(authResultEvent);
        checkWxIsRegister(authResultEvent.getCode());
    }

    public void checkWxIsRegister(String code) {
        showAlertDialog();
        addDisposableIoMain(userService.checkWxIsRegister(code), new DefaultConsumer<RegisterInfo>(mApplication) {
            @Override
            public void operateError(BaseResult<RegisterInfo> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity, "温馨提示", "该微信已经绑定账户", (View v) -> {
                    finish();
                });
            }

            @Override
            public void operateSuccess(BaseResult<RegisterInfo> baseBean) {
                mAppInfo.setRegisterKey(baseBean.getData().getRegisterKey());
            }
        });
    }

    public void register() {
        showAlertDialog("正在登录中，请稍后。。。");
        try {
            String inviteCode = CheckUtils.checkPhone(invite_phone_et, "没有输入推荐人");
            String phone = CheckUtils.checkPhone(mPhoneEdit, "号码格式有误");
            String verCode = CheckUtils.checkVerCode(mVerCodeEdit, "请输入验证码");
            addDisposableIoMain(userService.registerUser(mAppInfo.getRegisterKey(), phone, verCode, inviteCode), new DefaultConsumer<User>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<User> baseBean) {
                    hideAlertDialog();
                    handleLoginInfo(baseBean);
                }
            });
        } catch (MyNullException e) {
            showToast(e.getMessage());
            shake(e.getView());
            return;
        }

    }


    public void handleLoginInfo(BaseResult<User> baseBean) {
        if (baseBean.getData().getToken() != null) {
            mApplication.cleanUser();
            User user = baseBean.getData();//强转 去掉 店铺List
            mApplication.setUser(user);
        } else {
            showToast("数据有误");
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
