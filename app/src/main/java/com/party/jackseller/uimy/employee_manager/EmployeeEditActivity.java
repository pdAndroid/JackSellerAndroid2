package com.party.jackseller.uimy.employee_manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.EmployeeService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Employee;
import com.party.jackseller.bean.EmployeeRole;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.widget.dialog.EditTextDialog;
import com.party.jackseller.widget.dialog.SubmitSuccessDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.qqtheme.framework.picker.OptionPicker;

/**
 * 我的-员工管理-编辑员工
 */
public class EmployeeEditActivity extends BaseActivityTitle {

    @BindView(R.id.real_name_tv)
    TextView real_name_tv;
    @BindView(R.id.role_tv)
    TextView role_tv;
    @BindView(R.id.phone_tv)
    TextView phone_tv;
    @BindView(R.id.whole_layout)
    ViewGroup whole_layout;

    private EmployeeService employeeService;
    private Employee employee;
    private List<EmployeeRole> employeeRoleList = new ArrayList<>();
    private String[] employeeRoleArr;
    private boolean isEdit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_edit);
        initData();
        receivePassDataIfNeed(getIntent());
    }

    protected void receivePassDataIfNeed(Intent intent) {
        employee = (Employee) intent.getSerializableExtra("employee");
    }

    protected void initData() {
        handleTitle();
        setMiddleText("员工编辑");
        employeeService = new EmployeeService(this);
//        real_name_tv.setText(employee.getAlias_name() + "");
//        role_tv.setText(employee.getRole_name());
//        phone_tv.setText(employee.getPhone());
        helper = new LoadViewHelper(whole_layout);
        helper.showLoading();
        getRoleListData();
    }

    @OnClick({R.id.edit_btn, R.id.save_btn, R.id.delete_btn, R.id.role_ll})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.edit_btn:
                DialogController.showEditDialog(mActivity, "请输入名称", employee.getAlias_name(), new EditTextDialog.DialogListener() {
                    @Override
                    public void cancel(EditTextDialog dialog) {
                        dialog.dismiss();
                    }

                    @Override
                    public void ok(EditTextDialog dialog, String name) {
                        if (!TextUtils.isEmpty(name)) {
                            real_name_tv.setText(name);
                            isEdit = true;
                            dialog.dismiss();
                        } else {
                            showToast("名字不能为空！");
                        }
                    }
                });
                break;
            case R.id.role_ll:
                OptionPicker picker = new OptionPicker(mActivity, employeeRoleArr);
                picker.setOffset(4);
                picker.setTextSize(20);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
                        isEdit = true;
                        role_tv.setText(item);
                    }
                });
                picker.show();
                break;
            case R.id.save_btn:
                if (isEdit) {
                    showAlertDialog();
                    submitData();
                } else {
                    showToast("您没有更改数据");
                }
                break;
            case R.id.delete_btn:
                DialogController.showConfirmDialog(mActivity, "您确定要删除该员工", (View v) -> {
                    deleteEmployee();
                    finish();
                });

                break;
        }
    }

    public void getRoleListData() {
        addDisposableIoMain(employeeService.getEmployeeRoleList(), new DefaultConsumer<List<EmployeeRole>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<EmployeeRole>> baseBean) {
                employeeRoleList.clear();
                employeeRoleList.addAll(baseBean.getData());
                employeeRoleArr = new String[employeeRoleList.size()];
                for (int i = 0; i < employeeRoleList.size(); i++) {
                    employeeRoleArr[i] = employeeRoleList.get(i).getName();
                }
                helper.showContent();
            }
        });
    }

    public void submitData() {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("id", employee.getId() + "");
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("aliasName", real_name_tv.getText().toString().trim());
        tokenMap.put("employeeRoleId", getRoleIdByRoleName());
        addDisposableIoMain(employeeService.alterEmployee(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showAutoDismissMessageDialog(mActivity, "保存成功");
            }
        });
    }

    public String getRoleIdByRoleName() {
        for (EmployeeRole employeeRole : employeeRoleList) {
            if (employeeRole.getName().equals(role_tv.getText().toString().trim())) {
                return employeeRole.getId() + "";
            }
        }
        return null;
    }

    public void deleteEmployee() {
        addDisposableIoMain(employeeService.deleteEmployee(employee.getId()), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                DialogController.showSubmitSuccessDialog(mActivity, "删除成功！", new SubmitSuccessDialog.DialogListener() {
                    @Override
                    public void clickLeft(SubmitSuccessDialog dialog) {
                        Intent intent = new Intent();
                        intent.putExtra("isEdit", true);
                        setResult(1, intent);
                        finish();
                        dialog.dismiss();
                    }

                    @Override
                    public void clickRight(SubmitSuccessDialog dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }
}
