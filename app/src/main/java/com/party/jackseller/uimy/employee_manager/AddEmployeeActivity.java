package com.party.jackseller.uimy.employee_manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.EmployeeService;
import com.party.jackseller.api.UserService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.EmployeeRole;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.controller.VerCodeTimer;
import com.party.jackseller.widget.dialog.SubmitSuccessDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的-员工管理-新增员工
 */
public class AddEmployeeActivity extends BaseActivityTitle {

    @BindView(R.id.user_name_et)
    EditText user_name_et;
    @BindView(R.id.phone_et)
    EditText phone_et;
    @BindView(R.id.verify_code_et)
    EditText verify_code_et;
    @BindView(R.id.category_spinner)
    TextView category_spinner;

    private UserService userService;
    private EmployeeService employeeService;
    private List<EmployeeRole> dataList = new ArrayList<>();
    private boolean isChooseRole = false;
    private String firstPhone, secondPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("新增员工");
        userService = new UserService(this);
        employeeService = new EmployeeService(this);

        initSpinner();
    }

    public void initSpinner() {
        addDisposableIoMain(employeeService.getEmployeeRoleList(), new DefaultConsumer<List<EmployeeRole>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<EmployeeRole>> baseBean) {
                dataList.clear();
                dataList.addAll(baseBean.getData());
                List<String> ll = new ArrayList<>();
                for (int i = 0; i < dataList.size(); i++) {
                    ll.add(dataList.get(i).getName());
                }
                category_spinner.setOnClickListener((View v) -> {
                    DialogController.showMenuList(ll, mActivity, (int index) -> {
                        category_spinner.setTag(dataList.get(index));
                        category_spinner.setText(dataList.get(index).getName());
                    });
                });
            }
        });
    }

    public void verifyData() {
        if (TextUtils.isEmpty(user_name_et.getText().toString().trim())) {
            showToast("请输入姓名！");
            return;
        }
        if (TextUtils.isEmpty(phone_et.getText().toString().trim())) {
            showToast("请输入手机号！");
            return;
        } else {
            secondPhone = phone_et.getText().toString().trim();
            if (!secondPhone.equals(secondPhone)) {
                showToast("手机号发生更改！");
                return;
            }
        }
        if (TextUtils.isEmpty(verify_code_et.getText().toString().trim())) {
            showToast("请输入验证码");
        }
        addEmployee();
    }

    public void addEmployee() {
        Map<String, String> dataMap = getSignData();
        dataMap.put("shopId", mApplication.getCurrShopId() + "");
        dataMap.put("aliasName", user_name_et.getText().toString().trim());
        dataMap.put("phone", phone_et.getText().toString().trim());
        dataMap.put("verCode", verify_code_et.getText().toString().trim());
        dataMap.put("employeeRoleId", ((EmployeeRole) category_spinner.getTag()).getId() + "");//员工角色id
        addDisposableIoMain(employeeService.addEmployee(dataMap), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                DialogController.showSubmitSuccessDialog(mActivity, "添加员工成功！", new SubmitSuccessDialog.DialogListener() {
                    @Override
                    public void clickLeft(SubmitSuccessDialog dialog) {
                        Intent intent = new Intent();
                        intent.putExtra("isAdd", true);
                        setResult(RESULT_OK, intent);
                        finish();
                        dialog.dismiss();
                    }

                    @Override
                    public void clickRight(SubmitSuccessDialog dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    @OnClick({R.id.get_verify_code_btn, R.id.add_employee_btn})
    public void handleClickSth(final View view) {
        switch (view.getId()) {
            case R.id.get_verify_code_btn:
                String phone = phone_et.getText().toString();
                if (phone.trim().length() != 11 || !phone.startsWith("1")) {
                    showToast("号码格式有误");
                    return;
                }
                firstPhone = phone_et.getText().toString().trim();
                addDisposableIoMain(userService.getVerCode(phone, "0"), new DefaultConsumer<Object>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<Object> baseBean) {
                        VerCodeTimer mVerCodeTimer = new VerCodeTimer(40, (Button) view);
                        mVerCodeTimer.setChangeView(phone_et, (Button) view);
                        mVerCodeTimer.start();
                    }
                });
                break;
            case R.id.add_employee_btn:
                verifyData();
                break;
        }
    }
}
