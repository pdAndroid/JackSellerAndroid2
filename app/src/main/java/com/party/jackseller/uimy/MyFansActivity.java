package com.party.jackseller.uimy;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.MyFansService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.User;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.functions.Consumer;


public class MyFansActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private List<User> dataList = new ArrayList<>();
    private MyFansService mMyFansService;
    private CommonAdapter commonAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fans);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("我的粉丝");
        helper = new LoadViewHelper(refresh_layout);
        mMyFansService = new MyFansService(this);
        initLayout();
        helper.showLoading();
        loadData();
    }

    public void loadData() {
        addDisposableIoMain(mMyFansService.getFansList(mApplication.getCurrShopId()), new DefaultConsumer<List<User>>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<User>> baseBean) {
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty();
                } else {
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    private void initLayout() {
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<User>(this, R.layout.item_my_fans, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, User user, int position) {
                ImageView imageView = viewHolder.getView(R.id.header_image);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, user.getIcon(), imageView);
                viewHolder.setText(R.id.fans_name_tv, user.getNickname());
            }
        };
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {

            }
        });
    }
}
