package com.party.jackseller.uimy.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.User;
import com.party.jackseller.bean.VerifyUser;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class RealNameAuthenticationActivity extends BaseActivityTitle {

    @BindView(R.id.name_et)
    EditText name_et;
    @BindView(R.id.id_card_num_et)
    EditText id_card_num_et;

    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_name_authentication);
        initData();
    }

    protected void initData() {
        handleTitle();
        Utils.setIDCardKeyListener(id_card_num_et);
        setMiddleText("实名认证");
        userService = new UserService(this);
    }

    @OnClick(R.id.auth_btn)
    public void handleClickSth(View view) {
        verifyData();
    }

    public void auth(String idCard, String name) {
        addDisposableIoMain(userService.authentication(idCard, name), new DefaultConsumer<VerifyUser>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<VerifyUser> result) {
                User user = mApplication.getUser();
                user.setIsAuthentication(1);
                mApplication.setUser(user);

                Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
                intent.putExtra("verKey", result.getData().getVerKey());
                startActivity(intent);
                finish();
            }
        });
    }

    public void verifyData() {
        try {
            String idCard = CheckUtils.checkIDCard(id_card_num_et, "身份证号有误");
            String name = CheckUtils.checkData(name_et, "姓名不能为空");
            auth(idCard, name);
        } catch (MyNullException e) {
            showToast(e.getMessage());
            CheckUtils.shake(mActivity, e.getView());
        }

    }
}
