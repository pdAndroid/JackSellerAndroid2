package com.party.jackseller.uimy.employee_manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.EmployeeService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Employee;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.functions.Consumer;

/**
 * 我的-员工管理
 */
public class EmployeeListActivity extends BaseActivityTitle {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.whole_layout)
    RelativeLayout whole_layout;
    private List<Employee> dataList = new ArrayList<>();
    private CommonAdapter<Employee> commonAdapter;
    private EmployeeService employeeService;
    private final int ADD_EMPLOYEE = 51, EDIT_EMPLOYEE = 52;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("员工管理");
        helper = new LoadViewHelper(refresh_layout);
        employeeService = new EmployeeService(this);
        initView();
        helper.showLoading();
        loadData();
    }

    private void initView() {
        refresh_layout.setEnableLoadMore(false);
        commonAdapter = new CommonAdapter<Employee>(mActivity, R.layout.item_employee_manage, dataList) {
            @Override
            protected void convert(ViewHolder holder, Employee employee, int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, employee.getIcon(), (CircleImageView) holder.getView(R.id.header_image));
                holder.setText(R.id.real_name_tv, employee.getAlias_name());
                holder.setText(R.id.job_tv, employee.getRole_name());
            }
        };
        recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        recycler_view.setAdapter(commonAdapter);
        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Intent intent = new Intent(mActivity, EmployeeEditActivity.class);
                intent.putExtra("employee", dataList.get(position));
                mActivity.startActivityForResult(intent, EDIT_EMPLOYEE);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
    }

    private void loadData() {
        addDisposableIoMain(employeeService.getEmployeeList(mApplication.getCurrShopId()), new DefaultConsumer<List<Employee>>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<Employee>> baseBean) {
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty();
                } else {
                    dataList.clear();
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                    refresh_layout.finishRefresh();
                }

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    @OnClick(R.id.add_employee_btn)
    public void handleClickSth(View view) {
        startActivityForResult(new Intent(mActivity, AddEmployeeActivity.class), ADD_EMPLOYEE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ADD_EMPLOYEE:
                if (resultCode == RESULT_OK) {
                    if (data.getBooleanExtra("isAdd", false) == true) {
                        helper.showLoading();
                        loadData();
                    }
                }
                break;
            case EDIT_EMPLOYEE:
                if (resultCode == RESULT_OK) {
                    if (data.getBooleanExtra("isEdit", false) == true) {
                        helper.showLoading();
                        loadData();
                    }
                }
                break;
        }
    }
}
