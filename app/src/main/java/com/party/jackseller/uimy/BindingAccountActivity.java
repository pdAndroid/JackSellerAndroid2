package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.base.MyException;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BindingInfo;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.event.AuthResultEvent;
import com.party.jackseller.uimy.wallet.MyWalletActivity;
import com.party.jackseller.widget.InputPayPasswordView2;
import com.party.jackseller.wxapi.WXPayUtils;
import com.zyyoona7.popup.EasyPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class BindingAccountActivity extends BaseActivity {
    @BindView(R.id.binding_btn)
    Button binding_btn;
    @BindView(R.id.wx_icon)
    CircleImageView wx_icon;
    @BindView(R.id.wx_name)
    TextView wx_name;
    @BindView(R.id.iv_return)
    ImageView ivReturn;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    private String password;
    private BindingInfo mBindingInfo;
    private UserService userService;
    private AppInfo mAppInfo;
    private  EasyPopup passwordPopup;
    private String verKey;
    private String shopId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_wx);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        initData();
    }

    protected void initData() {
        shopId = mApplication.getCurrShopId() + "";
        tvTitle.setText("微信绑定");
        userService = new UserService(this);
        getAppId();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 获取一键登录信息。
     */
    private void getAppId() {
        userService.getAppId(new DefaultConsumer<AppInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AppInfo> baseBean) {
                mAppInfo = baseBean.getData();
                try {
                    WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
                    build.getUserCode(mActivity, mAppInfo.getWxAppId());
                } catch (MyException e) {
                    showToast(e.getMessage());
                }
            }
        });
    }
//        addDisposableIoMain(userService.getAppId();, new DefaultConsumer<ApplicationInfo>(mApplication) {
//            @Override
//            public void operateSuccess(BaseResult<ApplicationInfo> baseBean) {
//                ApplicationInfo mApplicationInfo = baseBean.getData();
//                try {
//                    WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
//                    build.getUserCode(mActivity, mApplicationInfo.getWxAppId());
//                } catch (MyException e) {
//                    showToast(e.getMessage());
//                }
//            }1q1
//        });

    /**
     * 微信授权成功,回调
     *
     * @param authResultEvent
     */
    @Subscribe(priority = 100)
    public void authFinish(AuthResultEvent authResultEvent) {
        EventBus.getDefault().cancelEventDelivery(authResultEvent);
        addDisposableIoMain(userService.getBindingUserInfo(authResultEvent.getCode(), "1", mApplication.getCurrShopId() + ""), new DefaultConsumer<BindingInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BindingInfo> baseBean) {
                mBindingInfo = baseBean.getData();
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, baseBean.getData().getAvatar(), wx_icon);
                wx_name.setText(baseBean.getData().getNickName() + "");
                verKey = baseBean.getData().getVerKey();

            }
        }, (Throwable throwable) -> {
            showToast(throwable.getMessage());
        });
    }

    @OnClick(R.id.binding_btn)
    public void bindingBtn() {
        passwordPopup = InputPayPasswordView2.showPop(mActivity, binding_btn, (String password) -> {
            addDisposableIoMain(userService.getConfirmBindingAccount(  verKey, password), new DefaultConsumer<ConfirmBindingAccountBean>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<ConfirmBindingAccountBean> baseBean) {
                    showToast(baseBean.getMessage() + "");
                }
            }, (Throwable throwable) -> {
                showToast(throwable.getMessage());
            });

            Intent intent = new Intent(this, MyWalletActivity.class);
            startActivity(intent);
            passwordPopup.dismiss();
        });
    }

    @OnClick({R.id.iv_return})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_return:
                finish();
                break;
        }
    }
}
//        showLoadingDialog();
//        addDisposableIoMain(userService.bindingWx(mBindingInfo.getBindingKey()), new DefaultConsumer<Object>(mApplication) {
//
//            @Override
//            public void operateError(String message) {
//                super.operateError(message);
//                DialogController.showMustConfirmDialog(mActivity, "提示", message, (View v) -> {
//                });
//            }
//
//            @Override
//            public void operateSuccess(BaseResult<Object> baseBean) {
//                hideAlertDialog();
//                User user = mApplication.getUser();
//                user.setIsBindingWx(1);
//                mApplication.setUser(user);
//                DialogController.showMustConfirmDialog(mActivity, "提示", baseBean.getMessage(), (View v) -> {
//                    finish();
//                });
//            }
//        });
