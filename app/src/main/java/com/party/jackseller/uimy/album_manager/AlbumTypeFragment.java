package com.party.jackseller.uimy.album_manager;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.BaseFragment;
import com.party.jackseller.PictureDetailActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PhotoService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.bean.Photo;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.uimy.AddPhotoActivity;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.widget.dialog.EditTextDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

public class AlbumTypeFragment extends BaseFragment {
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.controll_ll)
    LinearLayout controll_ll;

    private PhotoService photoService;
    private boolean isEdit = false;
    private CommonAdapter commonAdapter;
    private long currShopId;
    private List<Photo> dataList = new ArrayList<>();
    private ArrayList<ImageBean> imageBeanList = new ArrayList<>();
    private int albumType = 0;
    private int currEditPhotoPosition = 0;
    private AlertDialog dialog;

    public static AlbumTypeFragment newInstance(int albumType) {
        AlbumTypeFragment fragment = new AlbumTypeFragment();
        Bundle args = new Bundle();
        args.putInt("albumType", albumType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void receiveBundleFromActivity(Bundle arg) {
        albumType = arg.getInt("albumType");
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_album_type;
    }

    public void refreshData() {
        getNeedData();
    }


    @Override
    protected void initData() {
        super.initData();
        switch (albumType) {
            case 1:
                controll_ll.setVisibility(View.GONE);
                break;
            case 2:
                controll_ll.setVisibility(View.VISIBLE);
                break;
            case 3:
                controll_ll.setVisibility(View.GONE);
                break;
        }
        helper = new LoadViewHelper(refresh_layout);
        currShopId = mApplication.getCurrShopId();
        photoService = new PhotoService((BaseActivityTitle) mActivity);
        initView();
        helper.showLoading("正在加载...");
        getNeedData();
    }

    private void initView() {
        refresh_layout.setEnableRefresh(false);
        refresh_layout.setEnableLoadMore(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 4);
        commonAdapter = new CommonAdapter<Photo>(mActivity, R.layout.album_manage_item, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final Photo bean, final int position) {
                ImageView imageView = viewHolder.getView(R.id.iv_picture);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, bean.getImg_url(), imageView);
                viewHolder.setText(R.id.photo_name_tv, bean.getTitle());
                View deleteView = viewHolder.getView(R.id.delete_iv);
                TextView editTv = (TextView) viewHolder.getView(R.id.edit_tv);
                deleteView.setVisibility(isEdit ? View.VISIBLE : View.GONE);
                editTv.setVisibility(isEdit ? View.VISIBLE : View.GONE);
                editTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (dataList.isEmpty()) {
                            showToast("没有图片可编辑！");
                            return;
                        }
                        currEditPhotoPosition = position;
                        showEditPhotoNameDialog(dataList.get(position).getTitle());
                        showToast(dataList.get(currEditPhotoPosition).getId() + "");
                    }
                });
                viewHolder.setOnClickListener(R.id.delete_iv, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currEditPhotoPosition = position;
                        DialogController.showConfirmDialog(mActivity, "删除图片", (View vv) -> {
                            deletePic();
                        });
                    }
                });
                viewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mActivity, PictureDetailActivity.class);
                        intent.putExtra("currentItem", position);
                        intent.putExtra("dataList", imageBeanList);
                        intent.putExtra("deleteFlag", false);
                        startActivityForResult(intent, ConstUtils.IMAGE_DETAIL_CODE);
                    }
                });
            }
        };
        recycler_view.setLayoutManager(gridLayoutManager);
        recycler_view.setAdapter(commonAdapter);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                ImageView deleteIv = (ImageView) holder.itemView.findViewById(R.id.delete_iv);
                deleteIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deletePic();
                    }
                });
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    public void deletePic() {
        mActivity.addDisposableIoMain(photoService.deletePhoto(dataList.get(currEditPhotoPosition).getId()), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                showToast("删除成功");
                dataList.remove(dataList.get(currEditPhotoPosition));
            }
        });
    }

    @OnClick({R.id.edit_album_btn, R.id.add_photo_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.edit_album_btn:
                if (!isEdit) {
                    isEdit = true;
                } else {
                    isEdit = false;
                }
                commonAdapter.notifyDataSetChanged();
                break;
            case R.id.add_photo_btn:
                Intent intent = new Intent(mActivity, AddPhotoActivity.class);
                intent.putExtra("albumType", albumType);
                startActivity(intent);
                break;
        }
    }

    private void getNeedData() {
        switch (albumType) {
            case 1:
                //获取商品图片
                mActivity.addDisposableIoMain(photoService.getGoodsPhotoList(currShopId), new DefaultConsumer<List<Photo>>(mApplication) {

                    @Override
                    public void operateError(String message) {
                        super.operateError(message);
                        helper.showError("获取数据异常", "");
                    }

                    @Override
                    public void operateSuccess(BaseResult<List<Photo>> baseBean) {
                        if (baseBean.getData().size() == 0) {
                            helper.showEmpty("暂无数据", "重新加载");
                        } else {
                            dataList.clear();
                            dataList.addAll(baseBean.getData());
                            imageBeanList.clear();
                            for (Photo photo : dataList) {
                                ImageBean imageBean = new ImageBean();
                                imageBean.setImgPath(photo.getImg_url());
                                imageBean.setAdd(false);
                                imageBeanList.add(imageBean);
                            }
                            commonAdapter.notifyDataSetChanged();
                            helper.showContent();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        helper.showEmpty();
                    }
                });
                break;
            case 2:
                //获取店铺图片
                mActivity.addDisposableIoMain(photoService.getShopPhotoList(currShopId), new DefaultConsumer<List<Photo>>(mApplication) {

                    @Override
                    public void operateError(String message) {
                        super.operateError(message);
                        helper.showError("获取数据异常", "");
                    }

                    @Override
                    public void operateSuccess(BaseResult<List<Photo>> baseBean) {
                        if (baseBean.getData().size() == 0) {
                            helper.showEmpty("暂无数据", "重新加载");
                        } else {
                            dataList.clear();
                            dataList.addAll(baseBean.getData());
                            imageBeanList.clear();
                            for (Photo photo : dataList) {
                                ImageBean imageBean = new ImageBean();
                                imageBean.setImgPath(photo.getImg_url());
                                imageBean.setAdd(false);
                                imageBeanList.add(imageBean);
                            }
                            commonAdapter.notifyDataSetChanged();
                            helper.showContent();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        helper.showEmpty();
                    }
                });
                break;
            case 3:
                //获取用户图片
                mActivity.addDisposableIoMain(photoService.getUserPhotoList(currShopId), new DefaultConsumer<List<Photo>>(mApplication) {

                    @Override
                    public void operateError(String message) {
                        super.operateError(message);
                        helper.showError("获取数据异常", "");
                    }

                    @Override
                    public void operateSuccess(BaseResult<List<Photo>> baseBean) {
                        if (baseBean.getData().size() == 0) {
                            helper.showEmpty("暂无数据", "重新加载");
                        } else {
                            dataList.clear();
                            dataList.addAll(baseBean.getData());
                            imageBeanList.clear();
                            for (Photo photo : dataList) {
                                ImageBean imageBean = new ImageBean();
                                imageBean.setImgPath(photo.getImg_url());
                                imageBean.setAdd(false);
                                imageBeanList.add(imageBean);
                            }
                            commonAdapter.notifyDataSetChanged();
                            helper.showContent();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        helper.showEmpty();
                    }
                });
                break;
        }
    }

    public void updateData(final String photoName, long id) {
        mActivity.addDisposableIoMain(photoService.editPhoto(id, photoName), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                showToast("修改失败");
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                dataList.get(currEditPhotoPosition).setTitle(photoName);
                commonAdapter.notifyItemChanged(currEditPhotoPosition);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                showToast("修改失败");
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    public void showEditPhotoNameDialog(String phototTitle) {
        DialogController.showEditDialog(getContext(), " 请输入名称", phototTitle, new EditTextDialog.DialogListener() {
            @Override
            public void cancel(EditTextDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void ok(EditTextDialog dialog, String name) {
                if (TextUtils.isEmpty(name)) {
                    showToast("名称不能为空");
                } else {
                    updateData(name, dataList.get(currEditPhotoPosition).getId());
                }
            }
        });
    }

    private static final String TAG = "AlbumTypeFragment";
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cancel_tv:

                    break;
                case R.id.ok_tv:

                    break;
            }
        }
    };
}
