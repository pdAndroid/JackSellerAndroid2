package com.party.jackseller.uimy.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PayService;
import com.party.jackseller.api.UserService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.controller.PayController;
import com.party.jackseller.uihome.TradeRecordListActivity;
import com.party.jackseller.uimy.CashActivity;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * 提现
 */

public class MyWalletActivity extends BaseActivityTitle {

    @BindView(R.id.whole_layout)
    LinearLayout whole_layout;
    @BindView(R.id.balance_tv)
    TextView balance_tv;

    private double money;
    private PayService payService;
    private static final String TAG = "MyWalletActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        initData();
    }

    protected void initData() {
        helper = new LoadViewHelper(whole_layout);
        payService = new PayService(this);
        helper.showLoading();
        handleTitle();
        setMiddleText("商家钱包");
        setRightText("设置");

    }

    @Override
    protected void onResume() {
        super.onResume();
        initShopWalletBalance();
    }

    public void clickRightBtn() {
        startActivity(PaySettingActivity.class);
    }

    /**
     * 获取余额
     */
    public void initShopWalletBalance() {
        addDisposableIoMain(payService.getShopWallet(), new DefaultConsumer<CommonResult>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "重新加载");
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                balance_tv.setText(baseBean.getData().getMoney() + "");
                money = baseBean.getData().getMoney();
                helper.showContent();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    @OnClick({R.id.bank_card_ll, R.id.trade_record_ll, R.id.ti_xian_ll})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.bank_card_ll:
                startActivity(AccountListActivity.class);
                break;
            case R.id.trade_record_ll:
                startActivity(TradeRecordListActivity.class);
                break;
            case R.id.ti_xian_ll:
                Intent intent = new Intent(this, CashActivity.class);
                intent.putExtra("money", money);
                startActivity(intent);
                break;
        }
    }
}
