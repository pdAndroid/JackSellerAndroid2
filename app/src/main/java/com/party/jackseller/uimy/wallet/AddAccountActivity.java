package com.party.jackseller.uimy.wallet;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.controller.VerCodeTimer;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 钱包界面
 */
public class AddAccountActivity extends BaseActivity {
    @BindView(R.id.account_type)
    TextView account_type;
    @BindView(R.id.account_user_name)
    EditText account_user_name;
    @BindView(R.id.account_no)
    EditText account_no;
    @BindView(R.id.phone_et)
    TextView phone_et;
    @BindView(R.id.verify_code_btn)
    Button verify_code_btn;
    @BindView(R.id.verify_code_et)
    EditText verify_code_et;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    UserService userService;
    VerCodeTimer mVerCodeTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account);
        ButterKnife.bind(this);
        setTitle("添加账户");
        initData();
    }

    private void initData() {
        account_type.setOnClickListener((View v) -> {
            String[] data = {"支付宝", "微信"};
            DialogController.showMenuList(Arrays.asList(data), mActivity, (int which) -> {
                System.out.println(which);
            });
        });
    }

    @OnClick(R.id.verify_code_btn)
    public void sendCode(View view) {
        try {
            CheckUtils.checkData(account_type, "请选择类型");
            CheckUtils.checkData(account_no, "请输入账号");
            CheckUtils.checkData(account_user_name, "请输入账号用户人名称");
            String phone = CheckUtils.checkData(phone_et, "请输入手机号");
            addDisposableIoMain(userService.getVerCode(phone, "0"), new DefaultConsumer<Object>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<Object> baseBean) {
                    mVerCodeTimer = new VerCodeTimer(60, verify_code_btn);
                    mVerCodeTimer.setChangeView(account_type, account_no, account_user_name, phone_et, verify_code_btn);
                    mVerCodeTimer.start();
                }
            });
        } catch (Exception e) {
            showToast(e.getMessage());
        }
    }


    @OnClick(R.id.commit_btn)
    public void addAccount(View view) {
        try {
            CheckUtils.checkData(account_type, "请选择类型");
            CheckUtils.checkData(account_no, "请输入账号");
            CheckUtils.checkData(account_user_name, "请输入账号");
            CheckUtils.checkData(phone_et, "请输入手机号");
            CheckUtils.checkData(verify_code_et, "请输入验证码");
        } catch (Exception e) {
            showToast(e.getMessage());
        }
    }


    @OnClick({R.id.iv_back, R.id.verify_code_btn, R.id.commit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                return;
            case R.id.verify_code_btn:
                try {
                    CheckUtils.checkData(account_type, "请选择类型");
                    CheckUtils.checkData(account_no, "请输入账号");
                    CheckUtils.checkData(account_user_name, "请输入账号用户人名称");
                    String phone = CheckUtils.checkData(phone_et, "请输入手机号");
                    addDisposableIoMain(userService.getVerCode(phone, "0"), new DefaultConsumer<Object>(mApplication) {
                        @Override
                        public void operateSuccess(BaseResult<Object> baseBean) {
                            mVerCodeTimer = new VerCodeTimer(60, verify_code_btn);
                            mVerCodeTimer.setChangeView(account_type, account_no, account_user_name, phone_et, verify_code_btn);
                            mVerCodeTimer.start();
                        }
                    });
                } catch (Exception e) {
                    showToast(e.getMessage());
                }
                break;
            case R.id.commit_btn:
                try {
                    CheckUtils.checkData(account_type, "请选择类型");
                    CheckUtils.checkData(account_no, "请输入账号");
                    CheckUtils.checkData(account_user_name, "请输入账号");
                    CheckUtils.checkData(phone_et, "请输入手机号");
                    CheckUtils.checkData(verify_code_et, "请输入验证码");
                } catch (Exception e) {
                    showToast(e.getMessage());
                }
                break;
        }
    }
}
