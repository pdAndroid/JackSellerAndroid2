package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alipay.sdk.app.AuthTask;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BindingInfo;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.bean.User;
import com.party.jackseller.uimy.wallet.MyWalletActivity;
import com.party.jackseller.uimy.wallet.RealNameAuthenticationActivity;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.widget.InputPayPasswordView;
import com.party.jackseller.widget.InputPayPasswordView2;
import com.zyyoona7.popup.EasyPopup;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class BindingAccountZfbActivity extends BaseActivityTitle {
    @BindView(R.id.binding_btn)
    Button binding_btn;
    @BindView(R.id.wx_icon)
    CircleImageView wx_icon;

    UserService userService;
    BindingInfo mBindingInfo;
    Handler handler;
    @BindView(R.id.wx_name)
    TextView wxName;

    private EasyPopup passwordPopup;
    private String verKey;
    private String shopId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_ali);
        ButterKnife.bind(this);
        initData();
        requestData();
    }

    protected void initData() {
//        setMiddleText("实名认证");
        shopId = mApplication.getCurrShopId() + "";
        userService = new UserService(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Map<String, String> result = (Map<String, String>) msg.obj;
                String resultStr = result.get("result");
                if (!resultStr.contains("&")) return;
                String[] split = resultStr.split("&");
                Map<String, String> mapData = new HashMap<>();
                for (int i = 0; i < split.length; i++) {
                    String[] split1 = split[i].split("=");
                    mapData.put(split1[0], split1[1]);
                }
                if (!"200".equals(mapData.get("result_code"))) {
                    showToast("授权失败");
                    return;
                }
                String auth_code = mapData.get("auth_code");
                bindingZfbUser(auth_code);
            }
        };

        TextView auth_real_name = findViewById(R.id.auth_real_name);
        auth_real_name.setOnClickListener((View v) -> {
            startActivity(RealNameAuthenticationActivity.class);
            finish();
        });
    }

    private void requestData() {
        addDisposableIoMain(userService.getAliAuthInfoStr(), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                String infoStr = baseBean.getData();
                authTask(infoStr);
            }

            @Override
            public void operateError(String message) {
                super.operateError(message);
            }
        });
    }

    @OnClick(R.id.binding_btn)
    public void getAliAuthInfoStr(View v) {
        passwordPopup = InputPayPasswordView2.showPop(mActivity, binding_btn, (String password) -> {
            addDisposableIoMain(userService.getConfirmBindingAccount(verKey, password), new DefaultConsumer<ConfirmBindingAccountBean>(mApplication) {

                @Override
                public void operateError(BaseResult<ConfirmBindingAccountBean> baseBean) {
                    showToast(baseBean.getMessage());
                    InputPayPasswordView contentView = (InputPayPasswordView) passwordPopup.getContentView();
                    contentView.reset();
                }

                @Override
                public void operateSuccess(BaseResult<ConfirmBindingAccountBean> baseBean) {
                    showToast(baseBean.getMessage() + "");

//                            Intent intent = new Intent(mActivity, MyWalletActivity.class);
//                            startActivity(intent);
                    mActivity.finish();
                    passwordPopup.dismiss();
                }
            }, (Throwable throwable) -> {
                showToast(throwable.getMessage());
            });


        });
    }


    public void authTask(String infoStr) {
        new Thread(() -> {
            AuthTask authTask = new AuthTask(mActivity);
            Map<String, String> result = authTask.authV2(infoStr, true);
            Message msg = new Message();
            msg.obj = result;
            handler.sendMessage(msg);
        }).start();
    }


    /**
     * 使用授权code 去获取 用户信息
     *
     * @param auth_code
     */
    public void bindingZfbUser(String auth_code) {
        addDisposableIoMain(userService.getBindingUserInfo(auth_code, "2", mApplication.getCurrShopId() + ""), new DefaultConsumer<BindingInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BindingInfo> baseBean) {
                hideAlertDialog();
                mBindingInfo = baseBean.getData();
                User user = mApplication.getUser();
                user.setIsBindingAli(1);
                mApplication.setUser(user);
                wxName.setText(baseBean.getData().getNickName() + "");
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, baseBean.getData().getAvatar(), wx_icon);
                authOk(mBindingInfo.getVerKey());
            }

            @Override
            public void operateError(String message) {
                DialogController.showAutoDismissMessageDialog(mActivity, message);
                super.operateError(message);

            }
        }, (Throwable throwable) -> {
            showToast(throwable.getMessage());
        });
    }


    public void authOk(String bindingKey) {
        verKey = bindingKey;
    }
}
