package com.party.jackseller.uimy.wallet;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;

import com.party.jackseller.R;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.AccountBean;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的账户
 */
public class AccountListActivity extends BaseActivityTitle {

    String[] type = {"储蓄卡", "信用卡", "钱包", "钱包"};
    int[] color = {R.mipmap.bank_card_bg, R.mipmap.bank_card_bg, R.mipmap.weixin_bg, R.mipmap.zhifubao_bg};
    int[] icon = {R.mipmap.bank_card, R.mipmap.bank_card, R.mipmap.weixin, R.mipmap.zhifubao};

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    List<AccountBean> dataList;
    CommonAdapter<AccountBean> commonAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_list);
        handleTitle();
        setMiddleText("我的账户");
        setRightText("添加");
        initData();
    }

    @Override
    public void clickRightBtn() {
        startActivity(AddAccountActivity.class);
    }

    protected void initData() {
        dataList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this); //设置布局管理器
        recyclerView.setLayoutManager(layoutManager); //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(OrientationHelper.VERTICAL); //设置Adapter

        dataList.add(new AccountBean("支付宝", "***** ***** ***** 34523", 3));
        dataList.add(new AccountBean("微信支付", "***** ***** ***** 23123", 2));
        dataList.add(new AccountBean("中国银行", "***** ***** ***** 73123", 1));
        dataList.add(new AccountBean("瑞士银行", "***** ***** ***** 31231", 0));

        commonAdapter = new CommonAdapter<AccountBean>(mActivity, R.layout.item_account, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final AccountBean item, int position) {
                viewHolder.setBackgroundRes(R.id.whole_layout, color[item.getCardType()]);
                viewHolder.setImageResource(R.id.header_image, icon[item.getCardType()]);
                viewHolder.setText(R.id.account_name, item.getAccountName());
                viewHolder.setText(R.id.account_type, type[item.getCardType()]);
                viewHolder.setText(R.id.account_no, item.getAccountNo());
            }
        };

        recyclerView.setAdapter(commonAdapter); //设置分隔线
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}
