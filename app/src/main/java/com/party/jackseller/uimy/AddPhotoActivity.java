package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PhotoService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.bean.OSSConfig;
import com.party.jackseller.bean.Photo;
import com.party.jackseller.controller.AliyunOSSController;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.widget.decoration.GridSpacingItemDecoration;
import com.party.jackseller.widget.dialog.EditTextDialog;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

public class AddPhotoActivity extends BaseActivityTitle {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private ArrayList<ImageBean> imageBeanList = new ArrayList<>();
    private PhotoService photoService;
    private long shopId;
    private AliyunOSSController ossController;
    private CommonAdapter commonAdapter;
    private static final String TAG = "AddPhotoActivity";
    private AlertDialog dialog;
    private int currEditPosition = 0;//表示当前正在编辑哪个图片的名字的位置
    private int currUploadOSSPostion = 0;//表示当前正在上传至OSS的位置
    private int currUploadServerPosition = 0;//表示当前正在上传至服务器的位置
    private int albumType = 0;
    private String uploadType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        initData();
        receivePassDataIfNeed();
    }

    protected void receivePassDataIfNeed() {
        Intent intent = getIntent();
        albumType = intent.getIntExtra("albumType", 0);
        switch (albumType) {
            case 1:
                uploadType = AliyunOSSController.OSS_SHOP_GOODS;
                break;
            case 2:
                uploadType = AliyunOSSController.OSS_SHOP_IMG;
                break;
            case 3:
                uploadType = AliyunOSSController.OSS_USER_IMG;
                break;
        }
    }

    protected void initData() {
        handleTitle();
        setMiddleText("添加图片");
        photoService = new PhotoService(this);
        ossController = new AliyunOSSController(this);
        shopId = mApplication.getCurrShopId();
        imageBeanList.add(new ImageBean());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 3);
        commonAdapter = new CommonAdapter<ImageBean>(mActivity, R.layout.item_photo, imageBeanList) {
            @Override
            protected void convert(ViewHolder viewHolder, final ImageBean bean, final int position) {
                if (!TextUtils.isEmpty(bean.getImgPath())) {
                    mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, bean.getImgPath(), (ImageView) viewHolder.getView(R.id.iv_picture));
                    viewHolder.setText(R.id.photo_name_tv, !TextUtils.isEmpty(imageBeanList.get(position).getImgName()) ? imageBeanList.get(position).getImgName() : "编辑名称");
                } else {
                    mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, R.mipmap.add_img, (ImageView) viewHolder.getView(R.id.iv_picture));
                    viewHolder.setText(R.id.photo_name_tv, "");
                }
            }
        };
        int spanCount = 3;
        int spacing = 50;
        boolean includeEdge = true;
        recycler_view.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recycler_view.setLayoutManager(gridLayoutManager);
        recycler_view.setAdapter(commonAdapter);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                if (TextUtils.isEmpty(imageBeanList.get(position).getImgPath())) {
                    if (choosePictureController != null) {
                        choosePictureController.clearBeforeChoosePictures();
                        choosePictureController.handleToChooseCropPictures();
                    }
                } else {
                    showAddPhotoNameDialog();
                    currEditPosition = position;
                }
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        int size = imageBeanList.size();
        imageBeanList.addAll(size - 1, list);
        commonAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.upload_pic_btn)
    public void uploadPicture(View view) {
        uploadData();
    }

    private void showAddPhotoNameDialog() {
        DialogController.showEditDialog(this, "请输入名称", "", new EditTextDialog.DialogListener() {
            @Override
            public void cancel(EditTextDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void ok(EditTextDialog dialog, String name) {
                if (TextUtils.isEmpty(name)) {
                    showToast("名称不能为空");
                    return;
                }
                imageBeanList.get(currEditPosition).setImgName(name);
                commonAdapter.notifyItemChanged(currEditPosition);
                dialog.dismiss();
            }
        });
    }

    /**
     * 上传图片第一步
     */
    private void uploadData() {
        if (imageBeanList.isEmpty()) {
            showToast("没有图片！");
        } else {
            currUploadOSSPostion = 0;
            showAlertDialog("上传图片中...");
            doUploadPictures();
        }
    }

    /**
     * 上传图片第二步
     */
    private void doUploadPictures() {
        // 第一步先服务器获取oss
        addDisposableIoMain(ossController.getOssConfig(uploadType), new DefaultConsumer<OSSConfig>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                showToast("上传异常，请稍后上传");
                hideAlertDialog();
            }

            @Override
            public void operateSuccess(BaseResult<OSSConfig> baseBean) {
                doUploadPictures(baseBean.getData());
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                showToast("上传异常，请稍后上传");
                hideAlertDialog();
            }
        });
    }

    /**
     * 上传图片第三步
     *
     * @param ossConfig
     */
    private void doUploadPictures(OSSConfig ossConfig) {
        ossController.initOssConfig(ossConfig, mActivity);
        addDisposableIoMain(ossController.uploadPicturesSync(shopId, imageBeanList.get(currUploadOSSPostion).getImgPath(), false), new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                uploadPic(s);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                hideAlertDialog();
                showToast("上传图片失败," + throwable.getMessage());
            }
        });
    }

    /**
     * 上传图片最后一步
     *
     * @param OSSPath
     */
    private void uploadPic(String OSSPath) {
        String currImageName = imageBeanList.get(currUploadOSSPostion).getImgName();
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("imgUrl", OSSPath);
        tokenMap.put("photoName", TextUtils.isEmpty(currImageName) ? "" : currImageName);
        addDisposableIoMain(photoService.addPhoto(tokenMap), new DefaultConsumer<Photo>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                hideAlertDialog();
                showToast("上传第" + (currUploadOSSPostion + 1) + "张图片失败");
            }

            @Override
            public void operateSuccess(BaseResult<Photo> baseBean) {
                hideAlertDialog();
                if (currUploadOSSPostion == imageBeanList.size() - 2) {
                    hideAlertDialog();
                    showToast("成功上传了第" + (currUploadOSSPostion + 1) + "张图片");
                    imageBeanList.clear();
                    imageBeanList.add(new ImageBean());
                    commonAdapter.notifyDataSetChanged();
                } else {
                    currUploadOSSPostion++;
                    doUploadPictures();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                hideAlertDialog();
                showToast("上传第" + (currUploadOSSPostion + 1) + "张图片失败");
                showToast(throwable.getMessage());
            }
        });
    }
}
