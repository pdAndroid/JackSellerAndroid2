package com.party.jackseller.uimy;

import android.support.annotation.Nullable;
import android.os.Bundle;

import com.party.jackseller.R;
import com.party.jackseller.BaseActivityTitle;

public class CommonProblemActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_problem);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("常见问题");
    }
}
