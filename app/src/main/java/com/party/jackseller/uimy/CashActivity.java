package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PayService;
import com.party.jackseller.api.UserService;
import com.party.jackseller.base.MyException;
import com.party.jackseller.base.MyTextWatcher;
import com.party.jackseller.bean.AccountBean;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BindingInfo;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.controller.PayController;
import com.party.jackseller.event.AuthResultEvent;
import com.party.jackseller.uimy.wallet.EnterPaymentPasswordActivity;
import com.party.jackseller.uimy.wallet.MyWalletActivity;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.Utils;
import com.party.jackseller.widget.InputPayPasswordView;
import com.party.jackseller.widget.InputPayPasswordView2;
import com.party.jackseller.wxapi.WXPayUtils;
import com.zyyoona7.popup.EasyPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2019/1/13.
 * 设置自动提现
 */

public class CashActivity extends BaseActivityTitle {


    @BindView(R.id.account_icon)
    ImageView accountIcon;
    @BindView(R.id.account_name)
    TextView accountName;
    @BindView(R.id.select_account)
    LinearLayout selectAccount;
    @BindView(R.id.input_money)
    EditText inputMoney;
    @BindView(R.id.cash_all)
    TextView cashAll;
    @BindView(R.id.account_number)
    TextView accountNumber;
    @BindView(R.id.commit_btn)
    TextView commitBtn;
    @BindView(R.id.setting_auto_cash)
    TextView settingAutoCash;
    @BindView(R.id.main_layout)
    LinearLayout mainLayout;


    private UserService userService;
    private AppInfo appInfo;
    private PayService mPayService;
    private BindingInfo bindingInfo;
    private AccountBean mAccountBean;
    private Double money;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash);
        ButterKnife.bind(this);
        initView();
        initListener();
    }

    private void initListener() {
    }

    private void initView() {
        handleTitle();
        setTitle("提现");
        mPayService = new PayService(this);
        userService = new UserService(this);
        money = getIntent().getDoubleExtra("money", -1);
        if (money == -1) {
            mPayService.getShopMoney((CommonResult data) -> {
                money = data.getMoney();
                setMoney(money);
            });
        } else {
            setMoney(money);
        }

        getAccount();
    }

    public void setMoney(Double money) {
        accountNumber.setText("账户余额  ¥" + money);
    }


    private void getAccount() {
        mPayService.getAccount((AccountBean bean) -> {
            mAccountBean = bean;
            mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, mAccountBean.getAvatar(), accountIcon);
            accountName.setText(mAccountBean.getUserName() + "(" + mAccountBean.getAccountName() + ")");
        });
    }

    //获取一键登录信息
    private void getAppId() {
        userService.getAppId(new DefaultConsumer<AppInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AppInfo> baseBean) {
                appInfo = baseBean.getData();
                try {
                    WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
                    build.getUserCode(mActivity, appInfo.getWxAppId());
                } catch (MyException e) {
                    showToast(e.getMessage());
                }
                hideAlertDialog();
            }
        });
    }

    //微信授权成功,回调
    @Subscribe(priority = 100)
    public void authFinish(AuthResultEvent authResultEvent) {
        EventBus.getDefault().cancelEventDelivery(authResultEvent);
        System.out.println("" + authResultEvent.getCode());
        addDisposableIoMain(userService.getBindingUserInfo(authResultEvent.getCode(), "1", mApplication.getCurrShopId() + ""), new DefaultConsumer<BindingInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BindingInfo> baseBean) {
                hideAlertDialog();
                bindingInfo = baseBean.getData();
//                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, baseBean.getData().getAvatar(), wx_icon);
//                wx_name.setText(baseBean.getData().getNickname());
            }
        }, (Throwable throwable) -> {
            showToast(throwable.getMessage());
        });
    }

    private EasyPopup passwordPopup;

    @OnClick({R.id.select_account, R.id.cash_all, R.id.commit_btn})
    public void onClickLin(View v) {
        switch (v.getId()) {
            case R.id.select_account:
                String accout[] = {"提现到支付宝", "提现到微信"};
                DialogController.showMenuList(accout, this, (int index) -> {
                    Intent intent;
                    switch (index) {
                        case 0:
                            intent = new Intent(this, BindingAccountZfbActivity.class);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(this, BindingAccountActivity.class);
                            startActivity(intent);
                    }
                });
                break;
            case R.id.commit_btn:
                hideSoftKeyboard(inputMoney);
                try {
                    String s = CheckUtils.checkData(inputMoney, "请输入提现金额");
                    double v1 = Double.parseDouble(s);
                    if (v1 > money) {
                        shake(inputMoney);
                        inputMoney.setText(money + "");
                        inputMoney.setSelection((money + "").length());
                        return;
                    }
                    if (v1 < 1) {
                        throw new MyException("提现金额必须大于1元");
                    }

                    passwordPopup = InputPayPasswordView2.showPop(mActivity, commitBtn, (String password) -> {
                        mPayService.cashMoney(s, password, (BaseResult obj) -> {
                            passwordPopup.dismiss();
                            DialogController.showMustConfirmDialog(mActivity, obj.getMessage(), (View vv) -> {
                                finish();
                            });
                        }, (BaseResult obj) -> {
                            showToast(obj.getMessage());
                            InputPayPasswordView contentView = (InputPayPasswordView) passwordPopup.getContentView();
                            contentView.reset();
                        });
                    });
                } catch (Exception e) {
                    showToast(e.getMessage());
                    return;
                }
                break;
            case R.id.cash_all:
                inputMoney.setText(money + "");
                inputMoney.setSelection((money + "").length());
                break;
        }
    }


}
