package com.party.jackseller.uimy.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.VerifyUser;
import com.party.jackseller.controller.VerCodeTimer;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class UpdatePayPasswordActivity extends BaseActivityTitle {

    @BindView(R.id.phone_et)
    EditText phoneEt;
    @BindView(R.id.name_et)
    EditText nameEt;
    @BindView(R.id.id_card_num_et)
    EditText id_card_num_et;
    @BindView(R.id.verify_code_et)
    EditText verCodeEt;
    @BindView(R.id.verify_code_btn)
    Button verify_code_btn;


    String phone;
    UserService userService;
    VerCodeTimer mVerCodeTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pay_password);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("设置支付密码");
        Utils.setIDCardKeyListener(id_card_num_et);
        userService = new UserService(this);
        nameEt.setText(mApplication.getUser().getName());
        phoneEt.setText(mApplication.getUser().getPhone());
    }

    private void verifyPhone() {
        try {
            String idCard = CheckUtils.checkIDCard(id_card_num_et, "身份证有误");
            String name = CheckUtils.checkData(nameEt, "姓名为空");
            String phone = CheckUtils.checkPhone(phoneEt, "手机号有误");
            String verCode = CheckUtils.checkData(verCodeEt, "验证码有误");

            addDisposableIoMain(userService.verifVerCode(verCode, phone, idCard), new DefaultConsumer<VerifyUser>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<VerifyUser> result) {
                    Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
                    intent.putExtra("verKey", result.getData().getVerKey());
                    startActivity(intent);
                    finish();
                }
            });

        } catch (MyNullException e) {
            showToast(e.getMessage());
            CheckUtils.shake(this, e.getView());
            return;
        }
    }

    @OnClick({R.id.verify_code_btn, R.id.next_step_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.verify_code_btn:
                try {
                    CheckUtils.checkData(nameEt, "姓名为空");
                    CheckUtils.checkIDCard(id_card_num_et, "身份证有误");
                    String phone = CheckUtils.checkPhone(phoneEt, "手机号有误");
                    addDisposableIoMain(userService.getUpdatePasswordVerCode(phone), new DefaultConsumer<Object>(mApplication) {
                        @Override
                        public void operateSuccess(BaseResult<Object> baseBean) {
                            mVerCodeTimer = new VerCodeTimer(60, verify_code_btn);
                            mVerCodeTimer.setChangeView(phoneEt, verify_code_btn);
                            mVerCodeTimer.start();
                        }
                    });
                } catch (MyNullException e) {
                    showToast(e.getMessage());
                    CheckUtils.shake(this, e.getView());
                    return;
                }
                break;
            case R.id.next_step_btn:
                verifyPhone();
                break;
        }
    }
}
