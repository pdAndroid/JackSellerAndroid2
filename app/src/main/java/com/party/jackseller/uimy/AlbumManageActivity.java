package com.party.jackseller.uimy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.adapter.TabContentPagerAdapter;
import com.party.jackseller.uimy.album_manager.AlbumTypeFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

public class AlbumManageActivity extends BaseActivityTitle {

    @BindView(R.id.view_pager)
    ViewPager view_pager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> titleList;
    private TabContentPagerAdapter contentAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_manage);
        initData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("相册管理");
        titleList = Arrays.asList("商品相册", "店铺相册"/*, "用户相册"*/);
        fragmentList.add(AlbumTypeFragment.newInstance(1));
        fragmentList.add(AlbumTypeFragment.newInstance(2));
        //fragmentList.add(AlbumTypeFragment.newInstance(3));
        contentAdapter = new TabContentPagerAdapter(getSupportFragmentManager(), fragmentList, titleList);
        view_pager.setAdapter(contentAdapter);
        tab_layout.setTabMode(TabLayout.MODE_FIXED);
        tab_layout.setTabTextColors(ContextCompat.getColor(mActivity, R.color.black), ContextCompat.getColor(mActivity, R.color.yellow));
        tab_layout.setSelectedTabIndicatorColor(ContextCompat.getColor(mActivity, R.color.yellow));
        //ViewCompat.setElevation(tab_layout, 10);
        tab_layout.setupWithViewPager(view_pager);
    }

}
