package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.LoginActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ShopInfo;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.TimeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShopInfoActivity extends BaseActivityTitle {

    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.shop_image_iv)
    ImageView shop_image_iv;
    @BindView(R.id.address_tv)
    TextView address_tv;
    @BindView(R.id.symbol_build_tv)
    TextView symbol_build_tv;
    @BindView(R.id.shop_name_tv2)
    TextView shop_name_tv2;
    @BindView(R.id.shop_type_tv)
    TextView shop_type_tv;
    @BindView(R.id.phone_tv)
    TextView phone_tv;
    @BindView(R.id.address_tv2)
    TextView address_tv2;
    @BindView(R.id.business_time_tv)
    TextView business_time_tv;
    @BindView(R.id.business_state_tv)
    TextView business_state_tv;
    @BindView(R.id.tv_logout)
    TextView tvLogout;

    private ShopService shopService;
    private long shopId;
    private String state;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_info);
        ButterKnife.bind(this);
        getIntentData(getIntent());
        initData();
    }

    protected void getIntentData(Intent intent) {
        shopId = intent.getLongExtra("shopId", -1L);
        state = intent.getStringExtra("state");
    }

    protected void initData() {
        handleTitle();
        setMiddleText("店铺详情");
        shopService = new ShopService(this);
        addDisposableIoMain(shopService.getShopDetail(shopId), new DefaultConsumer<ShopInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<ShopInfo> baseBean) {
                ShopInfo shopInfo = baseBean.getData();
                shop_name_tv.setText(shopInfo.getShop_name());
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, shopInfo.getHome_img(), shop_image_iv);
                address_tv.setText(shopInfo.getShop_address());
                symbol_build_tv.setText(shopInfo.getSymbol_build());
                shop_name_tv2.setText(shopInfo.getShop_name());
                shop_type_tv.setText(shopInfo.getCategory_name());
                phone_tv.setText(shopInfo.getShop_phone());
                address_tv2.setText(shopInfo.getShop_address());
                business_time_tv.setText(TimeUtil.timeStamp2Date(shopInfo.getBuss_open() + "", "HH:mm") + "-" + TimeUtil.timeStamp2Date(shopInfo.getBuss_close() + "", "HH:mm"));
                business_state_tv.setText(state == null ? shopInfo.getShop_state_name() : state);
            }
        });
    }

    @OnClick(R.id.tv_logout)
    public void onViewClicked() {
        DialogController.showConfirmDialog(this, "退出账户", "退出该账户", (View v) -> {
            mApplication.cleanUser();
            mApplication.clearCurrShop();
            startActivity(LoginActivity.class);
            finish();
        });
    }
}
