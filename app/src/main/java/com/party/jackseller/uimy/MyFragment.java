package com.party.jackseller.uimy;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.BaseFragment;
import com.party.jackseller.R;
import com.party.jackseller.uimy.employee_manager.EmployeeListActivity;
import com.party.jackseller.uimy.wallet.MyWalletActivity;
import com.party.jackseller.uimy.wallet.RealNameAuthenticationActivity;
import com.party.jackseller.uimy.wallet.UpdatePayPasswordActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 我的
 */
public class MyFragment extends BaseFragment {

    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.user_iv)
    CircleImageView shop_Img;
    @BindView(R.id.my_fans_ll)
    LinearLayout myFansLl;
    @BindView(R.id.my_wallet_ll)
    LinearLayout myWalletLl;
    @BindView(R.id.my_shop_manager_ll)
    LinearLayout myShopManagerLl;
    @BindView(R.id.goods_manager_ll)
    LinearLayout goodsManagerLl;
    @BindView(R.id.album_manager_ll)
    LinearLayout albumManagerLl;
    @BindView(R.id.employee_manager_ll)
    LinearLayout employeeManagerLl;
    @BindView(R.id.my_contract_ll)
    LinearLayout myContractLl;
    @BindView(R.id.help_center_ll)
    LinearLayout helpCenterLl;
    Unbinder unbinder;

    public static BaseFragment newInstance() {
        BaseFragment fragment = new MyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void refreshData() {
        shop_name_tv.setText(mApplication.getCurrShop().getShop_name());
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, mApplication.getCurrShop().getHome_img(), shop_Img);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_my;
    }

    @Override
    protected void initData() {
        refreshData();
    }

    @OnClick({R.id.my_fans_ll, R.id.my_wallet_ll, R.id.my_shop_manager_ll, R.id.goods_manager_ll, R.id.album_manager_ll,
            R.id.employee_manager_ll, R.id.my_contract_ll, R.id.user_iv})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.my_fans_ll:
                if (!mApplication.isCurrShopOnline()) {
                    showToast("当前店铺尚未上线！");
                    return;
                }
                startActivity(MyFansActivity.class);
                break;
            case R.id.my_wallet_ll:
                if (!mApplication.isCurrShopOnline()) {
                    showToast("当前店铺尚未上线！");
                    return;
                }

                startActivity(MyWalletActivity.class);
                break;
            case R.id.my_shop_manager_ll:
                startActivityForResult(new Intent(mActivity, MyShopManagerActivity.class), 11);
                break;
            case R.id.goods_manager_ll:
                if (!mApplication.isCurrShopOnline()) {
                    showToast("当前店铺尚未上线！");
                    return;
                }
                startActivity(new Intent(mActivity, MyGoodsManagerActivity.class));
//                clickMyWallet();
                break;
            case R.id.album_manager_ll:
                if (!mApplication.isCurrShopOnline()) {
                    showToast("当前店铺尚未上线！");
                    return;
                }
                startActivity(AlbumManageActivity.class);
                break;
            case R.id.employee_manager_ll:
                startActivity(EmployeeListActivity.class);
                break;
            case R.id.my_contract_ll:
                startActivity(MyContractActivity.class);
                break;
            case R.id.user_iv:
                Intent intent = new Intent(mActivity, ShopInfoActivity.class);
                intent.putExtra("shopId", mApplication.getCurrShop().getId());
                startMyActivity(intent);
                break;
        }
    }

    final int UNINITIALIZED = 1;

    /**
     * 点击我的钱包
     */
    private void clickMyWallet() {
        //是否实名认证
        if (mApplication.getUser().getIsAuthentication() == UNINITIALIZED) {
            startActivity(RealNameAuthenticationActivity.class);
            return;
        }

        //是否初始化密码
        if (mApplication.getUser().getIsPassword() == UNINITIALIZED) {
            startActivity(UpdatePayPasswordActivity.class);
            return;
        }

        //进入钱包
        startActivity(MyWalletActivity.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.my_fans_ll, R.id.my_wallet_ll, R.id.my_shop_manager_ll, R.id.goods_manager_ll, R.id.album_manager_ll, R.id.employee_manager_ll, R.id.my_contract_ll, R.id.help_center_ll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_fans_ll:
                break;
            case R.id.my_wallet_ll:
                break;
            case R.id.my_shop_manager_ll:
                break;
            case R.id.goods_manager_ll:
                break;
            case R.id.album_manager_ll:
                break;
            case R.id.employee_manager_ll:
                break;
            case R.id.my_contract_ll:
                break;
            case R.id.help_center_ll:
                break;
        }
    }
}
