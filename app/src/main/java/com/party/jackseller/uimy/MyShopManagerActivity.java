package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.LoginActivity;
import com.party.jackseller.MainActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.base.ShopState;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.uicommon.ClaimShopListActivity;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 门店列表
 */
public class MyShopManagerActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.add_shop_btn)
    Button addShopBtn;
    @BindView(R.id.ll_container)
    LinearLayout llContainer;
    private ShopService shopService;
    private List<Shop> shopList = new ArrayList<>();
    private CommonAdapter commonAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop_manager);
        ButterKnife.bind(this);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("门店列表");
        setRightText("退出");
        initView();
        helper = new LoadViewHelper(refresh_layout);
        shopService = new ShopService(this);
        helper.showLoading("正在加载。。。");
    }

    public void clickRightBtn() {
        DialogController.showConfirmDialog(this, "退出账户", "退出该账户", (View v) -> {
            mApplication.cleanUser();
            mApplication.clearCurrShop();
            startActivity(LoginActivity.class);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    private void initView() {
        commonAdapter = new CommonAdapter<Shop>(this, R.layout.activity_my_shop_manager_item, shopList) {
            @Override
            protected void convert(final ViewHolder viewHolder, final Shop shop, int position) {
                ImageView imageView = viewHolder.getView(R.id.shop_icon);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, shop.getHome_img(), imageView);
                viewHolder.setText(R.id.shop_name, shop.getShop_name());
                viewHolder.setText(R.id.address, shop.getShop_address());
                viewHolder.setText(R.id.state, shop.getShop_state_name());
                if (shop.getMaster() == 1) {
                    TextView tvMyshopManagerMyshop = viewHolder.getView(R.id.tv_myshop_manager_myshop);
                    tvMyshopManagerMyshop.setVisibility(View.VISIBLE);
                }
                switch (ShopState.getState(shop)) {
                    case Open:
                        if (mApplication.getCurrShopId() == shop.getId()) {
                            viewHolder.getView(R.id.change_iv).setVisibility(View.GONE);
                            viewHolder.getView(R.id.dang_qian_iv).setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.getView(R.id.change_iv).setVisibility(View.VISIBLE);
                            viewHolder.getView(R.id.dang_qian_iv).setVisibility(View.GONE);
                        }
                        break;
                    default:
                        viewHolder.getView(R.id.change_iv).setVisibility(View.GONE);
                        break;
                }
                viewHolder.setOnClickListener(R.id.change_iv, (View view) -> {
                    if (mApplication.getCurrShopId() != shop.getId()) {
                        DialogController.showConfirmDialog(mActivity, "你确认要切换到这个商铺？", (View v) -> {
                            mApplication.setCurrShop(shop);
                            commonAdapter.notifyDataSetChanged();
                            if (mApplication.isMainActivity()) {
                                Intent intent = new Intent();
                                intent.putExtra("isSwitch", true);
                                setResult(RESULT_OK, intent);
                            } else {
                                startActivity(MainActivity.class);
                            }
                            finish();
                        });
                    }
                });
            }
        };

        refresh_layout.setEnableRefresh(false);
        refresh_layout.setEnableLoadMore(false);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Intent intent = new Intent(mActivity, ShopInfoActivity.class);
                intent.putExtra("shopId", shopList.get(position).getId());
                intent.putExtra("state", shopList.get(position).getShop_state_name());
                startActivity(intent);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {

            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                shopList.clear();
                loadData();
            }
        });
    }

    public void loadData() {
        addDisposableIoMain(shopService.getMyShopList(), new DefaultConsumer<List<Shop>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Shop>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                    helper.showEmpty("新增一个门店吧");
                } else {
                    shopList.clear();
                    shopList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                }
            }
        }, (Throwable throwable) -> {
            helper.showError();
            refresh_layout.finishRefresh();
        });
    }

    @OnClick(R.id.add_shop_btn)
    public void addShop(View view) {
        startActivity(ClaimShopListActivity.class);
    }
}
