package com.party.jackseller.uimy.wallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.uimy.CommonProblemActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class PaySettingActivity extends BaseActivityTitle {

    @BindView(R.id.real_name)
    TextView real_name;

    @BindView(R.id.setting_pass)
    TextView setting_pass;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_setting);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("支付设置");
        int isAuthentication = mApplication.getUser().getIsAuthentication();
        if (isAuthentication == 0) {
            real_name.setText("去认证");
            real_name.setOnClickListener((View v) -> {
                startActivity(RealNameAuthenticationActivity.class);
            });
        }

        int IsPassword = mApplication.getCurrShop().getIsPassword();
        if (IsPassword == 0) {
            setting_pass.setText("未设置");
        }
    }

    @OnClick(R.id.pay_password_ll)
    public void handleClickSth(View view) {
        startActivity(UpdatePayPasswordActivity.class);
    }

    @OnClick(R.id.common_problem_ll)
    public void commentQuestion(View view) {
        startActivity(CommonProblemActivity.class);
    }
}
