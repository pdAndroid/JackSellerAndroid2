package com.party.jackseller.uimy.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.utils.DensityUtils;

import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2019/1/14.
 */

public class EnterPaymentPasswordActivity extends BaseActivityTitle {
    private TextView[] tvList;
    private String firstPass;
    private String verKey;
    private int currentIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_paypassword);
        initView();
        handleTitle();
        initListener();
    }

    private void initView() {
        Intent intent = getIntent();
        verKey = intent.getStringExtra("verKey");
        firstPass = intent.getStringExtra("firstPass");
//        setMiddleText("设置支付密码");
        currentIndex = -1;
        tvList = new TextView[6];
        tvList[0] = findViewById(R.id.pay_box1);
        tvList[1] = findViewById(R.id.pay_box2);
        tvList[2] = findViewById(R.id.pay_box3);
        tvList[3] = findViewById(R.id.pay_box4);
        tvList[4] = findViewById(R.id.pay_box5);
        tvList[5] = findViewById(R.id.pay_box6);
    }

    public void handleTitle() {
        mTitleView = View.inflate(mActivity, R.layout.layout_common_title, null);
        int height = DensityUtils.dip2px(mActivity, 48f);
        FrameLayout contentView = (FrameLayout) findViewById(android.R.id.content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height);
        View rootView = contentView.getChildAt(0);
        contentView.addView(mTitleView, 0, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        layoutParams2.topMargin = height;
        rootView.setLayoutParams(layoutParams2);

        mLeftIv = mTitleView.findViewById(R.id.iv_back);
        mTitleTv = mTitleView.findViewById(R.id.tv_title);
        mRightTv = mTitleView.findViewById(R.id.tv_right);
        mLeftIv.setOnClickListener((View v) -> {
            clickLeftBtn();
        });
        mRightTv.setOnClickListener((View v) -> {
            clickRightBtn();
        });
    }

    protected void initListener() {
        tvList[5].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1) {
                    String strPassword = "";
                    for (int i = 0; i < 6; i++) {
                        strPassword += tvList[i].getText().toString().trim();
                    }
                    inputComplete(strPassword);
                }
            }
        });
    }

    /**
     * 输入密码完成
     *
     * @param password
     */
    public void inputComplete(String password) {
        //如果 第二次输入密码为空 说明是第一次输入密码。
        if (firstPass == null) {
            Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
            intent.putExtra("firstPass", password);
            intent.putExtra("verKey", verKey);
            startActivity(intent);
            finish();

            //说明是 第二次输入密码
        } else if (password.equals(firstPass)) {
            if (mApplication.getUser().getIsPassword() == 1) {
                showAlertDialog("支付密码修改中...");
            } else {
                showAlertDialog("支付密码设置中...");
            }
            Toast.makeText(mApplication, "66666666", Toast.LENGTH_SHORT).show();
        } else {
            showToast("2次输入密码不一致");
            currentIndex = -1;
            for (int i = 0; i < tvList.length; i++) {
                tvList[i].setText("");
            }
        }
    }

    @OnClick({R.id.pay_keyboard_one, R.id.pay_keyboard_two, R.id.pay_keyboard_three
            , R.id.pay_keyboard_four, R.id.pay_keyboard_five, R.id.pay_keyboard_sex
            , R.id.pay_keyboard_seven, R.id.pay_keyboard_eight, R.id.pay_keyboard_nine
            , R.id.pay_keyboard_del})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_keyboard_one:
                getPass("1");
                break;
            case R.id.pay_keyboard_two:
                getPass("2");
                break;
            case R.id.pay_keyboard_three:
                getPass("3");
                break;
            case R.id.pay_keyboard_four:
                getPass("4");
                break;
            case R.id.pay_keyboard_five:
                getPass("5");
                break;
            case R.id.pay_keyboard_sex:
                getPass("6");
                break;
            case R.id.pay_keyboard_seven:
                getPass("7");
                break;
            case R.id.pay_keyboard_eight:
                getPass("8");
                break;
            case R.id.pay_keyboard_nine:
                getPass("9");
                break;
            case R.id.pay_keyboard_zero:
                getPass("0");
                break;
            case R.id.pay_keyboard_del:
                if (currentIndex - 1 >= -1) {
                    tvList[currentIndex--].setText("");
                }
                break;
        }
    }

    public void getPass(String str) {
        if (currentIndex >= -1 && currentIndex < 5) {
            tvList[++currentIndex].setText(str);
        }
    }
}
