package com.party.jackseller.uimy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.GoodsService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.GoodsCategory;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.uihome.AddGoodsActivity;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

public class MyGoodsManagerActivity extends BaseActivityTitle {

    @BindView(R.id.whole_layout)
    LinearLayout whole_layout;
    @BindView(R.id.category_recycler_view)
    RecyclerView category_recycler_view;
    @BindView(R.id.goods_recycler_view)
    RecyclerView goods_recycler_view;

    protected GoodsService goodsService;
    private static final String TAG = "MyGoodsManagerFragment";
    /**
     * 左边列表上一个被选中的索引
     */
    protected int selectCategoryPosition = 0;
    protected List<Goods> goodsList = new ArrayList<>();
    protected boolean isDeleteIvVisible = false;//删除按钮是否可见
    /**
     * 左边的适配器
     */
    protected CommonAdapter<GoodsCategory> categoryAdapter;
    public CommonAdapter<Goods> goodsAdapter;
    private List<GoodsCategory> needShowGoodsCategoryList = new ArrayList<>();
    private LoadViewHelper goodsViewHelper;
    private int REQUEST_CODE = 12;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_manager);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("商品管理");
        helper = new LoadViewHelper(whole_layout);
        goodsViewHelper = new LoadViewHelper(goods_recycler_view);
        goodsService = new GoodsService((BaseActivityTitle) mActivity);
        initView();
        helper.showLoading();
        getGoodsCategoryList();
    }

    public void initGoodsView() {
        goodsAdapter = new CommonAdapter<Goods>(mActivity, R.layout.activity_goods_manager_goods_item, goodsList) {
            @Override
            protected void convert(ViewHolder holder, final Goods goods, final int position) {
                ImageView imageView = holder.getView(R.id.goods_image);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, goods.getImg_url(), imageView);
                holder.setText(R.id.goods_name, goods.getTitle());
                holder.setText(R.id.goods_price, "¥" + goods.getPrice());
                ImageView delete_iv = holder.getView(R.id.delete_iv);
                delete_iv.setVisibility(isDeleteIvVisible ? View.VISIBLE : View.GONE);
                holder.setOnClickListener(R.id.delete_iv, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteGoods(position);
                    }
                });
            }
        };
        goods_recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        goods_recycler_view.setAdapter(goodsAdapter);
    }

    protected void initCategoryView() {
        category_recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        categoryAdapter = new CommonAdapter<GoodsCategory>(mActivity, R.layout.activity_goods_manager_category_item, needShowGoodsCategoryList) {
            @Override
            protected void convert(ViewHolder holder, GoodsCategory goodsCategory, final int position) {
                holder.setText(R.id.tv_name, goodsCategory.getName());
                boolean check = goodsCategory.isCheck();
                holder.setBackgroundRes(R.id.fl, check ? android.R.color.white : R.color.grayEF);
                holder.setTextColorRes(R.id.tv_name, check ? R.color.colorMainBtn : R.color.black);
                holder.setVisible(R.id.view_bg, check);
            }
        };
        categoryAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                setLeftItemPosition(position);
                goodsViewHelper.showLoading();
                getGoodsByCategoryId(needShowGoodsCategoryList.get(position).getId());
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        category_recycler_view.setAdapter(categoryAdapter);
    }

    public void setLeftItemPosition(int position) {
        if (position == selectCategoryPosition) return;
        needShowGoodsCategoryList.get(selectCategoryPosition).setCheck(false);
        needShowGoodsCategoryList.get(position).setCheck(true);
        categoryAdapter.notifyItemChanged(selectCategoryPosition);
        categoryAdapter.notifyItemChanged(position);
        selectCategoryPosition = position;
    }

    @OnClick({R.id.add_goods_btn, R.id.edit_goods_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.add_goods_btn:
                startActivityForResult(new Intent(mActivity, AddGoodsActivity.class), REQUEST_CODE);
                break;
            case R.id.edit_goods_btn:
                if (needShowGoodsCategoryList.isEmpty()) {
                    showToast("没有商品可供编辑！");
                    return;
                }
                isDeleteIvVisible = !isDeleteIvVisible;
                ((Button) view).setText(isDeleteIvVisible ? "完成编辑" : "编辑商品");
                ((Button) view).setBackgroundResource(isDeleteIvVisible ? R.drawable.border_radius_gray : R.drawable.main_btn);
                goodsAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void initView() {
        initCategoryView();
        initGoodsView();
    }

    /**
     * 点击了右侧的商品item
     */
    public void deleteGoods(int position) {
        DialogController.showConfirmDialog(mActivity, "是否删除该商品？", (View v) -> {
            final long goodsId = goodsList.get(position).getId();
            ((BaseActivity) mActivity).showAlertDialog("删除商品...");
            addDisposableIoMain(goodsService.deleteGoods(goodsId), new DefaultConsumer<String>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<String> baseBean) {
                    ((BaseActivity) mActivity).hideAlertDialog();
                    goodsList.remove(position);
                    goodsAdapter.notifyDataSetChanged();
                    if (goodsList.isEmpty()) {
                        DialogController.showConfirmDialog(mActivity, "是否要删除这个分类？", (View v) -> {
                            final int position2 = selectCategoryPosition;
                            addDisposableIoMain(goodsService.delGoodsCategory(needShowGoodsCategoryList.get(position2).getId()), new DefaultConsumer<Long>(mApplication) {
                                @Override
                                public void operateSuccess(BaseResult<Long> baseBean) {
                                    needShowGoodsCategoryList.remove(position2);
                                    categoryAdapter.notifyDataSetChanged();
                                }
                            });
                        });
                    }
                }
            });
        });
    }

    public void getGoodsCategoryList() {
        addDisposableIoMain(goodsService.getGoodsCategory(mApplication.getCurrShopId()), new DefaultConsumer<List<GoodsCategory>>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError();
            }

            @Override
            public void operateSuccess(BaseResult<List<GoodsCategory>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                    helper.showEmpty();
                } else {
                    needShowGoodsCategoryList.clear();
                    needShowGoodsCategoryList.addAll(baseBean.getData());
                    categoryAdapter.notifyDataSetChanged();
                    showGoodsListFromFirstCategory();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showError();
            }
        });
    }

    public void getGoodsByCategoryId(long typeId) {
        addDisposableIoMain(goodsService.getShopGoodsByTypeId(mApplication.getCurrShopId(), typeId), new DefaultConsumer<List<Goods>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Goods>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                    goodsViewHelper.showEmpty();
                } else {
                    goodsViewHelper.showContent();
                }
                goodsList.clear();
                goodsList.addAll(baseBean.getData());
                goodsAdapter.notifyDataSetChanged();
            }
        });
    }

    public void showGoodsListFromFirstCategory() {
        addDisposableIoMain(goodsService.getShopGoodsByTypeId(mApplication.getCurrShopId(), needShowGoodsCategoryList.get(0).getId()), new DefaultConsumer<List<Goods>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Goods>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                    goodsViewHelper.showEmpty();
                } else {
                    goodsViewHelper.showContent();
                }
                goodsList.clear();
                goodsList.addAll(baseBean.getData());
                goodsAdapter.notifyDataSetChanged();
                helper.showContent();
            }
        });
        selectCategoryPosition = 0;
        needShowGoodsCategoryList.get(selectCategoryPosition).setCheck(true);
        categoryAdapter.notifyItemChanged(selectCategoryPosition);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            boolean isAdd = data.getBooleanExtra("isAdd", false);
            if (isAdd) {
                helper.showLoading();
                getGoodsCategoryList();
            }
        }
    }
}
