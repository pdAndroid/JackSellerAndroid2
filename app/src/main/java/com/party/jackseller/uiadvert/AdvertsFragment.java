package com.party.jackseller.uiadvert;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseFragment;
import com.party.jackseller.R;
import com.party.jackseller.adapter.MyCommonAdapter;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.VideoService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.VideoBean;
import com.party.jackseller.event.UploadVideoEvent;
import com.party.jackseller.litvedio.play.SingleTCVodPlayerActivity;
import com.party.jackseller.litvedio.videochoose.TCVideoChooseActivity;
import com.party.jackseller.litvedio.videorecord.TCVideoRecordActivity;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.PermissionUtils;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 推广通
 */
public class AdvertsFragment extends BaseFragment {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    List<VideoBean> dataList = new ArrayList<>();
    MyCommonAdapter<VideoBean> commonAdapter;
    int page = 1;
    int pageSize = ConstUtils.PAGE_SIZE;
    String emptyStr = "暂无视频数据";
    VideoService videoService;
    long shopId;

    public static BaseFragment newInstance() {
        BaseFragment fragment = new AdvertsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_adverts;
    }


    @Override
    protected void initData() {
        setNeedOnCreateRegister();
        iv_back.setVisibility(View.INVISIBLE);
        tv_title.setText("众享推广");
        tv_right.setText("发布");
        tv_right.setVisibility(View.VISIBLE);
        shopId = mApplication.getCurrShopId();

        helper = new LoadViewHelper(mRefreshLayout);
        helper.setListener(() -> {
            getNeedData();
        });

        if (shopId == 1534994219965L) {
            tv_title.setOnClickListener((View v) -> {
                startActivity(AdvertsActivity.class);
            });
        }

        tv_title.setOnClickListener((View v) -> {
            startActivity(AdvertsActivity.class);
        });


        videoService = new VideoService(mActivity);
        commonAdapter = new MyCommonAdapter<VideoBean>(mActivity, R.layout.item_fragment_adverts, dataList) {
            @Override
            protected void convert(ViewHolder holder, final VideoBean item, int position) {
                ImageView imageView = holder.getView(R.id.iv_cover);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, item.getVideoURL(), imageView);
                holder.setText(R.id.tv_video_status, item.getReviewStatus() + "");
                holder.setText(R.id.tv_desc, item.getShopName());

                holder.setText(R.id.tv_follow_count, "关注数 0");
                holder.setText(R.id.tv_comment_count, "评论数 0");
                holder.setText(R.id.tv_favor_count, "点赞数 0");

                holder.setOnClickListener(R.id.fl_video, (View v) -> {
                    Intent intent = new Intent(mActivity, SingleTCVodPlayerActivity.class);
                    intent.putExtra("coverURL", item.getVideoURL());
                    intent.putExtra("playURL", item.getVideoURL());
                    startActivity(intent);
                });
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mActivity, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(commonAdapter);
    }

    @OnClick(R.id.tv_right)
    public void clickRight() {
        DialogController.showMenuList(new String[]{"立即拍摄", "本地资源"}, mActivity, (int which) -> {
            PermissionUtils.checkSDK(mActivity);
            switch (which) {
                case 0:
                    startActivity(TCVideoRecordActivity.class);
                    //startActivity(CommitVideoActivity.class);
                    break;
                case 1:
                    startActivity(TCVideoChooseActivity.class);
                    break;
            }
        });
    }

    @Override
    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                getNeedData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                getNeedData();
            }
        });
        getNeedData();
    }

    /**
     * 发布视频成功了
     *
     * @param event
     */
    @Subscribe
    public void onUploadVideoEvent(UploadVideoEvent event) {
        mRefreshLayout.autoRefresh(0, 200, 1.75f);
    }

    /**
     * item点击事件
     *
     * @param position
     */
    private void handleItemClick(int position) {
//        Intent intent = new Intent(mActivity, ShopInfoActivity.class);
//        intent.putExtra("shop", dataList.get(position));
//        startActivity(intent);
    }

    /**
     * 获取数据
     */
    public void getNeedData() {
        mActivity.addDisposableIoMain(videoService.listVideoByShopId(shopId, page, pageSize), new DefaultConsumer<List<VideoBean>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<VideoBean>> baseBean) {
                commonAdapter.clear(page);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(page, pageSize, baseBean.getData().size(), mRefreshLayout);
                finishData(commonAdapter.getDatas());
            }
        });
    }

}
