package com.party.jackseller.uiadvert;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.LoginActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ShopAdverts;
import com.party.jackseller.litvedio.videochoose.TCVideoChooseActivity;
import com.party.jackseller.litvedio.videorecord.TCVideoRecordActivity;
import com.party.jackseller.uicommon.ClaimShopListActivity;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.PermissionUtils;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 推广通
 */
public class AdvertsActivity extends BaseActivityTitle {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.search_key)
    EditText mSearchKey;

    @BindView(R.id.search_btn)
    Button mSearchBtn;

    private ShopService shopService;
    private List<ShopAdverts> shopList = new ArrayList<>();
    private CommonAdapter commonAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adverts_shop_manager);
        initData();
        initView();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("广告发布专用列表");
        shopService = new ShopService(this);
    }

    @OnClick(R.id.search_btn)
    public void OnSearchBtn() {
        String key = mSearchKey.getText().toString();
        if (key.length() > 0) {
            loadData(key);
        }
    }

    public void clickRightBtn() {
        DialogController.showConfirmDialog(this, "退出账户", "退出该账户", (View v) -> {
            mApplication.cleanUser();
            mApplication.clearCurrShop();
            startActivity(LoginActivity.class);
            finish();
        });
    }


    private void initView() {
        commonAdapter = new CommonAdapter<ShopAdverts>(this, R.layout.activity_my_shop_manager_item, shopList) {
            @Override
            protected void convert(final ViewHolder viewHolder, final ShopAdverts shop, int position) {
                ImageView imageView = viewHolder.getView(R.id.shop_icon);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, shop.getHomeImg(), imageView);
                viewHolder.setText(R.id.shop_name, shop.getShopName());
                viewHolder.setText(R.id.address, shop.getShopAddress());
                viewHolder.setText(R.id.state, 1 == shop.getShopState() ? "营业" : "非营业");
            }
        };


        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                DialogController.showMenuList(new String[]{"立即拍摄", "本地资源"}, mActivity, (int which) -> {
                    PermissionUtils.checkSDK(mActivity);
                    mApplication.setReplaceShop(shopList.get(position));
                    switch (which) {
                        case 0:
                            startActivity(TCVideoRecordActivity.class);
                            break;
                        case 1:
                            startActivity(TCVideoChooseActivity.class);

//                            String videoId = "5285890784521842941";
//                            String videoURL = "http://1257403081.vod2.myqcloud.com/d0993821vodcq1257403081/ca29a6715285890784521842941/A6AuEXjsnsoA.mp4";
//                            String coverURL = "http://1257403081.vod2.myqcloud.com/d0993821vodcq1257403081/ca29a6715285890784521842941/5285890784521842942.jpg";
//                            long videoDuration = 5489;
//                            long videoFileSize = 2643014;
//                            Intent intent = new Intent(AdvertsActivity.this, CommitVideoActivity.class);
//                            intent.putExtra("videoId", videoId);
//                            intent.putExtra("videoURL", videoURL);
//                            intent.putExtra("coverURL", coverURL);
//                            intent.putExtra("videoDuration", videoDuration);
//                            intent.putExtra("videoFileSize", videoFileSize);
//                            startActivity(intent);

                            break;
                    }
                });
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    public void loadData(String key) {
        addDisposableIoMain(shopService.getShopByNameOrPhone(key), new DefaultConsumer<List<ShopAdverts>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ShopAdverts>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                } else {
                    shopList.clear();
                    shopList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mApplication.setReplaceShop(null);
    }

    @OnClick(R.id.add_shop_btn)
    public void addShop(View view) {
        startActivity(ClaimShopListActivity.class);
    }
}
