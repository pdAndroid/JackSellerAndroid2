package com.party.jackseller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.controller.AMapController;
import com.party.jackseller.controller.AppUpdateController;
import com.party.jackseller.event.AMapBeanEvent;
import com.party.jackseller.permission.RuntimeRationale;
import com.party.jackseller.uimy.MyShopManagerActivity;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.DialogController;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class StartActivity extends BaseActivity implements AppUpdateController.AppUpdateListener {
    Handler mHandler;
    AppUpdateController appUpdateController;

    /**
     * 是否有读写手机目录的权限
     */
    boolean isHasPer;
    AMapBeanEvent mapBeanEvent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNeedOnCreateRegister();
        setContentView(R.layout.activity_start);
        initData();
    }

    protected void initData() {
        mHandler = new Handler();
        appUpdateController = new AppUpdateController(this, this);
        appUpdateController.checkAppUpdate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    /**
     * 应用没有更新
     */
    @Override
    public void notUpdate(boolean showMsg) {
        initMap();
    }

    protected void initMap() {
        mApplication.updateMap((AMapBeanEvent beanEvent) -> {
            mapBeanEvent = beanEvent;
            loadData();
        });
    }


    public void loadData() {
        if (TextUtils.isEmpty(mApplication.getToken())) {
            startActivity(LoginActivity.class);
            finish();
        } else {
            ShopService shopService = new ShopService(this);
            addDisposableIoMain(shopService.getMyShopList(), new DefaultConsumer<List<Shop>>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<List<Shop>> baseBean) {
                    if (!baseBean.getData().isEmpty() && !TextUtils.isEmpty(mApplication.getCurrShopName())) {
                        for (int i = 0; i < baseBean.getData().size(); i++) {
                            long id = baseBean.getData().get(i).getId();
                            if (id == mApplication.getCurrShop().getId()) {
                                startActivity(MainActivity.class);
                                finish();
                                return;
                            }
                        }
                    }
                    startActivity(MyShopManagerActivity.class);
                    finish();
                }
            }, (Throwable throwable) -> {
                showToast("加载数据失败");
            });
        }
    }

    /**
     * 申请读写权限
     */
    public void applyNeedPermissions() {
        final String[] permissions = new String[]{Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE};
        AndPermission.with(this)
                .runtime()
                .permission(permissions)
                .rationale(new RuntimeRationale())
                .onGranted((List<String> data) -> {
                    isHasPer = true;
                    checkPermissionsAgain(true, permissions);
                })
                .onDenied((List<String> data) -> {
                    isHasPer = false;
                    checkPermissionsAgain(false, permissions);
                }).start();
    }

    public boolean isHasPer() {
        return isHasPer;
    }

    /**
     * 用户点击不在提示,或者在应用管理里面关闭权限之后
     */
    public void checkPermissionsAgain(boolean onGranted, String[] permissions) {
        if (onGranted) {
            if (AndPermission.hasPermissions(mActivity, permissions)) {
                if (appUpdateController != null) {
                    appUpdateController.showApkDownloadDialog();
                }
            } else {
                showSettingDialog(mActivity, permissions);
            }
        } else {
            if (AndPermission.hasAlwaysDeniedPermission(mActivity, permissions)) {
                showSettingDialog(mActivity, permissions);
            }
        }
    }

    /**
     * Display setting dialog.
     */
    public void showSettingDialog(Context context, final String[] permissions) {
        List<String> permissionNames = Permission.transformText(context, permissions);
        String message = context.getString(R.string.message_permission_always_failed, TextUtils.join("\n", permissionNames));
        DialogController.showConfirmDialog(mActivity, "提示", message, (View v) -> {
            toSystemSetting();
        });
    }

    /**
     * 调整到系统设置界面去申请权限
     */
    public void toSystemSetting() {
        AndPermission.with(mActivity).runtime().setting().start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 安装未知源请求码
        if (requestCode == ConstUtils.MANAGE_UNKNOWN_APP_SOURCES_CODE && resultCode == RESULT_OK) {
            if (appUpdateController != null) {
                appUpdateController.continueInstallApk();
            }
        }
    }

}
