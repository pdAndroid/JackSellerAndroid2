package com.party.jackseller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.api.VideoService;
import com.party.jackseller.base.MyException;
import com.party.jackseller.base.MyTextWatcher;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CacheVideo;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.ShopAdverts;
import com.party.jackseller.bean.VideoCondition;
import com.party.jackseller.bean.VideoPusher;
import com.party.jackseller.controller.AMapController;
import com.party.jackseller.controller.PayController;
import com.party.jackseller.event.AMapBeanEvent;
import com.party.jackseller.event.PayResultEvent;
import com.party.jackseller.event.UploadVideoEvent;
import com.party.jackseller.litvedio.config.TCConstants;
import com.party.jackseller.utils.AMapUtils;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.SPUtils;
import com.party.jackseller.utils.Utils;
import com.party.jackseller.view.flowlayout.FlowLayout;
import com.party.jackseller.view.flowlayout.TagAdapter;
import com.party.jackseller.view.flowlayout.TagFlowLayout;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;

import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 从{@link com.party.jackseller.litvedio.videopublish.TCVideoPublisherActivity#UploadUGCVideo(String, String, String)}
 * 跳转过来的
 */
public class CommitVideoActivity extends BaseActivityTitle {
    @BindView(R.id.whole_layout)
    ViewGroup whole_layout;
    @BindView(R.id.et_content)
    EditText et_content;
    @BindView(R.id.iv_thumb)
    ImageView iv_thumb;
    @BindView(R.id.tv_shop_name)
    TextView tv_shop_name;
    @BindView(R.id.tv_shop_address)
    TextView tv_shop_address;
    @BindView(R.id.tv_location_again)
    TextView tv_location_again;
    @BindView(R.id.flow_layout)
    TagFlowLayout flow_layout;
    @BindView(R.id.tv_location_info)
    TextView tv_location_info;
    @BindView(R.id.tv_age_tips_1)
    TextView tv_age_tips_1;
    @BindView(R.id.ll_age)
    LinearLayout ll_age;
    @BindView(R.id.et_age_min)
    EditText et_age_min;
    @BindView(R.id.et_age_max)
    EditText et_age_max;
    @BindView(R.id.tv_sex_tips_1)
    TextView tv_sex_tips_1;
    @BindView(R.id.cb_sex_male)
    CheckBox cb_sex_male;
    @BindView(R.id.cb_sex_female)
    CheckBox cb_sex_female;
    @BindView(R.id.tv_per_money)
    TextView tv_per_money;
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.tv_money)
    TextView tv_money;
    @BindView(R.id.btn_ok)
    TextView btn_ok;
    @BindView(R.id.et_visit_count)
    EditText et_visit_count;

    // 分为单位
    VideoService videoService;
    AMapBeanEvent curShopLoc;

    List<VideoPusher.Loc> nearDataList = new ArrayList<>();
    LayoutInflater inflater;
    AMapController mapController;
    AMapUtils aMapUtils;
    TagAdapter<VideoPusher.Loc> tagAdapter;
    String orderId;
    PayController payController;
    UserService userService;
    VideoPusher mVideoPusher;
    ShopAdverts mShop;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNeedOnCreateRegister();
        setContentView(R.layout.activity_commit_video);
        receivePassDataIfNeed(getIntent());
        initListener();
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        mShop = mApplication.getReplaceShop();
        if (mShop == null) {
            mShop = new ShopAdverts();
            Shop currShop = mApplication.getCurrShop();
            mShop.setId(currShop.getId());
            mShop.setShopName(currShop.getShop_name());
            mShop.setHomeImg(currShop.getHome_img());
            mShop.setLatitude(currShop.getLatitude());
            mShop.setLongitude(currShop.getLongitude());
            mShop.setShopAddress(currShop.getShop_address());
        }

        SPUtils.cacheVideo(new CacheVideo(intent));

        mVideoPusher = new VideoPusher(intent);
    }

    protected void initListener() {
        addListener(et_age_min);
        addListener(et_age_max);
        addListener(cb_sex_male, cb_sex_female);
        addListener(cb_sex_female, cb_sex_male);
    }

    private void addListener(EditText editText) {
        editText.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                calculateMoney();
            }
        });
    }

    private void addListener(CheckBox checkBox, CheckBox checkBox2) {
        checkBox.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            checkBox2.setChecked(!isChecked);
            if (cb_sex_female.isChecked()) {
                mVideoPusher.setSex(0);
            } else {
                mVideoPusher.setSex(1);
            }
        });
    }

    protected void initData() {
        setNeedOnCreateRegister();
        handleTitle();
        setMiddleText("发布");

        userService = new UserService(this);
        payController = new PayController(this);
        payController.setHelpView(btn_ok);
        videoService = new VideoService(this);
        mapController = new AMapController(this);
        aMapUtils = new AMapUtils(this);
        inflater = getLayoutInflater();
        // 重新定位下划线
        Utils.setBottomLine(tv_location_again);
        Utils.setBottomLine(tv_shop_name);

        flow_layout.setMaxSelectCount(1);
        tagAdapter = new TagAdapter<VideoPusher.Loc>(nearDataList) {
            @Override
            public View getView(FlowLayout parent, int position, VideoPusher.Loc loc) {
                TextView tv = (TextView) inflater.inflate(R.layout.layout_commit_video_location_tv, flow_layout, false);
                tv.setText(loc.getName());
                return tv;
            }
        };
        flow_layout.setAdapter(tagAdapter);
        flow_layout.setOnTagClickListener((View view, int position, FlowLayout parent) -> {
            return true;
        });


        flow_layout.setOnSelectListener((Set<Integer> selectPosSet) -> {
            int position = 0;
            Iterator<Integer> it = selectPosSet.iterator();
            if (it.hasNext()) position = it.next().intValue();
            mVideoPusher.setLoc(nearDataList.get(position));
            if (position == 0) {
                tv_location_info.setText("发布范围为" + mVideoPusher.getAddress() + "附近3km");
            } else {
                tv_location_info.setText("发布范围为" + nearDataList.get(position).getName());
            }
        });

        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, mVideoPusher.getCoverURL(), iv_thumb);
        tv_per_money.setText("¥ " + mVideoPusher.getPerPrice());
        tv_shop_name.setText(mShop.getShopName());
        helper = new LoadViewHelper(whole_layout);
        helper.showLoading();

        listVideoCondition();
        getLocMap();
    }

    private void getLocMap() {
        LatLonPoint point = new LatLonPoint(mShop.getLatitude(), mShop.getLongitude());
        aMapUtils.geoCoderSearch(point, (AMapBeanEvent bean) -> {
            curShopLoc = bean;
            curShopLoc.setAddress(mShop.getShopAddress());
            setMapLocation(bean);
        });
    }


    /**
     * 获取视频低价
     */
    private void listVideoCondition() {
        addDisposableIoMain(userService.listVideoCondition(), new DefaultConsumer<VideoCondition>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError();
            }

            @Override
            public void operateSuccess(BaseResult<VideoCondition> baseBean) {
                VideoCondition data = baseBean.getData();
                mVideoPusher.setMinPerMoney(data.getMinPrice() + "");
                mVideoPusher.setPerPrice(data.getMinPrice() + "");
                mVideoPusher.setSexPrice(data.getSexPrice() + "");
                mVideoPusher.setMaxPerMoney(data.getMaxPrice() + "");
                mVideoPusher.setSexRatioPrice(data.getSexRatioPrice() + "");
                tv_per_money.setText("¥ " + mVideoPusher.getPerPrice());
                payController.initWallet(helper);
            }
        }, (Throwable throwable) -> {
            helper.showError();
        });
    }

    /**
     * 根据店铺经纬度获取相关信息
     */
    private void setMapLocation(AMapBeanEvent bean) {
        mVideoPusher.setMapBean(bean);
        parseLoc(nearDataList, bean);
        tv_shop_address.setText("(" + bean.getAddress() + ")");
        tv_location_info.setText("发布范围为" + bean.getAddress());
        tagAdapter.notifyDataChanged();
    }

    public void parseLoc(List<VideoPusher.Loc> nearDataList, AMapBeanEvent bean) {
        nearDataList.clear();
        String adCode = bean.getAdCode();
        nearDataList.add(new VideoPusher.Loc("附近3km", 2, 3, adCode));
        nearDataList.add(new VideoPusher.Loc(bean.getDistrict(), 1, 0, adCode));
        //cityId   县：510123  市  5101  省 510000 国 000000
        nearDataList.add(new VideoPusher.Loc(bean.getCity(), 1, 0, adCode.substring(0, 4) + "00"));
        nearDataList.add(new VideoPusher.Loc(bean.getProvince(), 1, 0, adCode.substring(0, 2) + "0000"));
        nearDataList.add(new VideoPusher.Loc(bean.getCountry(), 1, 0, "000000"));
    }

    /**
     * 上传视频
     */
    private void uploadVideo() {
        Map<String, String> map = null;
        try {
            String avDescribe = et_content.getText().toString();
            if (avDescribe == null) avDescribe = "";
            map = mVideoPusher.getCommitData();
            map.put("describe", avDescribe);

            map.put("shopId", mShop.getId() + "");
            map.put("homeImg", mShop.getHomeImg());
            map.put("shopName", mShop.getShopName());

            if (mApplication.getCurrShop().getId() == 1534994219965L && mShop != null) {
                String introduce = mShop.getIntroduce();
                if (avDescribe.length() == 0)
                    map.put("describe", introduce == null ? "" : introduce);
                map.put("replaceShopId", mApplication.getCurrShop().getId() + "");
                map.put("replaceShopName", mApplication.getCurrShop().getShop_name() + "");
            }


        } catch (Exception e) {
            showToast(e.getMessage());
            return;
        }
        showAlertDialog("正在发布视频...");
        addDisposableIoMain(videoService.addVideo(map), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateError(String message) {
                helper.showContent();
                showToast(message);
                hideAlertDialog();
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                hideAlertDialog();
                helper.showContent();
                orderId = baseBean.getData().getOrderId();
                payController.setOrderIdStr(orderId);
                payController.setFinalNum(1);
                payController.setBuyPrice(Double.valueOf(mVideoPusher.getTotalMoney()));
                payController.setGoodsType(Constant.GOODS_TYPE_VIDEO);
                uploadVideoSuccess();
            }
        }, (Throwable throwable) -> {
            hideAlertDialog();
            showToast("发布视频失败==" + throwable.getMessage());
        });
    }

    /**
     * 上传视频成功
     */
    private void uploadVideoSuccess() {
        payController.showPayChoosePopupWindow();
    }

    @Subscribe
    public void onPayResultEvent(PayResultEvent event) {
        if (event.getResultType() == 1) {
            paySuccess();
        }
    }

    /**
     * 支付成功
     */
    private void paySuccess() {
        mApplication.post(new UploadVideoEvent());
        Toast.makeText(mApplication, "上传成功", Toast.LENGTH_SHORT).show();
        finish();
    }


    /**
     * 重新定位
     */
    @OnClick(R.id.tv_location_again)
    public void clickLocation() {
        mapController.choosePosition(mActivity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case ConstUtils.SEARCH_LOCATION_CODE:
                    PoiItem poiItem = data.getParcelableExtra("poiItem");
                    if (poiItem != null) {
                        aMapUtils.geoCoderSearch(poiItem.getLatLonPoint(), (AMapBeanEvent bean) -> {
                            setMapLocation(bean);
                        });
                    }
                    break;
            }
        }
    }


    /**
     * 点击店铺名字,使用店铺自己的地址
     */
    @OnClick(R.id.tv_shop_name)
    public void clickShopName() {
        if (curShopLoc == null) {
            getLocMap();
        } else {
            setMapLocation(curShopLoc);
        }
    }


    private BigDecimal getAgeMoney() throws MyException {
        String minAge = et_age_min.getText().toString();
        String maxAge = et_age_max.getText().toString();
        if (minAge.trim().length() == 0) minAge = "12";
        if (maxAge.trim().length() == 0) maxAge = "80";
        int min = Integer.parseInt(minAge);
        int max = Integer.parseInt(maxAge);
        if (min < 12 || max > 80) {
            throw new MyException(null);
        }
        if ((max - min) < 5) {
            throw new MyException(null);
        }

        BigDecimal maxPrice = new BigDecimal(mVideoPusher.getMaxPrice());
        BigDecimal rate = new BigDecimal("0.1");
        BigDecimal one = new BigDecimal("1");
        BigDecimal oldOff = new BigDecimal(max - min + "");
        BigDecimal rate1 = new BigDecimal("68.0");//最大年龄减去最新年龄
        //计算费用的公式

        BigDecimal needMoney = (maxPrice.subtract(rate)).multiply(one.subtract(oldOff.divide(rate1, 2, RoundingMode.HALF_UP)));
        mVideoPusher.setSelectAge(true);
        mVideoPusher.setMaxAge(max);
        mVideoPusher.setMinAge(min);
        // 年龄段拉满
        return needMoney.setScale(2, RoundingMode.HALF_UP);
    }

    public void calculateMoney() {
        BigDecimal perMoney = new BigDecimal("0.1");
        try {
            if (mVideoPusher.isAgeChecked()) perMoney = perMoney.add(getAgeMoney());
            if (mVideoPusher.isSexChecked())
                perMoney = perMoney.multiply(new BigDecimal(mVideoPusher.getSexRatioPrice())).setScale(2, BigDecimal.ROUND_HALF_UP);
        } catch (Exception e) {
            if (e.getMessage() != null) showToast(e.getMessage());
            return;
        }

        tv_per_money.setText("¥ " + perMoney);
        if (totalCount == 0) {
            tv_money.setText("");
            return;
        }
        String totalMoney = perMoney.multiply(new BigDecimal(totalCount)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        mVideoPusher.setTotalMoney(totalMoney);
        tv_money.setText("¥ " + totalMoney);
    }


    /**
     * 选择年龄
     */
    @OnClick({R.id.tv_age_tips_1, R.id.tv_age_tips_2})
    public void clickAge() {
        if (!mVideoPusher.isAgeChecked()) {
            mVideoPusher.setSelectAge(true);
            tv_age_tips_1.setTextColor(ContextCompat.getColor(mActivity, R.color.colorMain));
            ll_age.setVisibility(View.VISIBLE);
        } else {
            mVideoPusher.setSelectAge(false);
            tv_age_tips_1.setTextColor(ContextCompat.getColor(mActivity, R.color.black));
            ll_age.setVisibility(View.INVISIBLE);
        }
        calculateMoney();
    }

    private int totalCount;

    /**
     * 选择性别
     */
    @OnClick({R.id.tv_sex_tips_1, R.id.tv_sex_tips_2})
    public void clickSex() {
        if (!mVideoPusher.isSexChecked()) {
            if (mVideoPusher.getSex() == -1) {//如果当前 sex == -1 表示未设置过性别，需要设置默认值，
                cb_sex_female.setChecked(true);
                mVideoPusher.setSex(0);
            }
            tv_sex_tips_1.setTextColor(ContextCompat.getColor(mActivity, R.color.colorMain));
            cb_sex_male.setVisibility(View.VISIBLE);
            cb_sex_female.setVisibility(View.VISIBLE);
        } else {
            tv_sex_tips_1.setTextColor(ContextCompat.getColor(mActivity, R.color.black));
            cb_sex_male.setVisibility(View.INVISIBLE);
            cb_sex_female.setVisibility(View.INVISIBLE);
        }
        mVideoPusher.setSexChecked(!mVideoPusher.isSexChecked());
        calculateMoney();
    }

    @OnClick({R.id.rl_count, R.id.tv_count})
    public void clickCountRl() {
        String countStr = null;
        if (totalCount > 0) {
            countStr = totalCount + "";
        }
        DialogController.showInputCountDialog(mActivity, countStr, (String str) -> {
            try {
                totalCount = Integer.parseInt(str);
                mVideoPusher.setTotalCount(totalCount);
            } catch (Exception e) {
                showToast("输入的次数有问题");
                return;
            }
            tv_count.setText(totalCount + "次");
            calculateMoney();
        });
    }

    @OnClick(R.id.iv_thumb)
    public void clickThumb() {
        Intent intent = new Intent(mActivity, TCVideoCutterActivity.class);
        intent.putExtra(TCConstants.VIDEO_EDITER_PATH, mVideoPusher.getCoverURL());
        startActivity(intent);
    }

    @OnClick(R.id.btn_ok)
    public void clickOkBtn() {
        uploadVideo();
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(mActivity, TCVideoChooseActivity.class);
//        startActivity(intent);
        super.onBackPressed();
    }
}
