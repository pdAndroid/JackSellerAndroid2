package com.party.jackseller.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;

public class MineVideoActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_video);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("我的视频");
    }
}
