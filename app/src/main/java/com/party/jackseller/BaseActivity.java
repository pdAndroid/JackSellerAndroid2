package com.party.jackseller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.party.jackseller.MApplication;
import com.party.jackseller.PictureDetailActivity;
import com.party.jackseller.R;
import com.party.jackseller.RxSchedulers;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.controller.ChoosePictureController;
import com.party.jackseller.event.OnClickImageEvent;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.ToastUtils;
import com.party.jackseller.widget.dialog.AlertProgressDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public abstract class BaseActivity extends AppCompatActivity {
    /**
     * 在<code>onCreateView</code>的时候注册,需要在<code>onDestroy</code>取消注册
     */
    private boolean isNeedOnCreateRegister = false;
    /**
     * 在<code>OnResume</code>的时候注册,需要在<code>OnPause</code>取消注册
     */
    private boolean isNeedOnResumeRegister = false;
    public boolean IsEventBus = false;
    protected MApplication mApplication;
    protected Activity mActivity;
    protected String mClassName;
    protected Resources mResources;
    private Unbinder unbinder;
    public LoadViewHelper helper;

    /**
     * 请在{@link #setNeedOnCreateRegister()}里面调用
     */
    protected void setNeedOnCreateRegister() {
        isNeedOnCreateRegister = true;
    }

    /**
     * 请在{@link #setNeedOnResumeRegister()}里面调用
     */
    protected void setNeedOnResumeRegister() {
        isNeedOnResumeRegister = true;
    }

    protected CompositeDisposable compositeDisposable;
    /**
     * 登录进度框
     */
    protected AlertDialog mProgressDialog;
    /**
     * 提交数据进度框
     */
    protected AlertDialog commonProgressDialog;
    protected ChoosePictureController choosePictureController;

    /**
     * 显示进度框
     */
    public void showLoadingDialog() {
        showLoadingDialog("提交数据中");
    }

    /**
     * 显示进度框
     */
    public void showLoadingDialog(String str) {
        if (mProgressDialog == null) {
            mProgressDialog = new AlertProgressDialog.Builder(mActivity)
                    .setView(R.layout.layout_alert_dialog)
                    .setCancelable(false)
                    .setMessage(str)
                    .show();
        } else {
            mProgressDialog.show();
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//禁止横竖屏转换
        super.setContentView(layoutResID);
        init();
    }

    public void initEventBus() {
        EventBus.getDefault().register(this);
    }

    protected void bindButterKnife() {
        try {
            unbinder = ButterKnife.bind(this);
        } catch (Exception e) {
        }
    }

    protected void init() {
        mActivity = this;
        choosePictureController = new ChoosePictureController(this);
        mApplication = (MApplication) getApplication();
        mClassName = getClass().getSimpleName();
        mResources = getResources();
        bindButterKnife();
        if (isNeedOnCreateRegister) {
            mApplication.registerEventBus(this);
        }
        mApplication.pushActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedOnResumeRegister) {
            mApplication.registerEventBus(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isNeedOnResumeRegister) {
            mApplication.registerEventBus(this);
        }
    }

    /**
     * 添加一个请求,子线程中执行然后切换回主线程
     *
     * @param observable
     */
    public <T> void addDisposableIoMain(Observable<T> observable, Consumer<T> consumer) {
        addDisposableIoMain(observable, consumer, (Throwable throwable) -> {
            showToast("BaseActivity = " + throwable.getMessage());
            hideAlertDialog();
        });
    }

    /**
     * 添加一个请求,子线程中执行然后切换回主线程
     *
     * @param observable
     */
    public <T> void addDisposableIoMain(Observable<T> observable, Consumer<T> consumer, Consumer<Throwable> throwable) {
        if (compositeDisposable == null) {
            synchronized (this) {
                if (compositeDisposable == null) {
                    compositeDisposable = new CompositeDisposable();
                }
            }
        }
        if (observable != null) {
            Observable<T> observableCompose = observable.compose(RxSchedulers.io_main());
            compositeDisposable.add(observableCompose.subscribe((T t) -> {
                consumer.accept(t);
            }, throwable));
        }
    }


    /**
     * 结束所有请求
     */
    public void removeCompositeDisposable() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
        if (IsEventBus) EventBus.getDefault().unregister(this);
        if (isNeedOnCreateRegister) {
            mApplication.unregisterEventBus(this);
        }
        mApplication.removeActivity(this);
        removeCompositeDisposable();
    }

    /**
     * 显示软键盘
     *
     * @param context
     * @param editText
     */
    public void showSoftInput(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            editText.requestFocus();
            imm.showSoftInput(editText, 0);
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param context
     * @param view
     */
    public void hideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && softInputIsOpen(context)) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void forceHideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }

    /**
     * @param context
     * @return 若返回true，则表示输入法打开
     */
    public boolean softInputIsOpen(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        return imm.isActive();
    }

    /**
     * 显示进度框
     */
    public void showAlertDialog() {
        showAlertDialog(false, getResources().getString(R.string.commit_data_ing));
    }

    public void showAlertDialog(String message) {
        showAlertDialog(false, message);
    }

    /**
     * 显示进度框
     */
    public void showAlertDialog(boolean isCancel, int resId) {
        try {
            showAlertDialog(isCancel, getResources().getString(resId));
        } catch (Exception e) {
            showAlertDialog(isCancel, getResources().getString(R.string.commit_data_ing));
            e.printStackTrace();
        }
    }

    /**
     * 显示进度框
     */
    public void showAlertDialog(boolean isCancel, String str) {
        if (commonProgressDialog == null) {
            commonProgressDialog = new AlertProgressDialog.Builder(mActivity)
                    .setView(R.layout.layout_alert_dialog)
                    .show();
        } else {
            commonProgressDialog.setCancelable(isCancel);
            commonProgressDialog.setMessage(str);
            commonProgressDialog.show();
        }
    }

    public void startActivity(Class clazz) {
        Intent intent = new Intent(this, clazz);
        startMyActivity(intent);
    }

    public void startMyActivity(Intent intent) {
        startActivity(intent);
    }

    /**
     * 隐藏进度框
     */
    public void hideAlertDialog() {
        if (commonProgressDialog != null) {
            commonProgressDialog.dismiss();
        }
    }

    /**
     * 显示获取数据中加载框
     */
    public void showProgressDialog() {
        showProgressDialog(getResources().getString(R.string.get_data_ing));
    }

    /**
     * 显示获取数据中加载框
     */
    public void showProgressDialog(int resId) {
        try {
            showProgressDialog(getResources().getString(resId));
        } catch (Exception e) {
            showProgressDialog(getResources().getString(R.string.get_data_ing));
            e.printStackTrace();
        }
    }

    /**
     * 显示加载框
     */
    public void showProgressDialog(String msg) {
        if (mProgressDialog == null) {
            mProgressDialog = new AlertProgressDialog.Builder(mActivity)
                    .setView(R.layout.layout_progressbar_dialog)
                    .setCancelable(true)
                    .setMessage(msg)
                    .show();
            // 将对话框的大小按屏幕大小的百分比设置
            WindowManager windowManager = getWindowManager();
            DisplayMetrics outMetrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(outMetrics);
            WindowManager.LayoutParams lp = mProgressDialog.getWindow().getAttributes();
            lp.width = (int) (outMetrics.widthPixels * 0.55f);
            mProgressDialog.getWindow().setAttributes(lp);
        } else {
            mProgressDialog.show();
        }
    }

    /**
     * 隐藏加载框
     */
    public void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (choosePictureController != null) {
            choosePictureController.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * 选择完照片之后或者查看大图完之后更新数据到UI界面,请在包含gridLayout控件的界面重写此方法
     *
     * @param list
     */
    public void updateChoosePictures(List<ImageBean> list) {

    }

    /**
     * 如果原始有数据,比如编辑的时候
     *
     * @return
     */
    public List<ImageBean> getOriginDataList() {
        return null;
    }

    /**
     * 点击item
     *
     * @param position
     */
    public void handleClickItem(int position) {

    }

    long firstTime;

    public boolean dupCommit(long waitTime) {
        long secondTime = System.currentTimeMillis();
        if (secondTime - firstTime > waitTime) {
            firstTime = secondTime;
            return false;
        }
        showToast("手速太快了");
        return true;
    }

    /**
     * 是否能够添加图片
     *
     * @return
     */
    public boolean canAddPictures() {
        return true;
    }

    public Map<String, String> getSignData() {
        Map map = new HashMap();
        map.put("token", mApplication.getToken());
        return map;
    }

    public void showToast(String message) {
        ToastUtils.t(mApplication, message);
    }

    public MApplication getmApplication() {
        return mApplication;
    }

    public void setmApplication(MApplication mApplication) {
        this.mApplication = mApplication;
    }

    public void startActivity(Class clazz, String key, String value) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra(key, value);
        startActivity(intent);
    }

    public void startActivity(Class clazz, Intent intent) {
        intent.setClass(this, clazz);
        startActivity(intent);
    }


}
