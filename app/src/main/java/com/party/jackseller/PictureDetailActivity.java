package com.party.jackseller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackseller.adapter.PictureDetailAdapter;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.event.OnPhotoTapEvent;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static android.animation.ObjectAnimator.ofFloat;

/**
 * 查看图片详情
 */
public class PictureDetailActivity extends BaseActivityTitle {
    @BindView(R.id.view_pager)
    ViewPager view_pager;
    @BindView(R.id.fl_top)
    FrameLayout fl_top;
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.tv_delete)
    TextView tv_delete;
    int currentItem;
    ArrayList<ImageBean> dataList;
    RequestOptions ro1 = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_img_load_fail);
    RequestOptions ro2 = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_img_load_fail);
    PictureDetailAdapter detailAdapter;
    boolean deleteFlag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_detail);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        currentItem = intent.getIntExtra("currentItem", 0);
        deleteFlag = intent.getBooleanExtra("deleteFlag", true);
        dataList = (ArrayList<ImageBean>) intent.getSerializableExtra("dataList");
    }

    protected void initData() {
        setNeedOnCreateRegister();
        detailAdapter = new PictureDetailAdapter(mActivity, dataList);
        view_pager.setAdapter(detailAdapter);
        view_pager.setCurrentItem(currentItem);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentItem = position;
                tv_count.setText((currentItem + 1) + "/" + dataList.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tv_count.setText((currentItem + 1) + "/" + dataList.size());
        tv_delete.setVisibility(deleteFlag ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.tv_count)
    public void clickBack() {
        Intent intent = new Intent();
        intent.putExtra("dataList", dataList);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        clickBack();
    }

    @OnClick(R.id.tv_delete)
    public void clickDelete() {
        DialogController.showConfirmDialog(mActivity, "删除图片", (View v) -> {
            deleteItem();
        });
    }

    /**
     * 删除图片
     */
    private void deleteItem() {
        int size = dataList.size();
        dataList.remove(currentItem);
        if (size - 1 == 0) {
            clickBack();
            return;
        }
        if (currentItem == size - 1) {
            currentItem--;
        }
        tv_count.setText((currentItem + 1) + "/" + (size - 1));
        view_pager.removeAllViews();
        detailAdapter.notifyDataSetChanged();
        view_pager.setCurrentItem(currentItem);
    }

    @Subscribe
    public void onPhotoTapEvent(OnPhotoTapEvent event) {
        showOrHideBar();
    }

    private void showOrHideBar() {
        if (fl_top.getVisibility() == View.VISIBLE) {
            hideBar();
        } else {
            showBar();
        }
    }

    /**
     * 隐藏头部和尾部栏
     */
    private void hideBar() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(fl_top, "translationY",
                0, -fl_top.getHeight()).setDuration(300);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (fl_top != null) {
                    fl_top.setVisibility(View.GONE);
                }
            }
        });
        animator.start();
    }

    /**
     * 隐藏头部和尾部栏
     */
    private void showBar() {
        ObjectAnimator animator = ofFloat(fl_top, "translationY",
                fl_top.getTranslationY(), 0).setDuration(300);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (fl_top != null) {
                    fl_top.setVisibility(View.VISIBLE);
                }
            }
        });
        animator.start();
        animator.start();
    }
}
