package com.party.jackseller.fragment;

import com.party.jackseller.BaseFragment;

public class ReplaceMoneyCouponListFragment extends BaseFragment {
    @Override
    protected int getLayoutResId() {
        return 0;
    }
/*    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private int page = 1;
    private int pageSize = ConstUtils.PAGE_SIZE;
    private static final String ROLE_ID = "coupon_state";
    private CouponService mCouponService;
    private int couponState;
    private List<ReplaceMoneyCoupon> dataList = new ArrayList<>();
    private CommonAdapter<ReplaceMoneyCoupon> commonAdapter;
    private String emptyStr = "暂无代金券";
    private long shopId;

    public static ReplaceMoneyCouponListFragment getInstance(int roleId) {
        ReplaceMoneyCouponListFragment fragment = new ReplaceMoneyCouponListFragment();
        Bundle args = new Bundle();
        args.putInt(ROLE_ID, roleId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void receiveBundleFromActivity(Bundle arg) {
        couponState = arg.getInt(ROLE_ID);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_replace_money_coupon_list;
    }


    @Override
    protected void initData() {
        helper = new LoadViewHelper(mRefreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                getNeedData();
            }
        });
        helper.showLoading();

        mCouponService = new CouponService((BaseActivity) mActivity);
        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<ReplaceMoneyCoupon>(mActivity, R.layout.fragment_coupon_item, dataList) {
            @Override
            protected void convert(ViewHolder holder, ReplaceMoneyCoupon item, int position) {
                ImageView imageView = holder.getView(R.id.coupon_img);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, item.getHome_img(), imageView);
                holder.setText(R.id.coupon_name, item.getOriginal_price() + "元代金券");
                holder.setText(R.id.shop_name, item.getShop_name());
                String[] split = item.getBuy_price().split("\\.");
                holder.setText(R.id.buy_price, "¥" + split[0]);
                if (split.length > 1) {
                    holder.setText(R.id.buy_price_1, "." + split[1]);
                }
                TextView tv = holder.getView(R.id.original_price);
                tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                tv.setText("门店价：" + item.getOriginal_price());
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
        shopId = mApplication.getCheckShopId();
    }

    @Override
    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getNeedData();
            }
        });
        getNeedData();
    }

    *//**
     * item点击事件
     *
     * @param position
     *//*
    private void handleItemClick(int position) {
//        Intent intent = new Intent(mActivity, ShopInfoActivity.class);
//        intent.putExtra("shop", dataList.get(position));
//        startActivity(intent);
    }

    *//**
     * 下拉获取数据
     *//*
    public void getNeedData() {
        page = 1;
        addDisposableIoMain(mCouponService.loadingCouponList(shopId, couponState, page, pageSize), new DefaultConsumer<List<ReplaceMoneyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<ReplaceMoneyCoupon>> baseBean) {
                dataList.clear();
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                }
                commonAdapter.notifyDataSetChanged();
                mRefreshLayout.finishRefresh();
                if (dataList.size() > 0) {
                    helper.showContent();
                } else {
                    helper.showEmpty(emptyStr, "");
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    *//**
     * 加载更多
     *//*
    private void loadMoreData() {
        page++;
        addDisposableIoMain(mCouponService.loadingCouponList(shopId, couponState, page, pageSize), new DefaultConsumer<List<ReplaceMoneyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                mRefreshLayout.finishLoadMore();
            }

            @Override
            public void operateSuccess(BaseResult<List<ReplaceMoneyCoupon>> baseBean) {
                List<ReplaceMoneyCoupon> data = baseBean.getData();
                boolean noMoreData = false;
                if (data != null && data.size() > 0) {
                    dataList.addAll(data);
                    if (data.size() <= ConstUtils.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    mRefreshLayout.finishLoadMoreWithNoMoreData();
                } else {
                    mRefreshLayout.finishLoadMore();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                mRefreshLayout.finishLoadMore();
            }
        });
    }*/
}
