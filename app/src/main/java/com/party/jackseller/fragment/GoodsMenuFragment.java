package com.party.jackseller.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.BaseFragment;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.GoodsService;
import com.party.jackseller.base.MyLessThanZeroException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.GoodsCategory;
import com.party.jackseller.uihome.AddGoodsActivity;
import com.party.jackseller.uihome.GoodsContentChooseActivity;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class GoodsMenuFragment extends BaseFragment {

    @BindView(R.id.whole_layout)
    LinearLayout whole_layout;
    @BindView(R.id.category_recycler_view)
    RecyclerView category_recycler_view;
    @BindView(R.id.goods_recycler_view)
    RecyclerView goods_recycler_view;


    protected GoodsService goodsService;
    /**
     * 左边列表上一个被选中的索引
     */
    protected int selectCategoryPosition = 0;

    /**
     * 左边的适配器
     */
    protected CommonAdapter<GoodsCategory> categoryAdapter;
    public CommonAdapter<Goods> goodsAdapter;
    private List<Goods> goodsList = new ArrayList<>();
    private List<GoodsCategory> needShowGoodsCategoryList = new ArrayList<>();
    protected GoodsContentChooseActivity mActivity;

    public static GoodsMenuFragment getInstance() {
        GoodsMenuFragment fragment = new GoodsMenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_goods_menu;
    }

    @Override
    protected void initData() {
        mActivity = (GoodsContentChooseActivity) getActivity();
        goodsService = new GoodsService(mActivity);
        initView();

    }

    public void initView() {

        initCategoryView();
        initGoodsView();
    }

    @Override
    public void onResume() {
        super.onResume();
        getGoodsCategoryList();
    }

    public void initGoodsView() {
        goodsAdapter = new CommonAdapter<Goods>(mActivity, R.layout.item_goods_menu_fragment, goodsList) {
            @Override
            protected void convert(ViewHolder holder, Goods goods, final int position) {
                ImageView imageView = holder.getView(R.id.goods_image);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, goods.getImg_url(), imageView);
                holder.setText(R.id.goods_name, goods.getTitle());
                holder.setText(R.id.goods_price, "¥" + goods.getPrice());
                int exist = mActivity.getCart().isExist(goods);
                if (exist >= 0) {
                    TextView reduce_btn = holder.getView(R.id.reduce_btn);
                    TextView count = holder.getView(R.id.count);
                    reduce_btn.setVisibility(View.VISIBLE);
                    count.setVisibility(View.VISIBLE);
                    Goods goodsl = mActivity.getCart().get(exist);
                    count.setText(goodsl.getCount() + "");
                } else {
                    View reduce_btn = holder.getView(R.id.reduce_btn);
                    View count = holder.getView(R.id.count);
                    reduce_btn.setVisibility(View.GONE);
                    count.setVisibility(View.GONE);
                }
                holder.setOnClickListener(R.id.add_btn, (View v) -> {
                    mActivity.getCart().add(goods);
                    mActivity.notifyCartDataSetChanged();
                });
                holder.setOnClickListener(R.id.reduce_btn, (View v) -> {
                    try {
                        int count = mActivity.getCart().reduce(goods);
                        holder.setText(R.id.count, String.valueOf(count));
                    } catch (MyLessThanZeroException e) {
                        mActivity.getCart().remove(goods);
                    }
                    mActivity.notifyCartDataSetChanged();
                });
            }
        };
        goods_recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        goods_recycler_view.setAdapter(goodsAdapter);
    }

    protected void initCategoryView() {
        category_recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        categoryAdapter = new CommonAdapter<GoodsCategory>(mActivity, R.layout.activity_goods_manager_category_item, needShowGoodsCategoryList) {
            @Override
            protected void convert(ViewHolder holder, GoodsCategory goodsCategory, final int position) {
                holder.setText(R.id.tv_name, goodsCategory.getName());
                boolean check = goodsCategory.isCheck();
                holder.setBackgroundRes(R.id.fl, check ? android.R.color.white : R.color.grayEF);
                holder.setTextColorRes(R.id.tv_name, check ? R.color.colorMainBtn : R.color.black);
                holder.setVisible(R.id.view_bg, check);
            }
        };
        category_recycler_view.setAdapter(categoryAdapter);
        categoryAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                setLeftItemPosition(position);
                getGoodsByCategoryId(needShowGoodsCategoryList.get(position).getId());
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        category_recycler_view.setAdapter(categoryAdapter);
    }

    public void getGoodsByCategoryId(long typeId) {
        mActivity.addDisposableIoMain(goodsService.getShopGoodsByTypeId(mApplication.getCurrShopId(), typeId), new DefaultConsumer<List<Goods>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Goods>> baseBean) {
                goodsList.clear();
                goodsList.addAll(baseBean.getData());
                goodsAdapter.notifyDataSetChanged();
            }
        });
    }

    public void setLeftItemPosition(int position) {
        if (position == selectCategoryPosition) return;
        needShowGoodsCategoryList.get(selectCategoryPosition).setCheck(false);
        needShowGoodsCategoryList.get(position).setCheck(true);
        categoryAdapter.notifyItemChanged(selectCategoryPosition);
        categoryAdapter.notifyItemChanged(position);
        selectCategoryPosition = position;
    }


    @OnClick({R.id.add_goods_btn, R.id.see_choose_goods_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.add_goods_btn:
                startActivity(new Intent(mActivity, AddGoodsActivity.class));
                break;
            case R.id.see_choose_goods_btn:
                ((GoodsContentChooseActivity) getActivity()).seeMyGoods();
                break;
        }
    }

    public void getGoodsCategoryList() {
        mActivity.addDisposableIoMain(goodsService.getGoodsCategory(mApplication.getCurrShopId()), new DefaultConsumer<List<GoodsCategory>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<GoodsCategory>> baseBean) {
                needShowGoodsCategoryList.clear();
                needShowGoodsCategoryList.addAll(baseBean.getData());
                categoryAdapter.notifyDataSetChanged();
                showGoodsListFromFirstCategory();
            }
        });
    }

    public void showGoodsListFromFirstCategory() {
        mActivity.addDisposableIoMain(goodsService.getShopGoodsByTypeId(mApplication.getCurrShopId(), needShowGoodsCategoryList.get(0).getId()), new DefaultConsumer<List<Goods>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Goods>> baseBean) {
                goodsList.clear();
                goodsList.addAll(baseBean.getData());
                goodsAdapter.notifyDataSetChanged();
                helper.showContent();
            }
        });
        selectCategoryPosition = 0;
        needShowGoodsCategoryList.get(selectCategoryPosition).setCheck(true);
        categoryAdapter.notifyItemChanged(selectCategoryPosition);
    }

    public void notifyDataSetChanged() {
        goodsAdapter.notifyDataSetChanged();
    }
}
