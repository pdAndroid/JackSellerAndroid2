package com.party.jackseller.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.party.jackseller.BaseFragment;
import com.party.jackseller.R;
import com.party.jackseller.base.MyLessThanZeroException;
import com.party.jackseller.bean.Cart;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.uihome.GoodsContentChooseActivity;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import butterknife.BindView;
import butterknife.OnClick;


public class GoodsCartFragment extends BaseFragment {
    @BindView(R.id.add_cart_layout)
    View add_cart_layout;
    @BindView(R.id.total_price)
    TextView total_price;
    private RecyclerView mRecyclerView;
    private GoodsContentChooseActivity mActivity;
    private CommonAdapter<Goods> commonAdapter;
    private Cart mCart;

    public static GoodsCartFragment getInstance() {
        GoodsCartFragment fragment = new GoodsCartFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_goods_cart;
    }

    @Override
    protected void initData() {
        mActivity = (GoodsContentChooseActivity) getActivity();
        mCart = mActivity.getCart();
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        commonAdapter = new CommonAdapter<Goods>(mActivity, R.layout.fragment_goods_cart_item, mActivity.getGoodsList()) {
            @Override
            protected void convert(final ViewHolder holder, final Goods goods, final int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, goods.getImg_url(), holder.getView(R.id.image_icon));
                holder.setText(R.id.goods_name, goods.getTitle());
                holder.setText(R.id.goods_price, "¥" + goods.getPrice());
                holder.setText(R.id.count, String.valueOf(goods.getCount()));
                holder.setOnClickListener(R.id.add_btn, (View v) -> {
                    int count = mCart.add(goods);
                    holder.setText(R.id.count, String.valueOf(count));
                    mActivity.notifyCartDataSetChanged();
                });
                holder.setOnClickListener(R.id.reduce_btn, (View v) -> {
                    try {
                        int count = mCart.reduce(goods);
                        holder.setText(R.id.count, String.valueOf(count));
                        mActivity.notifyCartDataSetChanged();
                    } catch (MyLessThanZeroException e) {
                        DialogController.showConfirmDialog(mActivity, "温馨提示", "删除" + goods.getTitle()
                                , (View vv) -> {
                                    mActivity.getCart().remove(goods);
                                    mActivity.notifyCartDataSetChanged();
                                });
                    }
                });
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
    }

    /**
     * 处理点击事件
     *
     * @param view
     */
    @OnClick({R.id.ok_btn, R.id.add_goods_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.ok_btn:
                mActivity.finish();
                break;
            case R.id.add_goods_btn:
                mActivity.mViewPager.setCurrentItem(1);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        notifyDataSetChanged();
    }

    public void notifyDataSetChanged() {
        calculateTotalPrice();
        if (mActivity.getGoodsList().size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            add_cart_layout.setVisibility(View.GONE);
            commonAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            add_cart_layout.setVisibility(View.VISIBLE);
        }
    }

    public void calculateTotalPrice() {
        String totalPrice1 = mCart.getTotalPriceStr();
        total_price.setText("总价：¥" + totalPrice1);
    }


}
