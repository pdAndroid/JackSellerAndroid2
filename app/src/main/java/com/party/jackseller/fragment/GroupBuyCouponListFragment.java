package com.party.jackseller.fragment;

import com.party.jackseller.BaseFragment;

public class GroupBuyCouponListFragment extends BaseFragment {
    @Override
    protected int getLayoutResId() {
        return 0;
    }
/*    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private int state;
    private int page = 1;
    private int pageSize = ConstUtils.PAGE_SIZE;
    private CouponService couponService;
    private List<GroupBuyCoupon> dataList = new ArrayList<>();
    private CommonAdapter<GroupBuyCoupon> commonAdapter;
    private String emptyStr = "暂无代金券";
    private long shopId;

    public static BaseFragment getInstance(int state) {
        GroupBuyCouponListFragment fragment = new GroupBuyCouponListFragment();
        Bundle args = new Bundle();
        args.putInt("state", state);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void receiveBundleFromActivity(Bundle arg) {
        state = arg.getInt("state");
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_group_buy_coupon_list;
    }

    @Override
    protected void initData() {
        helper = new LoadViewHelper(mRefreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                getNeedData();
            }
        });
        helper.showLoading();
        couponService = new CouponService((BaseActivity) mActivity);
        commonAdapter = new CommonAdapter<GroupBuyCoupon>(mActivity, R.layout.fragment_group_buy_item, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, GroupBuyCoupon item, int position) {
                ImageView imageView = viewHolder.getView(R.id.group_buy_img);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, item.getGroup_buy_img(), imageView);
                viewHolder.setText(R.id.group_buy_name, item.getTitle());
                viewHolder.setText(R.id.shop_name, item.getShop_name());
                String[] prise = prise(item.getBuy_price());
                viewHolder.setText(R.id.buy_price, "¥" + prise[0]);
                viewHolder.setText(R.id.buy_price_1, "." + prise[1]);
                TextView view = viewHolder.getView(R.id.original_price);
                view.setText("原价:¥" + item.getOriginal_price());
                view.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
        shopId = mApplication.getCheckShopId();
    }

    @Override
    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getNeedData();
            }
        });
        getNeedData();
    }

    *//**
     * item点击事件
     *
     * @param position
     *//*
    private void handleItemClick(int position) {
        Intent intent = new Intent(mActivity, ShopInfoActivity.class);
        intent.putExtra("shop", dataList.get(position));
        startActivity(intent);
    }

    *//**
     * 下拉获取数据
     *//*
    public void getNeedData() {
        page = 1;
        addDisposableIoMain(couponService.getGroupBuyList(state, shopId, page, pageSize), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
                if(baseBean.getData().size()<=0){
                    helper.showEmpty();
                }else {
                    page++;
                    dataList.clear();
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    mRefreshLayout.finishRefresh();
                    helper.showContent();
                }

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    *//**
     * 加载更多
     *//*
    private void loadMoreData() {
        page++;
        addDisposableIoMain(couponService.getGroupBuyList(state, shopId, page, pageSize), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                mRefreshLayout.finishLoadMore();
            }

            @Override
            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
                List<GroupBuyCoupon> data = baseBean.getData();
                boolean noMoreData = false;
                if (data != null && data.size() > 0) {
                    dataList.addAll(data);
                    if (data.size() <= ConstUtils.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    mRefreshLayout.finishLoadMoreWithNoMoreData();
                } else {
                    mRefreshLayout.finishLoadMore();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                mRefreshLayout.finishLoadMore();
            }
        });
    }

    private String[] prise(String price) {
        String[] temp = new String[2];
        if (price.contains(".")) {
            String[] split = price.split("\\.");
            temp[0] = split[0];
            temp[1] = split[1].length() == 1 ? split[1] + "0" : split[1];
        } else {
            temp[0] = price;
            temp[1] = "00";
        }
        return temp;
    }*/
}
