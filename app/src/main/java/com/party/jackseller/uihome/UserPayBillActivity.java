package com.party.jackseller.uihome;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PhotoService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.QRCodeUtil;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 买单收款
 */
public class UserPayBillActivity extends BaseActivity {
    @BindView(R.id.iv_user_pay_bill_more)
    ImageView ivUserPayBillMore;
    @BindView(R.id.iv_return)
    ImageView ivReturn;
    @BindView(R.id.tv_switch_qrcode)
    TextView tvSwitchQrcode;
    @BindView(R.id.tv_save_picture)
    TextView tvSavePicture;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;

    private PhotoService photoService;
    private String shopId;
    private String qrCode;
    private Bitmap mBitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_pay_bill);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        photoService = new PhotoService(this);
        shopId = mApplication.getCurrShopId() + "";
    }

    private void requestData() {
        addDisposableIoMain(photoService.getQrCode(shopId), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> data) {
                qrCode = data.getData();
                mBitmap = CodeUtils.createImage(qrCode, 400, 400, null);
                ivQrCode.setImageBitmap(mBitmap);
            }
        });
    }

    public static void saveImageToGallery(Context context, Bitmap bmp) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(), "Boohee");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    file.getAbsolutePath(), fileName, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(file.getAbsolutePath())));
    }

    @OnClick({R.id.iv_return, R.id.iv_user_pay_bill_more, R.id.tv_save_picture})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_return:
                finish();
                break;
            case R.id.iv_user_pay_bill_more:
                DialogController.showMenuList(new String[]{"保存图片", "切换二维码", "优惠买单设置"}, mActivity, (int which) -> {
                    switch (which) {
                        case 0:
                            Toast.makeText(mApplication, "暂未开放", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            Toast.makeText(mApplication, "暂未开放", Toast.LENGTH_SHORT).show();
                            break;
                        case 2:
                            Intent intent = new Intent(this, PreferentialPaymentSettingActivity.class);
                            startActivity(intent);
                            break;
                    }
                });
            case R.id.tv_save_picture:
                saveImageToGallery(mActivity, mBitmap);
                Toast.makeText(mApplication, "已保存至相册", Toast.LENGTH_SHORT).show();
                break;
        }
    }


}