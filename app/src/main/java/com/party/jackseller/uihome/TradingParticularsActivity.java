package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.adapter.MyCommonAdapter;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.TradeRecordService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.KeyValue;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2018/12/29.
 * 交易流水-交易详情
 */

public class TradingParticularsActivity extends BaseActivity {
    @BindView(R.id.key_0)
    TextView key_0;
    @BindView(R.id.value_0)
    TextView value_0;

    @BindView(R.id.main_rv_video_list)
    RecyclerView mRecyclerView;

    List<KeyValue> mDates;
    CommonAdapter<KeyValue> mAdapter;

    private TradeRecordService tradeRecordService;
    private String tradeRecordId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trading_particulars);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        Intent intent = getIntent();
        tradeRecordId = intent.getStringExtra("tradeRecordId");
        tradeRecordService = new TradeRecordService(this);
        mDates = new ArrayList<>();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));

        mAdapter = new CommonAdapter<KeyValue>(mActivity, R.layout.record_list_item, mDates) {
            @Override
            protected void convert(ViewHolder holder, KeyValue keyValue, int position) {
                holder.setText(R.id.key, keyValue.getName());
                holder.setText(R.id.value, keyValue.getValue());
            }
        };

        mRecyclerView.setAdapter(mAdapter);
    }

    private void requestData() {
        showAlertDialog();
        addDisposableIoMain(tradeRecordService.getTradeRecordInfo(tradeRecordId), new DefaultConsumer<List<KeyValue>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<KeyValue>> data) {
                hideAlertDialog();
                mDates.addAll(data.getData());
                if (mDates.size() > 0) {
                    KeyValue remove = mDates.remove(0);
                    key_0.setText(remove.getName());
                    value_0.setText(remove.getValue());
                }
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @OnClick({R.id.iv_return})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_return:
                finish();
                break;
        }
    }
}
