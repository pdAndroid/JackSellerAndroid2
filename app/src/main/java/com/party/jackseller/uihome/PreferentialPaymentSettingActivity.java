package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.bean.AddShopReabteDiscountBean;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.DelShopRebateDiscountBean;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.UpdateRebateDiscountBean;
import com.party.jackseller.bean.UpdateShopDiscountBean;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.widget.dialog.PreferentialPaymentSettingDialog;
import com.party.jackseller.widget.dialog.PreferentialPaymentSettingDoubleDialog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2018/12/15.
 * 买单收款-优惠买单设置
 */

public class PreferentialPaymentSettingActivity extends BaseActivity {
    @BindView(R.id.iv_preferential_payment_setting_return)
    ImageView ivPreferentialPaymentSettingReturn;
    @BindView(R.id.iv_preferential_payment_setting_first_order)
    CheckBox ivPreferentialPaymentSettingFirstOrder;
    @BindView(R.id.iv_preferential_payment_setting_special_time)
    CheckBox ivPreferentialPaymentSettingSpecialTime;
    @BindView(R.id.tv_preferential_payment_setting_special_time)
    TextView tvPreferentialPaymentSettingSpecialTime;
    @BindView(R.id.btn_preferential_payment_setting_save)
    Button btnPreferentialPaymentSettingSave;
    @BindView(R.id.btn_preferential_payment_setting_first_order)
    Button btnPreferentialPaymentSettingFirstOrder;
    @BindView(R.id.btn_preferential_payment_setting_rebate)
    Button btnPreferentialPaymentSettingRebate;

    private ShopService mShopService;
    private PreferentialPaymentSettingDialog preferentialPaymentSettingDialog;
    private PreferentialPaymentSettingDoubleDialog preferentialPaymentSettingDoubleDialog;
    private String rebate_discount;
    private String discount;
    private String id;
    private String shopId;
    private String rebateDiscount;
    private boolean isChecked = false;
    private boolean isChecked2 = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferential_payment_setting);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        mShopService = new ShopService(this);
        Shop shop = mApplication.getCurrShop();
        shopId = mApplication.getCurrShopId() + "";
        //消费返利
        rebate_discount = shop.getRebate_discount();
        rebateDiscount = rebate_discount;
        //首单优惠
        String disStr = shop.getDiscount();
        if (disStr != null) {
            discount = new BigDecimal(disStr).multiply(new BigDecimal("10")).intValue() + "";
        } else {
            discount = "无折扣";
        }
    }

    private void requestData() {
        if (rebate_discount.equals("") && discount.equals("") && rebate_discount.equals("1") && discount.equals("1")) {

        } else {
            isChecked = true;
            isChecked2 = true;
            Double d = Double.parseDouble(rebate_discount);
            d = d * 100;
            btnPreferentialPaymentSettingFirstOrder.setText(discount);
            btnPreferentialPaymentSettingRebate.setText(d + "");
            ivPreferentialPaymentSettingFirstOrder.setChecked(isChecked);
            ivPreferentialPaymentSettingSpecialTime.setChecked(isChecked2);
        }
        ivPreferentialPaymentSettingFirstOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isChecked) {
                    preferentialPaymentSettingDialog = new PreferentialPaymentSettingDialog(mActivity);
                    preferentialPaymentSettingDialog.setTextContent("");
                    preferentialPaymentSettingDialog.show();
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.et_preferential_payment_setting).setVisibility(View.GONE);
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.tv_preferential_payment_setting_dialog).setVisibility(View.VISIBLE);
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.tv_preferential_payment_setting_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            preferentialPaymentSettingDialog.setCommitInterface((String content) -> {
                            addDisposableIoMain(mShopService.updateShopDiscount(shopId, discount), new DefaultConsumer<List<UpdateShopDiscountBean>>(mApplication) {
                                @Override
                                public void operateSuccess(BaseResult<List<UpdateShopDiscountBean>> data) {
                                    List<UpdateShopDiscountBean> bean = data.getData();
//                                        btnPreferentialPaymentSettingFirstOrder.setText(content + "");
                                    preferentialPaymentSettingDialog.dismiss();
                                }
                            });
//                            });
                        }
                    });
                }
            }
        });

//        ivPreferentialPaymentSettingFirstOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                preferentialPaymentSettingDialog.setCommitInterface((String content) -> {
//                    addDisposableIoMain(mShopService.updateShopDiscount(shopId, discount), new DefaultConsumer<List<UpdateShopDiscountBean>>(mApplication) {
//                        @Override
//                        public void operateSuccess(BaseResult<List<UpdateShopDiscountBean>> data) {
//                            List<UpdateShopDiscountBean> bean = data.getData();
//                            btnPreferentialPaymentSettingFirstOrder.setText(content + "");
//                        }
//                    });
//                });
//            }
//        });

        ivPreferentialPaymentSettingSpecialTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isChecked) {
                    preferentialPaymentSettingDialog = new PreferentialPaymentSettingDialog(mActivity);
                    preferentialPaymentSettingDialog.setTextContent("");
                    preferentialPaymentSettingDialog.show();
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.et_preferential_payment_setting).setVisibility(View.GONE);
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.tv_preferential_payment_setting_dialog).setVisibility(View.VISIBLE);
                    preferentialPaymentSettingDialog.getWindow().findViewById(R.id.tv_preferential_payment_setting_confirm).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addDisposableIoMain(mShopService.updateRebateDiscount(rebateDiscount, shopId), new DefaultConsumer<List<UpdateRebateDiscountBean>>(mApplication) {
                                @Override
                                public void operateSuccess(BaseResult<List<UpdateRebateDiscountBean>> data) {
                                    List<UpdateRebateDiscountBean> bean = data.getData();
                                    preferentialPaymentSettingDialog.dismiss();
                                }
                            });
                        }
                    });
                }
            }
        });
        ivPreferentialPaymentSettingSpecialTime.setChecked(isChecked2);
    }


    @OnClick({R.id.btn_preferential_payment_setting_first_order, R.id.btn_preferential_payment_setting_rebate, R.id.iv_preferential_payment_setting_return, R.id.tv_preferential_payment_setting_special_time, R.id.btn_preferential_payment_setting_save})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.iv_preferential_payment_setting_return:
                finish();
                break;
            //首单优惠
            case R.id.btn_preferential_payment_setting_first_order:
                preferentialPaymentSettingDialog = new PreferentialPaymentSettingDialog(mActivity);
                preferentialPaymentSettingDialog.setTextContent("");
                preferentialPaymentSettingDialog.setCommitInterface((String content) -> {
                    BigDecimal divide = new BigDecimal(content).divide(new BigDecimal("10")).setScale(2, RoundingMode.HALF_DOWN);
                    showAlertDialog();
                    addDisposableIoMain(mShopService.updateShopDiscount(shopId, divide.toString()), new DefaultConsumer<List<UpdateShopDiscountBean>>(mApplication) {
                        @Override
                        public void operateSuccess(BaseResult<List<UpdateShopDiscountBean>> data) {
                            hideAlertDialog();
                            btnPreferentialPaymentSettingFirstOrder.setText(content + "");
                            mApplication.getCurrShop().setDiscount(divide.toString());
                            showToast(data.getMessage());
                            mApplication.saveShop();
                        }
                    });
                });
                preferentialPaymentSettingDialog.show();
                break;
            //消费返利
            case R.id.btn_preferential_payment_setting_rebate:
                preferentialPaymentSettingDoubleDialog = new PreferentialPaymentSettingDoubleDialog(mActivity);
                preferentialPaymentSettingDoubleDialog.setTextContent("");
                showAlertDialog();
                preferentialPaymentSettingDoubleDialog.setCommitInterface((String content) -> {
                    BigDecimal divide = new BigDecimal(content).divide(new BigDecimal("100")).setScale(2, RoundingMode.HALF_DOWN);
                    addDisposableIoMain(mShopService.updateRebateDiscount(divide.toString(), shopId), new DefaultConsumer<List<UpdateRebateDiscountBean>>(mApplication) {
                        @Override
                        public void operateSuccess(BaseResult<List<UpdateRebateDiscountBean>> data) {
                            hideAlertDialog();
                            mApplication.getCurrShop().setRebate_discount(divide.toString());
                            mApplication.saveShop();
                            showToast(data.getMessage());
                            btnPreferentialPaymentSettingRebate.setText(content);
                        }
                    });
                });
                preferentialPaymentSettingDoubleDialog.show();
                break;
            case R.id.tv_preferential_payment_setting_special_time:
                startActivity(SpecialTimeActivity.class);
                break;
            case R.id.btn_preferential_payment_setting_save:
                break;
        }
    }
}
