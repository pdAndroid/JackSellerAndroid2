package com.party.jackseller.uihome.jpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.party.jackseller.BaseActivity;
import com.party.jackseller.adapter.MyCommonAdapter;
import com.party.jackseller.bean.TradeRecord;
import com.party.jackseller.utils.SPUtils;
import com.party.jackseller.utils.VoiceUtils.VoiceUtils;

import java.util.HashMap;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;

/**
 * 自定义接收器
 * <p>
 * 如果不定义这个 Receiver，则：
 * 1) 默认用户会打开主界面
 * 2) 接收不到自定义消息
 */
public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";

    public static String key = "record_toggle";
    public static Map<MyCommonAdapter, MyCommonAdapter> adapterMap = new HashMap();

    public static void addAdapter(MyCommonAdapter<TradeRecord> adapter) {
        adapterMap.put(adapter, null);
    }

    static OnListenerTradeRecord onListenerRecord;

    public interface OnListenerTradeRecord {
        void tradeRecord(TradeRecord record);
    }

    public static void addDataFinish(OnListenerTradeRecord onListenerTradeRecord) {
        onListenerRecord = onListenerTradeRecord;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
//		Log.d(TAG, "[MyReceiver] onReceive - " + intent.getAction() + ", extras: " + printBundle(bundle));

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {

//            boolean record_toggle = (boolean) SPUtils.get(context, key, false);
//            if (record_toggle) {
//                String string = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//                String at = bundle.getString(JPushInterface.EXTRA_EXTRA);
//                VoiceUtils.with(context).receiptPlay(string);
//            }
//            processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            String string = bundle.getString(JPushInterface.EXTRA_EXTRA);
            JSONObject parse = JSON.parseObject(string);

            boolean record_toggle = (boolean) SPUtils.get(context, key, false);
            if (record_toggle) {
                VoiceUtils.with(context).receiptPlay(parse.getString("totalPrice"));
            }
            try {
                TradeRecord record = new TradeRecord();
                record.setCreate(parse.getLong("time"));
                record.setTotalPrice(parse.getString("totalPrice"));
                record.setId(parse.getLong("recordId"));
                record.setTitle("");
                record.setPayer(parse.getString("payer"));
                record.setValue(parse.getDouble("finalPrice"));

                for (Map.Entry<MyCommonAdapter, MyCommonAdapter> adapter : adapterMap.entrySet()) {
                    if (adapter.getKey() != null) {
                        adapter.getKey().addData(0, record);
                        adapter.getKey().notifyDataSetChanged();
                    }
                }

                if (onListenerRecord != null) onListenerRecord.tradeRecord(record);
            } catch (Exception e) {

            }
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            Log.e(TAG, "[MyReceiver] 用户点击打开了通知");

            //打开自定义的Activity
//            Intent intent1 = new Intent(context, MainActivity.class);
//            i.putExtras(bundle);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(intent1);

        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {
            Log.e(TAG, "[MyReceiver] 用户收到到RICH PUSH CALLBACK: " + bundle.getString(JPushInterface.EXTRA_EXTRA));
            //在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity， 打开一个网页等..

        } else if (JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
            boolean connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
            Log.e(TAG, "[MyReceiver]" + intent.getAction() + " connected state change to " + connected);
        } else {
            Log.e(TAG, "[MyReceiver] Unhandled intent - " + intent.getAction());
        }
    }


    /**
     * 实现自定义推送声音
     *
     * @param context
     * @param bundle
     */

    private void processCustomMessage(Context context, Bundle bundle) {

//
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//        NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
//        notification.setAutoCancel(true)
//                .setContentText("自定义推送声音")
//                .setContentTitle("极光测试")
//                .setSmallIcon(R.mipmap.ic_beautiful_normal);
//        String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
//        if (!TextUtils.isEmpty(extras)) {
//            try {
//                JSONObject extraJson = new JSONObject(extras);
//                if (null != extraJson && extraJson.length() > 0) {
//                    String sound = extraJson.getString("sound");
//                    if ("test.mp3".equals(sound)) {
//                        notification.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.sound0));
//                    }
//                }
//            } catch (JSONException e) {
//            }
//        }
//
//        Intent mIntent = new Intent(context, JiguangPushActivity.class);
//        mIntent.putExtras(bundle);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, mIntent, 0);
//        notification.setContentIntent(pendingIntent);
//        notificationManager.notify(2, notification.build());

    }


}
