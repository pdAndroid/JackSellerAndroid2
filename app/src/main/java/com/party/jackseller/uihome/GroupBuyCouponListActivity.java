package com.party.jackseller.uihome;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.Constant;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.GroupBuyCoupon;
import com.party.jackseller.view.loadviewhelper.help.OnLoadViewListener;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class GroupBuyCouponListActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.add_coupon_btn)
    Button add_coupon_btn;

    private List<GroupBuyCoupon> dataList = new ArrayList<>();
    private CouponService couponService;
    private CommonAdapter<GroupBuyCoupon> commonAdapter;
    private int currPage = 1;
    private boolean noMoreData = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_buy_list);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("套餐卷");
        couponService = new CouponService(this);
        helper = new LoadViewHelper(refresh_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        initView();
        helper.showLoading();
        loadData();
    }

    public void initView() {
        refresh_layout.setEnableLoadMore(false);
        commonAdapter = new CommonAdapter<GroupBuyCoupon>(this, R.layout.item_group_buy_coupon, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final GroupBuyCoupon item, int position) {
                ImageView imageView = viewHolder.getView(R.id.group_buy_img);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, item.getGroup_buy_img(), imageView);
                viewHolder.setText(R.id.coupon_name, item.getTitle());
                viewHolder.setText(R.id.shop_name, item.getShop_name());
                viewHolder.setText(R.id.buy_price_tv, "¥" + item.getBuy_price());
                TextView view = viewHolder.getView(R.id.original_price_tv);
                view.setText("¥" + item.getOriginal_price());
                view.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            }
        };
        /*//添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this,R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);*/
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
    }

    public void loadData() {
        currPage = 1;
        addDisposableIoMain(couponService.getGroupBuyList(mApplication.getCurrShopId(), currPage), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {

            @Override
            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
                dataList.clear();
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty(getEmptyView());
                    if (add_coupon_btn.getVisibility() == View.VISIBLE) {
                        add_coupon_btn.setVisibility(View.GONE);
                    }
                } else {
                    if (baseBean.getData().size() < Constant.PAGE_SIZE) {
                        refresh_layout.setEnableLoadMore(false);
                    }
                    if (add_coupon_btn.getVisibility() == View.GONE) {
                        add_coupon_btn.setVisibility(View.VISIBLE);
                    }
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    refresh_layout.finishRefresh();
                    helper.showContent();
                }
            }
        });
    }

    /**
     * 上拉加载更多数据
     */
    public void loadMoreData() {
        currPage++;
        addDisposableIoMain(couponService.getGroupBuyList(mApplication.getCurrShopId(), currPage), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                    if (baseBean.getData().size() <= Constant.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    refresh_layout.finishLoadMoreWithNoMoreData();
                } else {
                    refresh_layout.finishLoadMore();
                }
            }
        });
    }

    private String[] prise(String price) {
        String[] temp = new String[2];
        if (price.contains(".")) {
            String[] split = price.split("\\.");
            temp[0] = split[0];
            temp[1] = split[1].length() == 1 ? split[1] + "0" : split[1];
        } else {
            temp[0] = price;
            temp[1] = "00";
        }
        return temp;
    }

    @OnClick(R.id.add_coupon_btn)
    public void handleClickSth(View view) {
        Intent intent = new Intent(this, AddGroupBuyActivity.class);
        startActivityForResult(intent, Constant.REQUEST_CODE_ADD_GROUP_BUY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Constant.REQUEST_CODE_ADD_GROUP_BUY == requestCode) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("isAdd", false) == true) {
                    helper.showLoading();
                    loadData();
                }
            }
        }
    }

    public View getEmptyView() {
        View emptyView = getLayoutInflater().inflate(R.layout.load_coupon_empty, null);
        ImageView empty_iv = emptyView.findViewById(R.id.empty_iv);
        TextView empty_tv = emptyView.findViewById(R.id.empty_tv);
        Button add_coupon_btn = emptyView.findViewById(R.id.add_coupon_btn);
        add_coupon_btn.setText("添加套餐卷");
        add_coupon_btn.setOnClickListener((View v) -> {
            startActivityForResult(new Intent(mActivity, AddGroupBuyActivity.class), Constant.REQUEST_CODE_ADD_GROUP_BUY);
        });
        Glide.with(mActivity).load(R.drawable.no_replace_money_coupon).into(empty_iv);
        empty_tv.setText("没有发布的套餐券");
        return emptyView;
    }
}
