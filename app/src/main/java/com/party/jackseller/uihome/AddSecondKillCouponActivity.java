package com.party.jackseller.uihome;

import android.app.DatePickerDialog;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.base.CouponBaseActivity;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DateUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.TimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class AddSecondKillCouponActivity extends CouponBaseActivity {

    @BindView(R.id.original_price_et)
    EditText original_price_et;
    @BindView(R.id.buy_price_et)
    EditText buy_price_et;
    @BindView(R.id.sell_limit_num_et)
    EditText sell_limit_num_et;
    @BindView(R.id.buy_limit_num_et)
    EditText buy_limit_num_et;
    @BindView(R.id.stock_balance_et)
    EditText stock_balance_et;
    @BindView(R.id.end_sell_date_tv)
    TextView end_sell_date_tv;
    @BindView(R.id.end_sell_time_tv)
    TextView end_sell_time_tv;
    @BindView(R.id.start_valid_date_tv)
    TextView start_valid_date_tv;
    @BindView(R.id.end_valid_date_tv)
    TextView end_valid_date_tv;
    @BindView(R.id.start_valid_time_tv)
    TextView start_valid_time_tv;
    @BindView(R.id.end_valid_time_tv)
    TextView end_valid_time_tv;
    @BindView(R.id.use_limit_num_et)
    EditText use_limit_num_et;
    @BindView(R.id.use_range_et)
    TextView use_range_et;
    @BindView(R.id.appointment_tv)
    TextView appointment_tv;
    @BindView(R.id.shop_name)
    TextView shop_name;

    private int startTimeSecond, endTimeSecond;//开始时间秒数和结束时间秒数
    private static final String TAG = "AddSecondKillCouponActi";
    private CouponService couponService;
    private SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
    private Date endSaleDate, startValidDate, endValidDate;
    private Date endSaleTime, startValidTime;
    private final int endSaleDateFlag = 1, endSaleTimeFlag = 2, startValidDateFlag = 3,
            startValidTimeFlag = 4, endValidDateFlag = 5, endValidTimeFlag = 6;
    private boolean isEqual = false;//结束售卖时间是否等于开始生效时间

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_second_kill_coupon);
        initData();
    }

    protected void initData() {
        handleTitle();
        shop_name.setText(mApplication.getCurrShop().getShop_name());
        setMiddleText("添加秒杀券");
        couponService = new CouponService(this);
    }

    /**
     * 提交数据
     */
    public void submitData() {
        showAlertDialog();

        Map<String, String> tokenMap = getSignData();
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("originalPrice", original_price_et.getText().toString());
        tokenMap.put("buyPrice", buy_price_et.getText().toString());
        tokenMap.put("appointment", CommonUtil.getAppointmentId(appointment_tv.getText().toString()));
        tokenMap.put("dateStart", TimeUtil.date2TimeStamp(start_valid_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("dateEnd", TimeUtil.date2TimeStamp(end_valid_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("timeStart", startTimeSecond + "");
        tokenMap.put("timeEnd", endTimeSecond + "");
        tokenMap.put("buyPersonLimit", buy_limit_num_et.getText().toString());
        tokenMap.put("stockCount", stock_balance_et.getText().toString());
        tokenMap.put("onceCount", use_limit_num_et.getText().toString());
        tokenMap.put("endSaleTime", TimeUtil.date2TimeStamp(formatEndSellTime(end_sell_date_tv.getText().toString(), end_sell_time_tv.getText().toString()), "yyyy-MM-dd HH:mm:ss"));
        tokenMap.put("startSaleTime", TimeUtil.date2TimeStamp(TimeUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"));
        tokenMap.put("goodsRange", use_range_et.getText().toString());
        tokenMap.put("ruleContent", "");
        Log.d(TAG, tokenMap.toString());
        addDisposableIoMain(couponService.addSecondKillCoupon(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity, "成功添加秒杀券！", (View v) -> {
                    finish();
                });
            }
        });
    }


    /**
     * 处理加减点击事件
     *
     * @param view
     */
    @OnClick({R.id.end_sell_date_tv, R.id.end_sell_time_tv, R.id.start_valid_date_tv, R.id.end_valid_date_tv,
            R.id.start_valid_time_tv, R.id.end_valid_time_tv, R.id.appointment_tv, R.id.submit_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
            case R.id.end_sell_date_tv:
                onPickDateEvent((TextView) view, endSaleDateFlag);
                break;
            case R.id.end_sell_time_tv:
                if (endSaleDate == null) {
                    showToast("请先设置结束售卖日期");
                    return;
                }
                timePicker(view, endSaleTimeFlag);
                break;
            case R.id.start_valid_date_tv:
                if (endSaleTime == null) {
                    showToast("请先设置结束售卖时间");
                    return;
                }
                onPickDateEvent((TextView) view, startValidDateFlag);
                break;
            case R.id.end_valid_date_tv:
                if (startValidDate == null) {
                    showToast("请先设置开始生效时间");
                    return;
                }
                onPickDateEvent((TextView) view, endValidDateFlag);
                break;
            case R.id.start_valid_time_tv:
                timePicker(view, startValidTimeFlag);
                break;
            case R.id.end_valid_time_tv:
                timePicker(view, endValidTimeFlag);
                break;
            case R.id.appointment_tv:
                DialogController.showMenuList(new String[]{"免预约", "电话预约"}, mActivity, (int index) -> {
                    appointment_tv.setText(new String[]{"免预约", "电话预约"}[index]);
                });
                break;
        }
    }

    public void timePicker(final View view, final int flag) {
        int[] time = DateUtils.splitTextTime(view);
        DialogController.showTimeDialog(mActivity, time, view, (TimePicker picker, int hourOfDay, int minute) -> {
            switch (flag) {
                case endSaleTimeFlag:
                    try {
                        endSaleTime = formatTime.parse(hourOfDay + ":" + minute);
                        ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case startValidTimeFlag:
                    try {
                        startValidTime = formatTime.parse(hourOfDay + ":" + minute);
                        if (isEqual) {
                            if (startValidTime.compareTo(endSaleTime) >= 0) {
                                ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                                startTimeSecond = hourOfDay * 3600 + minute * 60;
                            } else {
                                showToast("开始生效时间必须大于结束售卖时间");
                            }
                        } else {
                            ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                            startTimeSecond = hourOfDay * 3600 + minute * 60;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case endValidTimeFlag:
                    ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                    endTimeSecond = hourOfDay * 3600 + minute * 60;
                    break;
            }
        });
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }

    public void verifyData() {

        try {
            CheckUtils.checkMoney(original_price_et, "请输入面值", 1);
            CheckUtils.checkData(Double.parseDouble(original_price_et.getText().toString()) < 50, original_price_et, "面值必须大于50");
            CheckUtils.checkMoney(buy_price_et, "请输入秒杀价", 1);
            CheckUtils.checkData(Double.parseDouble(buy_price_et.getText().toString()) > 9.9, buy_price_et, "秒杀价必须小于9.9");
            CheckUtils.checkData(sell_limit_num_et, "请输入每天贩卖数量");
            CheckUtils.checkData(buy_limit_num_et, "请输入每人购买限制数量");
            CheckUtils.checkData(use_limit_num_et, "请输入每次最大使用数量");
            CheckUtils.checkData(stock_balance_et, "请输入库存量");
            CheckUtils.checkData(end_sell_date_tv, "请输入结束贩卖日期");
            CheckUtils.checkData(end_sell_time_tv, "请输入结束贩卖时间");
            CheckUtils.checkData(start_valid_date_tv, "请输入生效日期");
            CheckUtils.checkData(end_valid_date_tv, "请输入生效日期");
            CheckUtils.checkData(start_valid_time_tv, "请输入面值");
            CheckUtils.checkData(end_valid_time_tv, "请输入消费时间");
            CheckUtils.checkData(appointment_tv, "请选择预约方式");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }

        submitData();
    }


    public void onPickDateEvent(final TextView tView, final int flag) {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        selectedDate.setTime(new Date());
        new DatePickerDialog(mActivity,
                // 绑定监听器
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Date checkDate = null;
                        String tempDate = year + "-" + (month + 1) + "-" + day;
                        try {
                            checkDate = formatDate.parse(tempDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        switch (flag) {
                            case endSaleDateFlag:
                                endSaleDate = checkDate;
                                if (endSaleDate.compareTo(new Date()) >= 0) {
                                    tView.setText(tempDate);
                                } else {
                                    showToast("结束售卖日期必须大于当前日期");
                                }
                                break;
                            case startValidDateFlag:
                                startValidDate = checkDate;
                                isEqual = false;
                                if (startValidDate.compareTo(endSaleDate) >= 0) {
                                    if (startValidDate.compareTo(endSaleDate) == 0) {
                                        isEqual = true;
                                    }
                                    tView.setText(tempDate);
                                } else {
                                    showToast("开始生效日期必须大于结束售卖日期");
                                }
                                break;
                            case endValidDateFlag:
                                endValidDate = checkDate;
                                if (endValidDate.compareTo(startValidDate) >= 0) {
                                    tView.setText(tempDate);
                                } else {
                                    showToast("结束生效日期必须大于开始生效日期");
                                }
                                break;
                        }
                    }
                }
                // 设置初始时间
                , selectedDate.get(Calendar.YEAR)
                , selectedDate.get(Calendar.MONTH)
                , selectedDate.get(Calendar.DATE) + 1
        ).show();
    }


    /**
     * 处理结束贩卖时间
     */
    public String formatEndSellTime(String date, String time) {
        return date + " " + time + ":00";
    }

}
