package com.party.jackseller.uihome;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.EatPersonCountBean;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.utils.AliOssService;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DateUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.TimeUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.qqtheme.framework.picker.OptionPicker;

public class AddSpellingGroupCouponActivity extends BaseActivityTitle {

    @BindView(R.id.scroll_layout)
    ScrollView scroll_layout;
    @BindView(R.id.spelling_group_coupon_image_iv)
    ImageView spelling_group_coupon_image_iv;//商品图片
    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;//店名
    @BindView(R.id.spelling_group_coupon_name_et)
    EditText spelling_group_coupon_name_et;
    @BindView(R.id.spelling_group_coupon_content_tv)
    TextView spelling_group_coupon_content_tv;
    @BindView(R.id.spelling_group_buy_content_count_tv)
    TextView spelling_group_buy_content_count_tv;
    @BindView(R.id.original_price_tv)
    TextView original_price_tv;
    @BindView(R.id.buy_price_et)
    EditText buy_price_et;
    @BindView(R.id.eat_person_num_tv)
    TextView eat_person_num_tv;
    @BindView(R.id.start_valid_date_tv)
    TextView start_valid_date_tv;
    @BindView(R.id.end_valid_date_tv)
    TextView end_valid_date_tv;
    @BindView(R.id.start_valid_time_tv)
    TextView start_valid_time_tv;
    @BindView(R.id.end_valid_time_tv)
    TextView end_valid_time_tv;
    @BindView(R.id.appointment_tv)
    TextView appointment_tv;
    @BindView(R.id.end_sell_date_tv)
    TextView end_sell_date_tv;
    @BindView(R.id.end_sell_time_tv)
    TextView end_sell_time_tv;
    @BindView(R.id.spelling_group_num_from_et)
    EditText spelling_group_num_from_et;
    @BindView(R.id.spelling_group_num_to_et)
    EditText spelling_group_num_to_et;
    @BindView(R.id.spelling_group_coupon_image_ll)
    View spelling_group_coupon_image_ll;


    private static final String TAG = "AddConglomerationCoupon";
    private CouponService couponService;
    private AliOssService mOssService;
    private String[] personArr;
    private List<EatPersonCountBean> eatPersonCountBeanList;
    private SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
    private String[] goodsRangeStr;
    private int startTimeSecond, endTimeSecond;//开始时间秒数和结束时间秒数
    private Date endSaleDate, startValidDate, endValidDate;
    private Date endSaleTime, startValidTime, endValidTime;
    private final int endSaleDateFlag = 1, endSaleTimeFlag = 2, startValidDateFlag = 3,
            startValidTimeFlag = 4, endValidDateFlag = 5, endValidTimeFlag = 6;
    private boolean isEqual = false;//结束售卖时间是否等于开始生效时间
    private String groupBuyContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_spelling_group_coupon);
        initData();
    }

    protected void initData() {
        Constant.CART.clear();
        handleTitle();
        setMiddleText("添加拼团券");
        setNeedOnCreateRegister();
        shop_name_tv.setText(mApplication.getCurrShopName());
        mOssService = new AliOssService(this);
        couponService = new CouponService(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getGroupBuyContent();
    }

    /**
     * 处理团购内容
     */
    public void getGroupBuyContent() {
        JSONArray jsonArray = new JSONArray();
        StringBuffer content = new StringBuffer();
        StringBuffer count = new StringBuffer();
        List<Goods> goodsList = Constant.CART.getGoodsList();

        for (int i = 0; i < goodsList.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", goodsList.get(i).getId());
                jsonObject.put("count", goodsList.get(i).getCount());

                content.append(goodsList.get(i).getTitle());
                count.append("x ").append(goodsList.get(i).getCount()).append("         ");

                if (i != goodsList.size() - 1) {
                    content.append("\n");
                    count.append("\n");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        groupBuyContent = jsonArray.toString();
        spelling_group_coupon_content_tv.setText(content.toString());

        spelling_group_buy_content_count_tv.setText(count.toString());
        original_price_tv.setText(Constant.CART.getTotalPriceStr());
    }


    /**
     * 提交数据
     *
     * @param imagePath
     */
    public void submitData(String imagePath) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("itemList", groupBuyContent);
        tokenMap.put("endSaleTime", TimeUtil.date2TimeStamp(formatEndSellTime(end_sell_date_tv.getText().toString(), end_sell_time_tv.getText().toString()), "yyyy-MM-dd HH:mm:ss"));
        tokenMap.put("buyPrice", buy_price_et.getText().toString());
        tokenMap.put("originalPrice", original_price_tv.getText().toString());
        tokenMap.put("appointment", CommonUtil.getAppointmentId(appointment_tv.getText().toString()));
        tokenMap.put("dateStart", TimeUtil.date2TimeStamp(start_valid_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("dateEnd", TimeUtil.date2TimeStamp(end_sell_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("timeStart", startTimeSecond + "");
        tokenMap.put("timeEnd", endTimeSecond + "");
        tokenMap.put("title", spelling_group_coupon_name_et.getText().toString());
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("dinersNumberId", getDinnerNumberId(eat_person_num_tv.getText().toString()) + "");
        tokenMap.put("unionStartNumber", spelling_group_num_from_et.getText().toString());
        tokenMap.put("unionEndNumber", spelling_group_num_to_et.getText().toString());
        tokenMap.put("groupBuyImg", imagePath);

        addDisposableIoMain(couponService.addSpellingGroupCoupon(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity, "成功添加拼团券！", (View v) -> {
                    Intent intent = new Intent();
                    intent.putExtra("isAdd", true);
                    setResult(RESULT_OK, intent);
                    finish();
                });
            }
        });
    }

    /**
     * 获取用餐人数id
     *
     * @param name
     * @return
     */
    public long getDinnerNumberId(String name) {
        if (eatPersonCountBeanList == null) return -1L;
        for (EatPersonCountBean eatPersonCountBean : eatPersonCountBeanList) {
            if (eatPersonCountBean.getName().equals(name)) {
                return eatPersonCountBean.getId();
            }
        }
        return -1L;
    }


    /**
     * 处理点击事件
     *
     * @param view
     */
    @OnClick({R.id.spelling_group_coupon_image_ll, R.id.spelling_group_coupon_content_tv, R.id.end_sell_date_tv,
            R.id.eat_person_num_tv, R.id.start_valid_date_tv, R.id.end_valid_date_tv, R.id.start_valid_time_tv, R.id.end_valid_time_tv,
            R.id.appointment_tv, R.id.end_sell_time_tv, R.id.submit_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.eat_person_num_tv:
                showDinnerNumberPicker();
                break;
            case R.id.end_sell_date_tv:
                onPickDateEvent(view, endSaleDate, endSaleDateFlag);
                break;
            case R.id.end_sell_time_tv:
                if (endSaleDate == null) {
                    showToast("请先设置结束售卖日期");
                    scrollTo(end_sell_date_tv);
                    return;
                }
                timePicker(view, endSaleTimeFlag);
                break;
            case R.id.start_valid_date_tv:
                if (endSaleTime == null) {
                    showToast("请先设置结束售卖时间");
                    scrollTo(end_sell_time_tv);
                    return;
                }
                onPickDateEvent(view, startValidDate, startValidDateFlag);
                break;
            case R.id.end_valid_date_tv:
                if (startValidDate == null) {
                    showToast("请先设置开始生效时间");
                    scrollTo(start_valid_date_tv);
                    return;
                }
                onPickDateEvent(view, endValidDate, endValidDateFlag);
                break;
            case R.id.start_valid_time_tv:
                if (endValidDate == null) {
                    showToast("请先设置结束生效日期");
                    scrollTo(end_valid_date_tv);
                    return;
                }
                timePicker(view, startValidTimeFlag);
                break;
            case R.id.end_valid_time_tv:
                if (startValidTime == null) {
                    scrollTo(start_valid_time_tv);
                    showToast("请先设置开始生效时间");
                    return;
                }
                timePicker(view, endValidTimeFlag);
                break;
            case R.id.appointment_tv:
                OptionPicker picker = new OptionPicker(mActivity, new String[]{"免预约", "电话预约"});
                picker.setOffset(4);
                picker.setTextSize(20);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
                        appointment_tv.setText(item);
                    }
                });
                picker.show();
                break;
            case R.id.spelling_group_coupon_content_tv:
                startActivity(new Intent(mActivity, GoodsContentChooseActivity.class));
                break;
            case R.id.spelling_group_coupon_image_ll:
                if (choosePictureController != null) {
                    choosePictureController.clearBeforeChoosePictures();
                    choosePictureController.handleToChooseCropPictures();
                }
                break;
            case R.id.submit_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
        }
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        mOssService.putImagePath(R.id.spelling_group_coupon_image_iv, list.get(0).getImgPath());
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, list.get(0).getImgPath(), spelling_group_coupon_image_iv);
    }

    /**
     * OSS上传图片第一步
     */
    private void doUploadPictures() {
        showAlertDialog();
        mOssService.uploadImageAll((String url) -> {
            submitData(url);
        });
    }

    /**
     * 展示用餐人数选择器
     */
    public void showDinnerNumberPicker() {
        addDisposableIoMain(couponService.getDinnerNumber(), new DefaultConsumer<List<EatPersonCountBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<EatPersonCountBean>> baseBean) {
                eatPersonCountBeanList = baseBean.getData();
                personArr = new String[baseBean.getData().size()];
                for (int i = 0; i < baseBean.getData().size(); i++) {
                    personArr[i] = baseBean.getData().get(i).getName();
                }

                DialogController.showMenuList(personArr, mActivity, (int index) -> {
                    eat_person_num_tv.setText(personArr[index]);
                });
            }
        });
    }

    public void timePicker(final View view, final int flag) {
        int[] time = DateUtils.splitTextTime(view);
        DialogController.showTimeDialog(mActivity, time, view, (TimePicker picker, int hourOfDay, int minute) -> {
            switch (flag) {
                case endSaleTimeFlag:
                    try {
                        endSaleTime = formatTime.parse(hourOfDay + ":" + minute);
                        ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case startValidTimeFlag:
                    try {
                        startValidTime = formatTime.parse(hourOfDay + ":" + minute);
                        if (isEqual) {
                            if (startValidTime.compareTo(endSaleTime) >= 0) {
                                ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                                startTimeSecond = hourOfDay * 3600 + minute * 60;
                            } else {
                                showToast("开始生效时间必须大于结束售卖时间");
                            }
                        } else {
                            ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                            startTimeSecond = hourOfDay * 3600 + minute * 60;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case endValidTimeFlag:
                    ((TextView) view).setText(validate(hourOfDay) + ":" + validate(minute));
                    endTimeSecond = hourOfDay * 3600 + minute * 60;
                    break;
            }
        });
    }

    public void onPickDateEvent(View view, Date date, final int flag) {
        TextView tView = (TextView) view;
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        selectedDate.setTime(date == null ? new Date() : date);
        new DatePickerDialog(mActivity, (DatePicker datePicker, int year, int month, int day) -> {
            Date checkDate = null;
            String tempDate = year + "-" + (month + 1) + "-" + day;
            try {
                checkDate = formatDate.parse(tempDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            switch (flag) {
                case endSaleDateFlag:
                    if (checkDate.compareTo(new Date()) > 0) {
                        endSaleDate = checkDate;
                        tView.setText(tempDate);
                    } else {
                        showToast("结束售卖日期必须大于当前日期");
                    }
                    break;
                case startValidDateFlag:
                    isEqual = false;
                    if (checkDate.compareTo(endSaleDate) >= 0) {
                        if (checkDate.compareTo(endSaleDate) == 0) {
                            isEqual = true;
                        }
                        startValidDate = checkDate;
                        tView.setText(tempDate);
                    } else {
                        showToast("开始生效日期必须大于结束售卖日期");
                    }
                    break;
                case endValidDateFlag:
                    if (checkDate.compareTo(startValidDate) > 0) {
                        endValidDate = checkDate;
                        tView.setText(tempDate);
                    } else {
                        showToast("结束生效日期必须大于开始生效日期");
                    }
                    break;
            }
        }
                , selectedDate.get(Calendar.YEAR)
                , selectedDate.get(Calendar.MONTH)
                , selectedDate.get(Calendar.DATE) + 1
        ).show();

    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }

    /**
     * 处理结束贩卖时间
     */
    public String formatEndSellTime(String date, String time) {
        return date + " " + time + ":00";
    }


    public void scrollTo(View v) {
        scrollTo(v, spelling_group_coupon_image_ll, scroll_layout);
    }

    public void verifyData() {
        try {
            CheckUtils.checkData(mOssService.getLocalPath(R.id.spelling_group_coupon_image_iv), spelling_group_coupon_image_ll, "请选择图片");
            CheckUtils.checkData(spelling_group_coupon_name_et, "请输入拼团名称");
            CheckUtils.checkData(spelling_group_coupon_content_tv, "请选择拼团内容");
            CheckUtils.checkData(buy_price_et, "请输入团购价格");
            boolean isOk = Double.parseDouble(buy_price_et.getText().toString()) >= Double.parseDouble(original_price_tv.getText().toString());
            CheckUtils.checkData(isOk, buy_price_et, "团购价必须小于原价");
            CheckUtils.checkData(end_sell_date_tv, "请输入结束日期");
            CheckUtils.checkData(end_sell_time_tv, "请输入结束时间");
            CheckUtils.checkData(end_valid_date_tv, "请输入消费日期");
            CheckUtils.checkData(end_valid_time_tv, "请输入消费时间");
            CheckUtils.checkData(start_valid_date_tv, "请输入生效日期");
            CheckUtils.checkData(start_valid_time_tv, "请输入生效时间");
            CheckUtils.checkData(spelling_group_num_from_et, "最低人数");
            CheckUtils.checkData(spelling_group_num_to_et, "最大人数");
            boolean spellingIsOk = Integer.parseInt(spelling_group_num_to_et.getText().toString()) < Integer.parseInt(spelling_group_num_from_et.getText().toString());
            CheckUtils.checkData(spellingIsOk, spelling_group_num_to_et, "最大人数必须大于最小人数");
            CheckUtils.checkData(appointment_tv, "请选择预约方式");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }

        doUploadPictures();
    }
}
