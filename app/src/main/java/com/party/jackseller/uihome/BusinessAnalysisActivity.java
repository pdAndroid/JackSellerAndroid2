package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BusinessAnalysisBean;
import com.party.jackseller.bean.TradeAnalysisBean;
import com.party.jackseller.uimy.MyShopManagerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2019/1/5.
 * 首页-经营分析
 */

public class BusinessAnalysisActivity extends BaseActivity {
    @BindView(R.id.btn_ba_business_yesterday)
    RadioButton btnBaBusinessYesterday;
    @BindView(R.id.btn_ba_business_sevenday)
    RadioButton btnBaBusinessSevenday;
    @BindView(R.id.btn_ba_business_thirtyday)
    RadioButton btnBaBusinessThirtyday;
    @BindView(R.id.tv_ba_business_total)
    TextView tvBaBusinessTotal;
    @BindView(R.id.tv_ba_business_single)
    TextView tvBaBusinessSingle;
    @BindView(R.id.tv_ba_business_count)
    TextView tvBaBusinessCount;
    @BindView(R.id.tv_ba_business_evaluate)
    TextView tvBaBusinessEvaluate;
    @BindView(R.id.btn_ba_transaction_yesterday)
    RadioButton btnBaTransactionYesterday;
    @BindView(R.id.btn_ba_transaction_sevenday)
    RadioButton btnBaTransactionSevenday;
    @BindView(R.id.btn_ba_transaction_thirtyday)
    RadioButton btnBaTransactionThirtyday;
    @BindView(R.id.tv_ba_transaction_total)
    TextView tvBaTransactionTotal;
    @BindView(R.id.tv_ba_transaction_single)
    TextView tvBaTransactionSingle;
    @BindView(R.id.tv_ba_transaction_count)
    TextView tvBaTransactionCount;
    @BindView(R.id.tv_ba_more_reviews)
    TextView tvBaMoreReviews;
    @BindView(R.id.iv_ba_return)
    ImageView ivBaReturn;

    private ShopService shopService;
    private String shopId;
    private String day;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_analysis_web);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        shopService = new ShopService(this);
        shopId = mApplication.getCurrShopId() + "";
        day = 0 + "";
    }

    private void requestData() {
        getAnalysisBusiness(shopId, day);
        getAnalysisTrade(shopId, day);
    }

    public void getAnalysisBusiness(String shopId, String day) {
        addDisposableIoMain(shopService.getAnalysisBusiness(shopId, day), new DefaultConsumer<BusinessAnalysisBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BusinessAnalysisBean> data) {
                data.getData();
                tvBaBusinessCount.setText(data.getData().getCount() + "");
                tvBaBusinessEvaluate.setText(data.getData().getNew_comment() + "");
                tvBaBusinessSingle.setText(data.getData().getAvg() + "");
                tvBaBusinessTotal.setText(data.getData().getTotal() + "");
            }
        });
    }

    public void getAnalysisTrade(String shopId, String day) {
        addDisposableIoMain(shopService.getAnalysisTrade(shopId, day), new DefaultConsumer<TradeAnalysisBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<TradeAnalysisBean> data) {
                data.getData();
                tvBaTransactionCount.setText(data.getData().getCount() + "");
                tvBaTransactionSingle.setText(data.getData().getAvg() + "");
                tvBaTransactionTotal.setText(data.getData().getTotal() + "");
            }
        });
    }

    @OnClick({R.id.iv_ba_return, R.id.tv_ba_more_reviews, R.id.btn_ba_business_yesterday, R.id.btn_ba_business_sevenday, R.id.btn_ba_business_thirtyday, R.id.btn_ba_transaction_yesterday, R.id.btn_ba_transaction_sevenday, R.id.btn_ba_transaction_thirtyday})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_ba_return:
                finish();
                break;
            case R.id.tv_ba_more_reviews:
                Intent intent = new Intent(this, MyShopManagerActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_ba_business_yesterday:
                day = "0";
                getAnalysisBusiness(shopId, day);
                break;
            case R.id.btn_ba_business_sevenday:
                day = "7";
                getAnalysisBusiness(shopId, day);
                break;
            case R.id.btn_ba_business_thirtyday:
                day = "30";
                getAnalysisBusiness(shopId, day);
                break;
            case R.id.btn_ba_transaction_yesterday:
                day = "0";
                getAnalysisTrade(shopId, day);
                break;
            case R.id.btn_ba_transaction_sevenday:
                day = "7";
                getAnalysisTrade(shopId, day);
                break;
            case R.id.btn_ba_transaction_thirtyday:
                day = "30";
                getAnalysisTrade(shopId, day);
                break;
        }
    }
}
