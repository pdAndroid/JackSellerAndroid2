package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.controller.PayController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 输码验证
 */
public class InputCodeVerifyActivity extends BaseActivityTitle {
    @BindView(R.id.code_et)
    EditText code_et;
    @BindView(R.id.verify_btn)
    Button verifyBtn;

    private CouponService couponService;
    private String orderCode;
    PayController payController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_code_verify);
        ButterKnife.bind(this);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("输码验证");
        couponService = new CouponService(this);
        payController = new PayController(this);
        payController.setHelpView(code_et);
    }

//    @OnClick(R.id.verify_btn)
//    public void handleClickSth(View view) {
//        if (TextUtils.isEmpty(code_et.getText().toString().trim())) {
//            showToast("券码不能为空！");
//            return;
//        }
//        orderCode = code_et.getText().toString().trim();
//
////        Intent intent = new Intent(mActivity, VerifyResultActivity.class);
//        Intent intent = new Intent(this, LoseCodeValidationActivity.class);
////        JSONObject jsonObject = new JSONObject();
////        try {
////            jsonObject.put("orderCode", code_et.getText().toString().trim());
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////        Bundle bundle = new Bundle();
////        bundle.putInt(CodeUtils.RESULT_TYPE, CodeUtils.RESULT_SUCCESS);
////        bundle.putString(CodeUtils.RESULT_STRING, jsonObject.toString());
//        intent.putExtras("orderCode", orderCode);
//        startActivity(intent);
//        //DialogController.showVerifyFailedDialog(mActivity);
//    }

    @OnClick(R.id.verify_btn)
    public void onViewClicked() {
        if (TextUtils.isEmpty(code_et.getText().toString().trim())) {
            showToast("券码不能为空！");
            return;
        }
        orderCode = code_et.getText().toString().trim();
        Intent intent = new Intent(this, LoseCodeValidationActivity.class);
        intent.putExtra("orderCode", orderCode);
        startActivity(intent);
//        Intent intent = new Intent(mActivity, VerifyResultActivity.class);
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("orderCode", code_et.getText().toString().trim());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Bundle bundle = new Bundle();
//        bundle.putInt(CodeUtils.RESULT_TYPE, CodeUtils.RESULT_SUCCESS);
//        bundle.putString(CodeUtils.RESULT_STRING, jsonObject.toString());
        //DialogController.showVerifyFailedDialog(mActivity);
    }
}
