package com.party.jackseller.uihome;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.SpellingGroupCoupon;
import com.party.jackseller.bean.SpellingGroupCouponDetailBean;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.utils.PhoneUtil;
import com.party.jackseller.utils.TimeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.iwgang.countdownview.CountdownView;
import io.reactivex.functions.Consumer;

public class SpellingGroupCouponDetailActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.remaining_time_cv)
    CountdownView remaining_time_cv;

    private List<SpellingGroupCouponDetailBean> dataList = new ArrayList<>();
    private CouponService couponService;
    private CommonAdapter<SpellingGroupCouponDetailBean> commonAdapter;
    private int currPage = 1;
    private boolean noMoreData = false;
    private SpellingGroupCoupon spellingGroupCoupon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spelling_group_coupon_detail);
        initData();
        spellingGroupCoupon = (SpellingGroupCoupon) getIntent().getSerializableExtra("spellingGroupCouponBean");
    }

    protected void initData() {
        helper = new LoadViewHelper(refresh_layout);
        couponService = new CouponService(this);
        initView();

        helper.showLoading();
        loadData();
    }

    public void initView() {
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<SpellingGroupCouponDetailBean>(this, R.layout.item_spelling_group_detail, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final SpellingGroupCouponDetailBean item, int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, item.getIcon(), (ImageView) viewHolder.getView(R.id.user_iv));
                viewHolder.setText(R.id.user_name_tv, item.getNickname());
                viewHolder.setOnClickListener(R.id.call_phone_btn, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhoneUtil.callPhone(mActivity, item.getPhone());
                    }
                });
            }
        };
        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
    }

    public void setRemainingTime() {
        long currTimeStamp = TimeUtil.getCurrTimeStamp();
        /*long endTimeStamp = spellingGroupCoupon.get;
        long remainTimeStamp = endTimeStamp- currTimeStamp;*/
    }

    public void loadData() {
        currPage = 1;
        addDisposableIoMain(couponService.getSpellingGroupCouponDetail(spellingGroupCoupon.getId()), new DefaultConsumer<List<SpellingGroupCouponDetailBean>>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<SpellingGroupCouponDetailBean>> baseBean) {
                dataList.clear();
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty();
                } else {
                    if (baseBean.getData().size() < Constant.PAGE_SIZE) {
                        refresh_layout.setEnableLoadMore(false);
                    }
                    refresh_layout.finishRefresh();
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showError();
            }
        });
    }

    public void loadMoreData() {
        currPage++;
        addDisposableIoMain(couponService.getSpellingGroupCouponDetail(spellingGroupCoupon.getId()), new DefaultConsumer<List<SpellingGroupCouponDetailBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<SpellingGroupCouponDetailBean>> baseBean) {
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                    if (baseBean.getData().size() <= Constant.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    refresh_layout.finishLoadMoreWithNoMoreData();
                } else {
                    refresh_layout.finishLoadMore();
                }
            }
        });
    }
}
