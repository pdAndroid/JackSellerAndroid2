package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.party.jackseller.R;
import com.party.jackseller.adapter.MyCommonAdapter;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.api.TradeRecordService;
import com.party.jackseller.BaseFragment;
import com.party.jackseller.Constant;
import com.party.jackseller.base.MyOnRefreshLoadMoreListener;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BusinessAnalysis;
import com.party.jackseller.bean.DisplayableItem;
import com.party.jackseller.bean.NoTradeRecordBean;
import com.party.jackseller.bean.TradeRecord;
import com.party.jackseller.bean.VideoBean;
import com.party.jackseller.litvedio.play.SingleTCVodPlayerActivity;
import com.party.jackseller.ui_scan.ScanActivity;
import com.party.jackseller.uihome.jpush.MyReceiver;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.ConstUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.uuzuche.lib_zxing.activity.CodeUtils;
import com.zhy.adapter.recyclerview.base.ViewHolder;
import com.zhy.adapter.recyclerview.wrapper.HeaderAndFooterWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 首页
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    private TextView today_business_volume_tv;
    private TextView consume_count_tv;
    private TextView new_add_comment_count_tv;
    private TextView shop_name_tv;
    private TextView tv_shop_name;

    private MyCommonAdapter commonAdapter;
    private List<DisplayableItem> dataList = new ArrayList<>();
    private TradeRecordService tradeRecordService;
    private ShopService shopService;
    private BusinessAnalysis mBaseBean;

    public static BaseFragment newInstance() {
        BaseFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initData() {
        initView();
        shopService = new ShopService(mActivity);
        tradeRecordService = new TradeRecordService(mActivity);
        setCurrShopName(mApplication.getCurrShopName());
        initBusinessAnalysis();
        loadData();
    }

    public void setCurrShopName(String shopName) {
        shop_name_tv.setText(shopName);
        tv_shop_name.setText(shopName);
    }

    public void initBusinessAnalysis() {
        mActivity.addDisposableIoMain(shopService.getBusinessAnalysis(mApplication.getCurrShopId(), 0), new DefaultConsumer<BusinessAnalysis>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BusinessAnalysis> baseBean) {
                setStatistics(baseBean.getData());
            }
        });
    }

    public void setStatistics(BusinessAnalysis baseBean) {
        mBaseBean = baseBean;
        today_business_volume_tv.setText(baseBean.getTotal() + "元");
        consume_count_tv.setText(baseBean.getCount() + "笔");
        new_add_comment_count_tv.setText(baseBean.getNew_comment() + "条");
    }

    public void initView() {
        refresh_layout.setEnableLoadMore(false);
        refresh_layout.setEnableRefresh(true);

        recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));

        today_business_volume_tv = getView().findViewById(R.id.today_business_volume_tv);
        consume_count_tv = getView().findViewById(R.id.consume_count_tv);
        new_add_comment_count_tv = getView().findViewById(R.id.new_add_comment_count_tv);
        shop_name_tv = getView().findViewById(R.id.shop_name_tv);
        tv_shop_name = getView().findViewById(R.id.tv_shop_name);
        initHeaderListener(getView());

        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(mActivity, R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);


        refresh_layout.setOnRefreshLoadMoreListener(new MyOnRefreshLoadMoreListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });


        commonAdapter = new MyCommonAdapter(mActivity, R.layout.item_trade_record, dataList) {
            @Override
            protected void convert(ViewHolder holder, Object item, int position) {
                final TradeRecord tradeRecord = (TradeRecord) item;
                holder.setText(R.id.user_nickname_tv, tradeRecord.getTitle() + "");
                holder.setText(R.id.user_user_tv, tradeRecord.getPayer() + "");
                holder.setText(R.id.time_tv, CommonUtil.formatTimeText(tradeRecord.getCreate()));
                holder.setText(R.id.money_tv, tradeRecord.getTotalPrice() + "");
                if (tradeRecord.getTradeRecordType() == 1) {
                    holder.setText(R.id.consume_record_name_tv, "-" + tradeRecord.getValue());
                } else {
                    holder.setText(R.id.consume_record_name_tv, "+" + tradeRecord.getValue());
                }

                holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mActivity.startActivity(TradingParticularsActivity.class, "tradeRecordId", tradeRecord.getId() + "");
                    }
                });
            }
        };
        MyReceiver.addAdapter(commonAdapter);
        MyReceiver.addDataFinish((TradeRecord record) -> {
            mBaseBean.addRecord(record);
            setStatistics(mBaseBean);
        });


        recycler_view.setAdapter(commonAdapter);
    }


    /**
     * 处理二维码扫描结果
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ConstUtils.SCAN_CODE) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
                    showToast("解析结果:" + result);
                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                    showToast("解析二维码失败");
                }
            }
        }
    }


    public void initHeaderListener(View headerView) {
        headerView.findViewById(R.id.input_code_verify_ll).setOnClickListener(this);
        headerView.findViewById(R.id.scan_code_ll).setOnClickListener(this);
        headerView.findViewById(R.id.collect_money_ll).setOnClickListener(this);
        headerView.findViewById(R.id.spelling_group_coupon_ll).setOnClickListener(this);
        headerView.findViewById(R.id.group_buy_ll).setOnClickListener(this);
        headerView.findViewById(R.id.replace_money_coupon_ll).setOnClickListener(this);
        headerView.findViewById(R.id.second_kill_ll).setOnClickListener(this);
        headerView.findViewById(R.id.shop_name_tv).setOnClickListener(this);
        headerView.findViewById(R.id.ll_home_business_analysis).setOnClickListener(this);
        headerView.findViewById(R.id.shop_name_tv).setOnClickListener(this);
        headerView.findViewById(R.id.more_record).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mApplication.getCurrShop().getShop_state() != Constant.SHOP_STATUS_ON_LINE) {
            showToast(mApplication.getCurrShop().getShop_state_name());
            return;
        }
        switch (v.getId()) {
            case R.id.input_code_verify_ll:
                mActivity.startActivity(new Intent(mActivity, InputCodeVerifyActivity.class));
                break;
            case R.id.scan_code_ll:
                Intent intent = new Intent(mActivity, ScanActivity.class);
                startActivityForResult(intent, ConstUtils.SCAN_CODE);
                break;
            case R.id.collect_money_ll:
                mActivity.startActivity(UserPayBillActivity.class);
                break;
            //拼团
            case R.id.spelling_group_coupon_ll:
                Toast.makeText(mActivity, "暂未开放", Toast.LENGTH_SHORT).show();
                //mActivity.startActivity(SpellingGroupCouponListActivity.class);
                break;
            //套餐
            case R.id.group_buy_ll:
//                startActivity(new Intent(mActivity, GroupBuyCouponListActivity.class));
                Toast.makeText(mActivity, "暂未开放", Toast.LENGTH_SHORT).show();
                break;
            case R.id.replace_money_coupon_ll:
                mActivity.startActivity(PreferentialPaymentSettingActivity.class);
                break;
            case R.id.second_kill_ll:
                //秒杀
                Toast.makeText(mActivity, "暂未开放", Toast.LENGTH_SHORT).show();
                //mActivity.startActivity(SecondKillCouponListActivity.class);
                break;
            case R.id.shop_name_tv:
                break;
            case R.id.ll_home_business_analysis:
                mActivity.startActivity(BusinessAnalysisWebActivity.class);
                break;
            case R.id.more_record:
                mActivity.startActivity(TradeRecordListActivity.class);
                break;

        }
    }

    /**
     * 只获取3条交易记录
     */
    public void loadData() {
        mActivity.addDisposableIoMain(tradeRecordService.getTradeRecordList(mApplication.getCurrShopId(), 3, 1), new DefaultConsumer<List<TradeRecord>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<TradeRecord>> baseBean) {
                dataList.clear();
                if (baseBean.getData() != null && !baseBean.getData().isEmpty()) {
                    dataList.addAll(baseBean.getData());
                } else {
                    showToast("没有流水记录");
                }
                refresh_layout.finishRefresh();
                commonAdapter.notifyDataSetChanged();
                initBusinessAnalysis();
            }
        });
    }


}
