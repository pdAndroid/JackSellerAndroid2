package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.Constant;
import com.party.jackseller.base.CouponBaseActivity;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.EatPersonCountBean;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.utils.AliOssService;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DateUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.TimeUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class AddGroupBuyActivity extends CouponBaseActivity {

    @BindView(R.id.group_buy_image_iv)
    ImageView group_buy_image_iv;
    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.group_buy_name_et)
    EditText group_buy_name_et;
    @BindView(R.id.group_buy_content_tv)
    TextView group_buy_content_tv;
    @BindView(R.id.original_price_tv)
    TextView original_price_tv;
    @BindView(R.id.buy_price_et)
    EditText buy_price_et;
    @BindView(R.id.buy_limit_et)
    EditText buy_limit_et;
    @BindView(R.id.stock_balance_et)
    EditText stock_balance_et;
    @BindView(R.id.use_limit_num_et)
    EditText use_limit_num_et;
    @BindView(R.id.eat_person_num_tv)
    TextView eat_person_num_tv;
    @BindView(R.id.start_date_tv)
    TextView start_date_tv;
    @BindView(R.id.end_date_tv)
    TextView end_date_tv;
    @BindView(R.id.start_time_tv)
    TextView start_time_tv;
    @BindView(R.id.end_time_tv)
    TextView end_time_tv;
    @BindView(R.id.weeks_layout)
    View weeks_layout;
    @BindView(R.id.week_1)
    AppCompatCheckBox week_1;
    @BindView(R.id.week_2)
    AppCompatCheckBox week_2;
    @BindView(R.id.week_3)
    AppCompatCheckBox week_3;
    @BindView(R.id.week_4)
    AppCompatCheckBox week_4;
    @BindView(R.id.week_5)
    AppCompatCheckBox week_5;
    @BindView(R.id.week_6)
    AppCompatCheckBox week_6;
    @BindView(R.id.week_7)
    AppCompatCheckBox week_7;
    @BindView(R.id.appointment_tv)
    TextView appointment_tv;
    @BindView(R.id.group_buy_content_count_tv)
    TextView group_buy_content_count_tv;

    private CouponService couponService;
    private AliOssService mOssService;
    private String[] personArr;
    private List<EatPersonCountBean> eatPersonCountBeanList;
    private static final String TAG = "AddGroupBuyActivity";

    private int startTimeFlag = 1, endTimeFlag = 2;
    private int startTimeSecond, endTimeSecond;//开始时间秒数和结束时间秒数
    private String groupBuyContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group_buy_coupon);
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getGroupBuyContent();
    }


    /**
     * 处理团购内容
     */
    public void getGroupBuyContent() {
        JSONArray jsonArray = new JSONArray();
        StringBuffer content = new StringBuffer();
        StringBuffer count = new StringBuffer();
        List<Goods> goodsList = Constant.CART.getGoodsList();

        for (int i = 0; i < goodsList.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", goodsList.get(i).getId());
                jsonObject.put("count", goodsList.get(i).getCount());

                content.append(goodsList.get(i).getTitle());
                count.append("x ").append(goodsList.get(i).getCount()).append("         ");

                if (i != goodsList.size() - 1) {
                    content.append("\n");
                    count.append("\n");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        groupBuyContent = jsonArray.toString();
        group_buy_content_tv.setText(content.toString());

        group_buy_content_count_tv.setText(count.toString());
        this.original_price_tv.setText(Constant.CART.getTotalPriceStr());
    }


    protected void initData() {
        Constant.CART.clear();

        handleTitle();
        setMiddleText("添加团购券");
        setNeedOnCreateRegister();
        shop_name_tv.setText(mApplication.getCurrShop().getShop_name());
        mOssService = new AliOssService(this);
        couponService = new CouponService(this);
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        String path = list.get(0).getImgPath();
        mOssService.putImagePath(R.id.group_buy_image_iv, path);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, path, group_buy_image_iv);
    }


    /**
     * OSS上传图片第一步
     */
    private void doUploadPictures() {
        showAlertDialog();
        mOssService.uploadImageAll(new AliOssService.UploadListener() {
            @Override
            public void handler(String url) {
                submitData(url);
            }
        });
    }


    public void verifyData() {
        try {
            CheckUtils.checkData(mOssService.getLocalPath(R.id.group_buy_image_iv), findViewById(R.id.group_buy_image_ll), "请选择图片");
            CheckUtils.checkData(group_buy_name_et, "请输入套餐名称");
            CheckUtils.checkData(group_buy_content_tv, "请选择套餐内容");
            CheckUtils.checkData(buy_price_et, "请输入团购价格");
            boolean isOk = Double.parseDouble(buy_price_et.getText().toString()) >= Double.parseDouble(original_price_tv.getText().toString());
            CheckUtils.checkData(isOk, buy_price_et, "团购价必须小于原价");
            CheckUtils.checkData(buy_limit_et, "请输入购买限制");
            CheckUtils.checkData(stock_balance_et, "请输入库存");
            CheckUtils.checkData(use_limit_num_et, "请输入使用限制");
            CheckUtils.checkData(start_date_tv, "请输入消费日期");
            CheckUtils.checkData(end_date_tv, "请输入消费日期");
            CheckUtils.checkData(start_time_tv, "请输入消费时间");
            CheckUtils.checkData(end_time_tv, "请输入消费时间");
            CheckUtils.checkData(getAvailableDateIdStr(), weeks_layout, "请选择可用时段");
            CheckUtils.checkData(appointment_tv, "请选择预约方式");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }
        doUploadPictures();
    }

    public void submitData(String imagePath) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("title", group_buy_name_et.getText().toString());
        tokenMap.put("buyPrice", buy_price_et.getText().toString());
        tokenMap.put("originalPrice", original_price_tv.getText().toString());
        tokenMap.put("appointment", CommonUtil.getAppointmentId(appointment_tv.getText().toString()));
        tokenMap.put("dateStart", TimeUtil.date2TimeStamp(start_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("dateEnd", TimeUtil.date2TimeStamp(end_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("timeStart", startTimeSecond + "");
        tokenMap.put("timeEnd", endTimeSecond + "");
        tokenMap.put("groupBuyImg", imagePath);
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("buyPersonLimit", buy_limit_et.getText().toString());
        tokenMap.put("stockCount", stock_balance_et.getText().toString());
        tokenMap.put("onceCount", use_limit_num_et.getText().toString());
        tokenMap.put("availableDate", getAvailableDateIdStr());
        tokenMap.put("dinersNumberId", getDinnerNumberId(eat_person_num_tv.getText().toString()) + "");
        tokenMap.put("itemList", groupBuyContent);
        tokenMap.put("state", "1");
        Log.d(TAG, tokenMap.toString());
        addDisposableIoMain(couponService.addGroupBuyCoupon(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity, "成功添加团购券！", (View v) -> {
                    Intent intent = new Intent();
                    intent.putExtra("isAdd", true);
                    setResult(RESULT_OK, intent);
                    finish();
                });
            }
        }, (Throwable throwable) -> {
            hideAlertDialog();
            showToast("添加团购券失败");
        });
    }

    /**
     * 处理点击事件
     *
     * @param view
     */
    @OnClick({R.id.submit_btn, R.id.eat_person_num_ll, R.id.appointment_tv, R.id.group_buy_content_tv, R.id.group_buy_content_count_tv,
            R.id.start_date_tv, R.id.end_date_tv, R.id.start_time_tv, R.id.end_time_tv, R.id.group_buy_image_iv})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
            case R.id.eat_person_num_ll:
                showDinnerNumberPicker();
                break;
            case R.id.appointment_tv:
                DialogController.showMenuList(new String[]{"免预约", "电话预约"}, mActivity, (int index) -> {
                    appointment_tv.setText(new String[]{"免预约", "电话预约"}[index]);
                });
                break;
            case R.id.group_buy_content_tv:
            case R.id.group_buy_content_count_tv:
                startActivity(new Intent(mActivity, GoodsContentChooseActivity.class));
                break;
            case R.id.start_date_tv:
                onPickDateEvent((TextView) view, new Date(), "无效日期");
                break;
            case R.id.end_date_tv:
                if (TextUtils.isEmpty(start_date_tv.getText().toString())) {
                    showToast("请先选择开始消费日期！");
                    return;
                }
                String startDate = start_date_tv.getText().toString();
                try {
                    Date parse = formatDate.parse(startDate);
                    onPickDateEvent((TextView) view, parse, "必须小于开始日期");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.start_time_tv:
                timePicker(view, startTimeFlag);
                break;
            case R.id.end_time_tv:
                if (TextUtils.isEmpty(start_time_tv.getText().toString())) {
                    showToast("请先选择开始消费时间！");
                    return;
                }
                timePicker(view, endTimeFlag);
                break;
            case R.id.group_buy_image_iv:
                if (choosePictureController != null) {
                    choosePictureController.clearBeforeChoosePictures();
                    choosePictureController.handleToChooseCropPictures();
                }
                break;
        }
    }

    public void timePicker(final View view, final int flag) {
        int[] time = DateUtils.splitTextTime(view);
        DialogController.showTimeDialog(mActivity, time, view, (TimePicker picker, int hourOfDay, int minute) -> {
            TextView tv = (TextView) view;
            if (flag == startTimeFlag) {
                startTimeSecond = hourOfDay * 3600 + minute * 60;
                tv.setText(validate(hourOfDay) + ":" + validate(minute));
            } else if (flag == endTimeFlag) {
                endTimeSecond = hourOfDay * 3600 + minute * 60;
                tv.setText(validate(hourOfDay) + ":" + validate(minute));
            }
        });
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }


    /**
     * 展示用餐人数选择器
     */
    public void showDinnerNumberPicker() {
        addDisposableIoMain(couponService.getDinnerNumber(), new DefaultConsumer<List<EatPersonCountBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<EatPersonCountBean>> baseBean) {
                eatPersonCountBeanList = baseBean.getData();
                personArr = new String[baseBean.getData().size()];
                for (int i = 0; i < baseBean.getData().size(); i++) {
                    personArr[i] = baseBean.getData().get(i).getName();
                }
                DialogController.showMenuList(personArr, mActivity, (int which) -> {
                    eat_person_num_tv.setText(personArr[which]);
                });
            }
        });
    }


    /**
     * 获取用餐人数id
     *
     * @param name
     * @return
     */
    public long getDinnerNumberId(String name) {
        if (eatPersonCountBeanList == null) return -1L;
        for (EatPersonCountBean eatPersonCountBean : eatPersonCountBeanList) {
            if (eatPersonCountBean.getName().equals(name)) {
                return eatPersonCountBean.getId();
            }
        }
        return -1L;
    }

    public String getAvailableDateIdStr() {
        StringBuffer availableDateStr = new StringBuffer();
        if (week_1.isChecked() == true) {
            availableDateStr.append("1,");
        }
        if (week_2.isChecked() == true) {
            availableDateStr.append("2,");
        }
        if (week_3.isChecked() == true) {
            availableDateStr.append("3,");
        }
        if (week_4.isChecked() == true) {
            availableDateStr.append("4,");
        }
        if (week_5.isChecked() == true) {
            availableDateStr.append("5,");
        }
        if (week_6.isChecked() == true) {
            availableDateStr.append("6,");
        }
        if (week_7.isChecked() == true) {
            availableDateStr.append("7,");
        }
        String availableDate = null;
        String availableDateStr1 = availableDateStr.toString();
        if (availableDateStr1.length() > 0) {
            availableDate = availableDateStr.substring(0, availableDateStr1.length() - 1);
        }
        return availableDate;
    }

}
