package com.party.jackseller.uihome;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.base.CouponBaseActivity;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DateUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.TimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.qqtheme.framework.picker.OptionPicker;

public class AddReplaceMoneyCouponActivity extends CouponBaseActivity {

    @BindView(R.id.original_price_et)
    EditText original_price_et;
    @BindView(R.id.buy_price_et)
    EditText buy_price_et;
    @BindView(R.id.buy_limit_num_et)
    EditText buy_limit_num_et;
    @BindView(R.id.use_limit_num_et)
    EditText use_limit_num_et;
    @BindView(R.id.stock_balance_et)
    EditText stock_balance_et;
    @BindView(R.id.start_date_tv)
    TextView start_date_tv;
    @BindView(R.id.end_date_tv)
    TextView end_date_tv;
    @BindView(R.id.start_time_tv)
    TextView start_time_tv;
    @BindView(R.id.end_time_tv)
    TextView end_time_tv;
    @BindView(R.id.appointment_tv)
    TextView appointment_tv;
    @BindView(R.id.use_range_et)
    TextView use_range_et;

    @BindView(R.id.week_1)
    AppCompatCheckBox week_1;
    @BindView(R.id.week_2)
    AppCompatCheckBox week_2;
    @BindView(R.id.week_3)
    AppCompatCheckBox week_3;
    @BindView(R.id.week_4)
    AppCompatCheckBox week_4;
    @BindView(R.id.week_5)
    AppCompatCheckBox week_5;
    @BindView(R.id.week_6)
    AppCompatCheckBox week_6;
    @BindView(R.id.week_7)
    AppCompatCheckBox week_7;
    @BindView(R.id.shop_name)
    TextView shop_name;


    private SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    private long currShopId;
    private CouponService couponService;
    private int startTimeFlag = 1, endTimeFlag = 2;
    private int startTimeSecond, endTimeSecond;//开始时间秒数和结束时间秒数

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_replace_money_coupon);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("添加代金券");
        shop_name.setText(mApplication.getCurrShop().getShop_name());
        couponService = new CouponService(this);
        currShopId = mApplication.getCurrShopId();
    }

    public void onPickDateEvent(final TextView tView, final Date date, final String top) {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        selectedDate.setTime(date);
        new DatePickerDialog(mActivity, (DatePicker view, int year, int month, int day) -> {
            Date checkDate = null;
            String tempDate = year + "-" + (month + 1) + "-" + day;
            try {
                checkDate = formatDate.parse(tempDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date.compareTo(checkDate) == -1) {
                tView.setText(tempDate);
                return;
            }
            showToast(top);
        }
                // 设置初始时间
                , selectedDate.get(Calendar.YEAR)
                , selectedDate.get(Calendar.MONTH)
                , selectedDate.get(Calendar.DATE) + 1
        ).show();
    }

    public void timePicker(final View view, final int flag) {
        int[] time = DateUtils.splitTextTime(view);
        DialogController.showTimeDialog(mActivity, time, view, (TimePicker picker, int hourOfDay, int minute) -> {
            TextView tv = (TextView) view;
            if (flag == startTimeFlag) {
                startTimeSecond = hourOfDay * 3600 + minute * 60;
                tv.setText(validate(hourOfDay) + ":" + validate(minute));
            } else if (flag == endTimeFlag) {
                endTimeSecond = hourOfDay * 3600 + minute * 60;
                tv.setText(validate(hourOfDay) + ":" + validate(minute));
            }
        });
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }

    private static final String TAG = "AddReplaceMoneyCouponAc";

    /**
     * 提交数据
     */
    public void submitData() {
        showAlertDialog("正在上传中...");
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("shopId", currShopId + "");
        tokenMap.put("originalPrice", original_price_et.getText().toString());
        tokenMap.put("buyPrice", buy_price_et.getText().toString());
        tokenMap.put("appointment", CommonUtil.getAppointmentId(appointment_tv.getText().toString()));
        tokenMap.put("dateStart", TimeUtil.date2TimeStamp(start_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("dateEnd", TimeUtil.date2TimeStamp(end_date_tv.getText().toString(), "yyyy-MM-dd"));
        tokenMap.put("timeStart", startTimeSecond + "");
        tokenMap.put("timeEnd", endTimeSecond + "");
        tokenMap.put("buyPersonLimit", buy_limit_num_et.getText().toString());
        tokenMap.put("stockCount", stock_balance_et.getText().toString());
        tokenMap.put("onceCount", use_limit_num_et.getText().toString());
        tokenMap.put("availableDate", getAvailableDateIdStr());
        tokenMap.put("goodsRange", use_range_et.getText().toString());
        Log.d(TAG, tokenMap.toString());
        addDisposableIoMain(couponService.addReplaceMoneyCoupon(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showConfirmDialog(mActivity, "成功添加代金券！", (View v) -> {
                    Intent intent = new Intent();
                    intent.putExtra("isAdd", true);
                    setResult(RESULT_OK, intent);
                    finish();
                });
            }
        });
    }

    /**
     * 处理加减点击事件
     * @param view
     */
    @OnClick({R.id.start_date_tv, R.id.end_date_tv, R.id.start_time_tv, R.id.end_time_tv,
            R.id.appointment_tv, R.id.submit_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
            case R.id.start_date_tv:
                onPickDateEvent((TextView) view, new Date(), "大于今天");
                break;
            case R.id.end_date_tv:
                if (TextUtils.isEmpty(start_date_tv.getText().toString())) {
                    showToast("请先选择开始消费日期！");
                    return;
                }
                String startDate = start_date_tv.getText().toString();
                try {
                    Date parse = formatDate.parse(startDate);
                    onPickDateEvent((TextView) view, parse, "结束消费日期必须大于开始消费日期");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.start_time_tv:
                timePicker(view, startTimeFlag);
                break;
            case R.id.end_time_tv:
                timePicker(view, endTimeFlag);
                break;
            case R.id.appointment_tv:
                OptionPicker picker = new OptionPicker(mActivity, new String[]{"免预约", "电话预约"});
                picker.setOffset(4);
                picker.setTextSize(20);
                picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
                    @Override
                    public void onOptionPicked(int index, String item) {
                        appointment_tv.setText(item);
                    }
                });
                picker.show();
        }
    }

    public String getAvailableDateIdStr() {
        StringBuffer availableDateStr = new StringBuffer();
        if (week_1.isChecked() == true) {
            availableDateStr.append("1,");
        }
        if (week_2.isChecked() == true) {
            availableDateStr.append("2,");
        }
        if (week_3.isChecked() == true) {
            availableDateStr.append("3,");
        }
        if (week_4.isChecked() == true) {
            availableDateStr.append("4,");
        }
        if (week_5.isChecked() == true) {
            availableDateStr.append("5,");
        }
        if (week_6.isChecked() == true) {
            availableDateStr.append("6,");
        }
        if (week_7.isChecked() == true) {
            availableDateStr.append("7,");
        }
        String availableDateStr1 = availableDateStr.toString();
        String availableDate = null;
        if (availableDateStr1.length() > 0) {
            availableDate = availableDateStr.substring(0, availableDateStr1.length() - 1);
        }
        return availableDate;
    }

    public void verifyData() {
        try {
            CheckUtils.checkMoney(original_price_et, "请输入面值", 1);
            CheckUtils.checkMoney(buy_price_et, "请输入购买价", 1);
            CheckUtils.checkMoney(original_price_et, buy_price_et, buy_price_et, "购买价太高了");
            CheckUtils.checkData(buy_limit_num_et, "请输入每人购买限制数量");
            CheckUtils.checkData(use_limit_num_et, "请输入每次最大使用数量");
            CheckUtils.checkData(stock_balance_et, "请输入库存量");
            CheckUtils.checkData(start_date_tv, "请输入消费日期");
            CheckUtils.checkData(end_date_tv, "请输入消费日期");
            CheckUtils.checkData(start_time_tv, "请输入消费时间");
            CheckUtils.checkData(end_time_tv, "请输入消费时间");
            CheckUtils.checkData(getAvailableDateIdStr(), findViewById(R.id.radio_layout), "请选择预约方式");
            CheckUtils.checkData(appointment_tv, "请选择预约方式");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }
        submitData();
    }

}
