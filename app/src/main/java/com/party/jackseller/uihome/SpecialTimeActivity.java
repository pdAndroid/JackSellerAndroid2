package com.party.jackseller.uihome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.adapter.SpecialTimeAdapter;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.EmployeeService;
import com.party.jackseller.bean.AddShopReabteDiscountBean;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.DelShopRebateDiscountBean;
import com.party.jackseller.bean.WeekRebateRateBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 */

public class SpecialTimeActivity extends BaseActivity {
    @BindView(R.id.iv_special_time_add)
    ImageView ivSpecialTimeAdd;
    @BindView(R.id.rv_special_time)
    RecyclerView rvSpecialTime;
    @BindView(R.id.btn_special_time_save)
    Button btnSpecialTimeSave;
    @BindView(R.id.iv_special_time_return)
    ImageView ivSpecialTimeReturn;

    private boolean isShow;
    private SpecialTimeAdapter adapter;
    private EmployeeService employeeService;
    private String shopId;
    private String startTimeAxis;
    private String endTimeAxis;
    private int discount;
    private List<WeekRebateRateBean> itemList;
    private List<DelShopRebateDiscountBean> discountBeans;
    private List<AddShopReabteDiscountBean> addShopReabteDiscountBeans;
    private String id;
    private List<WeekRebateRateBean> datas;
    private boolean isDelete = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_time);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        employeeService = new EmployeeService(this);
        itemList = new ArrayList<>();

        shopId = mApplication.getCurrShopId() + "";
        adapter = new SpecialTimeAdapter(mActivity, itemList);
        rvSpecialTime.setLayoutManager(new LinearLayoutManager(this));
        rvSpecialTime.setAdapter(adapter);
    }

    private void requestData() {
        addDisposableIoMain(employeeService.listWeekRebateRate(shopId), new DefaultConsumer<List<WeekRebateRateBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<WeekRebateRateBean>> data) {
                adapter.clearData();
                adapter.addData(data.getData());
            }
        });
    }

    @OnClick({R.id.iv_special_time_return, R.id.iv_special_time_add, R.id.btn_special_time_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_special_time_return:
                finish();
                break;
            case R.id.iv_special_time_add:
                adapter.addItem();
                break;
//            case R.id.btn_special_time_delete:
//                if (!isDelete) {
//                    btnSpecialTimeDelete.setText("取消删除");
//                    isDelete = !isDelete;
//                } else {
//                    btnSpecialTimeDelete.setText("删除");
//                    isDelete = !isDelete;
//                }
//                adapter.changeDelVisibe();
//                adapter.notifyDataSetChanged();
//                isShow = !isShow;
//                if (isShow) {
//                    btnSpecialTimeDelete.setText("取消删除");
//                } else {
//                    btnSpecialTimeDelete.setText("删除");
//                }
//                //为自定义方法-控制另外一个变量
//                btnSpecialTimeDelete.setVisibility(View.VISIBLE);
//                List<WeekRebateRateBean> data = adapter.getDatas();
//                String jsonDatas = JSON.toJSONString(data);
//                addDisposableIoMain(employeeService.delShopRebateDiscount(id), new DefaultConsumer<List<DelShopRebateDiscountBean>>(mApplication) {
//                    @Override
//                    public void operateSuccess(BaseResult<List<DelShopRebateDiscountBean>> data) {
//                        List<DelShopRebateDiscountBean> bean = data.getData();
//                        if (data.getCode() == 200) {
//                            discountBeans.clear();
//                            discountBeans.addAll(data.getData());
//                        } else {
//                            Toast.makeText(SpecialTimeActivity.this, data.getMessage() + "", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                });
//                break;
            case R.id.btn_special_time_save:
                try {
                    datas = adapter.getCommitDatas();
                } catch (Exception e) {
                    showToast(e.getMessage());
                    return;
                }
                String rebateDiscounts = JSON.toJSONString(datas);
                addDisposableIoMain(employeeService.addShopRebateDiscount(rebateDiscounts), new DefaultConsumer<String>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<String> data) {
                        String data1 = data.getData();
                        showToast(data1);
                        finish();
                    }
                });
                break;
        }
    }
}
