package com.party.jackseller.uihome;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.geocoder.StreetNumber;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.bean.ShopCategory;
import com.party.jackseller.controller.AMapController;
import com.party.jackseller.utils.AliOssService;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.picker.AddressPickTask;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import cn.qqtheme.framework.entity.City;
import cn.qqtheme.framework.entity.County;
import cn.qqtheme.framework.entity.Province;

import static io.reactivex.internal.subscriptions.SubscriptionHelper.validate;

public class AddShopActivity extends BaseActivityTitle {

    @BindView(R.id.shop_image_iv)
    ImageView shop_image_iv;
    @BindView(R.id.scroll_layout)
    ScrollView mScrollView;
    @BindView(R.id.id_card_front_iv)
    ImageView id_card_front_iv;
    @BindView(R.id.id_card_behind_iv)
    ImageView id_card_behind_iv;
    @BindView(R.id.business_licence_iv)
    ImageView business_licence_iv;
    @BindView(R.id.other_iv)
    ImageView other_iv;
    @BindView(R.id.shop_name_et)
    EditText shop_name_et;
    @BindView(R.id.category_spinner)
    Spinner category_spinner;
    @BindView(R.id.shop_label_1_et)
    EditText shop_label_1_et;
    @BindView(R.id.shop_label_2_et)
    EditText shop_label_2_et;
    @BindView(R.id.shop_label_3_et)
    EditText shop_label_3_et;
    @BindView(R.id.start_time_tv)
    TextView start_time_tv;
    @BindView(R.id.end_time_tv)
    TextView end_time_tv;
    @BindView(R.id.address_tv)
    TextView address_tv;

    @BindView(R.id.address_detail_tv)
    EditText address_detail_tv;

    @BindView(R.id.map_location)
    TextView map_location;


    @BindView(R.id.longitude_tv)
    TextView longitude_tv;
    @BindView(R.id.latitude_tv)
    TextView latitude_tv;

    @BindView(R.id.symbol_build_et)
    EditText symbol_build_et;
    @BindView(R.id.shop_phone_et)
    EditText shop_phone_et;
    @BindView(R.id.boss_phone_et)
    EditText boss_phone_et;
    @BindView(R.id.id_card_et)
    EditText id_card_et;

    private ImageView mImageView;
    private ShopService shopService;
    private static final String TAG = "ClaimShopActivity";
    private int currPosition = 0;
    private AMapController mapController;
    private String areaId;
    private List<ShopCategory> dataList = new ArrayList<>();
    private ArrayAdapter<ShopCategory> baseRoledapter;
    private boolean isConfirmCurrShopCategory = false;
    RequestOptions requestOptions = new RequestOptions();
    private int startTimeSecond, endTimeSecond;//开始时间秒数和结束时间秒数
    private int startTimeFlag = 1, endTimeFlag = 2;
    private double longitude, latitude;
    private AliOssService mOssService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("商家入驻");
        mOssService = new AliOssService(this);
        Utils.setIDCardKeyListener(id_card_et);
        requestOptions
                .placeholder(R.color.color_b2b2b2)
                .error(R.color.color_8a8a8a)
//                .centerCrop()
                .skipMemoryCache(true)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        shopService = new ShopService(this);
        mapController = new AMapController(mActivity);
        getCategoryList();
    }

    /**
     * 长按删除掉之前选中的图片
     *
     * @param view
     * @return
     */
    @OnLongClick({R.id.id_card_behind_iv, R.id.id_card_front_iv, R.id.business_licence_iv, R.id.other_iv})
    public boolean deleteChoosePictures(final View view) {
        DialogController.showConfirmDialog(mActivity, "删除图片", (View v) -> {
            ImageView vv = (ImageView) view;
            vv.setImageBitmap(null);
        });
        return true;
    }

    public void timePicker(final View view, final int flag) {
        int[] time = getTextTime(view);
        DialogController.showTimeDialog(mActivity, time, view, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker time, int hourOfDay, int minute) {
                TextView tv = (TextView) view;
                if (flag == startTimeFlag) {
                    startTimeSecond = hourOfDay * 3600 + minute * 60;
                    tv.setText(validate(hourOfDay) + ":" + validate(minute));
                } else if (flag == endTimeFlag) {
                    endTimeSecond = hourOfDay * 3600 + minute * 60;
                    tv.setText(validate(hourOfDay) + ":" + validate(minute));
                }
            }
        });
    }

    private int[] getTextTime(View view) {
        int[] timeOfDay = {8, 0};
        String time = ((TextView) view).getText().toString();
        if (time.contains(":")) {
            String[] split = time.split(":");
            timeOfDay[0] = Integer.parseInt(split[0]);
            timeOfDay[1] = Integer.parseInt(split[1]);
        }
        return timeOfDay;
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        String imgPath = list.get(0).getImgPath();
        mOssService.putImagePath(mImageView.getId(), imgPath);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, imgPath, mImageView, requestOptions);
    }

    /**
     * 上传所有数据
     */
    public void submitData() {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("homeImg", mOssService.getOssUrl(R.id.shop_image_iv));
        tokenMap.put("categoryId", ((ShopCategory) category_spinner.getSelectedItem()).getId() + "");//分类id
        tokenMap.put("tab", getTabStr());
        tokenMap.put("bussOpen", startTimeSecond + "");
        tokenMap.put("bussClose", endTimeSecond + "");
        tokenMap.put("shopName", shop_name_et.getText().toString());
        tokenMap.put("ShopAddress", address_detail_tv.getText().toString());
        tokenMap.put("latitude", latitude + "");
        tokenMap.put("longitude", longitude + "");
        tokenMap.put("symbolBuild", symbol_build_et.getText().toString());
        tokenMap.put("areaId", areaId);//地域id
        tokenMap.put("shopPhone", shop_phone_et.getText().toString());
        tokenMap.put("idImg", mOssService.getOssUrl(R.id.id_card_front_iv) + "," + mOssService.getOssUrl(R.id.id_card_behind_iv));
        tokenMap.put("licenseImg", mOssService.getOssUrl(R.id.business_licence_iv));
        if (mOssService.getOssUrl(R.id.other_iv) != null)
            tokenMap.put("foodSafetyCertificateImg", mOssService.getOssUrl(R.id.other_iv));
        tokenMap.put("phone", boss_phone_et.getText().toString());
        tokenMap.put("IDCard", id_card_et.getText().toString());
        Log.d(TAG, tokenMap.toString());
        commitData(tokenMap);

    }

    private void commitData(Map<String, String> tokenMap) {
        addDisposableIoMain(shopService.addShop(tokenMap), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                showToast("添加失败");
                hideAlertDialog();
            }

            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity,
                        "申请成功",
                        "审核结果将以短信的形式通知您，请耐心等待！",
                        (View v) -> {
                            Intent intent = new Intent();
                            intent.putExtra("isAdd", true);
                            setResult(RESULT_OK, intent);
                            finish();
                        });
            }
        }, (Throwable throwable) -> {
            showToast("添加失败");
            hideAlertDialog();
        });
    }

    public void getCategoryList() {
        addDisposableIoMain(shopService.getShopCategoryList(), new DefaultConsumer<List<ShopCategory>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ShopCategory>> baseBean) {
                dataList.addAll(baseBean.getData());
                baseRoledapter = new ArrayAdapter<ShopCategory>(mActivity, R.layout.custom_spinner_style, dataList);
                baseRoledapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //加载适配器
                category_spinner.setAdapter(baseRoledapter);
                category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        isConfirmCurrShopCategory = true; //表示商铺分类已被用户点击过
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    /**
     * 获取标签组合字符串
     */
    public String getTabStr() {
        StringBuffer stringBuffer = new StringBuffer();
        if (!TextUtils.isEmpty(shop_label_1_et.getText().toString())) {
            stringBuffer.append(shop_label_1_et.getText().toString() + ",");
        }
        if (!TextUtils.isEmpty(shop_label_2_et.getText().toString())) {
            stringBuffer.append(shop_label_2_et.getText().toString() + ",");
        }
        if (!TextUtils.isEmpty(shop_label_3_et.getText().toString())) {
            stringBuffer.append(shop_label_3_et.getText().toString() + ",");
        }
        return stringBuffer.toString();
    }

    private String validateTime(int time) {
        return time < 10 ? "0" + time : "" + time;
    }


    public void handlerImage(View view) {
        mImageView = (ImageView) view;
        if (choosePictureController != null) {
            choosePictureController.clearBeforeChoosePictures();
            choosePictureController.handleToChooseCropPictures();
        }
    }

    @OnClick({R.id.locate_ll, R.id.start_time_tv, R.id.end_time_tv, R.id.address_rl,
            R.id.apply_enter_btn,})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.locate_ll:
                mapController.choosePosition(mActivity);
                break;
            case R.id.start_time_tv:
                timePicker(view, startTimeFlag);
                break;
            case R.id.end_time_tv:
                if (TextUtils.isEmpty(start_time_tv.getText().toString())) {
                    showToast("请先选择开始营业时间！");
                    return;
                }
                timePicker(view, endTimeFlag);
                break;
            case R.id.address_rl:
                addressPicker();
                break;
            case R.id.apply_enter_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
        }
    }

    public void addressPicker() {
        AddressPickTask task = new AddressPickTask(this);
        task.setHideProvince(false);
        task.setHideCounty(false);
        task.setCallback(new AddressPickTask.Callback() {
            @Override
            public void onAddressInitFailed() {
            }

            @Override
            public void onAddressPicked(Province province, City city, County county) {
                if (county == null) {
                    address_tv.setText(province.getAreaName() + " " + city.getAreaName());
                } else {
                    address_tv.setText(province.getAreaName() + " " + city.getAreaName() + " " + county.getAreaName());
                }
            }
        });
        task.execute("贵州", "毕节", "纳雍");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case ConstUtils.SEARCH_LOCATION_CODE:
                    PoiItem poiItem = data.getParcelableExtra("poiItem");
                    if (poiItem != null) {
                        longitude = poiItem.getLatLonPoint().getLongitude();
                        latitude = poiItem.getLatLonPoint().getLatitude();
                        areaId = poiItem.getAdCode();
                        getAddressByLatlng(poiItem.getLatLonPoint());
                    }
                    break;
            }
        }
    }

    public void scrollTo(View view) {
        int[] y = new int[2];
        shop_image_iv.getLocationInWindow(y);
        int[] y1 = new int[2];
        view.getLocationInWindow(y1); //获取在当前窗口内的绝对坐标
        mScrollView.scrollTo(0, y1[1] - y[1]);
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);//加载动画资源文件
        view.startAnimation(shake);
    }

    /**
     * 验证数据
     */
    public void verifyData() {
        try {
            CheckUtils.checkData(mOssService.getLocalPath(R.id.shop_image_iv), findViewById(R.id.shop_image_ll), "请上传店铺门店照片");
            CheckUtils.checkData(shop_name_et, "请输入商店名称");
            CheckUtils.checkData(isConfirmCurrShopCategory == false, category_spinner, "请确认商铺分类是否正确");

            if (TextUtils.isEmpty(shop_label_1_et.getText().toString()) && TextUtils.isEmpty(shop_label_2_et.getText().toString())
                    && TextUtils.isEmpty(shop_label_3_et.getText().toString())) {
                showToast("请输入至少一个商店标签");
                scrollTo(shop_label_1_et);
                return;
            }

            CheckUtils.checkData(start_time_tv, "请选择营业时间");
            CheckUtils.checkData(end_time_tv, "请选择营业时间");
            CheckUtils.checkData(address_tv, "请选择所在区域");
            CheckUtils.checkData(symbol_build_et, "请输入标志建筑");
            CheckUtils.checkData(map_location, "选择选择地图定位");
            CheckUtils.checkData(address_detail_tv, "请输入详细地址");
            CheckUtils.checkData(shop_phone_et, "请输入正确格式的店铺号码");
            CheckUtils.checkPhone(boss_phone_et, "请输入正确格式的个人号码");
            CheckUtils.checkIDCard(id_card_et, "输入的身份证号格式有误");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.id_card_front_iv), findViewById(R.id.plus_3), "请上传身份证正面");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.id_card_behind_iv), findViewById(R.id.plus_4), "请上传身份证背面");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.business_licence_iv), findViewById(R.id.plus_1), "请上传营业执照");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }

        doUploadPictures();
    }

    /**
     * 上传图片操作
     */
    private void doUploadPictures() {
        showAlertDialog("正在处理...");
        mOssService.uploadImageAll(new AliOssService.UploadListener() {
            @Override
            public void handler(String message) {
                submitData();
            }
        });
    }


    private void getAddressByLatlng(LatLonPoint latLonPoint) {
        GeocodeSearch geocoderSearch = new GeocodeSearch(this);
        geocoderSearch.setOnGeocodeSearchListener(new GeocodeSearch.OnGeocodeSearchListener() {
            @Override
            public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
                StreetNumber streetNumber = regeocodeResult.getRegeocodeAddress().getStreetNumber();
                address_detail_tv.setText(streetNumber.getStreet() + streetNumber.getNumber());
                map_location.setText(streetNumber.getStreet() + streetNumber.getNumber());
            }

            @Override
            public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

            }
        });
        //逆地理编码查询条件：逆地理编码查询的地理坐标点、查询范围、坐标类型。
        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 500f, GeocodeSearch.AMAP);
        //异步查询
        geocoderSearch.getFromLocationAsyn(query);
    }

}
