package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.GoodsService;
import com.party.jackseller.base.CouponBaseActivity;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.GoodsCategory;
import com.party.jackseller.bean.GoodsUnit;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.utils.AliOssService;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.view.ActionSheetDialog;
import com.party.jackseller.widget.dialog.EditTextDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


public class AddGoodsActivity extends CouponBaseActivity {
    @BindView(R.id.goods_image_iv)
    ImageView goods_image_iv;
    @BindView(R.id.goods_name_et)
    EditText goods_name_et;
    @BindView(R.id.goods_price_et)
    EditText goods_price_et;
    @BindView(R.id.goods_unit_spinner)
    TextView goods_unit_spinner;
    @BindView(R.id.goods_desc_et)
    EditText goods_desc_et;
    @BindView(R.id.is_hot_cb)
    CheckBox is_hot_cb;
    @BindView(R.id.goods_category_ll)
    LinearLayout goods_category_ll;
    @BindView(R.id.good_category_tv)
    TextView good_category_tv;

    private GoodsService goodsService;
    private AliOssService mOssService;
    private List<GoodsCategory> goodsCategoryList = new ArrayList<>();
    private List<GoodsUnit> goodsUnitList = new ArrayList<>();
    private Long currSelectedGoodCategoryId, curGoodUnitId;
    RequestOptions requestOptions = new RequestOptions();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goods);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("新增商品");

        requestOptions
                .placeholder(R.color.color_b2b2b2)
                .error(R.color.color_8a8a8a)
                .skipMemoryCache(true)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        mOssService = new AliOssService(this);
        goodsService = new GoodsService(this);
        initGoodsUnitSpinner();
        getGoodsCategoryData();
    }

    /**
     * 初始化商品单位Spinner
     */
    public void initGoodsUnitSpinner() {
        addDisposableIoMain(goodsService.getGoodsUnit(mApplication.getCurrShopId()), new DefaultConsumer<List<GoodsUnit>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<GoodsUnit>> baseBean) {
                goodsUnitList.addAll(baseBean.getData());
            }
        });
    }


    /**
     * OSS上传图片第一步
     */
    private void doUploadPictures() {
        showAlertDialog();
        mOssService.uploadImageAll(new AliOssService.UploadListener() {
            @Override
            public void handler(String url) {
                submitData(url);
            }
        });
    }

    @OnClick({R.id.goods_image_ll, R.id.add_goods_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.goods_image_ll:
                if (choosePictureController != null) {
                    choosePictureController.clearBeforeChoosePictures();
                    choosePictureController.handleToChooseCropPictures();
                }
                break;
            case R.id.add_goods_btn:
                if (!CommonUtil.isFastClick()) {
                    verifyData();
                }
                break;
        }
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        String imagePath = list.get(0).getImgPath();
        mOssService.putImagePath(R.id.goods_image_iv, imagePath);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, imagePath, goods_image_iv);
    }


    /**
     * 获取商品分类数据
     */
    public void getGoodsCategoryData() {
        addDisposableIoMain(goodsService.getGoodsCategory(mApplication.getCurrShopId()), new DefaultConsumer<List<GoodsCategory>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<GoodsCategory>> baseBean) {
                goodsCategoryList.addAll(baseBean.getData());
            }
        });
    }

    ActionSheetDialog categoryDialog;

    @OnClick(R.id.good_category_tv)
    public void chooseGoodCategory(final View view) {
        hideSoftInput(mActivity, goods_name_et);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < goodsCategoryList.size(); i++) {
            list.add(goodsCategoryList.get(i).getName());
        }
        categoryDialog = DialogController.showMenuList(list, AddGoodsActivity.this,
                (int which) -> {
                    good_category_tv.setText(goodsCategoryList.get(which).getName());
                    currSelectedGoodCategoryId = goodsCategoryList.get(which).getId();
                }, "+ 添加",
                (View v) -> {
                    addCategoryDialog();
                });
    }

    @OnClick(R.id.goods_unit_spinner)
    public void chooseGoodUnit(final View view) {
        hideSoftInput(mActivity, goods_unit_spinner);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < goodsUnitList.size(); i++) {
            list.add(goodsUnitList.get(i).getName());
        }
        DialogController.showMenuList(list, AddGoodsActivity.this, (int which) -> {
            goods_unit_spinner.setText(goodsUnitList.get(which).getName());
            curGoodUnitId = goodsUnitList.get(which).getId();
        });
    }

    /**
     * 添加新分类
     *
     * @param newGoodCategoryName
     */
    public void addNewGoodsCategory(String newGoodCategoryName) {
        if (!TextUtils.isEmpty(newGoodCategoryName)) {
            addDisposableIoMain(goodsService.addGoodsType(mApplication.getCurrShopId(), newGoodCategoryName), new DefaultConsumer<GoodsCategory>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<GoodsCategory> baseBean) {
                    good_category_tv.setText(baseBean.getData().getName());
                    currSelectedGoodCategoryId = baseBean.getData().getId();
                    goodsCategoryList.add(goodsCategoryList.size() - 1, baseBean.getData());
                    categoryDialog.dismiss();
                }
            });
        } else {
            showToast("输入为空！");
        }

    }


    public void addCategoryDialog() {
        DialogController.showEditDialog(this, "输入分类", "", new EditTextDialog.DialogListener() {
            @Override
            public void cancel(EditTextDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void ok(EditTextDialog dialog, String name) {
                addNewGoodsCategory(name);
                dialog.dismiss();
            }
        });
    }

    /**
     * 提交数据前的判断
     */
    public void verifyData() {
        try {
            CheckUtils.checkData(goods_name_et, "请输入商品名称");
            CheckUtils.checkData(currSelectedGoodCategoryId == null, good_category_tv, "请选择商品分类");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.goods_image_iv), findViewById(R.id.goods_image_ll), "请上传商品图片");
            CheckUtils.checkData(goods_price_et, "请输入商品价格");
            CheckUtils.checkData(curGoodUnitId == null, goods_unit_spinner, "请选择商品单位");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }
        doUploadPictures();
    }

    public void submitData(String imagePath) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("title", goods_name_et.getText().toString());
        tokenMap.put("unitId", curGoodUnitId + "");
        tokenMap.put("price", goods_price_et.getText().toString());
        tokenMap.put("imgUrl", imagePath);
        tokenMap.put("describe", goods_desc_et.getText().toString());
        tokenMap.put("shopId", mApplication.getCurrShopId() + "");
        tokenMap.put("categoryId", currSelectedGoodCategoryId + "");
        tokenMap.put("isHot", (is_hot_cb.isChecked() ? 1 : 0) + "");

        addDisposableIoMain(goodsService.addGoods(tokenMap), new DefaultConsumer<Long>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<Long> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity, "成功添加商品！", (View v) -> {
                    Intent intent = new Intent();
                    intent.putExtra("isAdd", true);
                    setResult(RESULT_OK, intent);
                    finish();
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("isAdd", true);
        setResult(RESULT_OK, intent);
    }
}
