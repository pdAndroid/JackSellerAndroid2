package com.party.jackseller.uihome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ScanSuccessBean;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import butterknife.BindView;
import butterknife.OnClick;


public class VerifyResultActivity extends BaseActivityTitle {

    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.goods_name_tv)
    TextView goods_name_tv;
    @BindView(R.id.buy_price_tv)
    TextView buy_price_tv;
    @BindView(R.id.whole_layout)
    ViewGroup whole_layout;

    private String orderCode;
    private CouponService couponService;
    private ScanSuccessBean scanSuccessBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_result);
        initData();
        Bundle bundle = getIntent().getExtras();
        orderCode = bundle.getString(CodeUtils.RESULT_STRING);
    }

    protected void initData() {
        handleTitle();
        couponService = new CouponService(this);
        setMiddleText("验证结果");
        helper = new LoadViewHelper(whole_layout);
        helper.showLoading("正在验证中。。。");
        loadData();
    }

    public void loadData() {
        String str = orderCode;
        addDisposableIoMain(couponService.verifyCoupon(orderCode), new DefaultConsumer<ScanSuccessBean>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
            }

            @Override
            public void operateSuccess(BaseResult<ScanSuccessBean> baseBean) {
                scanSuccessBean = baseBean.getData();
                shop_name_tv.setText(baseBean.getData().getShopName());
                goods_name_tv.setText(baseBean.getData().getItemName());
                buy_price_tv.setText("¥" + baseBean.getData().getPrice());
                helper.showContent();
            }
        });
    }

    @OnClick(R.id.ok_btn)
    public void handleClickSth(View view) {
        showAlertDialog();
        confirmConsume();
    }

    /**
     * 确认消费
     */
    public void confirmConsume() {
        addDisposableIoMain(couponService.confirmConsume(scanSuccessBean.getId() + ""), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                hideAlertDialog();
                DialogController.showConfirmDialog(mActivity, "核销成功", (View v) -> {
                            finish();
                        }
                );
            }
        });
    }
}
