package com.party.jackseller.uihome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.BaseFragment;
import com.party.jackseller.Constant;
import com.party.jackseller.R;
import com.party.jackseller.bean.Cart;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.State;
import com.party.jackseller.fragment.GoodsCartFragment;
import com.party.jackseller.fragment.GoodsMenuFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class GoodsContentChooseActivity extends BaseActivityTitle {
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.viewpager)

    public ViewPager mViewPager;
    MyAdapter mAddGoodsAdapter;
    private List<State> mList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_content_choose);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("套餐内容");
        setNeedOnCreateRegister();
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mAddGoodsAdapter = new MyAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAddGoodsAdapter);
        getNeeData();
    }


    private void getNeeData() {
        mList.add(new State(1, "我的商品", GoodsCartFragment.getInstance()));
        mList.add(new State(2, "商品管理", GoodsMenuFragment.getInstance()));

        mAddGoodsAdapter.setList(mList);
        mAddGoodsAdapter.notifyDataSetChanged();
        mTabLayout.setupWithViewPager(mViewPager);
    }

    public void notifyCartDataSetChanged() {
        ((GoodsCartFragment) mList.get(0).getFragment()).notifyDataSetChanged();
        ((GoodsMenuFragment) mList.get(1).getFragment()).notifyDataSetChanged();
    }

    class MyAdapter extends FragmentPagerAdapter {
        private List<State> mList = new ArrayList<>();
        private List<BaseFragment> fragmentList = new ArrayList<>();

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setList(List<State> mList) {
            this.mList.addAll(mList);
            int size = this.mList.size();
            for (int i = 0; i < size; i++) {
                fragmentList.add(i, mList.get(i).getFragment());
            }
            notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mList.get(position).getName();
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mList.size();
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0);
            return;
        }
        super.onBackPressed();
    }

    public Cart getCart() {
        return Constant.CART;
    }

    public List<Goods> getGoodsList() {
        return getCart().getGoodsList();
    }


    public void seeMyGoods() {
        mTabLayout.getTabAt(0).select();
    }
}
