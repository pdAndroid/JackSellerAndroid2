package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.SpellingGroupCoupon;
import com.party.jackseller.view.loadviewhelper.help.OnLoadViewListener;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

//拼团卷
public class SpellingGroupCouponListActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.add_coupon_btn)
    Button add_coupon_btn;

    private List<SpellingGroupCoupon> dataList = new ArrayList<>();
    private CouponService couponService;
    private CommonAdapter<SpellingGroupCoupon> commonAdapter;
    private int currPage = 1;
    private boolean noMoreData = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spelling_group_coupon_list);
        initData();
    }

    protected void initData() {
        handleTitle();
        setMiddleText("拼团券");
        couponService = new CouponService(this);
        helper = new LoadViewHelper(refresh_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        initView();
        helper.showLoading();
        loadData();
    }

    public void initView() {
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<SpellingGroupCoupon>(this, R.layout.item_spelling_group_coupon, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final SpellingGroupCoupon item, int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, item.getGroupBuyImg(), (ImageView) viewHolder.getView(R.id.coupon_iv));
                viewHolder.setText(R.id.coupon_name, item.getTitle());
                viewHolder.setText(R.id.shop_name, item.getShopName());
                viewHolder.setText(R.id.buy_price_tv, "¥" + item.getBuyPrice());
                viewHolder.setText(R.id.original_price_tv, "门店价：¥" + item.getOriginalPrice());
            }
        };
        /*//添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this,R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);*/
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Intent intent = new Intent(mActivity, SpellingGroupCouponDetailActivity.class);
                intent.putExtra("spellingGroupCouponBean", dataList.get(position));
                startActivity(intent);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    public void loadData() {
        currPage = 1;
        addDisposableIoMain(couponService.getSpellingGroupCouponList(mApplication.getCurrShopId()), new DefaultConsumer<List<SpellingGroupCoupon>>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<SpellingGroupCoupon>> baseBean) {
                dataList.clear();
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty(getEmptyView());
                    if (add_coupon_btn.getVisibility() == View.VISIBLE) {
                        add_coupon_btn.setVisibility(View.GONE);
                    }
                } else {
                    if (baseBean.getData().size() < Constant.PAGE_SIZE) {
                        refresh_layout.setEnableLoadMore(false);
                    }
                    if (add_coupon_btn.getVisibility() == View.GONE) {
                        add_coupon_btn.setVisibility(View.VISIBLE);
                    }
                    refresh_layout.finishRefresh();
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showError();
            }
        });
    }

    /**
     * 上拉加载更多数据
     */
    public void loadMoreData() {
        currPage++;
        addDisposableIoMain(couponService.getSpellingGroupCouponList(mApplication.getCurrShopId()), new DefaultConsumer<List<SpellingGroupCoupon>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<SpellingGroupCoupon>> baseBean) {
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                    if (baseBean.getData().size() <= Constant.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    refresh_layout.finishLoadMoreWithNoMoreData();
                } else {
                    refresh_layout.finishLoadMore();
                }
            }
        });
    }

    @OnClick(R.id.add_coupon_btn)
    public void handleClickSth(View view) {
        startActivityForResult(new Intent(mActivity, AddSpellingGroupCouponActivity.class), Constant.REQUEST_CODE_ADD_SPELLING_GROUP);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Constant.REQUEST_CODE_ADD_SPELLING_GROUP == requestCode) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("isAdd", false) == true) {
                    helper.showLoading();
                    loadData();
                }
            }
        }
    }

    public View getEmptyView() {
        View emptyView = getLayoutInflater().inflate(R.layout.load_coupon_empty, null);
        ImageView empty_iv = emptyView.findViewById(R.id.empty_iv);
        TextView empty_tv = emptyView.findViewById(R.id.empty_tv);
        Button add_coupon_btn = emptyView.findViewById(R.id.add_coupon_btn);
        add_coupon_btn.setText("添加拼团券");
        add_coupon_btn.setOnClickListener((View v) -> {
            startActivityForResult(new Intent(mActivity, AddSpellingGroupCouponActivity.class), Constant.REQUEST_CODE_ADD_SPELLING_GROUP);
        });
        Glide.with(mActivity).load(R.drawable.no_replace_money_coupon).into(empty_iv);
        empty_tv.setText("没有发布的拼团券");
        return emptyView;
    }
}
