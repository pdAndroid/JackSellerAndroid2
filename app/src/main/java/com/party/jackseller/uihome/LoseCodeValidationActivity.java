package com.party.jackseller.uihome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.R;
import com.party.jackseller.api.CouponService;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.VerifyCouponsBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 南宫灬绝痕 on 2018/12/10.
 * 输码验证-验证
 */

public class LoseCodeValidationActivity extends BaseActivityTitle {
    @BindView(R.id.iv_lose_code_validation_success)
    ImageView ivLoseCodeValidationSuccess;
    @BindView(R.id.tv_lose_code_validation_information)
    TextView tvLoseCodeValidationInformation;
    @BindView(R.id.tv_lose_code_validation_name)
    TextView tvLoseCodeValidationName;
    @BindView(R.id.tv_lose_code_validation_number)
    TextView tvLoseCodeValidationNumber;
    @BindView(R.id.tv_lose_code_validation_price)
    TextView tvLoseCodeValidationPrice;
    @BindView(R.id.btn_lose_code_validation_return)
    Button btnLoseCodeValidationReturn;
    @BindView(R.id.btn_lose_code_validation_consume)
    Button btnLoseCodeValidationConsume;

    private CouponService couponService;
    private String orderCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lose_code_validation);
        ButterKnife.bind(this);
        initView();
        loadData();
    }

    private void initView() {
        handleTitle();
        setMiddleText("输码验证");
        couponService = new CouponService(this);
    }

    private void loadData() {
//        if (orderCode){
//
//        }
        addDisposableIoMain(couponService.verifyCoupons(orderCode), new DefaultConsumer<VerifyCouponsBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<VerifyCouponsBean> baseBean) {
                Log.e("", baseBean + "");
//                deliciousFoodList.clear();
//                List<DeliciousFood> data = baseBean.getData();
//                if (data != null && data.size() > 0) {
//                    deliciousFoodList.addAll(data);
//                }
//                commonAdapter.notifyDataSetChanged();
//                mRefreshLayout.finishRefresh();
            }
        });
    }
}
