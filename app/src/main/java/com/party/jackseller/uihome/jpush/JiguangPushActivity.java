package com.party.jackseller.uihome.jpush;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.utils.SPUtils;
import com.party.jackseller.utils.VoiceUtils.VoiceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;

/**
 */

public class JiguangPushActivity extends BaseActivity {
    @BindView(R.id.iv_return)
    ImageView ivReturn;
    @BindView(R.id.sw_button)
    CheckBox swButton;
    boolean record_toggle = false;
    final int open = 1, close = 0;

    Handler recordHandle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case open:
                    VoiceUtils.with(mActivity).receiptPlay("open");
                    break;
            }
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jiguang_push_activity);
        ButterKnife.bind(this);
        record_toggle = (boolean) SPUtils.get(this, MyReceiver.key, false);
        swButton.setChecked(record_toggle);
        setTextData();
    }

    public void setTextData() {
        if (swButton.isChecked()) {
            swButton.setText("关闭播报");
        } else {
            swButton.setText("开启播报");
        }
    }


    @OnClick({R.id.iv_return, R.id.sw_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_return:
                finish();
                break;
            case R.id.sw_button:
                if (swButton.isChecked()) {
                    swButton.setText("关闭播报");
                    recordHandle.sendEmptyMessage(open);
                    showToast("开启语音播报");
                } else {
                    swButton.setText("开启播报");
                    recordHandle.sendEmptyMessage(close);
                    showToast("关闭语音播报");
                }
                SPUtils.put(this, MyReceiver.key, swButton.isChecked());
                break;
        }
    }
}
