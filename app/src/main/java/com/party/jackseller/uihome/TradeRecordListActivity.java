package com.party.jackseller.uihome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.Constant;
import com.party.jackseller.R;
import com.party.jackseller.adapter.MyCommonAdapter;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.api.TradeRecordService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.TradeRecord;
import com.party.jackseller.uihome.jpush.JiguangPushActivity;
import com.party.jackseller.uihome.jpush.MyReceiver;
import com.party.jackseller.utils.CommonUtil;
import com.party.jackseller.view.loadviewhelper.help.OnLoadViewListener;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import io.reactivex.functions.Consumer;

/**
 * 交易流水
 */

public class TradeRecordListActivity extends BaseActivity {
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.iv_return)
    ImageView ivReturn;
    @BindView(R.id.tv_began_broadcast)
    TextView tvBeganBroadcast;

    private List<TradeRecord> dataList;
    private TradeRecordService tradeRecordService;
    private MyCommonAdapter<TradeRecord> commonAdapter;
    private int currPage = 1;
    private boolean noMoreData = false;
    private int i = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trade_record_list);
        ButterKnife.bind(this);
        initData();
    }

    protected void initData() {
//        voice = mApplication.getCurrShopId() + "";
//        JPushInterface.setAlias(this, 0, voice);
//        VoiceUtils.with(this).receiptPlay(voice);
        tradeRecordService = new TradeRecordService(this);
        helper = new LoadViewHelper(refresh_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        initView();
        helper.showLoading();
        loadData();
    }

    public void initView() {
        dataList = new ArrayList<>();
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);


        commonAdapter = new MyCommonAdapter<TradeRecord>(this, R.layout.item_trade_record, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final TradeRecord tradeRecord, int position) {
                viewHolder.setText(R.id.user_nickname_tv, tradeRecord.getTitle() + "");
                viewHolder.setText(R.id.user_user_tv, tradeRecord.getPayer() + "");
                viewHolder.setText(R.id.time_tv, CommonUtil.formatTimeText(tradeRecord.getCreate()));
                viewHolder.setText(R.id.money_tv, tradeRecord.getTotalPrice() + "");
                if (tradeRecord.getTradeRecordType() == 1) {
                    viewHolder.setText(R.id.consume_record_name_tv, "-" + tradeRecord.getValue());
                } else {
                    viewHolder.setText(R.id.consume_record_name_tv, "+" + tradeRecord.getValue());
                }

                viewHolder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mActivity, TradingParticularsActivity.class);
                        intent.putExtra("tradeRecordId", tradeRecord.getId() + "");
                        startActivity(intent);
                    }
                });
            }
        };

        MyReceiver.addAdapter(commonAdapter);

        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
    }

    public void loadData() {
        currPage = 1;
        addDisposableIoMain(tradeRecordService.getTradeRecordList(mApplication.getCurrShopId(), Constant.PAGE_SIZE, currPage), new DefaultConsumer<List<TradeRecord>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<TradeRecord>> baseBean) {
                dataList.clear();
                if (baseBean.getData().size() <= 0) {
                    helper.showEmpty();
                } else {
                    if (baseBean.getData().size() < Constant.PAGE_SIZE) {
                        refresh_layout.setEnableLoadMore(false);
                    }
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    refresh_layout.finishRefresh();
                    helper.showContent();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    /**
     * 上拉加载更多数据
     */
    public void loadMoreData() {
        currPage++;
        addDisposableIoMain(tradeRecordService.getTradeRecordList(mApplication.getCurrShopId(), Constant.PAGE_SIZE, currPage), new DefaultConsumer<List<TradeRecord>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<TradeRecord>> baseBean) {
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                    if (baseBean.getData().size() <= Constant.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    refresh_layout.finishLoadMoreWithNoMoreData();
                } else {
                    refresh_layout.finishLoadMore();
                }
            }
        });
    }


    @OnClick({R.id.iv_return, R.id.tv_began_broadcast})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_return:
                finish();
                break;
            case R.id.tv_began_broadcast:
                startActivity( JiguangPushActivity.class);
                break;
        }
    }
}
