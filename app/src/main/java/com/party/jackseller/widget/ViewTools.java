package com.party.jackseller.widget;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Map;

/**
 * Created by 派对 on 2018/7/22.
 */

public class ViewTools {

    public static int dp2px(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, context.getResources().getDisplayMetrics());
    }

    public static void moveToView(final ScrollView sv, final View v) {
        sv.post(new Runnable() {
            @Override
            public void run() {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
//                int offset = sv.getMeasuredHeight() - location[1];
//                if (offset < 0) {
//                    offset = 0;
//                }
                sv.smoothScrollTo(0, location[1]);
            }
        });
    }

    /**
     * 递归方式获取所有tag带参数的 view
     *
     * @param vg
     * @return
     */
    public static View getViewDataByTag(ViewGroup vg, Map<String, String> data) {
        int childCount = vg.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = vg.getChildAt(i);
            if (childAt instanceof ViewGroup) {
                View view = getViewDataByTag((ViewGroup) childAt, data);
                if (view != null) return view;
            } else {
                if (childAt instanceof TextView && childAt.getTag() != null) {
                    CharSequence text = ((TextView) childAt).getText();
                    if ("".equals(text.toString().trim())) {
                        return (View) childAt.getParent();
                    }
                    data.put(childAt.getTag().toString(), text.toString());
                }
            }
        }
        return null;
    }

}
