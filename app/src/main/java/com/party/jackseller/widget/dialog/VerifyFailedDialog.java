package com.party.jackseller.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.party.jackseller.R;
import com.party.jackseller.BaseActivityTitle;

import java.util.Timer;
import java.util.TimerTask;

/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class VerifyFailedDialog extends AlertDialog {

    private Context mContext;
    private View mView;
    private TextView message_tv;
    private int second = 5;
    private Handler handler;
    private Timer timer;

    public VerifyFailedDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    public VerifyFailedDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_verify_failed, null);
        setView(mView);
        message_tv = mView.findViewById(R.id.message_tv);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                message_tv.setText((--second)+"后返回首页！");
                if (second == 0){
                    VerifyFailedDialog.this.dismiss();
                    if (timer!=null){
                        timer.cancel();
                    }
                    ((BaseActivityTitle)context).finish();
                }
            }
        };

        this.setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        Message message = handler.obtainMessage();
                        handler.sendMessage(message);
                    }
                };
                timer.schedule(task,100,1000);
            }
        });
    }
}
