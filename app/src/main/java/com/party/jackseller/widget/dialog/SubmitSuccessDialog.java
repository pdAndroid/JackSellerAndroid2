package com.party.jackseller.widget.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.party.jackseller.R;


/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class SubmitSuccessDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView title_tv;
    private TextView message_tv;
    private TextView left_tv;
    private TextView right_tv;
    private DialogListener dialogListener;

    protected SubmitSuccessDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle);
    }

    protected SubmitSuccessDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_submit_success, null);
        setView(mView);
        right_tv = mView.findViewById(R.id.right_tv);
        left_tv = mView.findViewById(R.id.left_tv);
        title_tv = mView.findViewById(R.id.title_tv);
        message_tv = mView.findViewById(R.id.message_tv);
        left_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.clickLeft(SubmitSuccessDialog.this);
            }
        });
        right_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.clickRight(SubmitSuccessDialog.this);
            }
        });
    }

    public void setMessage(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        message_tv.setText(message);
    }

    public void setTitle(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        title_tv.setText(message);
    }


    public interface DialogListener {
        void clickLeft(SubmitSuccessDialog dialog);
        void clickRight(SubmitSuccessDialog dialog);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private int position;
        private DialogListener dialogListener;
        private CharSequence message;
        private CharSequence normalMessage;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(CharSequence message) {
            this.normalMessage = message;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setDialogListener(DialogListener dialogListener) {
            this.dialogListener = dialogListener;
            return this;
        }

        public Builder setPosition(int position) {
            this.position = position;
            return this;
        }

        public SubmitSuccessDialog create() {
            final SubmitSuccessDialog dialog = new SubmitSuccessDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setTitle(normalMessage);
            dialog.setMessage(message);
            dialog.setDialogListener(dialogListener);
            return dialog;
        }
    }

    public DialogListener getDialogListener() {
        return dialogListener;
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}
