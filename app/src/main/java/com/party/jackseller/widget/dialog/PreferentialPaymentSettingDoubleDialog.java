package com.party.jackseller.widget.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.R;

/**
 * Created by 南宫灬绝痕 on 2018/12/29.
 */

public class PreferentialPaymentSettingDoubleDialog extends AlertDialog {
    private Context context;
    private View view;
    private EditText et_preferential_payment_setting;
    private TextView tv_preferential_payment_setting_cancle;
    private TextView tv_preferential_payment_setting_confirm;
    private PreferentialPaymentSettingDialog preferentialPaymentSettingDialog;

    public PreferentialPaymentSettingDoubleDialog(Context context) {
        this(context, R.style.MyAlertDialogStyle2);
        this.context = context;
    }

    public PreferentialPaymentSettingDoubleDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public PreferentialPaymentSettingDoubleDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        initView();
    }

    public void setTextContent(String textContent) {
        et_preferential_payment_setting.setText(textContent);
    }


    public interface CommitInterface {
        void commit(String textContent);
    }

    PreferentialPaymentSettingDialog.CommitInterface mCommitInterface;

    public void setCommitInterface(PreferentialPaymentSettingDialog.CommitInterface commitInterface) {
        mCommitInterface = commitInterface;
    }


    private void initView() {

        view = LayoutInflater.from(context).inflate(R.layout.dialog_preferential_payment_setting, null);
        setView(view);
        et_preferential_payment_setting = view.findViewById(R.id.et_preferential_payment_setting);
        tv_preferential_payment_setting_cancle = view.findViewById(R.id.tv_preferential_payment_setting_cancle);
        tv_preferential_payment_setting_confirm = view.findViewById(R.id.tv_preferential_payment_setting_confirm);
        et_preferential_payment_setting.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_preferential_payment_setting.setMaxEms(2);

        tv_preferential_payment_setting_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        tv_preferential_payment_setting_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textContent = et_preferential_payment_setting.getText().toString();
                if (mCommitInterface != null) {
                    mCommitInterface.commit(textContent);
                }
                cancel();
            }
        });
    }
}
