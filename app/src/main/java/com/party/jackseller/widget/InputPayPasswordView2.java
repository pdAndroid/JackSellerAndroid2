package com.party.jackseller.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.base.MyTextWatcher;
import com.party.jackseller.utils.OnPasswordInputFinish;
import com.zyyoona7.popup.EasyPopup;

/**
 * Created by Administrator on 2018/8/24.
 */

public class InputPayPasswordView2 extends LinearLayout implements View.OnClickListener {

    private TextView[] tvList;
    private TextView[] tv;
    private ImageView iv_del;
    private View view;
    private String strPassword;
    private int currentIndex = -1;
    private TextView payTitleTv;

    public InputPayPasswordView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public static EasyPopup showPop(Activity activity, View view, OnPasswordInputFinish pass) {
        InputPayPasswordView passwordView = new InputPayPasswordView(activity);
        passwordView.setOnFinishInput(pass);
        EasyPopup apply =  EasyPopup.create(activity)
                .setContentView(passwordView)
                .setAnimationStyle(R.style.Popupwindow)
                .setBackgroundDimEnable(true)
                .setDimValue(0.4f)
                .setDimColor(Color.GRAY)
                .setFocusAndOutsideEnable(true)
                .setWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply();

        apply.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        return apply;
    }

    public InputPayPasswordView2(Context context) {
        super(context);
        view = View.inflate(context, R.layout.layout_input_pay_password, null);
        payTitleTv = (TextView) view.findViewById(R.id.pay_title);
        tvList = new TextView[6];
        tvList[0] = (TextView) view.findViewById(R.id.pay_box1);
        tvList[1] = (TextView) view.findViewById(R.id.pay_box2);
        tvList[2] = (TextView) view.findViewById(R.id.pay_box3);
        tvList[3] = (TextView) view.findViewById(R.id.pay_box4);
        tvList[4] = (TextView) view.findViewById(R.id.pay_box5);
        tvList[5] = (TextView) view.findViewById(R.id.pay_box6);
        tv = new TextView[10];
        tv[0] = (TextView) view.findViewById(R.id.pay_keyboard_zero);
        tv[1] = (TextView) view.findViewById(R.id.pay_keyboard_one);
        tv[2] = (TextView) view.findViewById(R.id.pay_keyboard_two);
        tv[3] = (TextView) view.findViewById(R.id.pay_keyboard_three);
        tv[4] = (TextView) view.findViewById(R.id.pay_keyboard_four);
        tv[5] = (TextView) view.findViewById(R.id.pay_keyboard_five);
        tv[6] = (TextView) view.findViewById(R.id.pay_keyboard_sex);
        tv[7] = (TextView) view.findViewById(R.id.pay_keyboard_seven);
        tv[8] = (TextView) view.findViewById(R.id.pay_keyboard_eight);
        tv[9] = (TextView) view.findViewById(R.id.pay_keyboard_nine);
        iv_del = (ImageView) view.findViewById(R.id.pay_keyboard_del);
        for (int i = 0; i < 10; i++) {
            tv[i].setOnClickListener(this);
        }
        iv_del.setOnClickListener(this);
        addView(view);
    }


    public void setOnFinishInput(final OnPasswordInputFinish pass) {
        tvList[5].addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1) {
                    strPassword = "";
                    for (int i = 0; i < 6; i++) {
                        strPassword += tvList[i].getText().toString().trim();
                    }
                    pass.inputFinish(strPassword);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_keyboard_one:
                getPass("1");
                break;
            case R.id.pay_keyboard_two:
                getPass("2");
                break;
            case R.id.pay_keyboard_three:
                getPass("3");
                break;
            case R.id.pay_keyboard_four:
                getPass("4");
                break;
            case R.id.pay_keyboard_five:
                getPass("5");
                break;
            case R.id.pay_keyboard_sex:
                getPass("6");
                break;
            case R.id.pay_keyboard_seven:
                getPass("7");
                break;
            case R.id.pay_keyboard_eight:
                getPass("8");
                break;
            case R.id.pay_keyboard_nine:
                getPass("9");
                break;
            case R.id.pay_keyboard_zero:
                getPass("0");
                break;
            case R.id.pay_keyboard_del:
                if (currentIndex - 1 >= -1) {
                    tvList[currentIndex--].setText("");
                }
                break;
        }
    }

    public void getPass(String str) {
        if (currentIndex >= -1 && currentIndex < 5) {
            tvList[++currentIndex].setText(str);
        }
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setPayTitle(String title) {
        payTitleTv.setText(title);
    }

    public void setReturn() {

    }
}
