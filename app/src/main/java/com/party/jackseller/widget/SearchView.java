package com.party.jackseller.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.party.jackseller.R;


/**
 * Created by whieenz on 2017/7/19.
 *
 */

public class SearchView extends LinearLayout implements View.OnClickListener {

    /**
     * 输入框 
     */
    private EditText etInput;
    private View searchResultListView;
    /**
     * 删除键 
     */
    private ImageView ivDelete;

    /**
     * 上下文对象 
     */
    private Context mContext;

    /**
     * 搜索回调接口 
     */
    private onSearchViewListener mListener;

    /**
     * 设置搜索回调接口 
     *
     * @param listener 监听者 
     */
    public void setSearchViewListener(onSearchViewListener listener) {
        mListener = listener;
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.layout_search_view, this);
        initViews();
    }

    private void initViews() {
        etInput = (EditText) findViewById(R.id.et_search_text);
        ivDelete = (ImageView) findViewById(R.id.imb_search_clear);
        ivDelete.setOnClickListener(this);
        etInput.addTextChangedListener(new EditChangedListener());
        etInput.setOnClickListener(this);
    }

    private class EditChangedListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }
        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imb_search_clear:
                etInput.setText("");
                if (mListener != null) {
                    mListener.onQueryTextChange("");
                }
                ivDelete.setVisibility(GONE);
                setSearchResultListViewGone(searchResultListView);
                break;
        }
    }
    public void setSearchResultListView(View view){
        searchResultListView = view;
    }
    /**
     * search view回调方法 
     */
    public interface onSearchViewListener {
        boolean onQueryTextChange(String text);
    }
    public void setEditorActionListener(EditText.OnEditorActionListener onEditorActionListener){
        etInput.setOnEditorActionListener(onEditorActionListener);
    }
    public void setEditChangedListener(TextWatcher textWatcher){
        etInput.addTextChangedListener(textWatcher);
    }
    public String getSearchText(){
        return etInput.getText().toString();
    }
    public void setSearchResultListViewGone(View view){
        view.setVisibility(View.GONE);
    }
    public ImageView getIvDelete(){
        return ivDelete;
    }
}