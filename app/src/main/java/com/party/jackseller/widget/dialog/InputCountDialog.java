package com.party.jackseller.widget.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.utils.MContextUtils;

/**
 * Created by fank on 2018/7/26.
 * 提示公共弹框
 */
public class InputCountDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView titleTV;
    private EditText contentTV;
    private TextView mCancelTV;
    private TextView mOkTV;
    private InputCountDialog.OkListener mOkListener;

    protected InputCountDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle);
    }

    protected InputCountDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_input_count, null);
        setView(mView);
        titleTV = mView.findViewById(R.id.tv_custom_reminder_title);
        contentTV = mView.findViewById(R.id.tv_custom_reminder_content);
        mCancelTV = mView.findViewById(R.id.tv_custom_reminder_cancel);
        mOkTV = mView.findViewById(R.id.tv_custom_reminder_ok);

        mCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mOkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftInput(MContextUtils.scanForActivity(getContext()), contentTV);
                dismiss();
                if (mOkListener != null) {
                    mOkListener.ok(contentTV.getText().toString().trim());
                }
            }
        });
    }

    public void setTitle(String mTitle) {
        if (titleTV != null && !TextUtils.isEmpty(mTitle)) {
            titleTV.setText(mTitle);
        }
    }

    public void setContent(String mContent) {
        if (contentTV != null && !TextUtils.isEmpty(mContent)) {
            contentTV.setText(mContent);
            contentTV.setSelection(mContent.length());
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param context
     * @param view
     */
    private void hideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setOkListener(OkListener okListener) {
        this.mOkListener = okListener;
    }

    public interface OkListener {
        void ok(String str);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private OkListener mOkListener;
        private String mTitle, mContent;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public InputCountDialog.Builder setTitle(String flag) {
            mTitle = flag;
            return this;
        }

        public InputCountDialog.Builder setContent(String flag) {
            mContent = flag;
            return this;
        }

        public InputCountDialog.Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public InputCountDialog.Builder setOkListener(InputCountDialog.OkListener okListener) {
            this.mOkListener = okListener;
            return this;
        }

        public InputCountDialog create() {
            final InputCountDialog dialog = new InputCountDialog(mContext);
            dialog.setTitle(mTitle);
            dialog.setContent(mContent);
            dialog.setCancelable(mCancelable);
            dialog.setOkListener(mOkListener);
            return dialog;
        }
    }
}
