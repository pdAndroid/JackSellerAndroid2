package com.party.jackseller.widget.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.MContextUtils;

/**
 * Created by fank on 2018/7/26.
 * 提示公共弹框
 */
public class EditTextDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView titleTV;
    private EditText employee_name_et;
    private TextView cancel_tv;
    private TextView ok_tv;
    private DialogListener dialogListener;

    protected EditTextDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle);
    }

    protected EditTextDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_edit, null);
        setView(mView);
        titleTV = mView.findViewById(R.id.tv_custom_reminder_title);
        employee_name_et = mView.findViewById(R.id.employee_name_et);
        cancel_tv = mView.findViewById(R.id.cancel_tv);
        ok_tv = mView.findViewById(R.id.ok_tv);

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.cancel(EditTextDialog.this);
            }
        });
        ok_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftInput(MContextUtils.scanForActivity(getContext()), employee_name_et);
                dialogListener.ok(EditTextDialog.this, employee_name_et.getText().toString().trim());
            }
        });
    }

    public void setTitle(String mTitle) {
        if (titleTV != null && !TextUtils.isEmpty(mTitle)) {
            titleTV.setText(mTitle);
        }
    }

    public void setContent(String mContent) {
        if (employee_name_et != null && !TextUtils.isEmpty(mContent)) {
            employee_name_et.setText(mContent);
            employee_name_et.setSelection(mContent.length());
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param context
     * @param view
     */
    private void hideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private DialogListener dialogListener;
        private String mTitle, mContent;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(String flag) {
            mTitle = flag;
            return this;
        }

        public Builder setContent(String flag) {
            mContent = flag;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setDialogListener(DialogListener dialogListener) {
            this.dialogListener = dialogListener;
            return this;
        }

        public EditTextDialog create() {
            final EditTextDialog dialog = new EditTextDialog(mContext);
            dialog.setTitle(mTitle);
            dialog.setContent(mContent);
            dialog.setCancelable(mCancelable);
            dialog.setDialogListener(dialogListener);
            return dialog;
        }
    }

    public interface DialogListener {
        void cancel(EditTextDialog dialog);

        void ok(EditTextDialog dialog, String name);
    }

    public static class MyDialogListener implements DialogListener {

        @Override
        public void cancel(EditTextDialog dialog) {
            dialog.dismiss();
        }

        @Override
        public void ok(EditTextDialog dialog, String name) {
            dialog.dismiss();
        }
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}
