package com.party.jackseller.widget.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.party.jackseller.R;


/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class AutoDismissMessageDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView title_tv;
    private TextView message_tv;

    protected AutoDismissMessageDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected AutoDismissMessageDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_auto_dismiss_message, null);
        setView(mView);
        title_tv = mView.findViewById(R.id.title_tv);
        message_tv = mView.findViewById(R.id.message_tv);
    }

    public void setMessage(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        message_tv.setText(message);
    }

    public void setTitle(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        title_tv.setText(message);
    }


    public interface DialogListener {
        void clickLeft(AutoDismissMessageDialog dialog);
        void clickRight(AutoDismissMessageDialog dialog);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private OnDismissListener dismissListener;
        private CharSequence message;
        private CharSequence normalMessage;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(CharSequence message) {
            this.normalMessage = message;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setDismissListener(OnDismissListener dismissListener) {
            this.dismissListener = dismissListener;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public AutoDismissMessageDialog create() {
            final AutoDismissMessageDialog dialog = new AutoDismissMessageDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setTitle(normalMessage);
            dialog.setMessage(message);
            dialog.setOnDismissListener(dismissListener);
            return dialog;
        }
    }
}
