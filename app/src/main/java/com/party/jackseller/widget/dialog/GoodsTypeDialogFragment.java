package com.party.jackseller.widget.dialog;


import android.content.Context;
import android.support.v4.app.DialogFragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.party.jackseller.R;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2018/2/8.
 */
public class GoodsTypeDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final int TAG = 1001;
    private List<String> content = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private CommonAdapter commonAdapter;
    private MultiItemTypeAdapter.OnItemClickListener onItemClickListener;
    private View.OnClickListener onClickListener;
    private View mAddTypeLayout, mListTypeLayout, mGotoAddBtn, mAddTypeBtn;
    private EditText mTypeEdit;
    private InputMethodManager inputMethodManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //设置背景透明
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.BOTTOM; // 紧贴底部
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度持平
        window.setAttributes(lp);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_fragment_list, null);
        builder.setView(view);
        iniLayout(view);
        return builder.create();
    }

    private void iniLayout(final View view) {
        mAddTypeLayout = view.findViewById(R.id.add_type_layout);
        mListTypeLayout = view.findViewById(R.id.list_type_layout);
        mGotoAddBtn = view.findViewById(R.id.togo_add_btn);
        mAddTypeBtn = view.findViewById(R.id.add_type_btn);
        mTypeEdit = view.findViewById(R.id.type_edit);
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mGotoAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddTypeLayout.setVisibility(View.VISIBLE);
                mListTypeLayout.setVisibility(View.GONE);
                inputMethodManager.showSoftInput(mTypeEdit, 0);
            }
        });

        mAddTypeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddTypeLayout.setVisibility(View.GONE);
                mListTypeLayout.setVisibility(View.VISIBLE);
                String text = mTypeEdit.getText().toString();
                mTypeEdit.setText("");
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                content.add(text);
                commonAdapter.notifyDataSetChanged();
                v.setTag(text);
                onClickListener.onClick(v);
            }
        });

        mRecyclerView = view.findViewById(R.id.recycler_view);
        commonAdapter = new CommonAdapter<String>(getActivity(), R.layout.dialog_list_item, content) {
            @Override
            protected void convert(ViewHolder viewHolder, String name, int position) {
                viewHolder.setText(R.id.title, name);
            }
        };
        if (onItemClickListener != null) {
            commonAdapter.setOnItemClickListener(onItemClickListener);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(commonAdapter);
    }


    public void setOnItemClickListener(MultiItemTypeAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public void setList(List<String> list) {
        content.clear();
        this.content.addAll(list);
    }

    public void setOnBottomClickListener(View.OnClickListener onBottmClickListener) {
        this.onClickListener = onBottmClickListener;
    }

    private TextView textView;

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
