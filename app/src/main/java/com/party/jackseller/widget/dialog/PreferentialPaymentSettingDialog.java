package com.party.jackseller.widget.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.bean.AddShopReabteDiscountBean;
import com.party.jackseller.bean.BaseResult;

import java.util.List;

/**
 * Created by 南宫灬绝痕 on 2018/12/17.
 */

public class PreferentialPaymentSettingDialog extends AlertDialog {
    private Context context;
    private View view;
    private EditText et_preferential_payment_setting;
    private TextView tv_preferential_payment_setting_cancle;
    private TextView tv_preferential_payment_setting_confirm;
    private PreferentialPaymentSettingDialog preferentialPaymentSettingDialog;
    private static final int DECIMAL_DIGITS = 1;


    public PreferentialPaymentSettingDialog(Context context) {
        this(context, R.style.MyAlertDialogStyle2);
        this.context = context;
    }

    public PreferentialPaymentSettingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public PreferentialPaymentSettingDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        initView();
    }

    public void setTextContent(String textContent) {
        et_preferential_payment_setting.setText(textContent);
    }


    public interface CommitInterface {
        void commit(String textContent);
    }

    CommitInterface mCommitInterface;

    public void setCommitInterface(CommitInterface commitInterface) {
        mCommitInterface = commitInterface;
    }


    private void initView() {

        view = LayoutInflater.from(context).inflate(R.layout.dialog_preferential_payment_setting, null);
        setView(view);
        et_preferential_payment_setting = view.findViewById(R.id.et_preferential_payment_setting);
        tv_preferential_payment_setting_cancle = view.findViewById(R.id.tv_preferential_payment_setting_cancle);
        tv_preferential_payment_setting_confirm = view.findViewById(R.id.tv_preferential_payment_setting_confirm);
        et_preferential_payment_setting.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        et_preferential_payment_setting.setText(s);
                        et_preferential_payment_setting.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    et_preferential_payment_setting.setText(s);
                    et_preferential_payment_setting.setSelection(2);
                }

                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        et_preferential_payment_setting.setText(s.subSequence(0, 1));
                        et_preferential_payment_setting.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });


        tv_preferential_payment_setting_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        tv_preferential_payment_setting_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textContent = et_preferential_payment_setting.getText().toString();
                if (mCommitInterface != null) {
                    mCommitInterface.commit(textContent);
                }
                cancel();
            }
        });
    }
}
