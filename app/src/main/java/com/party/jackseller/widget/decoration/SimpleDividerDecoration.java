package com.party.jackseller.widget.decoration;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.party.jackseller.R;

public class SimpleDividerDecoration extends RecyclerView.ItemDecoration {

    private int dividerHeight;
    private Paint dividerPaint;
    private int marginLeft;

    public SimpleDividerDecoration(Context context) {
        dividerPaint = new Paint();
        dividerPaint.setColor(context.getResources().getColor(R.color.gray));
        dividerHeight = context.getResources().getDimensionPixelSize(R.dimen.divider_height);
        marginLeft = context.getResources().getDimensionPixelSize(R.dimen.divider_margin_left);
    }

    public SimpleDividerDecoration(Context context,int dividerHeightResources,int marginLeftResources) {
        dividerPaint = new Paint();
        dividerPaint.setColor(context.getResources().getColor(R.color.gray));
        dividerHeight = context.getResources().getDimensionPixelSize(dividerHeight);
        marginLeft = context.getResources().getDimensionPixelSize(marginLeft);
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = dividerHeight;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int childCount = parent.getChildCount();
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        for (int i = 0; i < childCount - 1; i++) {
            View view = parent.getChildAt(i);
            float top = view.getBottom();
            float bottom = view.getBottom() + dividerHeight;
            c.drawRect(marginLeft, top, right, bottom, dividerPaint);
        }
    }
}

