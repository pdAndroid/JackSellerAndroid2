package com.party.jackseller.adapter;

import android.view.View;

import com.party.jackseller.bean.ListItem;

/**
 * Created by 派对 on 2018/8/14.
 */

public interface MyViewHolder {

    View getView(ListItem item);

}
