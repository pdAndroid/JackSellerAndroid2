package com.party.jackseller.adapter;

import android.content.Context;

import com.party.jackseller.bean.TradeRecord;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zhy.adapter.recyclerview.CommonAdapter;

import java.util.List;

/**
 * Created by 派对 on 2018/11/19.
 */

public abstract class MyCommonAdapter<T> extends CommonAdapter<T> {
    public MyCommonAdapter(Context context, int layoutId, List<T> datas) {
        super(context, layoutId, datas);
    }


    /**
     * @param pageIndex      当前第几页
     * @param pageSize       每页显示条数
     * @param dataSize       查询出了条数
     * @param mRefreshLayout
     */
    public void finishLoading(int pageIndex, int pageSize, int dataSize, SmartRefreshLayout mRefreshLayout) {
        notifyDataSetChanged();
        if (getDatas().size() < pageSize) {
            mRefreshLayout.finishLoadMoreWithNoMoreData();
        }
        if (pageIndex == 1) {
            mRefreshLayout.finishRefresh();
        } else {
            mRefreshLayout.finishLoadMore();
        }
    }

    public void clear(int page) {
        if (page == 1)
            getDatas().clear();
    }

    public void addData(T beansBean) {
        if (beansBean != null) {
            getDatas().add(beansBean);
        }
    }

    public void addData(int index, T beansBean) {
        if (beansBean != null) {
            getDatas().add(index, beansBean);
        }
    }

    public void addData(List<T> beansBean) {
        if (beansBean != null && beansBean.size() > 0) {
            getDatas().addAll(beansBean);
        }
    }



}
