package com.party.jackseller.adapter;

public class ItemBean {

    private int icon;
    private String title;
    private String value;
    private boolean isClick;
    private Class activity;

    public Class getActivity() {
        return activity;
    }

    public void setActivity(Class activity) {
        this.activity = activity;
    }

    public ItemBean(int icon, String title, Class activity, boolean isClick) {
        this.icon = icon;
        this.title = title;
        this.activity = activity;
        this.isClick = isClick;
    }

    public ItemBean(String title, String value, Class activity, boolean isClick) {
        this.value = value;
        this.title = title;
        this.activity = activity;
        this.isClick = isClick;
    }


    public ItemBean(String title, String value, boolean isClick) {
        this.value = value;
        this.title = title;
        this.isClick = isClick;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
