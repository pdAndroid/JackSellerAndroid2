package com.party.jackseller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.R;
import com.party.jackseller.bean.WeekRebateRateBean;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.SpecialTimeDialog;
import com.party.jackseller.widget.dialog.PreferentialPaymentSettingDialog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 南宫灬绝痕 on 2018/12/17.
 */

public class SpecialTimeAdapter extends RecyclerView.Adapter<SpecialTimeAdapter.ViewHolder> {

    private BaseActivity context;
    private List<WeekModel> weekModelList;

    public SpecialTimeAdapter(Context context, List<WeekRebateRateBean> itemList) {
        this.context = (BaseActivity) context;
        this.weekModelList = changeModle(itemList);

    }

    public void addData(List<WeekRebateRateBean> itemList) {
        this.weekModelList = changeModle(itemList);
        notifyDataSetChanged();
    }

    public void clearData() {
        this.weekModelList.clear();
    }

    @Override
    public int getItemCount() {
        return weekModelList.size();
    }


    public List<WeekModel> changeModle(List<WeekRebateRateBean> data) {
        weekModelList = new ArrayList<>();
        boolean isAdd;
        for (int i = 0; i < data.size(); i++) {
            isAdd = true;
            WeekModel weekModel = new WeekModel(data.get(i));
            int week = changeStampToWeek(data.get(i).getStartTimeAxis());
            weekModel.getWeeks().add(week);
            for (int j = 0; j < weekModelList.size(); j++) {
                if (equals(weekModelList.get(j), weekModel)) {
                    weekModelList.get(j).getWeeks().add(week);
                    isAdd = false;
                }
            }
            if (isAdd) {
                weekModelList.add(weekModel);
            }
        }
        return weekModelList;
    }

    public boolean equals(WeekModel weekModel, WeekRebateRateBean weekRebateRateBean) {
        return weekModel.getStartTime() == weekRebateRateBean.getStartTime() &&
                weekModel.getEndTime() == weekRebateRateBean.getEndTime() &&
                new BigDecimal(weekModel.getDiscount()).compareTo(new BigDecimal(weekRebateRateBean.getDiscount())) == 0;

    }


    public int changeStampToWeek(int stamp) {
        return stamp / (24 * 60);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special_time, parent, false));
    }


    public List<WeekRebateRateBean> getCommitDatas() throws Exception {
        List<WeekRebateRateBean> data = new ArrayList<>();
        for (int i = 0; i < weekModelList.size(); i++) {
            WeekModel weekModel = weekModelList.get(i);
            if (weekModel.getWeeks().size() == 0) {
                throw new Exception("请选择星期");
            }
            for (int j = 0; j < weekModel.getWeeks().size(); j++) {
                Integer integer = weekModel.getWeeks().get(j);
                int timeStamp = integer * 24 * 60;
                WeekRebateRateBean bean = new WeekRebateRateBean();
                if (weekModel.getDiscount() == null) {
                    throw new Exception("请选择折扣");
                }
                BigDecimal bigDecimal = new BigDecimal(weekModel.getDiscount()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
                bean.setDiscount(bigDecimal.toString());
                bean.setStartTime(timeStamp + weekModel.getStartTime());
                bean.setEndTimeAxis(timeStamp + weekModel.getEndTime());
                data.add(bean);
            }
        }
        return data;
    }


    public void changeWeekView(LinearLayout weeksLl, int position) {
        List<Integer> weeks = weekModelList.get(position).getWeeks();
        for (int i = 0; i < 7; i++) {
            CheckBox checkBox = (CheckBox) weeksLl.getChildAt(i);
            checkBox.setChecked(false);
            for (int j = 0; j < weeks.size(); j++) {
                if (weeks.get(j).intValue() == i) {
                    checkBox.setChecked(true);
                }
            }
            checkBox.setTag(i);
            checkBox.setOnClickListener((View v) -> {
                int tag = (int) v.getTag();
                List<Integer> weeks1 = weekModelList.get(position).getWeeks();
                boolean isAdd = true;
                for (int h = 0; h < weeks1.size(); h++) {
                    if (weeks1.get(h).intValue() == tag) {
                        weeks1.remove(h);
                        isAdd = false;
                    }
                }
                if (isAdd) {
                    weeks1.add(tag);
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeekModel weekModel = weekModelList.get(position);
        changeWeekView(holder.weeksLl, position);

        holder.etSpecialTimeStart.setText(changeTime(weekModelList.get(position).getStartTime()));
        holder.etSpecialTimeEnd.setText(changeTime(weekModelList.get(position).getEndTime()));
        holder.etSpecialTimeRebate.setText(weekModel.getDiscount());

        holder.deleteItem.setOnClickListener((View v) -> {
            DialogController.showConfirmDialog(context, "温馨提示", "你确定要删除该条数据", (View vv) -> {
                removeData(position);
            });
        });

        holder.etSpecialTimeStart.setOnClickListener((View view) -> {
            timePicker(weekModel.getStartTime(), (int time) -> {
                weekModelList.get(position).setStartTime(time);
                notifyDataSetChanged();
            });
        });

        holder.etSpecialTimeEnd.setOnClickListener((View view) -> {
            timePicker(weekModel.getEndTime(), (int time) -> {
                weekModelList.get(position).setEndTime(time);
                notifyDataSetChanged();
            });
        });

        holder.etSpecialTimeRebate.setOnClickListener((View view) -> {
            SpecialTimeDialog specialTimeDialog = new SpecialTimeDialog(context);
            specialTimeDialog.show();
            specialTimeDialog.setSpecialTimeInterface((String content) -> {
                WeekModel item = weekModelList.get(position);
                try {
                    int dis = Integer.parseInt(content);
                    if (dis > 100) {
                        context.showToast("返利不能超过100%");
                        return;
                    }
                    if (dis < 10) {
                        context.showToast("返利不能小于10%");
                        return;
                    }
                    item.setDiscount(dis + "");
                    notifyDataSetChanged();
                    specialTimeDialog.dismiss();
                } catch (Exception e) {
                    context.showToast("格式有误");
                }
            });
        });
    }

    private String changeTime(int endTime) {
        int hour = endTime / 60;
        int min = endTime % 60;
        return (hour > 10 ? hour : "0" + hour) + ":" + (min > 10 ? min : "0" + min);
    }


    public void timePicker(int time, PickerInterface pickerInterface) {
        int times[] = {time / 60, time % 60};
        DialogController.showTimeDialog(context, times, (TimePicker picTime, int hourOfDay, int minute) -> {
            int minutes = hourOfDay * 60 + minute;
            pickerInterface.pickOk(minutes);
        });
    }


    public void addItem() {
        weekModelList.add(new WeekModel());
        notifyDataSetChanged();
    }


    //  删除数据
    public void removeData(int position) {
        weekModelList.remove(position);
        notifyDataSetChanged();
    }


    public interface PickerInterface {
        void pickOk(int time);
    }


    private int[] getTextTime(View view) {
        int[] timeOfDay = {8, 0};
        String time = ((TextView) view).getText().toString();
        if (time.contains(":")) {
            String[] split = time.split(":");
            timeOfDay[0] = Integer.parseInt(split[0]);
            timeOfDay[1] = Integer.parseInt(split[1]);
        }
        return timeOfDay;
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.weeks_ll)
        LinearLayout weeksLl;
        @BindView(R.id.btn_special_time_week)
        Button btnSpecialTimeWeek;
        @BindView(R.id.et_special_time_start)
        TextView etSpecialTimeStart;
        @BindView(R.id.et_special_time_end)
        TextView etSpecialTimeEnd;
        @BindView(R.id.et_special_time_rebate)
        TextView etSpecialTimeRebate;
        @BindView(R.id.delete_item)
        ImageView deleteItem;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class WeekModel extends WeekRebateRateBean {

        List<Integer> weeks = new ArrayList<>();

        public WeekModel(WeekRebateRateBean weekRebateRateBean) {
            setShopId(weekRebateRateBean.getShopId());
            setStartTimeAxis(weekRebateRateBean.getStartTimeAxis());
            setDiscount(new BigDecimal(weekRebateRateBean.getDiscount()).multiply(new BigDecimal(100)).toString());
            setEndTimeAxis(weekRebateRateBean.getEndTimeAxis());

            setStartTime(changeStampToTime(getStartTimeAxis()));
            setEndTime(changeStampToTime(getEndTimeAxis()));
        }

        public int changeStampToTime(int stamp) {
            return stamp % (24 * 60);
        }

        public WeekModel() {
            super();
        }

        public List<Integer> getWeeks() {
            return weeks;
        }

        public void setWeeks(List<Integer> weeks) {
            this.weeks = weeks;
        }
    }
}
