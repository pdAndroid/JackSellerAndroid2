package com.party.jackseller;


import com.alibaba.sdk.android.oss.OSSClient;
import com.party.jackseller.bean.Cart;

/**
 * Created by yy520 on 2018-4-9.
 */

public class Constant {
    public static final int SHOP_NO_CHECK = -2;
    public static final int SHOP_CHECK = -1;
    public static final int SHOP_FROZEN = 0;
    public static final int SHOP_OPEN = 1;
    public static final int SHOP_CLOSE = 2;

    public static final int INTENT_GOODS_CONTENT = 100;

    public static String url = "https://www.paiduikeji.com/jack_seller/";
    public static String url1 = "http://120.27.223.110/jack_seller/";
    public static String url2 = "http://120.27.223.110:82/jack_seller/";

    public static String BASE_URL = url2;
    public static final boolean LOG_DEBUG = true;

    private static OSSClient ossClient;

    /**
     * ACCESS_ID,ACCESS_KEY是在阿里云申请的
     */
    public static final String ACCESS_ID = "LTAIbHl7YNNih800";

    public static final String GROUP_BUY = "group_buy";
    /**
     * ACCESS_ID,ACCESS_KEY是在阿里云申请的
     */
    public static final String ACCESS_KEY = "SSZxlQ3Ko7iYFvQGuuJAwT70IMex1V";
    /**
     * OSS_ENDPOINT是一个OSS区域地址
     */
    public static final String OSS_ENDPOINT = "http://oss-cn-hangzhou.aliyuncs.com";
    /**
     * BUCKET
     */
    public static final String BUCKET = "lichuanshipin";
    /**
     * BUCKET_PATH
     */
    public static String BUCKET_PATH = "http://" + BUCKET + ".oss-cn-hangzhou.aliyuncs.com/";

    public static Cart CART = new Cart();
    //    public final static String BASE_URL = "https://www.paiduikeji.com/jack_seller/";
    public static final String UID_KEY = "uid";
    public static final String MID_KEY = "mid";
    public static final String CONTENT_TYPE_KEY = "Content-Type";
    public static final String CONTENT_TYPE = "application/json; charset=utf-8";
    public static final String ACCEPT_KEY = "Accept";
    public static final String ACCEPT = "application/json";
    /**
     * 图片上传地址
     */
    public static final String IMAGE_UPLOAD_URL = BASE_URL + "ImageUpload/upload";
    /**
     * 签名图片上传地址
     */
    public static final String SIGN_UPLOAD_URL = BASE_URL + "ImageUpload/uploadSign";

    /**
     * 每页请求多少数据
     */
    public static final Integer PAGE_SIZE = 30;
    public static final Integer PAGE_SIZE_MAX = 100000;
    public static final Integer DELICIOUS_FOOD_ID = 2;
    public static final Integer ENTERTAINMENT_ID = 1;
    public static final Integer HOTEL_ID = 4;
    public static final Integer HAIRDRESSING_ID = 3;
    public static final Integer SWIM_ID = 5;

    //商铺状态（ 0，冻结 ,1.已上线，2.未认领，3.审核中，4审核未通过，5.申诉中,6.申诉未通过）
    public static final int SHOP_STATUS_FREEZE = 0;
    public static final int SHOP_STATUS_ON_LINE = 1;
    public static final int SHOP_STATUS_NOT_CLAIM = 2;
    public static final int SHOP_STATUS_CHECKING = 3;
    public static final int SHOP_STATUS_CHECK_FAILED = 4;
    public static final int SHOP_STATUS_COMPLAINING = 5;
    public static final int SHOP_STATUS_COMPLAIN_FAILED = 6;

    public static final int SHOP_SWITCH = 11;

    //提交订单需要传的商品类型（1代金卷 2为套餐 3为菜品 4提升会员 5拼团 6秒杀 7其它）
    public final static int GOODS_TYPE_REPLACE_MONEY = 1;
    public final static int GOODS_TYPE_GROUP_BUY = 2;
    public final static int GOODS_TYPE_GOODS = 3;
    public final static int GOODS_TYPE_VIP = 4;
    public final static int GOODS_TYPE_SPELLING_GROUP = 5;
    public final static int GOODS_TYPE_SECOND_KILL = 6;
    public final static int GOODS_TYPE_OTHER = 7;
    public final static int GOODS_TYPE_VIDEO = 8;

    public final static int REQUEST_CODE_ADD_SHOP = 31;
    public final static int REQUEST_CODE_ADD_REPLACE_MONEY = 32;
    public final static int REQUEST_CODE_ADD_GROUP_BUY = 33;
    public final static int REQUEST_CODE_ADD_SECOND_KILL = 34;
    public final static int REQUEST_CODE_ADD_SPELLING_GROUP = 35;
}
