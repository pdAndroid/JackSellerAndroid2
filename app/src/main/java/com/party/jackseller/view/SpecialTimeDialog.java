package com.party.jackseller.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackseller.R;

/**
 * Created by 南宫灬绝痕 on 2019/1/11.
 */

public class SpecialTimeDialog extends AlertDialog {
    private Context context;
    private View view;
    private EditText et_preferential_payment_setting;
    private TextView tv_preferential_payment_setting_cancle;
    private TextView tv_preferential_payment_setting_confirm;
    private SpecialTimeDialog specialTimeDialog;

    public SpecialTimeDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
        this.context = context;
    }

    protected SpecialTimeDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        initView();
    }

    public void setTextContent(String textContent) {
        et_preferential_payment_setting.setText(textContent);
    }

    public void setHintContent(String textContent) {
        et_preferential_payment_setting.setHint(textContent);
    }

    protected SpecialTimeDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    SpecialTimeDialog.SpecialTimeInterface mSpecialTimeInterface;

    public interface SpecialTimeInterface {
        void specialTime(String textContent);
    }

    public void setSpecialTimeInterface(SpecialTimeDialog.SpecialTimeInterface specialTimeInterface) {
        mSpecialTimeInterface = specialTimeInterface;
    }

    private void initView() {
        view = LayoutInflater.from(context).inflate(R.layout.dialog_preferential_payment_setting, null);
        setView(view);
        et_preferential_payment_setting = view.findViewById(R.id.et_preferential_payment_setting);
        tv_preferential_payment_setting_cancle = view.findViewById(R.id.tv_preferential_payment_setting_cancle);
        tv_preferential_payment_setting_confirm = view.findViewById(R.id.tv_preferential_payment_setting_confirm);
        et_preferential_payment_setting.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_preferential_payment_setting.setMaxEms(2);

        tv_preferential_payment_setting_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        tv_preferential_payment_setting_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textContent = et_preferential_payment_setting.getText().toString();
                if (mSpecialTimeInterface != null) {
                    mSpecialTimeInterface.specialTime(textContent);
                }
                cancel();
            }
        });
    }
}
