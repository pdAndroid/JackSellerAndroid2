package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.Photo;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/13.
 */

public class PhotoService extends BaseService {

    public PhotoService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 获取商品图片列表
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<Photo>>> getGoodsPhotoList(long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<Photo>>> observable = getObservable().getGoodsPhotoList(getSign(data), data);
        return observable;
    }

    /**
     * 获取店铺图片列表
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<Photo>>> getShopPhotoList(long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<Photo>>> observable = getObservable().getShopPhotoList(getSign(data), data);
        return observable;
    }

    /**
     * 获取用户图片列表
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<Photo>>> getUserPhotoList(long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<Photo>>> observable = getObservable().getUserPhotoList(getSign(data), data);
        return observable;
    }

    /**
     * 添加图片
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<Photo>> addPhoto(Map<String, String> map) {
        Observable<BaseResult<Photo>> observable = getObservable().addPhoto(getSign(map), map);
        return observable;
    }

    /**
     * 批量添加图片
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<CommonResult>> addManyPhoto(Map<String, String> map) {
        Observable<BaseResult<CommonResult>> observable = getObservable().addManyPhoto(getSign(map), map);
        return observable;
    }

    //删除图片
    public Observable<BaseResult<CommonResult>> deletePhoto(long photoId) {
        Map<String, String> signData = getSignData();
        signData.put("photoId", photoId + "");
        Observable<BaseResult<CommonResult>> observable = getObservable().deletePhoto(getSign(signData), signData);
        return observable;
    }

    /**
     * 编辑相册
     *
     * @param id
     * @param photoName
     * @return
     */
    public Observable<BaseResult<CommonResult>> editPhoto(long id, String photoName) {
        Map<String, String> signData = getSignData();
        signData.put("photoName", photoName);
        signData.put("id", id + "");
        Observable<BaseResult<CommonResult>> observable = getObservable().editAlbum(getSign(signData), signData);
        return observable;
    }

    //获取二维码
    public Observable<BaseResult<String>> getQrCode(String shopId) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", shopId + "");
        Observable<BaseResult<String>> observable = getObservable().getQrCode(getSign(signData), signData);
        return observable;
    }
}
