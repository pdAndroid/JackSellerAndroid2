package com.party.jackseller.api;

import com.party.jackseller.bean.AccountBean;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BindingInfo;
import com.party.jackseller.bean.BusinessAnalysis;
import com.party.jackseller.bean.BusinessAnalysisBean;
import com.party.jackseller.bean.ClaimShopBean;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.bean.DelShopRebateDiscountBean;
import com.party.jackseller.bean.EatPersonCountBean;
import com.party.jackseller.bean.Employee;
import com.party.jackseller.bean.EmployeeRole;
import com.party.jackseller.bean.GetMyShopListBean;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.GoodsCategory;
import com.party.jackseller.bean.GoodsUnit;
import com.party.jackseller.bean.GroupBuyCoupon;
import com.party.jackseller.bean.KeyValue;
import com.party.jackseller.bean.LoginBean;
import com.party.jackseller.bean.OSSConfig;
import com.party.jackseller.bean.PayResult;
import com.party.jackseller.bean.Photo;
import com.party.jackseller.bean.RegisterInfo;
import com.party.jackseller.bean.ReplaceMoneyCoupon;
import com.party.jackseller.bean.ScanSuccessBean;
import com.party.jackseller.bean.SecondKillCoupon;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.ShopAdverts;
import com.party.jackseller.bean.ShopCategory;
import com.party.jackseller.bean.ShopInfo;
import com.party.jackseller.bean.SpellingGroupCoupon;
import com.party.jackseller.bean.SpellingGroupCouponDetailBean;
import com.party.jackseller.bean.TradeAnalysisBean;
import com.party.jackseller.bean.TradeRecord;
import com.party.jackseller.bean.UpdateRebateDiscountBean;
import com.party.jackseller.bean.UpdateShopDiscountBean;
import com.party.jackseller.bean.User;
import com.party.jackseller.bean.VerifyCouponsBean;
import com.party.jackseller.bean.VerifyUser;
import com.party.jackseller.bean.VideoBean;
import com.party.jackseller.bean.VideoCondition;
import com.party.jackseller.bean.WeekRebateRateBean;
import com.party.jackseller.bean.WxPaymentBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by 派对 on 2018/7/23.
 */

public interface BaseApi {


//    getAddEmployeeVerCode: service +"user/getAddEmployeeVerCode",
//    updateEmployee: service +"shop/updateEmployee",
//    register: service + "user/register",
//    shopRegister: service + "shop/addShop",
//    getShopList: service + "shop/getShopList",
//    getAllShopType: service + "shopType/getAllShopType",
//    searchShop: service + "shop/search",
//    getShopName: service + "shop/getShopName",
//    getHomeBanner: service + "banner/getHomeBanner",
//    login: service + "user/getlogin",
//    getUserInfo: service + "user/getUserInfo",
//    getShopInfo: service + "shop/getShop",
//    getFansList : service+"userConcern/getUserConcernList",
//    getCoupon: service + "coupon/getCouponByShopId",//获取优惠券
//    getGroupBuyByShopId: service + "groupBuy/getGroupBuyByShopId",//审核未通过获取团购卷
//    getIsPassGroupBuyList: service + "groupBuy/getIsPassGroupBuyList",//获取团购卷
//    submissionCoupons:service+"coupon/addCoupon",
//    getCouponList:service+"coupon/getCouponByShopId",
//    submissionGroupBuy:service+ "groupBuy/addGroupBuy",
//    getShopGoods: service +"goods/getShopGoods",
//    getEmployee: service + "shop/getEmployee",
//    getEmployeeRole: service +"shop/getEmployeeRole",
//    addEmployee: service +"shop/addEmployee",
//    deleteEmployee: service +"shop/delEmployee",
//    getShopPhoto: service + "shop/getShopPhoto",
//    getUserPhoto: service +"shop/getUserPhoto",
//    getGoodsPhoto: service +"shop/getGoodsPhoto",
//    addNewFood: service +"goods/addGoods",
//    postPoto: service + "shop/addPhoto",
//    getMyShopList: service + "user/getMyShopList",//获取我的所有商铺
//    addNewFood: service +"goods/addGoods",  //添加商品
//    getGoodsType: service +"goods/getGoodsType",//获取商品分类
//    getGoodsUnit: service +"goods/getGoodsUnit",//获取商品单位
//    addGoodsType: service +"goods/addGoodsType",//添加商品分类
//    analysisTrade: service + "analysis/trade", //交易分析
//    analysisGroupBuy: service +"analysis/groupBuy",//团购分析
//    analysisComment: service + "analysis/comment",//评价分析
//    analysisCustomer: service +"analysis/customer",//顾客分析
//    analysisBusiness: service + "analysis/business",//营业分析
//    verifyCoupons: service +"user/verifyCoupons",//消费验证
//    confirm: service +"user/confirm",//验证码


    //获取验证码
    @GET("user/getVerCode")
    Observable<BaseResult<Object>> getVerCode(@QueryMap Map<String, String> signData);

    /**
     * 获取商铺钱包
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("shopWallet/getShopWallet")
    Observable<BaseResult<CommonResult>> getShopWallet(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /*商家入驻*/

    //认领(申诉)商铺
    @FormUrlEncoded
    @POST("shop/claimShop")
    Observable<BaseResult<String>> claimShop(@Header("sign") String sign, @FieldMap Map<String, String> data);

    //商铺分类
    @GET("shopCategory/getShopCategoryList")
    Observable<BaseResult<List<ShopCategory>>> getShopCategoryList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //录入商铺
    @FormUrlEncoded
    @POST("shop/entryShop")
    Observable<BaseResult<String>> entryShop(@Header("sign") String sign, @FieldMap Map<String, String> data);

    //商铺认领列表
    @GET("shop/listAllShop")
    Observable<BaseResult<List<ClaimShopBean>>> getClaimShopList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //用户自己添加商铺
    @FormUrlEncoded
    @POST("shop/addShop")
    Observable<BaseResult<String>> addShop(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /*我的*/

    //获取我的粉丝
    @GET("userConcern/getShopConcernList")
    Observable<BaseResult<List<User>>> getFanList(@Header("sign") String sign, @QueryMap Map<String, String> data);

    //门店管理----获取我的店铺列表
    @GET("shop/getMyShopList")
    Observable<BaseResult<List<Shop>>> getMyShopList(@Header("sign") String sign, @QueryMap Map<String, String> data);

    //门店管理----获取我的店铺列表
    @GET("video/getShopByNameOrPhone")
    Observable<BaseResult<List<ShopAdverts>>> getShopByNameOrPhone(@Header("sign") String sign, @QueryMap Map<String, String> data);




    @GET("shop/getShop")
    Observable<BaseResult<ShopInfo>> getShopDetail(@Header("sign") String sign, @QueryMap Map<String, String> data);

    @GET("coupon/getCouponByShopId")
    Observable<BaseResult<List<ReplaceMoneyCoupon>>> getCouponList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /*员工管理*/

    //查看全部员工
    @GET("shop/getEmployee")
    Observable<BaseResult<List<Employee>>> getEmployeeList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //查看员工信息
    @GET("shop/getEmployeeById")
    Observable<BaseResult<Employee>> getEmployeeMessage(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //新增员工
    @FormUrlEncoded
    @POST("shop/addEmployee")
    Observable<BaseResult<CommonResult>> addEmployee(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 获取员工角色列表
     *
     * @param sign
     * @return
     */
    @GET("shop/getEmployeeRole")
    Observable<BaseResult<List<EmployeeRole>>> getEmployeeRoleList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    @GET("shop/getGoodsPhoto")
    Observable<BaseResult<List<Photo>>> getGoodsPhoto(@Header("sign") String sign, @QueryMap Map<String, String> signData);


    @GET("shop/getShopPhoto")
    Observable<BaseResult<List<Photo>>> getShopPhoto(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 删除员工
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("shop/delEmployee")
    Observable<BaseResult<Long>> deleteEmployee(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 修改员工信息
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("shop/updateEmployee")
    Observable<BaseResult<Long>> alterEmployee(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /*商品管理*/

    //获得商铺中商品的分类
    @GET("goods/getItemCategory")
    Observable<BaseResult<List<GoodsCategory>>> getGoodsCategory(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 获取该商铺商品
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("goods/getShopGoods")
    Observable<BaseResult<List<Goods>>> getAllGoods(@Header("sign") String sign, @QueryMap Map<String, String> signData);


    //获取商品单位
    @GET("goods/getItemUnit")
    Observable<BaseResult<List<GoodsUnit>>> getGoodsUnit(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //根据商铺分类获取商品
    @GET("goods/getShopGoodsByTypeId")
    Observable<BaseResult<List<Goods>>> getShopGoodsByTypeId(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //删除商品
    @POST("goods/delItem")
    Observable<BaseResult<String>> delGoods(@Header("sign") String sign, @QueryMap Map<String, String> signData);


    //添加商品
    @FormUrlEncoded
    @POST("goods/addGoods")
    Observable<BaseResult<Long>> addGoods(@Header("sign") String sign, @FieldMap Map<String, String> signData);


    /**
     * 删除一个商品分类
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("goods/delGoodsType")
    Observable<BaseResult<Long>> delGoodsType(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    //添加商品分类
    @FormUrlEncoded
    @POST("goods/addItemCategory")
    Observable<BaseResult<GoodsCategory>> addGoodsType(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    //获取OSS配置
    @GET("ossConfig/getOssConfig")
    Observable<BaseResult<OSSConfig>> getOssConfig(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    @GET("groupBuy/getGroupBuyByShopId")
    Observable<BaseResult<List<GroupBuyCoupon>>> getGroupBuyByShopId(@Header("sign") String sign, @QueryMap Map<String, String> signData);


    //查询团购券
    @GET("groupBuy/getIsPassGroupBuyList")
    Observable<BaseResult<GroupBuyCoupon>> getGroupCouponList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //添加团购券
    @FormUrlEncoded
    @POST("groupBuy/addGroupBuy")
    Observable<BaseResult<Long>> addGroupBuyCoupon(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    //查询代金券
    @GET("coupon/getCouponByShopId")
    Observable<BaseResult<ReplaceMoneyCoupon>> getReplaceMoneyCouponList(@Header("sign") String sign, @QueryMap Map<String, String> signData);


    @GET("groupBuy/getDinersNumber")
    Observable<BaseResult<List<EatPersonCountBean>>> getDinnerNumber(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //添加代金券
    @FormUrlEncoded
    @POST("coupon/addCoupon")
    Observable<BaseResult<Long>> addReplaceMoneyCoupon(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /*商品相册*/

    //获取商品相册
    @GET("shop/getGoodsPhoto")
    Observable<BaseResult<List<Photo>>> getGoodsPhotoList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //获取本店相册
    @GET("shop/getShopPhoto")
    Observable<BaseResult<List<Photo>>> getShopPhotoList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //获取用户相册
    @GET("shop/getUserPhoto")
    Observable<BaseResult<List<Photo>>> getUserPhotoList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //上传图片
    @FormUrlEncoded
    @POST("shop/addPhoto")
    Observable<BaseResult<Photo>> addPhoto(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    //批量上传图片
    @POST("shop/addPhotoList")
    Observable<BaseResult<CommonResult>> addManyPhoto(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    //删除图片
    @FormUrlEncoded
    @POST("shop/addPhotoList")
    Observable<BaseResult<CommonResult>> deletePhoto(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 上传视频到后台
     */
    @FormUrlEncoded
    @POST("video/addVideoOrder")
    Observable<BaseResult<CommonResult>> addVideo(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 编辑相册
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("shop/updatePhoto")
    Observable<BaseResult<CommonResult>> editAlbum(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 获取二维码
     */
    @FormUrlEncoded
    @POST("v2/user/getSellerMultCode")
    Observable<BaseResult<String>> getQrCode(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 获取我的视频
     */
    @FormUrlEncoded
    @POST("video/listVideoByShopId")
    Observable<BaseResult<List<VideoBean>>> listVideoByShopId(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 获取交易流水记录
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("tradeRecord/listShopTradeRecord")
    Observable<BaseResult<List<TradeRecord>>> getTradeRecordList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 获取交易流水详情
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("v2/tradeRecord/getShopTradeRecordInfo")
    Observable<BaseResult<List<KeyValue>>> getTradeRecordInfo(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 查询秒杀券
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("secondKill/listSecondKillByShopId")
    Observable<BaseResult<List<SecondKillCoupon>>> getSecondKillList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 添加秒杀券
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("secondKill/addSecondKill")
    Observable<BaseResult<Long>> addSecondKillCoupon(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 查询拼团券
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("spellingGroup/listSpellingGroupByShopId")
    Observable<BaseResult<List<SpellingGroupCoupon>>> getSpellingGroupCouponList(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 添加拼团券
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("spellingGroup/addSpellingGroup")
    Observable<BaseResult<Long>> addSpellingGroupCoupon(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 微信支付
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/generateAppWxPayOrder")
    Observable<BaseResult<WxPaymentBean>> wxPayOrder(@FieldMap Map<String, String> data);

    /**
     * 余额支付(用我的钱包支付)
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/walletPayment")
    Observable<BaseResult<PayResult>> doMyWalletPayOrder(@FieldMap Map<String, String> data);

    /**
     * 支付宝支付
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/generateAppAliPayOrder")
    Observable<BaseResult<String>> aliPayOrder(@FieldMap Map<String, String> data);


    /**
     * 验证卷的是否可用（扫码,输码）
     *
     * @param sign
     * @param signData
     * @return
     */
    @FormUrlEncoded
    @POST("user/verifyCoupons")
    Observable<BaseResult<ScanSuccessBean>> verifyCoupon(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    @FormUrlEncoded
    @POST("user/confirm")
    Observable<BaseResult<CommonResult>> confirmConsume(@Header("sign") String sign, @FieldMap Map<String, String> signData);

    /**
     * 查询拼团券
     *
     * @param sign
     * @param signData
     * @return
     */
    @GET("analysis/business")
    Observable<BaseResult<BusinessAnalysis>> getBusinessAnalysis(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 获得添加视频筛选条件
     *
     * @param sign
     * @return
     */
    @GET("v2/video/listVideoCondition")
    Observable<BaseResult<VideoCondition>> listVideoCondition(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    @GET("spellingGroup/listUser")
    Observable<BaseResult<List<SpellingGroupCouponDetailBean>>> getSpellingGroupDetail(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    @FormUrlEncoded
    @POST("user/authentication")
    Observable<BaseResult<VerifyUser>> authentication(@Header("sign") String sign, @FieldMap Map<String, String> data);

    //
    @POST("shop/updatePayPassword")
    Observable<BaseResult<String>> alterPayPassword(@QueryMap Map<String, String> data);

    @GET("shop/getUpdatePasswordVerCode")
    Observable<BaseResult<Object>> getUpdatePasswordVerCode(@QueryMap Map<String, String> phone);

    //营业分析
    @GET("analysis/business")
    Observable<BaseResult<BusinessAnalysisBean>> getAnalysisBusiness(@QueryMap Map<String, String> phone);

    //交易分析
    @GET("analysis/trade")
    Observable<BaseResult<TradeAnalysisBean>> getAnalysisTrade(@QueryMap Map<String, String> phone);


    @FormUrlEncoded
    @POST("shop/verifVerCode")
    Observable<BaseResult<VerifyUser>> verifVerCode(@FieldMap Map<String, String> map);


    @POST("v2/user/getAppId")
    Observable<BaseResult<AppInfo>> getAppId();

//    @FormUrlEncoded
//    @POST("v2/user/getAppId")
//    Observable<BaseResult<ApplicationInfo>> getApplicationId(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("v2/user/wxLogin")
    Observable<BaseResult<LoginBean>> wxLogin(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("v2/user/checkWxIsRegister")
    Observable<BaseResult<RegisterInfo>> checkWxIsRegister(@FieldMap Map<String, String> map);

    //获得绑定账户信息
    @FormUrlEncoded
    @POST("account/getBindingAccountInfo")
    Observable<BaseResult<BindingInfo>> getBindingUserInfo(@FieldMap Map<String, String> map);

    //确认绑定
    @FormUrlEncoded
    @POST("account/confirmBindingAccount")
    Observable<BaseResult<ConfirmBindingAccountBean>> getConfirmBindingAccount(@FieldMap Map<String, String> map);

    //确认绑定
    @FormUrlEncoded
    @POST("account/getAccount")
    Observable<BaseResult<AccountBean>> getAccount(@FieldMap Map<String, String> map);

    //手动提现
    @FormUrlEncoded
    @POST("payBank/addPayBankRecord")
    Observable<BaseResult<Object>> addPayBankRecord(@FieldMap Map<String, String> map);


    //获取阿里认证信息
    @GET("aliAuth/getAliAuthInfo")
    Observable<BaseResult<String>> getAliAuthInfo(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("aliAuth/checkZMScoreAndBindingAli")
    Observable<BaseResult<BindingInfo>> checkZMScoreAndBindingAli(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("user/userBindingWx")
    Observable<BaseResult<Object>> userBindingWx(@FieldMap Map<String, String> map);

    @POST("v2/user/register")
    Observable<BaseResult<User>> register(@QueryMap Map<String, String> data);

    //登录
    @POST("v2/user/login")
    Observable<BaseResult<LoginBean>> login(@QueryMap Map<String, String> data);

    //验证卷的是否可用（扫码,输码）
    @FormUrlEncoded
    @POST("user/verifyCoupons")
    Observable<BaseResult<VerifyCouponsBean>> verifyCoupons(@FieldMap Map<String, String> map);

    //门店管理
    @GET("shop/getMyShopList")
    Observable<BaseResult<List<GetMyShopListBean>>> getMyShopList(@QueryMap Map<String, String> map);

    //获得商铺中的全部特殊返利比例
    @GET("shop/listWeekRebateRate")
    Observable<BaseResult<List<WeekRebateRateBean>>> listWeekRebateRate(@QueryMap Map<String, String> map);

    //添加特殊返利
    @FormUrlEncoded
    @POST("shop/addShopRebateDiscount")
    Observable<BaseResult<String>> addShopRebateDiscount(@FieldMap Map<String, String> map);

    //删除特殊折扣
    @FormUrlEncoded
    @POST("shop/delShopRebateDiscount")
    Observable<BaseResult<List<DelShopRebateDiscountBean>>> delShopRebateDiscount(@FieldMap Map<String, String> map);

    //修改店铺首单折扣
    @FormUrlEncoded
    @POST("shop/updateShopDiscount")
    Observable<BaseResult<List<UpdateShopDiscountBean>>> updateShopDiscount(@FieldMap Map<String, String> map);

    //修改店铺基本返利
    @FormUrlEncoded
    @POST("shop/updateRebateDiscount")
    Observable<BaseResult<List<UpdateRebateDiscountBean>>> updateRebateDiscount(@FieldMap Map<String, String> map);

}
