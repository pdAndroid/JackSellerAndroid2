package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.TradeRecord;
import com.party.jackseller.bean.KeyValue;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/25.
 */

public class TradeRecordService extends BaseService {

    public TradeRecordService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<TradeRecord>>> getTradeRecordList(long shopId, int pageSize, int pageNumber) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", shopId + "");
        signData.put("row", pageSize + "");
        signData.put("page", pageNumber + "");
        Observable<BaseResult<List<TradeRecord>>> observable = getObservable().getTradeRecordList(getSign(signData), signData);
        return observable;
    }

    public Observable<BaseResult<List<KeyValue>>> getTradeRecordInfo(String tradeRecordId) {
        Map<String, String> signData = getSignData();
        signData.put("tradeRecordId", tradeRecordId + "");
        Observable<BaseResult<List<KeyValue>>> observable = getObservable().getTradeRecordInfo(getSign(signData), signData);
        return observable;
    }
}
