package com.party.jackseller.api;

import android.text.TextUtils;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.MApplication;
import com.party.jackseller.net.RetrofitRxClient;
import com.party.jackseller.utils.SignUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 派对 on 2018/7/23.
 */

public class BaseService {
    BaseActivity baseActivity;
    MApplication mApplication;

    public BaseApi getObservable() {
        return RetrofitRxClient.INSTANCE
                .getRetrofit()
                .create(BaseApi.class);
    }

    public BaseService(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        this.mApplication = (MApplication) baseActivity.getApplication();
    }

    /**
     * 基本参数的容器
     *
     * @return
     */
    public Map<String, String> getSignData() {
        Map map = new HashMap();
        map.put("token", mApplication.getToken());
        map.put("shopId", mApplication.getCurrShop().getId() + "");
        return map;
    }

    /**
     * 基本参数的容器
     * @return
     */
    public HashMap<String, String> getTokenMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("token", TextUtils.isEmpty(mApplication.getToken()) ? "" : mApplication.getToken());
        return hashMap;
    }

    public Map<String, String> getLocalMap() {
        Map map = new HashMap<>();
        map.put("lon", mApplication.getMap().getLongitude() + "");
        map.put("lat", mApplication.getMap().getLatitude() + "");
        map.put("cityId", mApplication.getMap().getAdCode() + "");
        return map;
    }

    public Map<String, String> getLocalMap(Map map) {
        if (map == null) map = new HashMap<>();
        map.put("lon", mApplication.getMap().getLongitude() + "");
        map.put("lat", mApplication.getMap().getLatitude() + "");
        return map;
    }



    public String getSign(Map data) {
        return SignUtils.getSign(data);
    }
}
