package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BusinessAnalysis;
import com.party.jackseller.bean.BusinessAnalysisBean;
import com.party.jackseller.bean.ClaimShopBean;
import com.party.jackseller.bean.DelShopRebateDiscountBean;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.ShopAdverts;
import com.party.jackseller.bean.ShopCategory;
import com.party.jackseller.bean.ShopInfo;
import com.party.jackseller.bean.TradeAnalysisBean;
import com.party.jackseller.bean.UpdateRebateDiscountBean;
import com.party.jackseller.bean.UpdateShopDiscountBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by 派对 on 2018/7/26.
 */

public class ShopService extends BaseService {

    public ShopService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<ShopInfo>> getShopDetail(long shop_id) {
        Map<String, String> data = getSignData();
        data.put("shopId", String.valueOf(shop_id));
        Observable<BaseResult<ShopInfo>> observable = getObservable().getShopDetail(getSign(data), data);
        return observable;
    }

    /**
     * 用户自己添加店铺
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<String>> addShop(Map<String, String> map) {
        Observable<BaseResult<String>> observable = getObservable().addShop(getSign(map), map);
        return observable;
    }

    /**
     * 业务员录入店铺
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<String>> entryShop(Map<String, String> map) {
        Observable<BaseResult<String>> observable = getObservable().entryShop(getSign(map), map);
        return observable;
    }

    /**
     * 获取商铺认领列表
     */
    public Observable<BaseResult<List<ClaimShopBean>>> getShopClaimList(int pageNumber, int pageSize) {
        Map<String, String> data = getSignData();
        data.put("pageNumber", pageNumber + "");
        Observable<BaseResult<List<ClaimShopBean>>> observable = getObservable().getClaimShopList(getSign(data), data);
        return observable;
    }

    /**
     * 认领商铺
     */
    public Observable<BaseResult<String>> claimShop(Map<String, String> data) {
        Observable<BaseResult<String>> observable = getObservable().claimShop(getSign(getSignData()), data);
        return observable;
    }

    public Observable<BaseResult<List<ShopCategory>>> getShopCategoryList() {
        Observable<BaseResult<List<ShopCategory>>> observable = getObservable().getShopCategoryList(getSign(getSignData()), getSignData());
        return observable;
    }

    /**
     * 获取我的店铺列表
     *
     * @return
     */
    public Observable<BaseResult<List<Shop>>> getMyShopList() {
        Map<String, String> data = getSignData();
        Observable<BaseResult<List<Shop>>> observable = getObservable().getMyShopList(getSign(data), data);
        return observable;
    }

    /**
     * 获取所有门店
     *
     * @return
     */
    public Observable<BaseResult<List<ShopAdverts>>> getShopByNameOrPhone(String key) {
        Map<String, String> data = getTokenMap();
        data.put("key",key);
        Observable<BaseResult<List<ShopAdverts>>> observable = getObservable().getShopByNameOrPhone(getSign(data), data);
        return observable;
    }


    public Observable<BaseResult<BusinessAnalysis>> getBusinessAnalysis(long shopId, int day) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        data.put("day", day + "");
        Observable<BaseResult<BusinessAnalysis>> observable = getObservable().getBusinessAnalysis(getSign(data), data);
        return observable;
    }

    /**
     * 删除特殊折扣
     */
    public Observable<BaseResult<List<DelShopRebateDiscountBean>>> delShopRebateDiscount(String id) {
        Map<String, String> data = getSignData();
        data.put("id", id);
        Observable<BaseResult<List<DelShopRebateDiscountBean>>> observable = getObservable().delShopRebateDiscount(data);
        return observable;
    }

    /**
     * 修改店铺首单折扣
     */
    public Observable<BaseResult<List<UpdateShopDiscountBean>>> updateShopDiscount(String shopId, String discount) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        data.put("discount", discount + "");
        Observable<BaseResult<List<UpdateShopDiscountBean>>> observable = getObservable().updateShopDiscount(data);
        return observable;
    }

    /**
     * 修改店铺基本返利
     */
    public Observable<BaseResult<List<UpdateRebateDiscountBean>>> updateRebateDiscount(String rebateDiscount, String shopId) {
        Map<String, String> data = getSignData();
        data.put("rebateDiscount", rebateDiscount + "");
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<UpdateRebateDiscountBean>>> observable = getObservable().updateRebateDiscount(data);
        return observable;
    }

    /**
     * 营业分析
     */
    public Observable<BaseResult<BusinessAnalysisBean>> getAnalysisBusiness(String shopId, String day) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        data.put("day", day + "");
        Observable<BaseResult<BusinessAnalysisBean>> observable = getObservable().getAnalysisBusiness(data);
        return observable;
    }

    /**
     * 交易分析
     */
    public Observable<BaseResult<TradeAnalysisBean>> getAnalysisTrade(String shopId, String day) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        data.put("day", day + "");
        Observable<BaseResult<TradeAnalysisBean>> observable = getObservable().getAnalysisTrade(data);
        return observable;
    }
}
