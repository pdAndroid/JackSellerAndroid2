package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.AddShopReabteDiscountBean;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.DelShopRebateDiscountBean;
import com.party.jackseller.bean.Employee;
import com.party.jackseller.bean.EmployeeRole;
import com.party.jackseller.bean.GetMyShopListBean;
import com.party.jackseller.bean.WeekRebateRateBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/12.
 */

public class EmployeeService extends BaseService {

    public EmployeeService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 查看全部员工
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<Employee>>> getEmployeeList(Long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<Employee>>> observable = getObservable().getEmployeeList(getSign(data), data);
        return observable;
    }

    /**
     * 查看员工信息
     *
     * @param userShopId
     * @return
     */
    public Observable<BaseResult<Employee>> getEmployeeMessage(String userShopId) {
        Map<String, String> data = getSignData();
        data.put("userShopId", userShopId);
        Observable<BaseResult<Employee>> observable = getObservable().getEmployeeMessage(getSign(data), data);
        return observable;
    }

    /**
     * 新增员工
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<CommonResult>> addEmployee(Map<String, String> map) {
        Observable<BaseResult<CommonResult>> observable = getObservable().addEmployee(getSign(map), map);
        return observable;
    }

    /**
     * 获取员工角色列表
     */
    public Observable<BaseResult<List<EmployeeRole>>> getEmployeeRoleList() {
        Observable<BaseResult<List<EmployeeRole>>> observable = getObservable().getEmployeeRoleList(getSign(getSignData()), getSignData());
        return observable;
    }

    /**
     * 修改员工信息
     *
     * @return
     */
    public Observable<BaseResult<Long>> alterEmployee(Map<String, String> tokenMap) {
        Observable<BaseResult<Long>> observable = getObservable().alterEmployee(getSign(getSignData()), tokenMap);
        return observable;
    }

    /**
     * 删除员工
     *
     * @return
     */
    public Observable<BaseResult<Long>> deleteEmployee(long employeeId) {
        Map<String, String> signData = getSignData();
        signData.put("id", employeeId + "");
        Observable<BaseResult<Long>> observable = getObservable().deleteEmployee(getSign(getSignData()), signData);
        return observable;
    }

    /**
     * 门店管理
     */
    public Observable<BaseResult<List<GetMyShopListBean>>> getMyShopList() {
        Map<String, String> data = getSignData();
        Observable<BaseResult<List<GetMyShopListBean>>> observable = getObservable().getMyShopList(data);
        return observable;
    }

    /**
     * 获得商铺中的全部特殊返利比例
     */
    public Observable<BaseResult<List<WeekRebateRateBean>>> listWeekRebateRate(String shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId);
        Observable<BaseResult<List<WeekRebateRateBean>>> observable = getObservable().listWeekRebateRate(data);
        return observable;
    }

    /**
     * 添加特殊返利
     */
    public Observable<BaseResult<String>> addShopRebateDiscount(String rebateDiscounts) {
        Map<String, String> data = getSignData();
        data.put("rebateDiscounts", rebateDiscounts);
        Observable<BaseResult<String>> observable = getObservable().addShopRebateDiscount(data);
        return observable;
    }

    /**
     * 删除特殊折扣
     */
    public Observable<BaseResult<List<DelShopRebateDiscountBean>>> delShopRebateDiscount(String id) {
        Map<String, String> data = getSignData();
        data.put("id", id);
        Observable<BaseResult<List<DelShopRebateDiscountBean>>> observable = getObservable().delShopRebateDiscount(data);
        return observable;
    }
}
