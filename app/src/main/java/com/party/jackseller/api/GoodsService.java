package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.Goods;
import com.party.jackseller.bean.GoodsCategory;
import com.party.jackseller.bean.GoodsUnit;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/16.
 */

public class GoodsService extends BaseService{


    public GoodsService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<GoodsCategory>> addGoodsType(long shop_id, String name) {
        Map<String, String> signData = getSignData();
        signData.put("name", name);
        signData.put("shopId", String.valueOf(shop_id));
        Observable<BaseResult<GoodsCategory>> observable = getObservable().addGoodsType(getSign(signData), signData);
        return observable;
    }

    /**
     * 添加商品
     * @param data
     * @return
     */
    public Observable<BaseResult<Long>> addGoods(Map<String, String> data) {
        Map<String, String> signData = getSignData();
        Observable<BaseResult<Long>> observable = getObservable().addGoods(getSign(signData), data);
        return observable;
    }

    /**
     * 获取商品分类
     * @param shop_id
     * @return
     */
    public Observable<BaseResult<List<GoodsCategory>>> getGoodsCategory(long shop_id) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shop_id));
        Observable<BaseResult<List<GoodsCategory>>> observable = getObservable().getGoodsCategory(getSign(signData), signData);
        return observable;
    }


    /**
     * 根据商品分类获取商品
     * @param shopId
     * @param typeId
     * @return
     */
    public Observable<BaseResult<List<Goods>>> getShopGoodsByTypeId(long shopId,long typeId) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shopId));
        signData.put("typeId",typeId+"");
        Observable<BaseResult<List<Goods>>> observable = getObservable().getShopGoodsByTypeId(getSign(signData), signData);
        return observable;
    }

    /**
     * 获取商品单位
     * @param shop_id
     * @return
     */
    public Observable<BaseResult<List<GoodsUnit>>> getGoodsUnit(long shop_id) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shop_id));
        Observable<BaseResult<List<GoodsUnit>>> observable = getObservable().getGoodsUnit(getSign(signData), signData);
        return observable;
    }

    /**
     * 删除一个商品
     * @param itemId
     * @return
     */
    public Observable<BaseResult<String>> deleteGoods(long itemId) {
        Map<String, String> signData = getSignData();
        signData.put("itemId",itemId+"");
        Observable<BaseResult<String>> observable = getObservable().delGoods(getSign(signData), signData);
        return observable;
    }

    /**
     * 获取店铺所有商品
     * @param shop_id
     * @return
     */
    public Observable<BaseResult<List<Goods>>> getAllGoods(long shop_id) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shop_id));
        Observable<BaseResult<List<Goods>>> observable = getObservable().getAllGoods(getSign(signData), signData);
        return observable;
    }

    /**
     * 删除一个商品分类
     * @param goodsTypeId
     * @return
     */
    public Observable<BaseResult<Long>> delGoodsCategory(long goodsTypeId) {
        Map<String, String> signData = getSignData();
        signData.put("goodsTypeId", String.valueOf(goodsTypeId));
        Observable<BaseResult<Long>> observable = getObservable().delGoodsType(getSign(signData), signData);
        return observable;
    }

}
