package com.party.jackseller.api;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.User;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class MyFansService extends BaseService {

    public MyFansService(BaseActivityTitle baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<User>>> getFansList(long shopId) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shopId));
        Observable<BaseResult<List<User>>> observable = getObservable().getFanList(getSign(signData), signData);
        return observable;
    }
}
