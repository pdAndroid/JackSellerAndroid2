package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.base.OnNetListener;
import com.party.jackseller.bean.AccountBean;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.bean.PayResult;
import com.party.jackseller.bean.WxPaymentBean;

import java.util.Map;

import io.reactivex.Observable;

public class PayService extends BaseService {

    BaseActivity mBaseActivity;

    public PayService(BaseActivity baseActivity) {
        super(baseActivity);
        mBaseActivity = baseActivity;
    }

    /**
     * 支付宝支付
     *
     * @param orderId
     * @return
     */
    public Observable<BaseResult<String>> aliPayOrder(long orderId, String ip) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId + "");
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<String>> observable = getObservable().aliPayOrder(tokenMap);
        return observable;
    }

    /**
     * 钱包
     */
    public Observable<BaseResult<CommonResult>> getShopWallet() {
        Map<String, String> signData = getSignData();
        Observable<BaseResult<CommonResult>> observable = getObservable().getShopWallet(getSign(signData), signData);
        return observable;
    }


    /**
     * 微信支付
     *
     * @param orderId
     * @param ip
     * @return
     */
    public Observable<BaseResult<WxPaymentBean>> wxPayOrder(long orderId, String ip) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId + "");
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<WxPaymentBean>> observable = getObservable().wxPayOrder(tokenMap);
        return observable;
    }

    /**
     * 余额支付
     *
     * @param orderId
     * @param payPwd
     * @return
     */
    public Observable<BaseResult<PayResult>> doMyWalletPayOrder(String orderId, long shopId, String payPwd) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId);
        tokenMap.put("shopId", shopId + "");
        tokenMap.put("pwd", payPwd);
        Observable<BaseResult<PayResult>> observable = getObservable().doMyWalletPayOrder(tokenMap);
        return observable;
    }

    /**
     * 余额支付
     *
     * @param orderId
     * @param payPwd
     * @return
     */
    public Observable<BaseResult<PayResult>> doMyWalletPayOrder(long orderId, String payPwd) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId + "");
        tokenMap.put("pwd", payPwd);
        Observable<BaseResult<PayResult>> observable = getObservable().doMyWalletPayOrder(tokenMap);
        return observable;
    }

    /**
     * 支付宝支付
     *
     * @param orderId
     * @return
     */
    public Observable<BaseResult<String>> aliPayOrder(String orderId, String ip) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId);
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<String>> observable = getObservable().aliPayOrder(tokenMap);
        return observable;
    }

    /**
     * 微信支付
     *
     * @param orderId
     * @param ip
     * @return
     */
    public Observable<BaseResult<WxPaymentBean>> wxPayOrder(String orderId, String ip) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId);
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<WxPaymentBean>> observable = getObservable().wxPayOrder(tokenMap);
        return observable;
    }

    /**
     * 余额支付
     *
     * @param orderId
     * @param payPwd
     * @return
     */
    public Observable<BaseResult<PayResult>> doMyWalletPayOrder(String orderId, String payPwd) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId);
        tokenMap.put("pwd", payPwd);
        Observable<BaseResult<PayResult>> observable = getObservable().doMyWalletPayOrder(tokenMap);
        return observable;
    }

    /**
     * 余额支付
     *
     * @param orderId
     * @param payPwd
     * @return
     */
    public Observable<BaseResult<PayResult>> doMyWalletPayOrder(long shopId, String orderId, String payPwd) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("orderId", orderId);
        tokenMap.put("pwd", payPwd);
        Observable<BaseResult<PayResult>> observable = getObservable().doMyWalletPayOrder(tokenMap);
        return observable;
    }

    public Observable<BaseResult<AccountBean>> getAccount() {
        Map<String, String> tokenMap = getSignData();
        Observable<BaseResult<AccountBean>> observable = getObservable().getAccount(tokenMap);
        return observable;
    }

    public void getAccount(OnNetListener<AccountBean> netListener) {
        mBaseActivity.addDisposableIoMain(getAccount(), new DefaultConsumer<AccountBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AccountBean> baseBean) {
                netListener.complete(baseBean.getData());
            }
        });
    }

    public void cashMoney(String money, String password, OnNetListener<BaseResult> netListener, OnNetListener<BaseResult> error) {
        mBaseActivity.addDisposableIoMain(cashMoney(money, password), new DefaultConsumer<Object>(mApplication) {

            @Override
            public void operateError(BaseResult<Object> baseBean) {
                error.complete(baseBean);
            }

            @Override
            public void operateSuccess(BaseResult<Object> baseBean) {
                netListener.complete(baseBean);
            }
        });
    }

    public Observable<BaseResult<Object>> cashMoney(String money, String password) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("money", money);
        tokenMap.put("password", password);
        Observable<BaseResult<Object>> observable = getObservable().addPayBankRecord(tokenMap);
        return observable;
    }


    public void getShopMoney(OnNetListener<CommonResult> netListener) {
        mBaseActivity.addDisposableIoMain(getShopWallet(), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                netListener.complete(baseBean.getData());
            }
        });
    }
}
