package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.Constant;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.EatPersonCountBean;
import com.party.jackseller.bean.GroupBuyCoupon;
import com.party.jackseller.bean.ReplaceMoneyCoupon;
import com.party.jackseller.bean.ScanSuccessBean;
import com.party.jackseller.bean.SecondKillCoupon;
import com.party.jackseller.bean.SpellingGroupCoupon;
import com.party.jackseller.bean.SpellingGroupCouponDetailBean;
import com.party.jackseller.bean.VerifyCouponsBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by 派对 on 2018/8/8.
 */

public class CouponService extends BaseService {

    public CouponService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<ReplaceMoneyCoupon>>> getReplaceMoneyCouponList(long shop_id, int couponState, int pageNumber) {
        Map<String, String> signData = getSignData();
        signData.put("shopId", String.valueOf(shop_id));
        signData.put("couponState", couponState + "");
        signData.put("row", Constant.PAGE_SIZE + "");
        signData.put("page", pageNumber + "");
        Observable<BaseResult<List<ReplaceMoneyCoupon>>> observable = getObservable().getCouponList(getSign(signData), signData);
        return observable;
    }

    public Observable<BaseResult<Long>> commit(Map<String, String> data) {
        Map<String, String> signData = getSignData();
        signData.putAll(data);
        Observable<BaseResult<Long>> observable = getObservable().addGroupBuyCoupon(getSign(signData), signData);
        return observable;
    }

    /**
     * 添加代金券
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<Long>> addReplaceMoneyCoupon(Map<String, String> map) {
        Observable<BaseResult<Long>> observable = getObservable().addReplaceMoneyCoupon(getSign(map), map);
        return observable;
    }

    /**
     * 添加团购券
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<Long>> addGroupBuyCoupon(Map<String, String> map) {
        Observable<BaseResult<Long>> observable = getObservable().addGroupBuyCoupon(getSign(map), map);
        return observable;
    }

    public Observable<BaseResult<List<EatPersonCountBean>>> getDinnerNumber() {
        Map<String, String> signData = getSignData();
        Observable<BaseResult<List<EatPersonCountBean>>> observable = getObservable().getDinnerNumber(getSign(signData), signData);
        return observable;
    }

    /**
     * 通过state获取团购劵列表
     *
     * @param shop_id
     * @return
     */
    public Observable<BaseResult<List<GroupBuyCoupon>>> getGroupBuyList(long shop_id, int pageNumber) {
        Map<String, String> data = getSignData();
        data.put("shopId", shop_id + "");
        data.put("row", Constant.PAGE_SIZE + "");
        data.put("page", pageNumber + "");
        Observable<BaseResult<List<GroupBuyCoupon>>> observable = getObservable().getGroupBuyByShopId(getSign(data), data);
        return observable;
    }

    /**
     * 获取秒杀券列表
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<SecondKillCoupon>>> getSecondKillCouponList(long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<SecondKillCoupon>>> observable = getObservable().getSecondKillList(getSign(data), data);
        return observable;
    }

    /**
     * 添加秒杀券
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<Long>> addSecondKillCoupon(Map<String, String> map) {
        Observable<BaseResult<Long>> observable = getObservable().addSecondKillCoupon(getSign(map), map);
        return observable;
    }

    /**
     * 获取拼团券列表
     *
     * @param shopId
     * @return
     */
    public Observable<BaseResult<List<SpellingGroupCoupon>>> getSpellingGroupCouponList(long shopId) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        Observable<BaseResult<List<SpellingGroupCoupon>>> observable = getObservable().getSpellingGroupCouponList(getSign(data), data);
        return observable;
    }

    /**
     * 添加拼团券
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<Long>> addSpellingGroupCoupon(Map<String, String> map) {
        Observable<BaseResult<Long>> observable = getObservable().addSpellingGroupCoupon(getSign(map), map);
        return observable;
    }


    /**
     * 验证券是否可用
     *
     * @param orderCode
     * @return
     */
    public Observable<BaseResult<ScanSuccessBean>> verifyCoupon(String orderCode) {
        Map<String, String> signData = getSignData();
        signData.put("orderCode", orderCode);
        Observable<BaseResult<ScanSuccessBean>> observable = getObservable().verifyCoupon(getSign(signData), signData);
        return observable;
    }

    /**
     * 商家确认消费
     *
     * @param orderDetailId
     * @return
     */
    public Observable<BaseResult<CommonResult>> confirmConsume(String orderDetailId) {
        Map<String, String> signData = getSignData();
        signData.put("orderDetailId", orderDetailId);
        Observable<BaseResult<CommonResult>> observable = getObservable().confirmConsume(getSign(signData), signData);
        return observable;
    }

    public Observable<BaseResult<List<SpellingGroupCouponDetailBean>>> getSpellingGroupCouponDetail(long spellingGroupCouponId) {
        Map<String, String> data = getSignData();
        data.put("spellingGroupId", spellingGroupCouponId + "");
        Observable<BaseResult<List<SpellingGroupCouponDetailBean>>> observable = getObservable().getSpellingGroupDetail(getSign(data), data);
        return observable;
    }

    public Observable<BaseResult<VerifyCouponsBean>> verifyCoupons(String orderCode) {
        Map<String, String> data = getSignData();
        data.put("orderCode", orderCode);
        Observable<BaseResult<VerifyCouponsBean>> observable = getObservable().verifyCoupons(data);
        return observable;
    }

}
