package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.ApplicationInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.BindingInfo;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.ConfirmBindingAccountBean;
import com.party.jackseller.bean.LoginBean;
import com.party.jackseller.bean.RegisterInfo;
import com.party.jackseller.bean.User;
import com.party.jackseller.bean.VerifyUser;
import com.party.jackseller.bean.VideoCondition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by 派对 on 2018/7/23.
 */

public class UserService extends BaseService {

    public UserService(BaseActivityTitle baseActivity) {
        super(baseActivity);
    }

    public UserService(BaseActivity activity) {
        super(activity);
    }

    public Observable<BaseResult<LoginBean>> loginUser(String phone, String verCode) {
        Map<String, String> map = getLocalMap();
        map.put("phone", phone);
        map.put("verCode", verCode);
        Observable<BaseResult<LoginBean>> observable = getObservable().login(map);
        return observable;
    }

    public Observable<BaseResult<User>> registerUser(String registerKey, String phone, String verCode, String inviteCode) {
        Map<String, String> map = getLocalMap();
        map.put("verCode", verCode);
        map.put("phone", phone);
        map.put("phoneParent", inviteCode);
        map.put("registerKey", registerKey);
        Observable<BaseResult<User>> observable = getObservable().register(map);
        return observable;
    }


    public Observable<BaseResult<Object>> getVerCode(String phone, String type) {
        Map<String, String> signData = new HashMap<>();
        signData.put("phone",phone);
        signData.put("type",type);
        Observable<BaseResult<Object>> observable = getObservable().getVerCode(signData);
        return observable;
    }


    /**
     * 获得添加视频筛选条件
     */
    public Observable<BaseResult<VideoCondition>> listVideoCondition() {
        Map<String, String> signData = getSignData();
        Observable<BaseResult<VideoCondition>> observable = getObservable().listVideoCondition(getSign(signData), signData);
        return observable;
    }

    public Observable<BaseResult<VerifyUser>> authentication(String idCard, String name) {
        Map<String, String> signData = getSignData();
        signData.put("idCard", idCard);
        signData.put("name", name);
        Observable<BaseResult<VerifyUser>> observable = getObservable().authentication(getSign(signData), signData);
        return observable;
    }

    public Observable<BaseResult<String>> alterPayPassword(String payPassword, String verKey) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("payPassword", payPassword);
        tokenMap.put("verKey", verKey);

        Observable<BaseResult<String>> observable = getObservable().alterPayPassword(tokenMap);
        return observable;
    }

    public Observable<BaseResult<VerifyUser>> verifVerCode(String verCode, String phone, String idCard) {
        Map<String, String> map = getSignData();
        map.put("verCode", verCode);
        map.put("phone", phone);
        map.put("idcard", idCard);
        Observable<BaseResult<VerifyUser>> observable = getObservable().verifVerCode(map);
        return observable;
    }


    public Observable<BaseResult<Object>> getUpdatePasswordVerCode(String phone) {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("phone", phone);
        Observable<BaseResult<Object>> observable = getObservable().getUpdatePasswordVerCode(tokenMap);
        return observable;
    }

    public Observable<BaseResult<LoginBean>> wxLogin(String code) {
        HashMap<String, String> map = new HashMap<>();
        map.put("wxCode", code);
        Observable<BaseResult<LoginBean>> observable = getObservable().wxLogin(map);
        return observable;
    }


    public void getAppId(DefaultConsumer consumer) {
        Observable<BaseResult<AppInfo>> observable = getObservable().getAppId();
        baseActivity.addDisposableIoMain(observable, consumer);
    }

    public Observable<BaseResult<RegisterInfo>> checkWxIsRegister(String code) {
        HashMap<String, String> map = new HashMap<>();
        map.put("wxCode", code);
        Observable<BaseResult<RegisterInfo>> observable = getObservable().checkWxIsRegister(map);
        return observable;
    }

    public Observable<BaseResult<BindingInfo>> getBindingUserInfo(String code, String cardType, String shopId) {
        HashMap<String, String> map = getTokenMap();
        map.put("code", code);
        map.put("cardType", cardType);
        map.put("shopId", shopId);
        Observable<BaseResult<BindingInfo>> observable = getObservable().getBindingUserInfo(map);
        return observable;
    }

    public Observable<BaseResult<ConfirmBindingAccountBean>> getConfirmBindingAccount(String verKey, String password) {
        Map<String, String> map = getSignData();
        map.put("verKey", verKey);
        map.put("password", password);
        Observable<BaseResult<ConfirmBindingAccountBean>> observable = getObservable().getConfirmBindingAccount(map);
        return observable;
    }

    public Observable<BaseResult<Object>> bindingWx(String bindingKey) {
        HashMap<String, String> map = getTokenMap();
        map.put("bindingKey", bindingKey);
        Observable<BaseResult<Object>> observable = getObservable().userBindingWx(map);
        return observable;
    }

//    public Observable<BaseResult<ApplicationInfo>> getApplicationId() {
//        HashMap<String, String> map = getTokenMap();
//        Observable<BaseResult<ApplicationInfo>> observable = getObservable().getApplicationId(map);
//        return observable;
//    }

    /**
     * 获取阿里认证信息
     *
     * @return
     */
    public Observable<BaseResult<String>> getAliAuthInfoStr() {
        HashMap<String, String> map = getTokenMap();
        Observable<BaseResult<String>> observable = getObservable().getAliAuthInfo(map);
        return observable;
    }

    public Observable<BaseResult<BindingInfo>> checkZMScoreAndBindingAli(String auth_code) {
        HashMap<String, String> map = getTokenMap();
        map.put("accessToken", auth_code);
        Observable<BaseResult<BindingInfo>> observable = getObservable().checkZMScoreAndBindingAli(map);
        return observable;
    }
}
