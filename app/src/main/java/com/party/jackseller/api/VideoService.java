package com.party.jackseller.api;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.VideoBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * <a href="https://www.cnblogs.com/blest-future/p/4628871.html">Map集合的四种遍历方式</a>
 */
public class VideoService extends BaseService {
    public VideoService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 上传视频到后台
     *
     * @param map
     * @return
     */
    public Observable<BaseResult<CommonResult>> addVideo(Map<String, String> map) {
        Map<String, String> data = getSignData();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            data.put(entry.getKey(), entry.getValue());
        }
        Observable<BaseResult<CommonResult>> observable = getObservable().addVideo(getSign(data), data);
        return observable;
    }

    /**
     * 获取我的视频
     */
    public Observable<BaseResult<List<VideoBean>>> listVideoByShopId(long shopId, int page, int pageSize) {
        Map<String, String> data = getSignData();
        data.put("shopId", shopId + "");
        data.put("page", page + "");
        data.put("row", pageSize + "");
        Observable<BaseResult<List<VideoBean>>> observable = getObservable().listVideoByShopId(getSign(data), data);
        return observable;
    }
}
