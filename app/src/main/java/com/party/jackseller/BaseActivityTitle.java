package com.party.jackseller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.party.jackseller.choose_location.ChooseLocationActivity;
import com.party.jackseller.permission.RuntimeRationale;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.DensityUtils;
import com.party.jackseller.utils.ToastUtils;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;

import java.util.List;

public abstract class BaseActivityTitle extends BaseActivity {
    protected View mTitleView;
    protected ImageView mLeftIv;
    protected TextView mTitleTv;
    protected TextView mRightTv;


    /**
     * @param toView
     * @param topView    界面定模图片，用于匹配界面
     * @param scrollView
     */
    public void scrollTo(View toView, View topView, ScrollView scrollView) {
        int[] y = new int[2];
        topView.getLocationInWindow(y);
        int[] y1 = new int[2];
        toView.getLocationInWindow(y1); //获取在当前窗口内的绝对坐标
        scrollView.scrollTo(0, y1[1] - y[1]);
        shake(toView);
    }

    public void shake(View v) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);//加载动画资源文件
        v.startAnimation(shake);
    }

    /**
     * 点击左边按钮事件
     */
    public void clickLeftBtn() {
        finish();
    }

    /**
     * 点击右边按钮事件
     */
    public void clickRightBtn() {

    }

    public void handleTitle() {
        mTitleView = View.inflate(mActivity, R.layout.layout_common_title, null);
        int height = DensityUtils.dip2px(mActivity, 48f);
        FrameLayout contentView = (FrameLayout) findViewById(android.R.id.content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height);
        View rootView = contentView.getChildAt(0);
        contentView.addView(mTitleView, 0, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        layoutParams2.topMargin = height;
        rootView.setLayoutParams(layoutParams2);

        mLeftIv = mTitleView.findViewById(R.id.iv_back);
        mTitleTv = mTitleView.findViewById(R.id.tv_title);
        mRightTv = mTitleView.findViewById(R.id.tv_right);
        mLeftIv.setOnClickListener((View v) -> {
            clickLeftBtn();
        });
        mRightTv.setOnClickListener((View v) -> {
            clickRightBtn();
        });
    }

    public void setMiddleText(String text) {
        mTitleTv.setText(text);
    }

    public void setRightText(String text) {
        mRightTv.setVisibility(View.VISIBLE);
        mRightTv.setText(text);
    }


    protected void hideSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void checkPermission() {
        final String[] permissions = new String[]{Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION};
        AndPermission.with(mActivity)
                .runtime()
                .permission(permissions)
                .rationale(new RuntimeRationale())
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限成功");
                        ToastUtils.t(mApplication, "ok");
                        Intent intent = new Intent(mActivity, ChooseLocationActivity.class);
                        startActivityForResult(intent, ConstUtils.SEARCH_LOCATION_CODE);
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限失败");

                    }
                }).start();
    }
}
