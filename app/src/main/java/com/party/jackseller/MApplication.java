package com.party.jackseller;

import android.app.Application;
import android.app.Notification;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.facebook.stetho.Stetho;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.ShopAdverts;
import com.party.jackseller.bean.User;
import com.party.jackseller.controller.AMapController;
import com.party.jackseller.event.AMapBeanEvent;
import com.party.jackseller.imageloader.GlideLoaderFactory;
import com.party.jackseller.imageloader.ImageLoaderFactory;
import com.party.jackseller.litvedio.TCInitController;
import com.party.jackseller.net.okhttp.CacheUtils;
import com.party.jackseller.net.okhttp.HttpLogger;
import com.party.jackseller.net.okhttp.OkHttpUtils;
import com.party.jackseller.utils.NetworkUtils;
import com.party.jackseller.utils.SPUtils;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.tencent.bugly.crashreport.CrashReport;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import cn.jpush.android.api.BasicPushNotificationBuilder;
import cn.jpush.android.api.JPushInterface;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * <a href="https://www.jianshu.com/p/03c28e64b636">Android的multidex带来NoClassDefFoundError?</a><br>
 * Created by Tangxb on 2017/2/14.
 */

public class MApplication extends Application {
    private ImageLoaderFactory imageLoaderFactory;
    private Stack<WeakReference<BaseActivity>> mActivityStack;
    private Shop currShop;
    private User mUser;
    private String mToken;
    private double currShopLongitude = -1F, currShopLatitude = -1F;
    Set<String> tags = new HashSet<String>();
    /**
     * 地理位置相关的
     */
    private AMapBeanEvent aMapBeanEvent;

    public AMapBeanEvent getMap() {
        return aMapBeanEvent;
    }

    public void setMap(AMapBeanEvent aMapBeanEvent) {
        this.aMapBeanEvent = aMapBeanEvent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        initDebugSth();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public void initDebugSth() {
        CrashReport.initCrashReport(getApplicationContext(), "359bc025ff", false);
        Stetho.initializeWithDefaults(this);
    }

    private void init() {
        initTCSdk();
        initZxing();
        initLoadView();
        NetworkUtils.setContext(this);
        CacheUtils.setContext(this);
        initOkHttpUtils();
        initImageLoaderFactory();
        initBugly();
        Logger.addLogAdapter(new AndroidLogAdapter());
        initActivityStack();
        initJPush();
    }

    private void initJPush() {
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        changeJpushData();

    }

    private void initTCSdk() {
        TCInitController.initTCSdk(this);
    }

    private void initZxing() {
        ZXingLibrary.initDisplayOpinion(this);
    }

    private void jpushSet() {
        BasicPushNotificationBuilder builder = new BasicPushNotificationBuilder(this);
        builder.statusBarDrawable = R.mipmap.scan_image;
        builder.notificationDefaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;
        // 设置为铃声、震动、呼吸灯闪烁都要
        JPushInterface.setPushNotificationBuilder(1, builder);
//        CustomPushNotificationBuilder builder1 = new CustomPushNotificationBuilder(this, R.layout.customer_notitfication_layout, R.id.icon, R.id.title, R.id.text);
//        builder1.layoutIconDrawable = R.mipmap.shenqi;
//        builder.developerArg0 = "developerArg2";
//        JPushInterface.setPushNotificationBuilder(2, builder1);
    }

    private void initLoadView() {
        LoadViewHelper.getBuilder()
                .setLoadEmpty(R.layout.load_empty)
                .setLoadError(R.layout.load_error)
                .setLoadIng(R.layout.load_ing);
    }

    private void initActivityStack() {
        mActivityStack = new Stack<>();
    }

    private void initOkHttpUtils() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLogger());
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10 * 1000L, TimeUnit.MILLISECONDS)
                .readTimeout(60 * 1000L, TimeUnit.MILLISECONDS)
                .writeTimeout(60 * 1000L, TimeUnit.MILLISECONDS)
//                .addInterceptor(logInterceptor)
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }


    private void initImageLoaderFactory() {
        imageLoaderFactory = new GlideLoaderFactory();
    }

    public ImageLoaderFactory getImageLoaderFactory() {
        return imageLoaderFactory;
    }


    public void registerEventBus(Object object) {
        try {
            EventBus.getDefault().register(object);
        } catch (Exception e) {
        }
    }

    public void unregisterEventBus(Object object) {
        try {
            EventBus.getDefault().unregister(object);
        } catch (Exception e) {
        }
    }

    private void initBugly() {
        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化Bugly
        CrashReport.initCrashReport(context, BuildConfig.BUG_APP_ID, BuildConfig.DEBUG, strategy);
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 添加Activity到ActivityStack里面
     *
     * @param activity
     */
    public void pushActivity(BaseActivity activity) {
        mActivityStack.push(new WeakReference<>(activity));
    }


    public void removeActivity(BaseActivity baseActivity) {
        WeakReference<BaseActivity> weak = null;
        for (WeakReference<BaseActivity> reference : mActivityStack) {
            if (reference != null && reference.get() != null) {
                if (reference.get().getClass().getName().equals(baseActivity.getClass().getName())) {
                    weak = reference;
                    break;
                }
            }
        }
        if (weak != null) {
            mActivityStack.remove(weak);
        }
    }


    public BaseActivity getTopActivity() {
        WeakReference<BaseActivity> weak = null;
        try {
            weak = mActivityStack.peek();
        } catch (Exception e) {
        }
        if (weak != null) {
            return weak.get();
        }
        return null;
    }

    public void post(Object object) {
        EventBus.getDefault().post(object);
    }

    /**
     * sp相关操作请用SPUtils,里面的context请用application
     *
     * @param user
     */
    public void setUser(User user) {
        mUser = user;
        String objectStr = JSON.toJSONString(user);
        SPUtils.put(this, "current_user", objectStr);
        if (mUser != null && !TextUtils.isEmpty(mUser.getPhone())) {
            TCInitController.checkUserHasRegister(mUser.getPhone());
        }
    }


    public User getUser() {
        if (mUser == null) mUser = SPUtils.getObject(this, "current_user", User.class);
        //防止空指针异常
        if (mUser == null) return new User();

        return mUser;
    }

    public void saveShop() {
        setCurrShop(currShop);
    }

    public Shop getCurrShop() {
        if (currShop == null) currShop = SPUtils.getObject(this, "current_shop", Shop.class);
        if (currShop == null) return new Shop();
        return currShop;
    }

    public void setCurrShop(Shop shop) {
        currShop = shop;
        changeJpushData();
        String objectStr = JSON.toJSONString(shop);
        SPUtils.put(this, "current_shop", objectStr);
    }

    public void changeJpushData() {
        if (getCurrShop() != null && getCurrShop().getId() > 0)
            JPushInterface.setAlias(this, 1, "record" + getCurrShop().getId());
    }


    public String getToken() {
        if (mToken != null) {
            return mToken;
        }
        return getUser().getToken();
    }

    public void cleanUser() {
        mToken = null;
        mUser = null;
        SPUtils.remove(this, "current_user");
    }

    public void clearCurrShop() {
        SPUtils.remove(this, "current_shop");
        currShop = null;
        currShopLongitude = -1F;
        currShopLatitude = -1F;
    }

    /**
     * 获取用户选择的最新门店id
     *
     * @return
     */
    public long getCurrShopId() {
        return getCurrShop().getId();
    }

    public String getCurrShopName() {
        return getCurrShop().getShop_name();
    }


    public boolean isMainActivity() {
        int size = mActivityStack.size();
        if (size < 2) return false;
        try {
            WeakReference<BaseActivity> reference = mActivityStack.elementAt(size - 1 - 1);
            if (reference.get().getClass().getName().equals(MainActivity.class.getName())) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isCurrShopOnline() {
        if (getCurrShop().getShop_state() != Constant.SHOP_STATUS_ON_LINE) {
            return false;
        } else {
            return true;
        }
    }

    public double getCurrShopLongitude() {
        if (currShopLongitude == -1D) {
            String str = (String) SPUtils.get(this, "curr_shop_longitude", "");
            if (!TextUtils.isEmpty(str)) {
                try {
                    currShopLongitude = Double.valueOf(str);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return currShopLongitude;
    }

    public double getCurrShopLatitude() {
        if (currShopLatitude == -1D) {
            String str = (String) SPUtils.get(this, "curr_shop_latitude", "");
            if (!TextUtils.isEmpty(str)) {
                try {
                    currShopLatitude = Double.valueOf(str);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return currShopLatitude;
    }


    public AMapController updateMap(AMapController.LocationFinish finishListener) {
        AMapController mapController = new AMapController(getTopActivity());
        mapController.startLocation();
        mapController.setFinishListener((AMapBeanEvent mapBeanEvent) -> {
            mapController.destroyLocation();
            finishListener.finish(mapBeanEvent);
        });
        return mapController;
    }

    private ShopAdverts replaceShop;
    public void setReplaceShop(ShopAdverts replaceShop) {
        this.replaceShop = replaceShop;
    }

    public ShopAdverts getReplaceShop(){
        return replaceShop;
    }


}
