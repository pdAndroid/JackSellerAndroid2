package com.party.jackseller.event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.party.jackseller.bean.Shop;

public class AMapBeanEvent {
    private int errorCode;
    private double longitude;
    private double latitude;
    private String address;
    private String cityCode;
    private String road;
    private String adCode;

    // 中国
    private String country;
    // 四川省
    private String province;
    // 成都市
    private String city;
    // 温江区
    private String district;
    // 公平街道
    private String street;

    public AMapBeanEvent() {
    }

    public AMapBeanEvent(double latitude, double longitude, String address, String country, String province, String city, String district, String street, String cityCode, String adCode) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
        this.cityCode = cityCode;
        this.adCode = adCode;
        this.country = country;
        this.province = province;
        this.city = city;
        this.district = district;
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
