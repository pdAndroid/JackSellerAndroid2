package com.party.jackseller.event;

/**
 * 支付宝和微信的支付回调
 */
public class AuthResultEvent {

    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AuthResultEvent(String code) {
        this.code = code;
    }
}
