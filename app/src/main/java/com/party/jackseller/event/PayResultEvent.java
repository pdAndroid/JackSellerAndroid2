package com.party.jackseller.event;

/**
 * 支付宝和微信的支付回调
 */
public class PayResultEvent {
    /**
     * 0:微信支付  1:支付宝支付 2 :钱包支付
     */
    private int payType;
    /**
     * 0:取消付款 -1:参数错误 1:付款成功
     */
    private int resultType;

    public PayResultEvent(int payType, int resultType) {
        this.payType = payType;
        this.resultType = resultType;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    /**
     * 获取支付结果字符串
     *
     * @return
     */
    public String getResultMsg() {
        String resultMsg = "支付结果未知";
        if (resultType == 1) {
            resultMsg = "付款成功";
        } else if (resultType == -1) {
            resultMsg = "参数错误";
        } else if (resultType == 0) {
            resultMsg = "取消付款";
        }
        return resultMsg;
    }
}
