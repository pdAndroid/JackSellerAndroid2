package com.party.jackseller.event;

import com.party.jackseller.bean.ImageBean;

import java.util.ArrayList;

public class OnClickImageEvent {
    public boolean isAdded;
    public int itemPosition;
    public int position;
    public String url;
    public ArrayList<ImageBean> urlList;

    public OnClickImageEvent(boolean isAdded, int itemPosition, int position, String url, ArrayList<ImageBean> urlList) {
        this.isAdded = isAdded;
        this.itemPosition = itemPosition;
        this.position = position;
        this.url = url;
        this.urlList = urlList;
    }
}
