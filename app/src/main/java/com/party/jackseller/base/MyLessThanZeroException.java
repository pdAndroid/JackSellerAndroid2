package com.party.jackseller.base;

import android.view.View;

/**
 * Created by 派对 on 2018/10/22.
 */

public class MyLessThanZeroException extends Exception {

    private Integer position;

    public MyLessThanZeroException(Integer position, String message) {
        super(message);
        this.position = position;
    }


    public int getPosition() {
        return position;
    }
}
