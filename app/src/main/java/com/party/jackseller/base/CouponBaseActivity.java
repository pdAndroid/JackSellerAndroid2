package com.party.jackseller.base;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ScrollView;
import android.widget.TextView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;


public class CouponBaseActivity extends BaseActivityTitle {

    @BindView(R.id.scroll_layout)
    ScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

    public void onPickDateEvent(final TextView tView, final Date date, final String top) {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        selectedDate.setTime(date);
        new DatePickerDialog(mActivity, (DatePicker view, int year, int month, int day) -> {
            Date checkDate = null;
            String tempDate = year + "-" + (month + 1) + "-" + day;
            try {
                checkDate = formatDate.parse(tempDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date.compareTo(checkDate) == -1) {
                tView.setText(tempDate);
                return;
            }
            showToast(top);
        }
                , selectedDate.get(Calendar.YEAR)
                , selectedDate.get(Calendar.MONTH)
                , selectedDate.get(Calendar.DATE) + 1
        ).show();
    }

    public void scrollTo(View v) {
        scrollTo(v, scrollView, scrollView);
    }

}
