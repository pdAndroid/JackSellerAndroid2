package com.party.jackseller.base;

import android.view.View;

/**
 * Created by 派对 on 2018/10/22.
 */

public class MyNullException extends Exception {

    private View view;

    public MyNullException(View view,String message) {
        super(message);
        this.view = view;
    }

    public View getView() {
        return view;
    }
}
