package com.party.jackseller.base;

import com.party.jackseller.bean.Shop;

/**
 * Created by 派对 on 2018/10/23.
 */

public enum ShopState {

    error(-1, "未知状态", false),
    Frozen(0, "冻结", false),
    Open(1, "已上线", true),
    NotUser(2, "未认领", false),
    Auditing(3, "审核中", false),
    AuditFail(4, "审核未通过", false),
    Appeal(5, "申诉中", false),
    AppealFail(6, "申诉未通过", false);

    private int state;
    private String name;
    private boolean enable;

    ShopState(int state, String name, boolean enable) {
        this.state = state;
        this.name = name;
        this.enable = enable;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public static ShopState getState(Shop shop) {
        int shop_state = shop.getShop_state();
        ShopState[] values = ShopState.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].getState() == shop_state) {
                return values[i];
            }
        }
        return error;
    }
}
