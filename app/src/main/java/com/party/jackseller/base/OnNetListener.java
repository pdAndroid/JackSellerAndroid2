package com.party.jackseller.base;

/**
 * Created by 派对 on 2019/1/25.
 */

public interface OnNetListener<T> {
    void complete(T data);
}