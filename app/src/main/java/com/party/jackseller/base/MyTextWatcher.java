package com.party.jackseller.base;


import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by 派对 on 2018/12/27.
 */

public class MyTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
