package com.party.jackseller.utils;

import android.content.Context;
import android.text.TextUtils;

import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.geocoder.RegeocodeRoad;
import com.party.jackseller.event.AMapBeanEvent;

import java.util.List;

/**
 * 高德地图逆地理编码
 */
public class AMapUtils implements GeocodeSearch.OnGeocodeSearchListener {
    private GeocodeSearch geoCoderSearch;
    private RegeocodeSearchedListener listener;

    public interface RegeocodeSearchedListener {
        /**
         * 逆地理编码回调
         *
         * @param bean
         */
        void onRegeocodeSearched(AMapBeanEvent bean);
    }

    public AMapUtils(Context context) {
        geoCoderSearch = new GeocodeSearch(context);
        geoCoderSearch.setOnGeocodeSearchListener(this);
    }

    /**
     * 逆地理编码
     */
    public void geoCoderSearch(LatLonPoint latLonPoint, RegeocodeSearchedListener listener) {
        this.listener = listener;
        // 第一个参数表示一个Latlng,第二参数表示范围多少米,第三个参数表示是火系坐标系还是GPS原生坐标系
        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200, GeocodeSearch.AMAP);
        // 设置异步逆地理编码请求
        geoCoderSearch.getFromLocationAsyn(query);
    }

    /**
     * 逆地理编码回调
     */
    @Override
    public void onRegeocodeSearched(RegeocodeResult result, int rCode) {
        if (rCode == AMapException.CODE_AMAP_SUCCESS) {
            RegeocodeAddress regeocodeAddress = result.getRegeocodeAddress();
            if (result != null && regeocodeAddress != null && regeocodeAddress.getFormatAddress() != null) {
                // 花都大道西段341号
                String formatAddress = regeocodeAddress.getFormatAddress();
                // 中国
                String country = regeocodeAddress.getCountry();
                // 四川省
                String province = regeocodeAddress.getProvince();
                // 成都市
                String city = regeocodeAddress.getCity();
                // 温江区
                String district = regeocodeAddress.getDistrict();
                // 公平街道
                String street = regeocodeAddress.getTownship();
                String cityCode = regeocodeAddress.getCityCode();
                String adCode = regeocodeAddress.getAdCode();
                double latitude = 0d;
                double longitude = 0d;
                if (TextUtils.isEmpty(street)) {
                    List<RegeocodeRoad> roads = regeocodeAddress.getRoads();
                    if (roads != null) {
                        int size = roads.size();
                        if (size > 0) {
                            street = roads.get(Math.max(size - 1, 0)).getName();
                            latitude = roads.get(Math.max(size - 1, 0)).getLatLngPoint().getLatitude();
                            longitude = roads.get(Math.max(size - 1, 0)).getLatLngPoint().getLongitude();
                        }
                    }
                }

                if (listener != null) {
                    if (latitude == 0d) {
                        List<PoiItem> poiItemList = regeocodeAddress.getPois();
                        if (poiItemList != null && poiItemList.size() > 0) {
                            latitude = poiItemList.get(0).getLatLonPoint().getLatitude();
                            longitude = poiItemList.get(0).getLatLonPoint().getLongitude();
                        }
                    }

                    AMapBeanEvent bean = new AMapBeanEvent(latitude, longitude, formatAddress, country, province, city, district, street, cityCode, adCode);
                    listener.onRegeocodeSearched(bean);
                }
            } else {

            }
        }
    }

    /**
     * 地理编码查询回调
     */
    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }
}
