package com.party.jackseller.utils;

import com.tencent.ugc.TXVideoEditConstants;

public class ConstUtils {
    /**
     * 表示本地图片的前缀
     */
    public static final String LOCAL_STORAGE = "localStorage";
    /**
     * 保存登录的用户名key
     */
    public static final String ACCOUNT_KEY = "account";
    /**
     * 保存登录的密码key
     */
    public static final String PASSWORD_KEY = "password";
    /**
     * 每一页的页数大小
     */
    public static final int PAGE_SIZE = 12;
    /**
     * 查看图片详情请求码
     */
    public static final int IMAGE_DETAIL_CODE = 500;
    /**
     * 添加粘鼠板请求码
     */
    public static final int ADD_STEP_TYPE_1_CODE = 501;
    /**
     * 添加害虫请求码
     */
    public static final int CHOOSE_PEST_CODE = 502;
    /**
     * 安装未知源请求码
     */
    public static final int MANAGE_UNKNOWN_APP_SOURCES_CODE = 503;
    /**
     * 修改系统设置请求码
     */
    public static final int MANAGE_WRITE_SETTINGS_CODE = 504;
    /**
     * 风险评估类型1
     */
    public static final int RISK_TYPE_1 = 1;
    /**
     * 风险评估类型2
     */
    public static final int RISK_TYPE_2 = 2;
    /**
     * 风险评估类型3
     */
    public static final int RISK_TYPE_3 = 3;

    /**
     * 登录之后有值
     */
    public static String UID = null;
    /**
     * 登录之后有值
     */
    public static String MID = null;
    /**
     * 任务助手消息类型
     */
    public static final int TASK_ASSISTANT_TYPE = 2;
    /**
     * 系统消息消息类型
     */
    public static final int SYSTEM_MESSAGE_TYPE = 1;
    /**
     * 企业公告消息类型
     */
    public static final int ENTERPRISE_ANNOUNCEMENT_TYPE = 0;
    /**
     * 选择相册
     */
    public static final int RC_ALBUM = 1;
    /**
     * 选择拍照
     */
    public static final int RC_CAMERA = 0;
    /**
     * 选择相册最大数量
     */
    public static final int CHOOSE_PICTURE_MAX_COUNT = 1;
    /**
     * 选择相册每一行显示的数量
     */
    public static final int CHOOSE_PICTURE_EVERY_ROW_COUNT = 5;
    /**
     * 选择相册的列数
     */
    public static final int CHOOSE_PICTURE_COLUMN = 3;

    /**
     * 搜索请求码
     */
    public static final int SEARCH_CODE = 700;
    /**
     * 搜索地址请求码
     */
    public static final int SEARCH_LOCATION_CODE = 701;
    /**
     * 扫一扫请求码
     */
    public static final int SCAN_CODE = 702;
    /**
     * 相册选择图片
     */
    public static final int CHOOSE_ALBUM_CODE = 703;
    /**
     * 距离误差在多少范围内算是距离改变了,单位为米
     */
    public static final int LOCATION_CHANGE_MIN_DISTANCE = 500;
    /**
     * 是否一直定位
     */
    public static boolean LOCATION_ALWAYS = false;
    /**
     * 当前应用类型 0:客户端 1:商家端 2:管理端
     */
    public static final int CURRENT_APP_TYPE = 1;
    /**
     * 录制视频的最短时间
     */
    public static final int RECORD_VIDEO_MIN_DURATION = 6 * 1000;
    /**
     * 录制视频的最长时间
     */
    public static final int RECORD_VIDEO_MAX_DURATION = 60 * 1000;
    /**
     * 录制视频的分辨率
     */
    public static final int VIDEO_RECORD_RESOLUTION = TXVideoEditConstants.VIDEO_COMPRESSED_480P;

    /**
     * 上传图片压缩时候保存的文件夹
     *
     * @return
     */
    public static String uploadNeedPictureDir() {
        String sdCardPath = SDCardFileUtils.getSDCardPath();
        String dir2SDCard = SDCardFileUtils.creatDir2SDCard(sdCardPath + "KillDebug/OperateApp/uploadNeedPicture");
        return dir2SDCard;
    }

    /**
     * 用户签名图片保存的文件夹
     *
     * @return
     */
    public static String signaturePictureDir() {
        String sdCardPath = SDCardFileUtils.getSDCardPath();
        String dir2SDCard = SDCardFileUtils.creatDir2SDCard(sdCardPath + "KillDebug/OperateApp/signaturePicture");
        return dir2SDCard;
    }
}
