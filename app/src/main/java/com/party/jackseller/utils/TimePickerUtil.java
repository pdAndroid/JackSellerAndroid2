package com.party.jackseller.utils;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by Administrator on 2018/8/31.
 */

public class TimePickerUtil {
    public static void timePicker(final View view, Activity activity){
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        new TimePickerDialog(activity,
                // 绑定监听器
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker time, int hourOfDay, int minute) {
                        TextView tv = (TextView) view;
                        tv.setText(validate(hourOfDay) + ":" +validate(minute) );
                    }
                }
                // 设置初始时间
                , selectedDate.get(Calendar.HOUR_OF_DAY)
                , selectedDate.get(Calendar.MINUTE)
                , true).show();
    }
    private static String validate(int time){
        return time<10? "0"+time:""+time;
    }
}
