package com.party.jackseller.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

public class MDrawableUtils {
    /**
     * 设置TextView左边的Drawable
     *
     * @param context
     * @param textView
     * @param resId
     */
    public static void setTvLeftDrawable(Context context, TextView textView, int resId) {
        Drawable drawable = null;
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(context, resId);
        }
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        }
        textView.setCompoundDrawables(drawable, null, null, null);
    }
}
