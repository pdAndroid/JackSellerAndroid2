package com.party.jackseller.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

/**
 * <a href="https://blog.csdn.net/yaphetzhao/article/details/49639097">Android ContextThemeWrapper cannot be cast to android.app.Activity</a>
 */
public class MContextUtils {
    /**
     * Android ContextThemeWrapper cannot be cast to android.app.Activity
     *
     * @param cont
     * @return
     */
    public static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());
        return null;
    }

    public static Application getApplication(Context cont) {
        return scanForActivity(cont).getApplication();
    }
}
