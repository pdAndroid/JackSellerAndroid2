package com.party.jackseller.utils;

import com.google.gson.Gson;

public class GsonUtils {
    private static GsonUtils instance;
    private Gson gson;

    private GsonUtils() {
        gson = new Gson();
    }

    public static GsonUtils getInstance() {
        if (instance == null) {
            synchronized (GsonUtils.class) {
                if (instance == null) {
                    instance = new GsonUtils();
                }
            }
        }
        return instance;
    }

    public String toJson(Object obj) {
        return gson.toJson(obj);
    }

}
