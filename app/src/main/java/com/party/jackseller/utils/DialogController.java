package com.party.jackseller.utils;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.party.jackseller.R;
import com.party.jackseller.view.ActionSheetDialog;
import com.party.jackseller.view.MessageDialog;
import com.party.jackseller.widget.InputPayPasswordView;
import com.party.jackseller.widget.dialog.AutoDismissMessageDialog;
import com.party.jackseller.widget.dialog.InputCountDialog;
import com.party.jackseller.widget.dialog.EditTextDialog;
import com.party.jackseller.widget.dialog.SubmitSuccessDialog;
import com.zyyoona7.popup.EasyPopup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.reactivex.internal.subscriptions.SubscriptionHelper.validate;

public class DialogController {
    /**
     * 显示消息提示对话框
     */
    public static void showConfirmDialog(Context context
            , String title
            , String message
            , String okTxt
            , String cancelTxt
            , View.OnClickListener okListener
            , View.OnClickListener cancelListener) {
        new MessageDialog(context).builder().setTitle(title)
                .setMsg(message)
                .setPositiveButton(okTxt, okListener)
                .setNegativeButton(cancelTxt, cancelListener).show();
    }


    public static void showConfirmDialog(Context context, String title, String message, View.OnClickListener okListener, View.OnClickListener cancelListener) {
        showConfirmDialog(context, title, message, "确认", "取消", okListener, cancelListener);
    }

    public static void showConfirmDialog(Context context, String title, String message, String okStr, View.OnClickListener okListener) {
        showConfirmDialog(context, title, message, okStr, "取消", okListener, (View v) -> {
        });
    }

    public static void showConfirmDialog(Context context, String title, String message, View.OnClickListener okListener) {
        new MessageDialog(context).builder().setTitle(title)
                .setMsg(message)
                .setPositiveButton("确认", okListener)
                .setNegativeButton("取消", (View v) -> {
                }).show();
    }

    public static void showMustConfirmDialog(Context context, String title, String message, View.OnClickListener okListener) {
        new MessageDialog(context).builder().setTitle(title)
                .setMsg(message)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .setPositiveButton("确认", okListener).show();
    }

    public static void showMustConfirmDialog(Context context, String message, View.OnClickListener okListener) {
        showMustConfirmDialog(context, "温馨提示", message, okListener);
    }

    public static void showConfirmDialog(Context context, String message, View.OnClickListener okListener) {
        showConfirmDialog(context, "温馨提示", message, okListener);
    }

    /**
     * 显示输入次数的对话框
     *
     * @param activity
     */
    public static void showInputCountDialog(Activity activity, String str, InputCountDialog.OkListener listener) {
        InputCountDialog alertDialog = new InputCountDialog.Builder(activity)
                .setCancelable(true)
                .setTitle("")
                .setContent(str)
                .setOkListener(listener)
                .create();
        alertDialog.show();
    }


    public static void showEditDialog(Context context, String title, String content, EditTextDialog.DialogListener dialogListener) {
        EditTextDialog alertDialog = new EditTextDialog.Builder(context)
                .setCancelable(true)
                .setTitle(title)
                .setContent(content)
                .setDialogListener(dialogListener)
                .create();
        alertDialog.show();
    }


    public static ActionSheetDialog showMenuList(String[] datas, final Context context, ActionSheetDialog.OnSheetItemClickListener listener) {
        ActionSheetDialog dialog = showMenuList(Arrays.asList(datas), context, listener, null, null);
        return dialog;
    }

    public static ActionSheetDialog showMenuList(final List<String> data, final Context context, ActionSheetDialog.OnSheetItemClickListener listener) {
        ActionSheetDialog dialog = showMenuList(data, context, listener, null, null);
        return dialog;
    }

    public static ActionSheetDialog showMenuList(final List<String> data,
                                                 final Context context,
                                                 ActionSheetDialog.OnSheetItemClickListener listener,
                                                 String cancelTxt, View.OnClickListener bottomListener) {
        ActionSheetDialog dialog = new ActionSheetDialog(context)
                .builder()
                .setCancelable(true)
                .setCanceledOnTouchOutside(true);
        for (int i = 0; i < data.size(); i++) {
            dialog.addSheetItem(data.get(i), ActionSheetDialog.SheetItemColor.Blue, listener);
        }

        //新增按钮
        if (cancelTxt != null) dialog.setBottomText(cancelTxt);
        if (bottomListener != null) dialog.setBottomListener(bottomListener);
        dialog.show();
        return dialog;
    }

    public static void showCountDialog(Activity mActivity, String s, Object o) {

    }

    /**
     * 展示提交数据成功对话框
     *
     * @param activity
     * @param message
     * @param dialogListener
     */
    public static void showSubmitSuccessDialog(Activity activity, String message, SubmitSuccessDialog.DialogListener dialogListener) {
        final SubmitSuccessDialog alertDialog = new SubmitSuccessDialog.Builder(activity)
                .setCancelable(true)
                .setMessage(message)
                .setDialogListener(dialogListener)
                .create();
        alertDialog.show();
    }

    public static void showAutoDismissMessageDialog(Activity activity, String message) {
        final AutoDismissMessageDialog alertDialog = new AutoDismissMessageDialog.Builder(activity)
                .setCancelable(true)
                .setMessage(message)
                .create();
        alertDialog.show();
    }

    public static void setWidthAndHeight(Activity activity, AlertDialog alertDialog) {
        // 将对话框的大小按屏幕大小的百分比设置
        WindowManager windowManager = activity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = alertDialog.getWindow().getAttributes();
        lp.width = (int) (display.getWidth() * 0.8); //设置宽度
        alertDialog.getWindow().setAttributes(lp);
    }

    public static void showTimeDialog(Context context, int[] time, final View view, TimePickerDialog.OnTimeSetListener listener) {
        new TimePickerDialog(context, android.app.AlertDialog.THEME_HOLO_LIGHT, listener, time[0], time[1], true).show();
    }

    public static void showTimeDialog(Context context, int[] time, TimePickerDialog.OnTimeSetListener listener) {
        new TimePickerDialog(context, android.app.AlertDialog.THEME_HOLO_LIGHT, listener, time[0], time[1], true).show();
    }


}
