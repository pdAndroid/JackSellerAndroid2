package com.party.jackseller.utils;

/**
 * Created by Administrator on 2018/10/19.
 */

public class MoneyUtil {
    public static boolean isAllZero(String money) {
        for(int i=0;i<money.length();i++) {
            if(money.charAt(i) != '0') {
                return false;
            }
        }
        return true;
    }
    public static String formatMoney(String money) {
        String money2 = null;
        String money1 = money.substring(money.indexOf('.')+1, money.length());
        if(isAllZero(money1)) {
            money2=money.substring(0,money.indexOf('.'));
        }else {
            money2 = money;
        }
        return money2;
    }
}
