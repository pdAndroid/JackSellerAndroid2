package com.party.jackseller.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class DialUtils {
    /**
     * 跳转到系统拨号界面
     *
     * @param activity
     * @param phoneNum
     */
    public static void dial(Activity activity, String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNum));
        activity.startActivity(intent);
    }
}
