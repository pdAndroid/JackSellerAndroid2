package com.party.jackseller.utils;

import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.BaseActivity;
import com.party.jackseller.MApplication;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.OSSConfig;
import com.party.jackseller.controller.AliyunOSSController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 派对 on 2018/10/28.
 */

public class AliOssService {

    private BaseActivity mContext;
    private MApplication mApplication;
    private AliyunOSSController ossController;
    private List<UploadImage> images;
    private UploadListener mSuccessListener;
    private UploadListener mFailListener;

    public interface UploadListener {
        void handler(String message);
    }

    public AliOssService(BaseActivity context) {
        this.mContext = context;
        images = new ArrayList<>();
        mApplication = context.getmApplication();
        ossController = new AliyunOSSController(context);
    }

    public void uploadImageAll(UploadListener success) {
        uploadImageAll(success, (String message) -> {
            mContext.showToast("上传图片失败");
            mContext.hideAlertDialog();
        });
    }

    public void uploadImageAll(UploadListener success, UploadListener fail) {
        mSuccessListener = success;
        mFailListener = fail;
        mContext.addDisposableIoMain(ossController.getOssConfig(AliyunOSSController.OSS_BANNER), new DefaultConsumer<OSSConfig>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OSSConfig> baseBean) {
                ossController.initOssConfig(baseBean.getData(), mContext);
                doUploadPictures(0);
            }
        });
    }

    private void doUploadPictures(int position) {
        long userId = mApplication.getUser().getId();
        String localPath = images.get(position).getLocalPath();
        mContext.addDisposableIoMain(ossController.uploadPicturesSync(userId, localPath, false), (String s) -> {
            images.get(position).setOssUrl(s);
            if (images.size() - 1 <= position) {
                mSuccessListener.handler(s);
            } else {
                doUploadPictures(position + 1);
            }
        }, (Throwable throwable) -> {
            mFailListener.handler(throwable.getMessage());
        });
    }


    /**
     * 插入本地地址
     *
     * @param key
     * @param imgPath
     */
    public void putImagePath(int key, String imgPath) {
        for (int i = 0; i < images.size(); i++) {
            UploadImage uploadImage = images.get(i);
            if (uploadImage.getId() == key) {
                uploadImage.setLocalPath(imgPath);
                return;
            }
        }
        UploadImage uploadImage = new UploadImage(key, imgPath);
        images.add(uploadImage);
    }

    public UploadImage remove(int key) {
        UploadImage remove = images.remove(key);
        return remove;
    }

    public String getLocalPath(int key) {
        for (int i = 0; i < images.size(); i++) {
            UploadImage uploadImage = images.get(i);
            if (uploadImage.getId() == key) {
                return uploadImage.getLocalPath();
            }
        }
        return null;
    }

    public String getOssUrl(int key) {
        for (int i = 0; i < images.size(); i++) {
            UploadImage uploadImage = images.get(i);
            if (uploadImage.getId() == key) {
                return uploadImage.getOssUrl();
            }
        }
        return null;
    }

    class UploadImage {
        private int key;
        private String localPath;
        private String ossUrl;

        public UploadImage(int key, String localPath) {
            this.key = key;
            this.localPath = localPath;
        }

        public int getId() {
            return key;
        }

        public void setId(int id) {
            this.key = id;
        }

        public String getLocalPath() {
            return localPath;
        }

        public void setLocalPath(String localPath) {
            this.localPath = localPath;
        }

        public String getOssUrl() {
            return ossUrl;
        }

        public void setOssUrl(String ossUrl) {
            this.ossUrl = ossUrl;
        }
    }
}

