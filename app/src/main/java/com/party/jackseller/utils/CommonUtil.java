package com.party.jackseller.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2018/9/15.
 */

public class CommonUtil {


    private static SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd ");
    private static final int MIN_DELAY_TIME = 500;  // 两次点击间隔不能少于1000ms
    private static long lastClickTime;

    public static String formatTime(Long timeStamp) {
        return df2.format(timeStamp);
    }

    public static String formatTimeText(Long timeStamp) {
        long now = System.currentTimeMillis();
        long currentTime = (now - timeStamp) / 1000;
        if (currentTime < 60) {
            return "刚刚";
        }

        long minute = currentTime / 60;
        if (minute < 59) {
            return  minute + "分钟前";
        }

        return df2.format(timeStamp);
    }

    /**
     * 将数据保留两位小数
     */
    public static double getTwoDecimal(double num) {
        DecimalFormat dFormat = new DecimalFormat("#.00");
        String yearString = dFormat.format(num);
        Double temp = Double.valueOf(yearString);
        return temp;
    }

    public static String getAppointmentId(String appointmentStr) {
        if (appointmentStr.equals("免预约")) {
            return "1";
        }
        if (appointmentStr.equals("电话预约")) {
            return "2";
        }
        return null;
    }

    public static boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= MIN_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = currentClickTime;
        return flag;
    }
}
