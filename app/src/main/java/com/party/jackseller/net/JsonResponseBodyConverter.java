package com.party.jackseller.net;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.party.jackseller.api.BaseService;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.utils.AesEncryptUtil;

import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class JsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson mGson;//gson对象
    private final TypeAdapter<T> adapter;

    /**
     * 构造器
     */
    public JsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.mGson = gson;
        this.adapter = adapter;
    }

    /**
     * 转换
     *
     * @param responseBody
     * @return
     * @throws IOException
     */
    @Override
    public T convert(ResponseBody responseBody) throws IOException {

        String response = responseBody.string();

        String strResult = response.substring(1, response.length() - 1);
        String result = null;//解密
        /*try {
            result = AesEncryptUtil.aesDecrypt(strResult, AesEncryptUtil.KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        //Log.i("xiaozhang", "解密的服务器数据：" + result);
        Log.i("xiaozhang", "服务器数据：" + strResult);
        //BaseResult baseResult = mGson.fromJson(result, BaseResult.class);
        BaseResult baseResult = mGson.fromJson(strResult, BaseResult.class);
        return (T) baseResult;
    }

}
