package com.party.jackseller.uicommon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ClaimShopBean;
import com.party.jackseller.bean.ImageBean;
import com.party.jackseller.utils.AliOssService;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnLongClick;

/**
 * 门店认领
 */
public class ClaimShopActivity extends BaseActivityTitle {

    @BindView(R.id.shop_name_et)
    EditText shopNameEt;
    @BindView(R.id.address_detail_et)
    EditText detailAddressEt;
    @BindView(R.id.shop_phone_et)
    EditText shop_phone_et;
    @BindView(R.id.boss_phone_et)
    EditText boss_phone_et;
    @BindView(R.id.idcard_front)
    ImageView idCardFrontIv;
    @BindView(R.id.idcard_behind)
    ImageView idCardBehindIv;
    @BindView(R.id.shop_licence)
    ImageView businessLicenceIv;
    @BindView(R.id.shop_other)
    ImageView otherIv;
    @BindView(R.id.id_card_et)
    EditText id_card_et;
    @BindView(R.id.scroll_layout)
    ScrollView scroll_layout;


    private ImageView mImageView;
    private ShopService shopService;
    private AliOssService mOssService;
    private ClaimShopBean claimShopBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_shop);
        initData();
    }

    protected void initData() {
        claimShopBean = (ClaimShopBean) getIntent().getSerializableExtra("shop");
        handleTitle();
        setMiddleText("商铺认领");
        mOssService = new AliOssService(this);
        shopService = new ShopService(this);
        shopNameEt.setText(claimShopBean.getShopName());
        boss_phone_et.setText(mApplication.getUser().getPhone());
        detailAddressEt.setText(claimShopBean.getShopAddress());
    }

    /**
     * 长按删除掉之前选中的图片
     *
     * @param view
     * @return
     */
    @OnLongClick({R.id.id_card_behind_iv, R.id.id_card_front_iv, R.id.business_licence_iv, R.id.other_iv})
    public boolean deleteChoosePictures(final View view) {
        DialogController.showConfirmDialog(mActivity, "确定删除", (View v) -> {
            ImageView vv = (ImageView) view;
            vv.setImageBitmap(null);
        });
        mOssService.remove(view.getId());
        return true;
    }


    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        ImageBean imageBean = list.get(0);
        mOssService.putImagePath(mImageView.getId(), imageBean.getImgPath());
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, imageBean.getImgPath(), mImageView);
    }

    /**
     * 上传所有数据
     */
    public void submitData() {
        Map<String, String> tokenMap = getSignData();
        tokenMap.put("shopId", claimShopBean.getId() + "");
        tokenMap.put("shopPhone", shop_phone_et.getText().toString());
        tokenMap.put("phone", boss_phone_et.getText().toString());
        tokenMap.put("shopName", shopNameEt.getText().toString());
        tokenMap.put("idImg", mOssService.getOssUrl(R.id.idcard_front) + "," + mOssService.getOssUrl(R.id.idcard_behind));
        tokenMap.put("licenseImg", mOssService.getOssUrl(R.id.shop_licence));
        if (mOssService.getOssUrl(R.id.shop_other) != null)
            tokenMap.put("foodSafetyCertificateImg", mOssService.getOssUrl(R.id.shop_other));
        tokenMap.put("idCard", id_card_et.getText().toString());
        tokenMap.put("type", "2");//2位店铺认领类型
        addDisposableIoMain(shopService.claimShop(tokenMap), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                hideAlertDialog();
                DialogController.showMustConfirmDialog(mActivity,
                        "申诉店铺认领成功",
                        "申诉结果将以短信的形式通知您，请您耐心等待！",
                        (View v) -> {
                            finish();
                        });
            }
        });
    }

    public void scrollTo(View view) {
        scrollTo(view, shopNameEt, scroll_layout);
    }

    /**
     * 上传图片操作
     */
    private void doUploadPictures() {
        mOssService.uploadImageAll((String message) -> {
            submitData();
        });
    }

    public void handleClickSth(View view) {
        mImageView = (ImageView) view;
        if (choosePictureController != null) {
            choosePictureController.clearBeforeChoosePictures();
            choosePictureController.handleToChooseCropPictures();
        }
    }

    public void commitData(View view) {
        try {
            CheckUtils.checkData(shop_phone_et, "请输入店铺号码");
            CheckUtils.checkData(boss_phone_et, "请输入联系电话");
            CheckUtils.checkIDCard(id_card_et, "请输入身份证号码");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.idcard_front), findViewById(R.id.plus_0), "请上传身份证正面");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.idcard_behind), findViewById(R.id.plus_1), "请上传身份证背面");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.shop_licence), findViewById(R.id.plus_10), "请上传营业执照");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            scrollTo(e.getView());
            return;
        }
        showAlertDialog("正在上传数据...");
        doUploadPictures();
    }
}
