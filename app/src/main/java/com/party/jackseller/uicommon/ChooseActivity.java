package com.party.jackseller.uicommon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.R;

import butterknife.OnClick;

public class ChooseActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
    }

    @OnClick({R.id.register_shop_btn, R.id.register_extend_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.register_shop_btn:
                Intent intent = new Intent(mActivity, ClaimShopListActivity.class);
                startActivity(intent);
                break;
            case R.id.register_extend_btn:
                showToast("暂未开放");
                break;
        }
    }

}
