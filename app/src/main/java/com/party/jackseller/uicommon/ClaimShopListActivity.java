package com.party.jackseller.uicommon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.ShopService;
import com.party.jackseller.BaseActivityTitle;
import com.party.jackseller.Constant;
import com.party.jackseller.base.ShopState;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.ClaimShopBean;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.uihome.AddShopActivity;
import com.party.jackseller.uimy.ShopInfoActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * 认领门店列表
 */
public class ClaimShopListActivity extends BaseActivityTitle {
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private int pageNumber = 1;
    private int pageSize = 30;
    private List<ClaimShopBean> dataList = new ArrayList<>();
    private ShopService shopService;
    private CommonAdapter<ClaimShopBean> commonAdapter;
    private Map<String, Shop> myShopMap = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_shop_list);
        initData();
    }

    protected void initData() {

        handleTitle();
        setMiddleText("门店认领");
        shopService = new ShopService(this);
        initView();
        helper = new LoadViewHelper(refresh_layout);
        helper.setListener(() -> {
            loadData();
        });
        helper.showLoading();
        loadMyShop();
    }

    public void loadMyShop() {
        addDisposableIoMain(shopService.getMyShopList(), new DefaultConsumer<List<Shop>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<Shop>> baseBean) {
                if (baseBean.getData().isEmpty()) {
                    helper.showEmpty();
                } else {
                    myShopMap.clear();
                    List<Shop> myShopList = baseBean.getData();
                    for (int i = 0; i < myShopList.size(); i++) {
                        myShopMap.put(myShopList.get(i).getId() + "", myShopList.get(i));
                    }
                }
                loadData();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
                refresh_layout.finishRefresh();
            }
        });
    }


    public void initView() {
        commonAdapter = new CommonAdapter<ClaimShopBean>(mActivity, R.layout.item_claim_shop_list, dataList) {
            @Override
            protected void convert(ViewHolder holder, final ClaimShopBean claimShopBean, int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, claimShopBean.getHomeImg(), (ImageView) holder.getView(R.id.shop_image_civ));
                holder.setText(R.id.shop_name_tv, claimShopBean.getShopName());
                holder.setText(R.id.shop_address_tv, claimShopBean.getShopAddress());
                Shop shop = myShopMap.get(claimShopBean.getId() + "");
                if (shop == null) {
                    holder.setText(R.id.claim_shop_btn, claimShopBean.getShopState() == ShopState.Open.getState() ? "申诉" : "认领");
                    holderItem(holder, claimShopBean);
                } else {
                    holder.setText(R.id.claim_shop_btn, shop.getShop_state_name());
                    holder.setOnClickListener(R.id.claim_shop_btn, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showToast("已经为你提交申请了");
                        }
                    });
                }
            }
        };
        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                loadData();
            }
        });
    }

    public void holderItem(ViewHolder holder, final ClaimShopBean claimShopBean) {
        holder.setOnClickListener(R.id.claim_shop_btn, (View v) -> {
            if (claimShopBean.getShopState() == Constant.SHOP_STATUS_CHECKING) {  //申诉
                DialogController.showConfirmDialog(mActivity, "门店认领承诺书", getResources().getString(R.string.claim_shop),
                        (View vv) -> {
                            Intent intent = new Intent(mActivity, ClaimShopActivity.class);
                            intent.putExtra("shop", claimShopBean);
                            startActivity(intent);
                        });
            } else//认领
                DialogController.showConfirmDialog(mActivity, "门店认领承诺书", getResources().getString(R.string.complain),
                        (View vv) -> {
                            Intent intent = new Intent(mActivity, ComplainActivity.class);
                            intent.putExtra("shop", claimShopBean);
                            startActivity(intent);
                        });
        });
    }

    /**
     * 下拉刷新数据
     */
    private void loadData() {
        pageNumber = 1;
        addDisposableIoMain(shopService.getShopClaimList(Constant.PAGE_SIZE, pageNumber), new DefaultConsumer<List<ClaimShopBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ClaimShopBean>> baseBean) {
                dataList.clear();
                if (baseBean.getData().isEmpty()) {
                    helper.showEmpty();
                } else {
                    if (baseBean.getData().size() < Constant.PAGE_SIZE) {
                        refresh_layout.setEnableLoadMore(false);
                    }
                    dataList.addAll(baseBean.getData());
                    commonAdapter.notifyDataSetChanged();
                    helper.showContent();
                }
                refresh_layout.finishRefresh();
            }
        });

        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Intent intent = new Intent(mActivity, ShopInfoActivity.class);
                intent.putExtra("shopId", dataList.get(position).getId());
                startActivity(intent);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    /**
     * 上拉加载更多数据
     */
    private void loadMoreData() {
        pageNumber++;
        addDisposableIoMain(shopService.getShopClaimList(pageSize, pageNumber), new DefaultConsumer<List<ClaimShopBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ClaimShopBean>> baseBean) {
                boolean noMoreData = false;
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                    if (baseBean.getData().size() <= pageSize) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    refresh_layout.finishLoadMoreWithNoMoreData();
                } else {
                    refresh_layout.finishLoadMore();
                }
            }
        });
    }

    @OnClick(R.id.add_shop_btn)
    public void addShop(View view) {
        Intent intent = new Intent(this, AddShopActivity.class);
        startActivityForResult(intent, Constant.REQUEST_CODE_ADD_SHOP);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Constant.REQUEST_CODE_ADD_SHOP == requestCode) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("isAdd", false) == true) {
                    helper.showLoading();
                    loadData();
                }
            }
        }
    }
}
