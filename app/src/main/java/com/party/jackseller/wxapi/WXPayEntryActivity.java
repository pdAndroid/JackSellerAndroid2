package com.party.jackseller.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.party.jackseller.BuildConfig;
import com.party.jackseller.event.AuthResultEvent;
import com.party.jackseller.event.PayResultEvent;
import com.party.jackseller.utils.ToastUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI api;
    /**
     * 配置到build.gradle文件里面
     */
    private String wxAppId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, null);
        wxAppId = BuildConfig.WX_APP_ID;
        api.registerApp(wxAppId);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {
        switch (resp.getType()) {
            case ConstantsAPI.COMMAND_PAY_BY_WX:
                if (resp.errCode == 0) {
                    Toast.makeText(this, "您已付款成功了", Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new PayResultEvent(0, 1));
                } else if (resp.errCode == -2) {
                    Toast.makeText(this, "您已取消付款!", Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new PayResultEvent(0, 0));
                } else {
                    Toast.makeText(this, "参数错误", Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new PayResultEvent(0, -1));
                }
                overridePendingTransition(0, 0);
                break;
        }
        finish();
    }


}