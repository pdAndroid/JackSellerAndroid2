package com.party.jackseller.litvedio;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.party.jackseller.MApplication;
import com.party.jackseller.litvedio.common.utils.TCHttpEngine;
import com.party.jackseller.litvedio.config.TCConfigManager;
import com.party.jackseller.litvedio.config.TCConstants;
import com.party.jackseller.litvedio.config.TCUserMgr;
import com.tencent.ugc.TXUGCBase;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class TCInitController {
    private static Application sApplication;
    private static String password = "123456789";
    //    private static String ugcKey = "09bb91939d9ef9669f7ff16a850c92e5";
//    private static String ugcLicenceUrl = "http://download-1252463788.cossh.myqcloud.com/xiaoshipin/licence_xsp/TXUgcSDK.licence";
    private static String ugcKey = "24d4aa6d8bb515d05ba4c27838e604d0";
    private static String ugcLicenceUrl = "http://license.vod2.myqcloud.com/license/v1/104f9b312dd73748bb340b67f33472b1/TXUgcSDK.licence";

    public static Application getApplication() {
        return sApplication;
    }

    public static void initTCSdk(Application application) {
        sApplication = application;
        TCConfigManager.init(application);
        initSDK(application);
        // 短视频licence设置
        TXUGCBase.getInstance().setLicence(application, ugcLicenceUrl, ugcKey);
        // 上报启动次数
        TCUserMgr.getInstance().uploadLogs(TCConstants.ELK_ACTION_START_UP, TCUserMgr.getInstance().getUserId(), 0, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
        if (application instanceof MApplication) {
            String phone = ((MApplication) application).getUser().getPhone();
            if (!TextUtils.isEmpty(phone)) {
                checkUserHasRegister(phone);
            }
        }
    }

    public static void checkUserHasRegister(final String username) {
        final TCUserMgr tcLoginMgr = TCUserMgr.getInstance();
        tcLoginMgr.login(username, password, new TCUserMgr.Callback() {
            @Override
            public void onSuccess(JSONObject data) {
                Log.d("TCUserMgr", "=========login success");
            }

            @Override
            public void onFailure(int code, final String msg) {
                Log.d("TCUserMgr", "=========login failure");
                handleUserRegister(username);
            }
        });
    }

    private static void handleUserRegister(final String username) {
        final TCUserMgr tcLoginMgr = TCUserMgr.getInstance();
        tcLoginMgr.register(username, password, new TCUserMgr.Callback() {
            @Override
            public void onSuccess(JSONObject data) {
                Log.d("TCUserMgr", "=========注册成功");
                tcLoginMgr.uploadLogs(TCConstants.ELK_ACTION_REGISTER, username, TCUserMgr.SUCCESS_CODE, "注册成功", new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d("TCUserMgr", "uploadLogs onFailure");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.d("TCUserMgr", "uploadLogs onResponse");
                    }
                });
                tcLoginMgr.login(username, password, new TCUserMgr.Callback() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        Log.d("TCUserMgr", "=========login success");
                    }

                    @Override
                    public void onFailure(int code, final String msg) {
                        Log.d("TCUserMgr", "=========login failure");
                    }
                });
            }

            @Override
            public void onFailure(int code, final String msg) {
                String errorMsg = msg;
                tcLoginMgr.uploadLogs(TCConstants.ELK_ACTION_REGISTER, username, code, errorMsg, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d("TCUserMgr", "uploadLogs onFailure");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.d("TCUserMgr", "uploadLogs onResponse");
                    }
                });
            }
        });
    }

    /**
     * 初始化SDK，包括Bugly，LiteAVSDK等
     */
    private static void initSDK(Application application) {
        TCUserMgr.getInstance().initContext(application);
        TCHttpEngine.getInstance().initContext(application);
        application.registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks(application));
    }

    private static class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
        private int foregroundActivities;
        private boolean isChangingConfiguration;
        private long time;

        public MyActivityLifecycleCallbacks(Application application) {

        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
            foregroundActivities++;
            if (foregroundActivities == 1 && !isChangingConfiguration) {
                // 应用进入前台
                time = System.currentTimeMillis();
            }
            isChangingConfiguration = false;
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {
            foregroundActivities--;
            if (foregroundActivities == 0) {
                // 应用切入后台
                long bgTime = System.currentTimeMillis();
                long diff = (bgTime - time) / 1000;
                uploadStayTime(diff);
            }
            isChangingConfiguration = activity.isChangingConfigurations();
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    private static void uploadStayTime(long diff) {
        TCUserMgr.getInstance().uploadLogs(TCConstants.ELK_ACTION_STAY_TIME, TCUserMgr.getInstance().getUserId(), diff, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }
}
