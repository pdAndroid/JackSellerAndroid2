package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/9/14.
 */

public class ShopCategory {

    /**
     * id : 4
     * imgUrl : https://pdkj.oss-cn-beijing.aliyuncs.com/shopTypeIcon/Hotel.png
     * name : 酒店/住宿
     */

    private long id;
    private String imgUrl;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name.toString();
    }
}
