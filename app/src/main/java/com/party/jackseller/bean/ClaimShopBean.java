package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/10/8.
 */

public class ClaimShopBean implements Serializable{

    /**
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/home_img/250_1538979071625.jpg
     * id : 1534994219539
     * shopAddress : 温江区四川省成都市温江区花都大道西段341号靠近融信智慧广场
     * shopName : 一品天下
     * shopState : 0
     */

    private String homeImg;
    private long id;
    private String shopAddress;
    private String shopName;
    private int shopState;

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getShopState() {
        return shopState;
    }

    public void setShopState(int shopState) {
        this.shopState = shopState;
    }
}
