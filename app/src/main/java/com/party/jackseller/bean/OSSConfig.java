package com.party.jackseller.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 派对 on 2018/8/16.
 */

public class OSSConfig {
    @Expose
    @SerializedName("accessKeyId")
    private String accessKeyId;
    @Expose
    @SerializedName("endpoint")
    private String endpoint;
    @Expose
    @SerializedName("accessKeySecret")
    private String accessKeySecret;
    @Expose
    @SerializedName("address")
    private String address;
    @Expose
    @SerializedName("bucketName")
    private String bucketName;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
