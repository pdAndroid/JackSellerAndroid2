package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/9/15.
 */

public class ShopInfo {

    /**
     * shop_name : 寇芊芊的包子店
     * shop_phone : 124578824965
     * shop_address : 四川省成都市温江区海川路
     * buss_open : 6918000
     * symbol_build : 融信广场
     * buss_close : 6918000
     * working_day : 1234567
     * home_img : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * shop_state : 1
     * shop_id : 1530523930811
     * category_name : KTV
     * shop_state_name : 已上线
     */

    private String shop_name;
    private String shop_phone;
    private String shop_address;
    private long buss_open;
    private String symbol_build;
    private long buss_close;
    private String working_day;
    private String home_img;
    private int shop_state;
    private long shop_id;
    private String category_name;
    private String shop_state_name;

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public long getBuss_open() {
        return buss_open;
    }

    public void setBuss_open(long buss_open) {
        this.buss_open = buss_open;
    }

    public String getSymbol_build() {
        return symbol_build;
    }

    public void setSymbol_build(String symbol_build) {
        this.symbol_build = symbol_build;
    }

    public long getBuss_close() {
        return buss_close;
    }

    public void setBuss_close(long buss_close) {
        this.buss_close = buss_close;
    }

    public String getWorking_day() {
        return working_day;
    }

    public void setWorking_day(String working_day) {
        this.working_day = working_day;
    }

    public String getHome_img() {
        return home_img;
    }

    public void setHome_img(String home_img) {
        this.home_img = home_img;
    }

    public int getShop_state() {
        return shop_state;
    }

    public void setShop_state(int shop_state) {
        this.shop_state = shop_state;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getShop_state_name() {
        return shop_state_name;
    }

    public void setShop_state_name(String shop_state_name) {
        this.shop_state_name = shop_state_name;
    }
}
