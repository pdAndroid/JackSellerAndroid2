package com.party.jackseller.bean;

/**
 * Created by 派对 on 2018/8/8.
 */

public class ReplaceMoneyCoupon {

    /**
     * id : 1
     * original_price : 100
     * buy_price : 80
     * shop_name : 达吉的包子店
     * home_img : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     */

    private long id;
    private String original_price;
    private String buy_price;
    private String shop_name;
    private String home_img;
    private String goodsRange;

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(String buy_price) {
        this.buy_price = buy_price;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getHome_img() {
        return home_img;
    }

    public void setHome_img(String home_img) {
        this.home_img = home_img;
    }
}
