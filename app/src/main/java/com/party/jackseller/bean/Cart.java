package com.party.jackseller.bean;

import android.content.res.Resources;
import android.widget.TextView;

import com.party.jackseller.base.MyLessThanZeroException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 派对 on 2018/8/23.
 */

public class Cart {

    ArrayList<Goods> mGoodsList = new ArrayList<>();

    private BigDecimal totalPrice = new BigDecimal(0);

    private List<TextView> mShowViews = new ArrayList<>();

    public void addGoodsAll(ArrayList<Goods> tempGoods) {
        mGoodsList.addAll(tempGoods);
    }

    public int size() {
        return mGoodsList.size();
    }

    public Goods get(int index) {
        return mGoodsList.get(index);
    }

    public int add(Goods goods) {
        int exist = isExist(goods);
        if (exist >= 0) {
            return mGoodsList.get(exist).increase(1);
        }
        mGoodsList.add(goods);
        return 1;
    }

    public int reduce(Goods goods) throws MyLessThanZeroException {
        int exist = isExist(goods);
        if (exist >= 0) {
            int count = mGoodsList.get(exist).getCount();
            if (count <= 1) throw new MyLessThanZeroException(exist, "小于0");
            return mGoodsList.get(exist).reduce(1);
        }
        throw new Resources.NotFoundException("没有该商品");
    }

    public Goods goodsAddCount(int position, int i) {
        Goods good = mGoodsList.get(position);
        good.setCount(good.getCount() + 1);
        return good;
    }

    public Goods goodsReduceCount(int position, int i) {
        Goods good = mGoodsList.get(position);
        good.setCount(good.getCount() - 1);
        return good;
    }

    public Goods setGoodsCount(int position, int i) {
        Goods good = mGoodsList.get(position);
        good.setCount(i);
        return good;
    }


    public List<Goods> getGoodsList() {
        return mGoodsList;
    }

    public int isExist(Goods goods) {
        for (int i = 0; i < mGoodsList.size(); i++) {
            if (mGoodsList.get(i).getId() == goods.getId()) {
                return i;
            }
        }
        return -1;
    }

    public Goods remove(Goods goods) {
        Iterator<Goods> iterator = mGoodsList.iterator();
        while (iterator.hasNext()) {
            Goods next = iterator.next();
            if (next.getId() == goods.getId()) {
                iterator.remove();
                return next;
            }
        }
        return null;
    }

    public void addShowTotalPrice(TextView view) {
        mShowViews.add(view);
    }

    public void calTotalPrice() {
        totalPrice = new BigDecimal(0);
        for (int i = 0; i < mGoodsList.size(); i++) {
            Goods goods = mGoodsList.get(i);
            BigDecimal price = new BigDecimal(goods.getPrice());
            BigDecimal count = new BigDecimal(goods.getCount());
            totalPrice = totalPrice.add(price.multiply(count));
        }
        for (TextView view : mShowViews) {
            view.setText("总价：¥" + totalPrice.setScale(2).toString());
        }
    }

    public BigDecimal getTotalPrice() {
        calTotalPrice();
        return totalPrice;
    }

    public String getTotalPriceStr() {
        calTotalPrice();
        return totalPrice.setScale(2).toString();
    }

    public void clear() {
        mGoodsList.clear();
        totalPrice = new BigDecimal(0);
        mShowViews.clear();
    }
}
