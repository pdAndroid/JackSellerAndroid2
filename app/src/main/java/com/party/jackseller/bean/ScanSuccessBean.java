package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/10/9.
 */

public class ScanSuccessBean {

    /**
     * id : 469
     * itemName : 三人餐
     * price : 300.0
     * shopName : 迪奥口红专卖店
     */

    private long id;
    private String itemName;
    private double price;
    private String shopName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
