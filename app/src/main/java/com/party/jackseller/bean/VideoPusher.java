package com.party.jackseller.bean;

import android.content.Intent;

import com.party.jackseller.event.AMapBeanEvent;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 派对 on 2018/12/27.
 */

public class VideoPusher {

    private String videoId;
    private String coverURL;
    private String videoURL;
    private int videoTime;
    private long videoFileSize;
    private Loc location;
    private AMapBeanEvent mapBean;
    private String originalPerMoney;
    private String maxPerMoney;
    private String minPerMoney;
    private String sexPrice;
    private int maxAge;
    private int minAge;
    private boolean ageChecked, sexChecked;
    private int totalCount;
    private String address;
    private int visitCount = 10;
    private int sex = -1;
    private String totalMoney;
    private String perPrice;
    private String sexRatioPrice;


    public String getCoverURL() {
        return coverURL;
    }

    public String getMaxPrice() {
        return maxPerMoney;
    }

    public String getAddress() {
        return address;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public void setMapBean(AMapBeanEvent aMapBean) {
        this.mapBean = aMapBean;
        address = aMapBean.getAddress();
    }

    public VideoPusher(Intent intent) {
        videoId = intent.getStringExtra("videoId");
        videoURL = intent.getStringExtra("videoURL");
        coverURL = intent.getStringExtra("coverURL");
        long videoDuration = intent.getLongExtra("videoDuration", 0);
        // 时长为秒
        videoTime = (int) (videoDuration / 1000);
        videoFileSize = intent.getLongExtra("videoFileSize", 0);
    }

    public int getTotalCount() {
        return totalCount;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setLoc(Loc loc) {
        location = loc;
    }

    public void setOriginalPerMoney(String originalPerMoney) {
        this.originalPerMoney = originalPerMoney;
    }

    public void setMaxPerMoney(String maxPerMoney) {
        this.maxPerMoney = maxPerMoney;
    }


    public void setMinPerMoney(String minPerMoney) {
        this.minPerMoney = minPerMoney;
    }

    public String getPerPrice() {
        return perPrice;
    }

    public void setSexPrice(String sexPrice) {
        this.sexPrice = sexPrice;
    }

    public void setSelectAge(boolean isCheck) {
        ageChecked = isCheck;
    }

    public boolean isAgeChecked() {
        return ageChecked;
    }

    public boolean isSexChecked() {
        return sexChecked;
    }

    public void setSexChecked(boolean sexChecked) {
        this.sexChecked = sexChecked;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getSex() {
        return sex;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public void setPerPrice(String perPrice) {
        this.perPrice = perPrice;
    }

    public String getSexRatioPrice() {
        return sexRatioPrice;
    }

    public void setSexRatioPrice(String sexRatioPrice) {
        this.sexRatioPrice = sexRatioPrice;
    }

    public static class Loc {
        private String name;
        private int type;
        private int rangeLimit;
        private String cityId;

        public Loc(String name, int type, int rangeLimit, String cityId) {
            this.name = name;
            this.type = type;
            this.rangeLimit = rangeLimit;
            this.cityId = cityId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map getData() {
            Map<String, String> map = new HashMap<>();
            map.put("type", type + "");
            map.put("rangeLimit", rangeLimit + "");
            map.put("cityID", cityId);
            return map;
        }
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public Map<String, String> getCommitData() throws Exception {
        if (location == null) throw new Exception("请选择距离范围");
        if (totalCount == 0) throw new Exception("请输入发布次数");

        if (ageChecked) {
            // 如果年龄低于12或高于80报错
            if (maxAge < 12 || minAge > 80) new Exception("年龄不能低于12,不能高于80");
            // 年龄差别小于5
            if ((minAge - maxAge) < 5) new Exception("年龄区间最少五年");
        }
        if (!ageChecked) {
            maxAge = -1;
            minAge = -1;
        }
        if (!sexChecked) sex = -1;

        int totalMoneyInt = new BigDecimal(totalMoney).multiply(new BigDecimal(100)).intValue();
        Map<String, String> map = new HashMap<>();
        map.put("videoId", videoId);
        map.put("coverURL", coverURL);
        map.put("videoURL", videoURL);
        map.put("videoTime", videoTime + "");
        map.put("videoSize", videoFileSize + "");
        map.put("totalCount", totalCount + "");
        map.put("reCount", totalCount + "");
        map.put("totalMoney", totalMoneyInt + "");
        map.putAll(location.getData());
        map.put("latitude", mapBean.getLatitude() + "");
        map.put("longitude", mapBean.getLongitude() + "");
        map.put("remarks", "推广视频");
        map.put("couponId", "-1");
        map.put("couponNum", "-1");

        map.put("sexLimit", sex + "");
        map.put("seeTime", visitCount + "");
        map.put("ageMaxLimit", maxAge + "");
        map.put("ageMinLimit", minAge + "");

        return map;
    }
}
