package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by 派对 on 2018/11/23.
 */

public class ApplicationInfo implements Serializable {

    private String wxAppId;
    private String aliAppId;
    private String registerKey;

    public String getAliAppId() {
        return aliAppId;
    }

    public void setAliAppId(String aliAppId) {
        this.aliAppId = aliAppId;
    }

    public String getRegisterKey() {
        return registerKey;
    }

    public void setRegisterKey(String registerKey) {
        this.registerKey = registerKey;
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }
}
