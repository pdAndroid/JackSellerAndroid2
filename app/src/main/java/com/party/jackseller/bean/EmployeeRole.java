package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/9/14.
 */

public class EmployeeRole {

    /**
     * id : 1
     * name : 店长
     * create : 1535263471000
     * modified : 1535263471000
     */

    private long id;
    private String name;
    private long create;
    private long modified;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    @Override
    public String toString() {
        return name;
    }
}
