package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by 派对 on 2018/8/10.
 */

public class GroupBuyCoupon implements Serializable {


    private String title;
    private int original_price;
    private String buy_price;
    private String appointment;
    private long date_start;
    private long date_end;
    private int time_start;
    private int time_end;
    private String group_buy_img;
    private long created;
    private long shop_id;
    private int state;
    private int buy_person_limit;
    private int stock_count;
    private int once_count;
    private String unavailable_date;
    private String diners_number;
    private long id;
    private String shop_name;
    private int type_of_id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(int original_price) {
        this.original_price = original_price;
    }

    public String getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(String buy_price) {
        this.buy_price = buy_price;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public long getDate_start() {
        return date_start;
    }

    public void setDate_start(long date_start) {
        this.date_start = date_start;
    }

    public long getDate_end() {
        return date_end;
    }

    public void setDate_end(long date_end) {
        this.date_end = date_end;
    }

    public int getTime_start() {
        return time_start;
    }

    public void setTime_start(int time_start) {
        this.time_start = time_start;
    }

    public int getTime_end() {
        return time_end;
    }

    public void setTime_end(int time_end) {
        this.time_end = time_end;
    }

    public String getGroup_buy_img() {
        return group_buy_img;
    }

    public void setGroup_buy_img(String group_buy_img) {
        this.group_buy_img = group_buy_img;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getBuy_person_limit() {
        return buy_person_limit;
    }

    public void setBuy_person_limit(int buy_person_limit) {
        this.buy_person_limit = buy_person_limit;
    }

    public int getStock_count() {
        return stock_count;
    }

    public void setStock_count(int stock_count) {
        this.stock_count = stock_count;
    }

    public int getOnce_count() {
        return once_count;
    }

    public void setOnce_count(int once_count) {
        this.once_count = once_count;
    }

    public String getUnavailable_date() {
        return unavailable_date;
    }

    public void setUnavailable_date(String unavailable_date) {
        this.unavailable_date = unavailable_date;
    }

    public String getDiners_number() {
        return diners_number;
    }

    public void setDiners_number(String diners_number) {
        this.diners_number = diners_number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public int getType_of_id() {
        return type_of_id;
    }

    public void setType_of_id(int type_of_id) {
        this.type_of_id = type_of_id;
    }
}
