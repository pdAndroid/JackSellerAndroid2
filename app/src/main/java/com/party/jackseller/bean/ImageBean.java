package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by fank on 2018/7/26.
 * 图片bean
 */

public class ImageBean implements Serializable{
    private String imgPath = "";//图片路径
    private boolean isAdd;//是否是添加图片
    private String imgName;

    public ImageBean() {
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public ImageBean(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }
}
