package com.party.jackseller.bean;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Administrator on 2018/10/9.
 */

public class BusinessAnalysis {

    /**
     * count : 41
     * total : 9434.4
     * avg : 230.107317
     * new_comment : 9
     */

    private int count;
    private double total;
    private double avg;
    private int new_comment;

    public void addRecord(TradeRecord record) {
        total = new BigDecimal(total).add(new BigDecimal(record.getTotalPrice())).setScale(2, RoundingMode.HALF_DOWN).doubleValue();
        count++;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public int getNew_comment() {
        return new_comment;
    }

    public void setNew_comment(int new_comment) {
        this.new_comment = new_comment;
    }


}
