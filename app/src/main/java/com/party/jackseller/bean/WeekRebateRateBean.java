package com.party.jackseller.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by 南宫灬绝痕 on 2018/12/17.
 */

public class WeekRebateRateBean {

    /**
     * startTimeAxis : 1
     * discount : 1
     * endTimeAxis : 1
     */
    private String shopId;
    private int startTimeAxis;
    private String discount;
    private int endTimeAxis;

    @JSONField(serialize = false)
    private int week;

    @JSONField(serialize = false)
    private int startTime;

    @JSONField(serialize = false)
    private int endTime;


    public void setWeek(int week) {
        this.week = week;
    }

    public int getWeek() {
        return week;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
        this.startTimeAxis = week * 24 * 60 + startTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
        this.endTimeAxis = week * 24 * 60 + endTime;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public int getStartTimeAxis() {
        return startTimeAxis;
    }

    public void setStartTimeAxis(int startTimeAxis) {
        this.startTimeAxis = startTimeAxis;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public int getEndTimeAxis() {
        return endTimeAxis;
    }

    public void setEndTimeAxis(int endTimeAxis) {
        this.endTimeAxis = endTimeAxis;
    }


}
