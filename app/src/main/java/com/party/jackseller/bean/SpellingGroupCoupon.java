package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/9/26.
 */

public class SpellingGroupCoupon implements Serializable{

    /**
     * buyPrice : 599
     * dinersNumberName : 双人餐
     * groupBuyImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/group_buy/1534994219535_1538735567761.png
     * id : 4
     * originalPrice : 9999
     * shopId : 1534994219535
     * shopName : 迪奥口红专卖店
     * state : 1
     * title : 口红超值
     */

    private double buyPrice;
    private String dinersNumberName;
    private String groupBuyImg;
    private long id;
    private double originalPrice;
    private long shopId;
    private String shopName;
    private int state;
    private String title;

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getDinersNumberName() {
        return dinersNumberName;
    }

    public void setDinersNumberName(String dinersNumberName) {
        this.dinersNumberName = dinersNumberName;
    }

    public String getGroupBuyImg() {
        return groupBuyImg;
    }

    public void setGroupBuyImg(String groupBuyImg) {
        this.groupBuyImg = groupBuyImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
