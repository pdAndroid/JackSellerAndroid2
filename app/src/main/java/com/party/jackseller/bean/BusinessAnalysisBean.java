package com.party.jackseller.bean;

/**
 * Created by 南宫灬绝痕 on 2019/1/5.
 */

public class BusinessAnalysisBean {

    /**
     * count : 41
     * total : 9434.4
     * avg : 230.107317
     * new_comment : 9
     */

    private int count;
    private double total;
    private double avg;
    private int new_comment;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public int getNew_comment() {
        return new_comment;
    }

    public void setNew_comment(int new_comment) {
        this.new_comment = new_comment;
    }
}
