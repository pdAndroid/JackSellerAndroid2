package com.party.jackseller.bean;

import java.io.Serializable;

/**
 * Created by 派对 on 2018/8/10.
 */

public class Employee implements Serializable{

    /**
     * icon : https://wx.qlogo.cn/mmopen/vi_32/pnrj1uqNRpcvz08VI9HzmG9Vw3TiabJsp63AujArh3NACMI6QmPtolrHkTicpexgh48IBjssLMWS8gfLg0MM6jtA/132
     * id : 1534729129324
     * employee_role_id : 2
     * role_name : 收银
     * alias_name : 涂德刚
     * phone : 19983160709
     */

    private String icon;
    private long id;
    private int employee_role_id;
    private String role_name;
    private String alias_name;
    private String phone;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getEmployee_role_id() {
        return employee_role_id;
    }

    public void setEmployee_role_id(int employee_role_id) {
        this.employee_role_id = employee_role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getAlias_name() {
        return alias_name;
    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
