package com.party.jackseller.bean;

import java.util.List;

/**
 * Created by 派对 on 2018/8/11.
 */

public class GoodsCategory implements CommonInface{

    private long id;
    private String name;
    private boolean check;
    private List<Goods> GoodsList;
    private long shop_id;

    public GoodsCategory(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public GoodsCategory() {
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }


    public List<Goods> getGoodsList() {
        return GoodsList;
    }

    public void setGoodsList(List<Goods> goodsList) {
        GoodsList = goodsList;
    }

    @Override
    public boolean equals(Object object) {
        if(this.getId() == ((long)object)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "GoodsCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", check=" + check +
                ", GoodsList=" + GoodsList +
                ", shop_id=" + shop_id +
                '}';
    }
}
