package com.party.jackseller.bean;

/**
 * Created by 南宫灬绝痕 on 2019/1/7.
 */

public class TradeAnalysisBean {
    private int count;
    private double total;
    private double avg;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }
}
