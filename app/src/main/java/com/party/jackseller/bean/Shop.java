package com.party.jackseller.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 派对 on 2018/7/25.
 */

public class Shop implements Parcelable {
    /**
     * id : 1533195097805
     * shop_name : 寇芊芊的冰糕店
     * shop_address : 啦啦啦
     * shop_state : 1
     * home_img : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img1.jpg
     * shop_state_name : 已上线
     */

    private long id;

    @Expose
    @SerializedName("shop_name")
    private String shop_name;
    @Expose
    @SerializedName("shop_address")
    private String shop_address;
    @Expose
    @SerializedName("shop_state")
    private int shop_state;
    @Expose
    @SerializedName("home_img")
    private String home_img;
    @Expose
    @SerializedName("shop_state_name")
    private String shop_state_name;
    @Expose
    @SerializedName("latitude")
    private double latitude;
    @Expose
    @SerializedName("longitude")
    private double longitude;
    @Expose
    @SerializedName("is_password")
    private int isPassword;
    @Expose
    @SerializedName("rebate_discount")
    private String rebate_discount;
    @Expose
    @SerializedName("discount")
    private String discount;

    public String getRebate_discount() {
        return rebate_discount;
    }

    public void setRebate_discount(String rebate_discount) {
        this.rebate_discount = rebate_discount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    protected Shop(Parcel in) {
        id = in.readLong();
        shop_name = in.readString();
        shop_address = in.readString();
        shop_state = in.readInt();
        home_img = in.readString();
        shop_state_name = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        isPassword = in.readInt();
        rebate_discount = in.readString();
        discount = in.readString();
        master = in.readInt();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };


    private int master;

    public int getMaster() {
        return master;
    }

    public void setMaster(int master) {
        this.master = master;
    }

    public int getIsPassword() {
        return isPassword;
    }

    public void setIsPassword(int isPassword) {
        this.isPassword = isPassword;
    }

    private transient boolean isChecked;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public int getShop_state() {
        return shop_state;
    }

    public void setShop_state(int shop_state) {
        this.shop_state = shop_state;
    }

    public String getHome_img() {
        return home_img;
    }

    public void setHome_img(String home_img) {
        this.home_img = home_img;
    }

    public String getShop_state_name() {
        return shop_state_name;
    }

    public void setShop_state_name(String shop_state_name) {
        this.shop_state_name = shop_state_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.shop_name);
        dest.writeString(this.shop_address);
        dest.writeInt(this.shop_state);
        dest.writeString(this.home_img);
        dest.writeString(this.shop_state_name);
        dest.writeString(this.rebate_discount);
        dest.writeString(this.discount);
    }

    public Shop() {
    }


}
