package com.party.jackseller.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    /**
     * icon : null
     * nickname : null
     * phone : 15808207560
     * roleId : 1
     * token : f963d3cad4ff4cb68065a2e256f1de8d
     */
    private long id;
    private String icon;
    private String nickname;
    private String phone;
    private String name;
    private int roleId;
    private double balance;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 1 是绑定 0 没有通过认证
     */
    private int isBindingWx;
    /**
     * 1 是绑定 0 没有通过认证
     */
    private int isBindingAli;
    /**
     * 1 是有通过认证 0 没有通过认证
     */
    @Expose
    @SerializedName("isAuthentication")
    private int isAuthentication;
    /**
     * 1 是有密码 0 没有密码
     */
    private int isPassword;

    private String token;

    public int getIsAuthentication() {
        return isAuthentication;
    }

    public void setIsAuthentication(int isAuthentication) {
        this.isAuthentication = isAuthentication;
    }

    public int getIsBindingWx() {
        return isBindingWx;
    }

    public void setIsBindingWx(int isBindingWx) {
        this.isBindingWx = isBindingWx;
    }

    public int getIsBindingAli() {
        return isBindingAli;
    }

    public void setIsBindingAli(int isBindingAli) {
        this.isBindingAli = isBindingAli;
    }

    public int getIsPassword() {
        return isPassword;
    }

    public void setIsPassword(int isPassword) {
        this.isPassword = isPassword;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
