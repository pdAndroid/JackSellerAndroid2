package com.party.jackseller.bean;

import android.os.Parcel;

import com.party.jackseller.R;

/**
 * Created by 派对 on 2018/8/11.
 */

public class Goods implements CommonInface {

    /**
     * id : 1
     * title : 土豆
     * price : 1000
     * img_url : https://pdkj.oss-cn-beijing.aliyuncs.com/goods/e40b2e1de365e2f4.jpg
     * describe : 精品大土豆
     * unit : 份
     * type_id : 1
     * type_name : 素菜
     * type_of_id : 3
     */
    private long id;
    private String title;
    private String price;
    private String img_url;
    private String describe;
    private String unit;
    private long type_id;
    private String type_name;
    private int count = 1;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int increase(int count) {
        this.count += count;
        return this.count;
    }

    public int reduce(int count) {
        this.count -= count;
        return this.count;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public long getType_id() {
        return type_id;
    }

    public void setType_id(long type_id) {
        this.type_id = type_id;
    }

    @Override
    public boolean equals(Object obj) {
        if (((Goods) obj).getId() == this.getId()) {
            return true;
        } else {
            return false;
        }
    }
}
