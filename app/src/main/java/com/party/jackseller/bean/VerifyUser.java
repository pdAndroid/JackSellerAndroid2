package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/10/18.
 * 邻里惠中的查询实体
 */

public class VerifyUser {

    private String verKey;

    public String getVerKey() {
        return verKey;
    }

    public void setVerKey(String verKey) {
        this.verKey = verKey;
    }
}
