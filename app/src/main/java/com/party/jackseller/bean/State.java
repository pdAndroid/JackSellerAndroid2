package com.party.jackseller.bean;

import com.party.jackseller.BaseFragment;

/**
 * Created by 派对 on 2018/8/8.
 */

public class State {

    private int id;
    private String name;
    private BaseFragment fragment;


    public BaseFragment getFragment() {
        return fragment;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }

    public State(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public State(int id, String name, BaseFragment fragment) {
        this.id = id;
        this.name = name;
        this.fragment = fragment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
