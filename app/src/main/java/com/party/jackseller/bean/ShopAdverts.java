package com.party.jackseller.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class ShopAdverts implements Parcelable {

    private Long id;
    private String shopName;
    private String shopAddress;
    private String introduce;
    private String homeImg;
    private Double latitude;
    private Double longitude;
    private Integer shopState;

    public ShopAdverts() {
    }

    protected ShopAdverts(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        shopName = in.readString();
        shopAddress = in.readString();
        introduce = in.readString();
        homeImg = in.readString();
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            shopState = null;
        } else {
            shopState = in.readInt();
        }
    }

    public static final Creator<ShopAdverts> CREATOR = new Creator<ShopAdverts>() {
        @Override
        public ShopAdverts createFromParcel(Parcel in) {
            return new ShopAdverts(in);
        }

        @Override
        public ShopAdverts[] newArray(int size) {
            return new ShopAdverts[size];
        }
    };

    public Integer getShopState() {
        return shopState;
    }

    public void setShopState(Integer shopState) {
        this.shopState = shopState;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(shopName);
        parcel.writeString(shopAddress);
        parcel.writeString(introduce);
        parcel.writeString(homeImg);
        if (latitude == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(latitude);
        }
        if (longitude == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(longitude);
        }
    }


    /*private transient Double buyPrice;*/

}
