package com.party.jackseller.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/9/19.
 */

public class ShopCar {
    private Double originalPrice;
    private List<Goods> goodsList = new ArrayList<>();

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public List<Goods> getGoodsList() {
        return goodsList;
    }

}
