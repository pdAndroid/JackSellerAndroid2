package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/10/20.
 */

public class SpellingGroupCouponDetailBean {

    private String nickname;
    private String phone;
    private String icon;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
