package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/7/26.
 */

public class TradeRecord implements DisplayableItem {

    /**
     * create : 1537338047000
     * title : 寇芊芊的包子店二十五元团购卷
     * tradeRecordType : 0
     * userPhone : 19983372907
     * value : 297
     * <p>
     * <p>
     * private String title;
     * private String userPhone;
     * private Date create;时间
     * private String payer;括号里的
     * private Double value;红钱
     * private Integer tradeRecordType;0+ 1-
     * private int type;
     * private Long id;
     * private Double totalPrice;总价 黑色字
     */
    private Long id;
    private long create;
    private String title;
    private int tradeRecordType;
    private String userPhone;
    private String payer;
    private double value;
    private String totalPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTradeRecordType() {
        return tradeRecordType;
    }

    public void setTradeRecordType(int tradeRecordType) {
        this.tradeRecordType = tradeRecordType;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
