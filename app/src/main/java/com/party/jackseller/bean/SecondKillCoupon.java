package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/9/25.
 */

public class SecondKillCoupon {

    /**
     * availableDate : null
     * buyPrice : 9.9
     * couponState : 1
     * goodsRange : 全店通用
     * id : 11
     * originalPrice : 50
     * shopName : 迪奥口红专卖店
     */

    private Object availableDate;
    private double buyPrice;
    private int couponState;
    private String goodsRange;
    private long id;
    private int originalPrice;
    private String shopName;

    public Object getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(Object availableDate) {
        this.availableDate = availableDate;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getCouponState() {
        return couponState;
    }

    public void setCouponState(int couponState) {
        this.couponState = couponState;
    }

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(int originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
