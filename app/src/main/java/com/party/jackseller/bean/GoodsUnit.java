package com.party.jackseller.bean;

/**
 * Created by Administrator on 2018/9/16.
 */

public class GoodsUnit {

    /**
     * id : 2
     * name : 次
     */

    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
