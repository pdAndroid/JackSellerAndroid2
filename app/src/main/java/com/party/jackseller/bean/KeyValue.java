package com.party.jackseller.bean;

public class KeyValue {

    private String id;
    private String name;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    private Double value;//入账金额
//    private Long shopId;
//    private Long OrderId;
//    private Long tradeRecordStateId;
//    private String title;//类别
//    private String userPhone;
//    private String payer;//付款人
//    private String remarks;//类别
//    private long create;//时间
//    private Double fee;//手续费
//    private Double totalPrice;//总价
//    private Integer couponType;
//    private Double discount;//折扣
//    private Long couponId;
//    private Integer couponNum;
//    private String itemName;//商品名称
//    private String addRebateMoney;//返利金
//    private String useRebateMoney;//使用的返利金
//    private String noDiscountPrice;//不参与优惠金额
//
//    public Double getValue() {
//        return value;
//    }
//
//    public void setValue(Double value) {
//        this.value = value;
//    }
//
//    public Long getShopId() {
//        return shopId;
//    }
//
//    public void setShopId(Long shopId) {
//        this.shopId = shopId;
//    }
//
//    public Long getOrderId() {
//        return OrderId;
//    }
//
//    public void setOrderId(Long orderId) {
//        OrderId = orderId;
//    }
//
//    public Long getTradeRecordStateId() {
//        return tradeRecordStateId;
//    }
//
//    public void setTradeRecordStateId(Long tradeRecordStateId) {
//        this.tradeRecordStateId = tradeRecordStateId;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getUserPhone() {
//        return userPhone;
//    }
//
//    public void setUserPhone(String userPhone) {
//        this.userPhone = userPhone;
//    }
//
//    public String getPayer() {
//        return payer;
//    }
//
//    public void setPayer(String payer) {
//        this.payer = payer;
//    }
//
//    public String getRemarks() {
//        return remarks;
//    }
//
//    public void setRemarks(String remarks) {
//        this.remarks = remarks;
//    }
//
//    public long getCreate() {
//        return create;
//    }
//
//    public void setCreate(long create) {
//        this.create = create;
//    }
//
//    public Double getFee() {
//        return fee;
//    }
//
//    public void setFee(Double fee) {
//        this.fee = fee;
//    }
//
//    public Double getTotalPrice() {
//        return totalPrice;
//    }
//
//    public void setTotalPrice(Double totalPrice) {
//        this.totalPrice = totalPrice;
//    }
//
//    public Integer getCouponType() {
//        return couponType;
//    }
//
//    public void setCouponType(Integer couponType) {
//        this.couponType = couponType;
//    }
//
//    public Double getDiscount() {
//        return discount;
//    }
//
//    public void setDiscount(Double discount) {
//        this.discount = discount;
//    }
//
//    public Long getCouponId() {
//        return couponId;
//    }
//
//    public void setCouponId(Long couponId) {
//        this.couponId = couponId;
//    }
//
//    public Integer getCouponNum() {
//        return couponNum;
//    }
//
//    public void setCouponNum(Integer couponNum) {
//        this.couponNum = couponNum;
//    }
//
//    public String getItemName() {
//        return itemName;
//    }
//
//    public void setItemName(String itemName) {
//        this.itemName = itemName;
//    }
//
//    public String getAddRebateMoney() {
//        return addRebateMoney;
//    }
//
//    public void setAddRebateMoney(String addRebateMoney) {
//        this.addRebateMoney = addRebateMoney;
//    }
//
//    public String getUseRebateMoney() {
//        return useRebateMoney;
//    }
//
//    public void setUseRebateMoney(String useRebateMoney) {
//        this.useRebateMoney = useRebateMoney;
//    }
//
//    public String getNoDiscountPrice() {
//        return noDiscountPrice;
//    }
//
//    public void setNoDiscountPrice(String noDiscountPrice) {
//        this.noDiscountPrice = noDiscountPrice;
//    }
}
