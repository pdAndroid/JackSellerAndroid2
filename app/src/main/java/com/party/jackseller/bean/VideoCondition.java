package com.party.jackseller.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoCondition {
    /**
     * price : 1
     * condition :
     * id : 1
     */
    private String minPrice;
    private String maxPrice;
    private String sexPrice;
    private String agePrice;
    private String sexRatioPrice;

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getSexPrice() {
        return sexPrice;
    }

    public void setSexPrice(String sexPrice) {
        this.sexPrice = sexPrice;
    }

    public String getAgePrice() {
        return agePrice;
    }

    public void setAgePrice(String agePrice) {
        this.agePrice = agePrice;
    }

    public String getSexRatioPrice() {
        return sexRatioPrice;
    }

    public void setSexRatioPrice(String sexRatioPrice) {
        this.sexRatioPrice = sexRatioPrice;
    }
}
