package com.party.jackseller.bean;

import android.view.View;
import android.view.ViewGroup;

import com.party.jackseller.adapter.MyViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 派对 on 2018/8/13.
 */

public class ListItem {


    public static class type {
        public static final int CLICK = 0;
        public static final int NUMBER = 1;
        public static final int CHECKBOX = 2;
        public static final int TEXT = 3;
        public static final int EDIT = 4;
        public static final int PRICE = 5;
        public static final int DATE_OR_DATE = 6;
        public static final int TIME_OR_TIME = 7;
        public static final int RADIO = 8;
        public static final int DIALOG = 9;
    }

    private String name;
    private String hint;
    private String tag;
    private View view;
    private boolean clickable;
    private List<String> valueList = new ArrayList<>();
    private String[] valueArr;
    private View.OnClickListener onClickListener;

    public ListItem(String name, String hint, String tag, String... value) {
        this.name = name;
        this.hint = hint;
        this.tag = tag;
        valueArr = value;
        valueList.addAll(Arrays.asList(value));
    }

    public ListItem setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    public String getValue(int i) {
        if (valueList.size() > i) {
            return valueList.get(i);
        }
        return null;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }


    public String getValueArrString() {
        String result = "";
        for (int i = 0; i < valueList.size(); i++) {
            result += valueList.get(i);
            if (result.length() > 10) {
                result = result.substring(0, 10) + "...";
                break;
            }
            result += ",";
        }
        return result;
    }

    public ListItem setViewHolder1(MyViewHolder myViewHolder, ViewGroup viewGroup) {
        view = myViewHolder.getView(this);
        viewGroup.addView(view, viewGroup.getChildCount() - 2);
        return this;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public ListItem setViewHolder(MyViewHolder myViewHolder, ViewGroup viewGroup) {
        View view = myViewHolder.getView(this);
        viewGroup.addView(view);
        return this;
    }

    public String[] getValueArr() {
        return valueArr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }
}
