package com.party.jackseller.bean;

public class VideoBean {

    /**
     * reviewStatus : 0
     * sexLimit : 0
     * shopId : 1534994219559
     * shopName : 杰哥烧烤店
     * totalCount : 1000
     * totalMoney : 45000
     * type : 1
     * videoId : 5285890782375070423
     * videoSize : 4737464
     * videoTime : 10
     * videoURL : http://1257403081.vod2.myqcloud.com/d0993821vodcq1257403081/97d993ca5285890782375070423/SS2q3jnGT2MA.mp4
     */

    private int reviewStatus;
    private int sexLimit;
    private long shopId;
    private String shopName;
    private int totalCount;
    private int totalMoney;
    private int type;
    private String videoId;
    private String videoSize;
    private String videoTime;
    private String videoURL;

    public int getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(int reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public int getSexLimit() {
        return sexLimit;
    }

    public void setSexLimit(int sexLimit) {
        this.sexLimit = sexLimit;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(int totalMoney) {
        this.totalMoney = totalMoney;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }

    public String getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(String videoTime) {
        this.videoTime = videoTime;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }
}
