package com.party.jackseller.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * app升级实体<br>
 * Created by tangxuebing on 2018/5/14.
 */

public class AppUpdateBean {
    @Expose
    @SerializedName("versionCode")
    private int versionCode;
    @Expose
    @SerializedName("versionName")
    private String versionName;
    @Expose
    @SerializedName("forceUpdate")
    private boolean forceUpdate;
    @Expose
    @SerializedName("currentVersionDesc")
    private String currentVersionDesc;
    @Expose
    @SerializedName("apkDownLoadUrl")
    private String apkDownLoadUrl;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getCurrentVersionDesc() {
        return currentVersionDesc;
    }

    public void setCurrentVersionDesc(String currentVersionDesc) {
        this.currentVersionDesc = currentVersionDesc;
    }

    public String getApkDownLoadUrl() {
        return apkDownLoadUrl;
    }

    public void setApkDownLoadUrl(String apkDownLoadUrl) {
        this.apkDownLoadUrl = apkDownLoadUrl;
    }
}
