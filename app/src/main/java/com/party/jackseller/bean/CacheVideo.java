package com.party.jackseller.bean;

import android.content.Intent;

public class CacheVideo {

    private String videoId;
    private String videoURL;
    private String coverURL;
    private long videoDuration;
    private long videoFileSize;
    private long videoTime;

    public CacheVideo(Intent intent) {
        videoId = intent.getStringExtra("videoId");
        videoURL = intent.getStringExtra("videoURL");
        coverURL = intent.getStringExtra("coverURL");
        long videoDuration = intent.getLongExtra("videoDuration", 0);
        // 时长为秒
        videoTime = (int) (videoDuration / 1000);
        videoFileSize = intent.getLongExtra("videoFileSize", 0);
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public long getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(long videoDuration) {
        this.videoDuration = videoDuration;
    }

    public long getVideoFileSize() {
        return videoFileSize;
    }

    public void setVideoFileSize(long videoFileSize) {
        this.videoFileSize = videoFileSize;
    }

    public long getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(long videoTime) {
        this.videoTime = videoTime;
    }
}
