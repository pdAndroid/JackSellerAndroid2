package com.party.jackseller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.party.jackseller.controller.AMapController;
import com.party.jackseller.uiadvert.AdvertsFragment;
import com.party.jackseller.uihome.HomeFragment;
import com.party.jackseller.uimy.MyFragment;
import com.party.jackseller.utils.ConstUtils;
import com.yinglan.alphatabs.AlphaTabsIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivityTitle {
    @BindView(R.id.bottom_bar)
    AlphaTabsIndicator mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private HomeFragment homeFragment;
    private AdvertsFragment advertsFragment;
    private MyFragment myFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
    }


    protected void initData() {
        ConstUtils.LOCATION_ALWAYS = true;
        setNeedOnCreateRegister();

        homeFragment = (HomeFragment) HomeFragment.newInstance();
        advertsFragment = (AdvertsFragment) AdvertsFragment.newInstance();
        myFragment = (MyFragment) MyFragment.newInstance();

        MainAdapter mainAdapter = new MainAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mainAdapter);
        mViewPager.addOnPageChangeListener(mainAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mTabLayout.setViewPager(mViewPager);

        mTabLayout.getTabView(0).showNumber(0);
        mTabLayout.getTabView(1).showNumber(0);
        mTabLayout.getTabView(2).showNumber(0);
    }


    private class MainAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

        private List<Fragment> fragments = new ArrayList<>();

        public MainAdapter(FragmentManager fm) {
            super(fm);
            fragments.add(homeFragment);
            fragments.add(advertsFragment);
            fragments.add(myFragment);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println(position);
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (0 == position) {
                mTabLayout.getTabView(0).showNumber(mTabLayout.getTabView(0).getBadgeNumber() - 1);
            } else if (2 == position) {
                myFragment.refreshData();
                mTabLayout.getCurrentItemView().removeShow();
            } else if (3 == position) {
                mTabLayout.removeAllBadge();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    long firstTime;

    @Override
    public void onBackPressed() {
        long secondTime = System.currentTimeMillis();
        if (secondTime - firstTime > 2000L) {
            showToast("再按一次退出程序");
            firstTime = secondTime;
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            boolean isSwitch = data.getBooleanExtra("isSwitch", false);
            if (isSwitch) {
                notifyHomeFragmentDataChanged();
            }
        }
    }

    public void notifyHomeFragmentDataChanged() {
        homeFragment.initBusinessAnalysis();
        homeFragment.setCurrShopName(mApplication.getCurrShopName());
        mTabLayout.getTabView(0).callOnClick();
    }
}
