package com.party.jackseller.controller;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.party.jackseller.R;
import com.party.jackseller.MApplication;
import com.party.jackseller.choose_location.ChooseLocationActivity;
import com.party.jackseller.event.AMapBeanEvent;
import com.party.jackseller.permission.RuntimeRationale;
import com.party.jackseller.utils.ConstUtils;
import com.party.jackseller.utils.MLocationUtils;
import com.party.jackseller.utils.MLogUtils;
import com.party.jackseller.utils.NetworkUtils;
import com.party.jackseller.utils.ToastUtils;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;
import com.yanzhenjie.permission.Setting;

import java.lang.ref.WeakReference;
import java.util.List;

public class AMapController {
    private static final String TAG = AMapController.class.getSimpleName();
    private AMapLocationClient locationClient = null;
    private AMapLocationClientOption locationOption = new AMapLocationClientOption();
    private WeakReference<Activity> mActivity;
    private AlertDialog mAlertDialog;
    /**
     * 定位监听
     */
    AMapLocationListener locationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation loc) {
            if (null != loc) {
                // 当前位置是否发生了改变
                String[] permissions = new String[]{Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION};
                if (loc.getErrorCode() == 0) {
                    // 获取到位置之后把弹窗消失掉
                    if (mAlertDialog != null) mAlertDialog.dismiss();

                    AMapBeanEvent aMapBeanEvent = new AMapBeanEvent();
                    aMapBeanEvent.setErrorCode(loc.getErrorCode());
                    aMapBeanEvent.setLongitude(loc.getLongitude());
                    aMapBeanEvent.setLatitude(loc.getLatitude());
                    aMapBeanEvent.setProvince(loc.getProvince());
                    aMapBeanEvent.setAdCode(loc.getAdCode());
                    aMapBeanEvent.setCity(loc.getCity());
                    aMapBeanEvent.setDistrict(loc.getDistrict());
                    aMapBeanEvent.setCountry(loc.getCountry());
                    aMapBeanEvent.setCityCode(loc.getCityCode());
                    aMapBeanEvent.setAddress(loc.getAddress());
                    aMapBeanEvent.setRoad(loc.getRoad());
                    aMapBeanEvent.setStreet(loc.getStreet());
                    // 保存一份数据到全局变量里面
                    ((MApplication) mActivity.get().getApplication()).setMap(aMapBeanEvent);
                    //完成定位并回调
                    if (mLocationFinish != null) mLocationFinish.finish(aMapBeanEvent);
                } else {
                    showSettingDialog(mActivity.get(), permissions);
                    MLogUtils.d(TAG, "定位失败" + " 错误码:" + loc.getErrorCode());
                }
            } else {
                MLogUtils.d(TAG, "定位失败，loc is null");
            }
            // 这里如果不调用停止,会一直定位
            if (!ConstUtils.LOCATION_ALWAYS) {
                destroyLocation();
            }
        }
    };

    public AMapController(Activity activity) {
        this.mActivity = new WeakReference<>(activity);
    }

    /**
     * Display setting dialog.
     */
    public void showSettingDialog(Context context, final String[] permissions) {
        // 这里由于手机熄灭屏幕太久之后就会出现,之前获取到过数据就不再使用权限弹窗了
        if (mActivity.get().getApplication() instanceof MApplication) {
            AMapBeanEvent mapBeanEvent = ((MApplication) mActivity.get().getApplication()).getMap();
            if (mapBeanEvent != null) {
                return;
            }
        }
        List<String> permissionNames = Permission.transformText(context, permissions);
        String message = context.getString(R.string.message_permission_always_failed, TextUtils.join("\n", permissionNames));
        if (mAlertDialog == null) {
            mAlertDialog = new AlertDialog.Builder(mActivity.get())
                    .setCancelable(false)
                    .setTitle(R.string.title_dialog)
                    .setMessage(message)
                    .setPositiveButton(R.string.setting, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setPermission();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
        } else {
            if (!mAlertDialog.isShowing()) {
                mAlertDialog.show();
            }
        }
    }

    public void setPermission() {
        AndPermission.with(mActivity.get()).runtime().setting().onComeback(new Setting.Action() {
            @Override
            public void onAction() {
                String[] permissions = new String[]{Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION};
                if (AndPermission.hasPermissions(mActivity.get(), permissions)) {
                    startLocation2();
                }
            }
        }).start();
    }


    /**
     * 开始定位
     *
     * @author hongming.wang
     * @since 2.8.0
     */
    public void startLocation2() {
        initLocation();
        // 设置定位参数
        locationClient.setLocationOption(locationOption);
        // 启动定位
        locationClient.startLocation();
    }

    /**
     * 销毁定位
     *
     * @author hongming.wang
     * @since 2.8.0
     */
    public void destroyLocation() {
        ConstUtils.LOCATION_ALWAYS = false;
        if (null != locationClient) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            stopLocation();
            locationClient.onDestroy();
            locationClient = null;
            locationOption = null;
        }
    }

    /**
     * 默认的定位参数
     *
     * @author hongming.wang
     * @since 2.8.0
     */
    private AMapLocationClientOption getDefaultOption() {
        AMapLocationClientOption mOption = new AMapLocationClientOption();
        mOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
        mOption.setGpsFirst(false);//可选，设置是否gps优先，只在高精度模式下有效。默认关闭
        mOption.setHttpTimeOut(30 * 1000);//可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
        mOption.setInterval(60 * 1000);//可选，设置定位间隔。默认为2秒
        mOption.setNeedAddress(true);//可选，设置是否返回逆地理地址信息。默认是true
        mOption.setOnceLocation(false);//可选，设置是否单次定位。默认是false
        mOption.setOnceLocationLatest(false);//可选，设置是否等待wifi刷新，默认为false.如果设置为true,会自动变为单次定位，持续定位时不要使用
        AMapLocationClientOption.setLocationProtocol(AMapLocationClientOption.AMapLocationProtocol.HTTP);//可选， 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
        mOption.setSensorEnable(false);//可选，设置是否使用传感器。默认是false
        mOption.setWifiScan(true); //可选，设置是否开启wifi扫描。默认为true，如果设置为false会同时停止主动刷新，停止以后完全依赖于系统刷新，定位位置可能存在误差
        mOption.setLocationCacheEnable(true); //可选，设置是否使用缓存定位，默认为true
        return mOption;
    }

    /**
     * 初始化定位
     *
     * @author hongming.wang
     * @since 2.8.0
     */
    private void initLocation() {
        //初始化client
        locationClient = new AMapLocationClient(mActivity.get().getApplicationContext());
        //设置定位参数
        locationClient.setLocationOption(getDefaultOption());
        // 设置定位监听
        locationClient.setLocationListener(locationListener);
    }

    public void startLocation() {
        String[] permissions = new String[]{Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION};
        AndPermission.with(mActivity.get())
                .runtime()
                .permission(permissions)
                .rationale(new RuntimeRationale())
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限成功");
                        startLocation2();
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限失败");

                    }
                }).start();
    }

    /**
     * 停止定位
     *
     * @author hongming.wang
     * @since 2.8.0
     */
    private void stopLocation() {
        // 停止定位
        locationClient.stopLocation();
    }

    public void choosePosition(final Activity activity) {
        String[] permissions = new String[]{Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION};
        AndPermission.with(mActivity.get())
                .runtime()
                .permission(permissions)
                .rationale(new RuntimeRationale())
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限成功");
                        Intent intent = new Intent(activity, ChooseLocationActivity.class);
                        activity.startActivityForResult(intent, ConstUtils.SEARCH_LOCATION_CODE);
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        Log.e("checkPermissions", "获取权限失败");

                    }
                }).start();
    }

    AMapController.LocationFinish mLocationFinish;

    public interface LocationFinish {
        void finish(AMapBeanEvent mapBeanEvent);
    }

    public void setFinishListener(AMapController.LocationFinish finishListener) {
        this.mLocationFinish = finishListener;
    }
}
