package com.party.jackseller.controller;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

public class VerCodeTimer extends CountDownTimer {

    TextView textView;
    View[] views;

    public VerCodeTimer(int second,TextView textView) {
        super(second* 1000L, 1000L);
        this.textView = textView;
    }

    public void setChangeView(View... views) {
        this.views = views;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        int leaveSecond = (int) (millisUntilFinished / 1000L);
        textView.setText(String.format("请稍候(%1$ds)", leaveSecond));
        setEnable(false);
    }

    private void setEnable(boolean enable) {
        textView.setEnabled(enable);
        if(views!=null)
        for (View v : views) {
            v.setEnabled(enable);
        }
    }

    @Override
    public void onFinish() {
        setEnable(true);
        textView.setText("获取验证码");
    }
}