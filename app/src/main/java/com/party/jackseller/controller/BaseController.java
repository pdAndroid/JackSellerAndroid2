package com.party.jackseller.controller;

import com.party.jackseller.BaseActivity;
import com.party.jackseller.MApplication;

import java.lang.ref.WeakReference;

/**
 * Created by tangxuebing on 2018/6/26.
 */

public class BaseController {
    protected WeakReference<BaseActivity> baseActivity;
    protected MApplication mApplication;

    public BaseController(BaseActivity baseActivity) {
        this.baseActivity = new WeakReference<>(baseActivity);
        this.mApplication = (MApplication) baseActivity.getApplication();
    }
}
