package com.party.jackseller.controller;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.party.jackseller.BaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChooseDateController extends BaseController {
    private SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

    public ChooseDateController(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public void onPickDateEvent(final TextView tView, final Date date, final String top) {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        selectedDate.setTime(date);
        new DatePickerDialog(baseActivity.get(),
                // 绑定监听器
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Date checkDate = null;
                        String tempDate = year + "-" + (month + 1) + "-" + day;
                        try {
                            checkDate = formatDate.parse(tempDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (date.compareTo(checkDate) == -1) {
                            tView.setText(tempDate);
                            return;
                        }
                        baseActivity.get().showToast(top);
                    }
                }
                // 设置初始时间
                , selectedDate.get(Calendar.YEAR)
                , selectedDate.get(Calendar.MONTH)
                , selectedDate.get(Calendar.DATE) + 1
        ).show();
    }


    public void onPickTimeEvent(final View view) {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        new TimePickerDialog(baseActivity.get(),
                // 绑定监听器
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker time, int hourOfDay, int minute) {
                        TextView tv = (TextView) view;
                        tv.setText(validate(hourOfDay) + ":" + validate(minute));
                    }
                }
                // 设置初始时间
                , selectedDate.get(Calendar.HOUR_OF_DAY)
                , selectedDate.get(Calendar.MINUTE)
                , true).show();
    }

    private String validate(int time) {
        return time < 10 ? "0" + time : "" + time;
    }
}
