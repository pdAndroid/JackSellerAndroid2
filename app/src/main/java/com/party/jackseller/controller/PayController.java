package com.party.jackseller.controller;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackseller.R;
import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.PayService;
import com.party.jackseller.BaseActivity;
import com.party.jackseller.Constant;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.CommonResult;
import com.party.jackseller.bean.PayResult;
import com.party.jackseller.bean.WxPaymentBean;
import com.party.jackseller.event.PayResultEvent;
import com.party.jackseller.pay.alipay.Alipay;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.NetworkUtils;
import com.party.jackseller.utils.OnPasswordInputFinish;
import com.party.jackseller.view.loadviewhelper.load.LoadViewHelper;
import com.party.jackseller.widget.InputPayPasswordView;
import com.party.jackseller.wxapi.WXPayUtils;
import com.zyyoona7.popup.EasyPopup;

import org.greenrobot.eventbus.EventBus;

import javax.annotation.Nullable;

public class PayController extends BaseController implements OnPasswordInputFinish {
    PayService payService;
    private BaseActivity activity;
    private EasyPopup payChoosePopupWindow, inputPayPwdPopupWindow;
    private ViewGroup wholeLayout;
    private View helpView;
    private String orderIdStr;
    private long orderId;
    private int goodsType, finalNum;
    private final int PAY_TYPE_ZHIFUBAO = 1, PAY_TYPE_WEIXIN = 2, PAY_TYPE_MY_WALLET = 3;
    private int currChoosePayType = PAY_TYPE_MY_WALLET;
    private ImageView my_wallet_iv, weixin_pay_iv, zhifubao_pay_iv;
    private LinearLayout my_wallet_pay_ll, weixin_pay_ll, zhifubao_pay_ll;
    private double walletBalance, buyPrice;
    private TextView pay_account_tv;

    public PayController(BaseActivity baseActivity) {
        super(baseActivity);
        payService = new PayService(baseActivity);
        activity = baseActivity;
    }

    public EasyPopup getPayChoosePopupWindow() {
        return payChoosePopupWindow;
    }

    public void setPayChoosePopupWindow(EasyPopup payChoosePopupWindow) {
        this.payChoosePopupWindow = payChoosePopupWindow;
    }

    public View getHelpView() {
        return helpView;
    }

    public void setHelpView(View helpView) {
        this.helpView = helpView;
    }

    public ViewGroup getWholeLayout() {
        return wholeLayout;
    }

    public void setWholeLayout(ViewGroup wholeLayout) {
        this.wholeLayout = wholeLayout;
    }

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    /**
     * 微信支付,先从后台服务器获取微信支付参数
     *
     * @param orderId
     */
    public void doWxPayWork(String orderId) {
        String ip = NetworkUtils.getIPAddress(baseActivity.get());
        if (TextUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        baseActivity.get().addDisposableIoMain(payService.wxPayOrder(orderId, ip), new DefaultConsumer<WxPaymentBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<WxPaymentBean> result) {
                doWxPay(result.getData());
                activity.hideAlertDialog();
            }
        });
    }

    /**
     * 支付宝支付,先从后台服务器获取支付宝支付参数
     *
     * @param orderId
     */
    public void doAliPayWork(String orderId) {
        String ip = NetworkUtils.getIPAddress(baseActivity.get());
        if (TextUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        baseActivity.get().addDisposableIoMain(payService.aliPayOrder(orderId, ip), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> result) {
                doAliPay(result.getData());
                activity.hideAlertDialog();
            }
        });
    }

    /**
     * 余额支付
     *
     * @param orderId
     */
    public void doMyWalletPay(final String orderId, String payPwd, final int goodsType, @Nullable final int finalNum, @Nullable final View view) {
        baseActivity.get().addDisposableIoMain(payService.doMyWalletPayOrder(orderId, payPwd), new DefaultConsumer<PayResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<PayResult> result) {
                if (result.getData().getResult().equals("SUCCESS")) {
                    switch (goodsType) {

                        case Constant.GOODS_TYPE_SECOND_KILL:
                        case Constant.GOODS_TYPE_REPLACE_MONEY:

                            break;
                        case Constant.GOODS_TYPE_SPELLING_GROUP:
                        case Constant.GOODS_TYPE_GROUP_BUY:

                            break;
                        case Constant.GOODS_TYPE_VIP:
                            ((Button) view).setText("已开通");
                            break;
                        case Constant.GOODS_TYPE_OTHER:
                            activity.finish();
                    }
                }
            }
        });
    }

    /**
     * 微信支付,先从后台服务器获取微信支付参数
     *
     * @param orderId
     */
    public void doWxPayWork(long orderId) {
        String ip = NetworkUtils.getIPAddress(baseActivity.get());
        if (TextUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        baseActivity.get().addDisposableIoMain(payService.wxPayOrder(orderId, ip), new DefaultConsumer<WxPaymentBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<WxPaymentBean> result) {
                doWxPay(result.getData());
                activity.hideAlertDialog();
            }
        });
    }

    public void initWallet(LoadViewHelper helper) {
        activity.addDisposableIoMain(payService.getShopWallet( ), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                if (helper != null) helper.showError();
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                if (helper != null) helper.showContent();
                setWalletBalance(baseBean.getData().getMoney());
            }
        });
    }

    public void initWallet() {
        initWallet(null);
    }

    /**
     * 支付宝支付,先从后台服务器获取支付宝支付参数
     *
     * @param orderId
     */
    public void doAliPayWork(long orderId) {
        String ip = NetworkUtils.getIPAddress(baseActivity.get());
        if (TextUtils.isEmpty(ip)) {
            ip = "127.0.0.1";
        }
        baseActivity.get().addDisposableIoMain(payService.aliPayOrder(orderId, ip), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> result) {
                doAliPay(result.getData());
                activity.hideAlertDialog();
            }
        });
    }

    /**
     * 余额支付
     *
     * @param orderId
     */
    public void doMyWalletPay(final long orderId, String payPwd, final int goodsType, @Nullable final int finalNum, @Nullable final View view) {
        baseActivity.get().addDisposableIoMain(payService.doMyWalletPayOrder(orderId, payPwd), new DefaultConsumer<PayResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<PayResult> result) {
                if (result.getData().getResult().equals("SUCCESS")) {
                    switch (goodsType) {

                        case Constant.GOODS_TYPE_SECOND_KILL:
                        case Constant.GOODS_TYPE_REPLACE_MONEY:

                            break;
                        case Constant.GOODS_TYPE_SPELLING_GROUP:
                        case Constant.GOODS_TYPE_GROUP_BUY:

                            break;
                        case Constant.GOODS_TYPE_VIP:
                            ((Button) view).setText("已开通");
                            break;
                        case Constant.GOODS_TYPE_OTHER:
                            activity.finish();
                    }
                }
            }
        });
    }

    /**
     * 余额支付
     *
     * @param orderId
     */
    public void doMyWalletPay(final long shopId, String payPwd, String orderId, @Nullable final View view) {
        baseActivity.get().addDisposableIoMain(payService.doMyWalletPayOrder(orderId, shopId, payPwd), new DefaultConsumer<PayResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<PayResult> result) {
                if (result.getData().getResult().equals("SUCCESS")) {
                    if (goodsType == Constant.GOODS_TYPE_VIDEO) {
                        if (inputPayPwdPopupWindow != null) {
                            inputPayPwdPopupWindow.dismiss();
                        }
                        mApplication.post(new PayResultEvent(2, 1));
                        return;
                    }
                    switch (goodsType) {

                        case Constant.GOODS_TYPE_SECOND_KILL:
                        case Constant.GOODS_TYPE_REPLACE_MONEY:

                            break;
                        case Constant.GOODS_TYPE_SPELLING_GROUP:
                        case Constant.GOODS_TYPE_GROUP_BUY:

                            break;
                        case Constant.GOODS_TYPE_VIP:
                            ((Button) view).setText("已开通");
                            break;
                        case Constant.GOODS_TYPE_OTHER:
                            activity.finish();
                    }
                }
            }
        });
    }

    /**
     * 拉起微信支付
     *
     * @param wxPaymentBean
     */
    private void doWxPay(WxPaymentBean wxPaymentBean) {
        wxPaymentBean.setPackageValue("Sign=WXPay");
        WXPayUtils.WXPayBuilder builder = new WXPayUtils.WXPayBuilder();
        builder.setAppId(wxPaymentBean.getAppId())
                .setPartnerId(wxPaymentBean.getPartnerId())
                .setPrepayId(wxPaymentBean.getPrepayId())
                .setPackageValue(wxPaymentBean.getPackageValue())
                .setNonceStr(wxPaymentBean.getNonceStr())
                .setTimeStamp(wxPaymentBean.getTimeStamp())
                .setSign(wxPaymentBean.getSign())
                .build().toWXPayNotSign(baseActivity.get());
    }

    /**
     * 拉起支付宝支付
     *
     * @param params
     */
    private void doAliPay(String params) {
        Alipay alipay = new Alipay(baseActivity.get(), params, new Alipay.AlipayResultCallBack() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new PayResultEvent(1, 1));
            }

            @Override
            public void onDealing() {
                System.out.println();
            }

            @Override
            public void onError(int error_code) {
                EventBus.getDefault().post(new PayResultEvent(1, -1));
            }

            @Override
            public void onCancel() {
                EventBus.getDefault().post(new PayResultEvent(1, 0));
            }
        });
        alipay.doPay();
    }

    public void showPayChoosePopupWindow() {
//        if (payChoosePopupWindow == null) {
        View payChooseView = getPayChooseView();
        payChooseView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (payChoosePopupWindow != null) {
                        payChoosePopupWindow.dismiss();
                        payChoosePopupWindow = null;
                    }
                    return true;
                }
                return false;
            }
        });

        payChoosePopupWindow = EasyPopup.create(activity)
                .setContentView(payChooseView)
                .setAnimationStyle(R.style.Popupwindow)
                .setBackgroundDimEnable(true)
                .setDimValue(0.4f)
                .setDimColor(Color.GRAY)
                .setDimView(wholeLayout)
                .setFocusAndOutsideEnable(true)
                .setWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply();
//        }
        payChoosePopupWindow.showAtLocation(helpView, Gravity.BOTTOM, 0, 0);
    }

    public View getPayChooseView() {
        View layout = activity.getLayoutInflater().inflate(R.layout.choose_pay_type_popup, null);
        TextView wallet_balance_tv = layout.findViewById(R.id.wallet_balance_tv);
        pay_account_tv = layout.findViewById(R.id.pay_account_tv);
        wallet_balance_tv.setText("（余额:" + walletBalance + "元）");
        pay_account_tv.setText("支付金额：¥" + buyPrice + "元");
        setButtonListeners(layout);
        return layout;
    }

    public void setButtonListeners(View layout) {
        ImageView dismiss_iv = layout.findViewById(R.id.dismiss_iv);
        TextView pay_account_tv = layout.findViewById(R.id.pay_account_tv);
        my_wallet_iv = layout.findViewById(R.id.my_wallet_iv);
        weixin_pay_iv = layout.findViewById(R.id.weixin_pay_iv);
        zhifubao_pay_iv = layout.findViewById(R.id.zhifubao_pay_iv);
        my_wallet_pay_ll = layout.findViewById(R.id.my_wallet_pay_ll);
        weixin_pay_ll = layout.findViewById(R.id.weixin_pay_ll);
        zhifubao_pay_ll = layout.findViewById(R.id.zhifubao_pay_ll);
        Button pay_btn = layout.findViewById(R.id.pay_btn);

        my_wallet_iv.setImageResource(R.drawable.pay_type_choose_selected);

        dismiss_iv.setOnClickListener(onClickListener);
        pay_account_tv.setOnClickListener(onClickListener);
        my_wallet_pay_ll.setOnClickListener(onClickListener);
        weixin_pay_ll.setOnClickListener(onClickListener);
        zhifubao_pay_ll.setOnClickListener(onClickListener);
        pay_btn.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dismiss_iv:
                    if (payChoosePopupWindow != null && payChoosePopupWindow.isShowing()) {
                        payChoosePopupWindow.dismiss();
                    }
                    break;
                case R.id.my_wallet_pay_ll:
                    currChoosePayType = PAY_TYPE_MY_WALLET;
                    zhifubao_pay_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    my_wallet_iv.setImageResource(R.drawable.pay_type_choose_selected);
                    weixin_pay_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    break;
                case R.id.weixin_pay_ll:
                    currChoosePayType = PAY_TYPE_WEIXIN;
                    zhifubao_pay_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    my_wallet_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    weixin_pay_iv.setImageResource(R.drawable.pay_type_choose_selected);
                    break;
                case R.id.pay_btn:
                    switch (currChoosePayType) {
                        case PAY_TYPE_MY_WALLET:
                            showInputPwdPopup();
                            break;
                        case PAY_TYPE_WEIXIN:
                            if (TextUtils.isEmpty(orderIdStr)) {
                                doWxPayWork(orderId);
                            } else {
                                doWxPayWork(orderIdStr);
                            }
                            break;
                        case PAY_TYPE_ZHIFUBAO:
                            if (TextUtils.isEmpty(orderIdStr)) {
                                doAliPayWork(orderId);
                            } else {
                                doAliPayWork(orderIdStr);
                            }
                            break;
                    }
                    if (payChoosePopupWindow != null && payChoosePopupWindow.isShowing()) {
                        payChoosePopupWindow.dismiss();
                    }
                    break;
                case R.id.zhifubao_pay_ll:
                    currChoosePayType = PAY_TYPE_ZHIFUBAO;
                    zhifubao_pay_iv.setImageResource(R.drawable.pay_type_choose_selected);
                    my_wallet_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    weixin_pay_iv.setImageResource(R.drawable.pay_type_choose_normal);
                    break;

            }
        }
    };



    public void showInputPwdPopup() {
        InputPayPasswordView passwordView = new InputPayPasswordView(activity);
        passwordView.setOnFinishInput(this);
        if (inputPayPwdPopupWindow == null) {
            inputPayPwdPopupWindow = EasyPopup.create(activity)
                    .setContentView(passwordView)
                    .setAnimationStyle(R.style.Popupwindow)
                    .setBackgroundDimEnable(true)
                    .setDimValue(0.4f)
                    .setDimColor(Color.GRAY)
                    .setDimView(wholeLayout)
                    .setFocusAndOutsideEnable(true)
                    .setWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                    .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                    .apply();
        }
        inputPayPwdPopupWindow.showAtLocation(helpView, Gravity.BOTTOM, 0, 0);
    }

    @Override
    public void inputFinish(String password) {
        doMyWalletPay(mApplication.getCurrShopId(), password, orderIdStr, null);
    }

    public String getOrderIdStr() {
        return orderIdStr;
    }

    public void setOrderIdStr(String orderIdStr) {
        this.orderIdStr = orderIdStr;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(int goodsType) {
        this.goodsType = goodsType;
    }

    public int getFinalNum() {
        return finalNum;
    }

    public void setFinalNum(int finalNum) {
        this.finalNum = finalNum;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }


}
