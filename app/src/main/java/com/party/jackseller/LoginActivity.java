package com.party.jackseller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.party.jackseller.api.DefaultConsumer;
import com.party.jackseller.api.UserService;
import com.party.jackseller.base.MyException;
import com.party.jackseller.base.MyNullException;
import com.party.jackseller.bean.AppInfo;
import com.party.jackseller.bean.BaseResult;
import com.party.jackseller.bean.LoginBean;
import com.party.jackseller.bean.Shop;
import com.party.jackseller.bean.User;
import com.party.jackseller.controller.VerCodeTimer;
import com.party.jackseller.event.AuthResultEvent;
import com.party.jackseller.uimy.MyShopManagerActivity;
import com.party.jackseller.utils.CheckUtils;
import com.party.jackseller.utils.DialogController;
import com.party.jackseller.utils.PhoneUtil;
import com.party.jackseller.wxapi.WXPayUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivityTitle {

    private UserService userService;
    private String phone;
    AppInfo mAppInfo;

    @BindView(R.id.et_account)
    EditText mPhoneEdit;

    @BindView(R.id.et_code)
    EditText mVerCodeEdit;

    @BindView(R.id.getVerCodeBtn)
    Button getVerCodeBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initData();
        initEventBus();
        getAppId();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 获取一键登录信息。
     */
    private void getAppId() {
        showAlertDialog();
        userService.getAppId(new DefaultConsumer<AppInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AppInfo> baseBean) {
                mAppInfo = baseBean.getData();
                hideAlertDialog();
            }
        });
    }


    protected void initData() {
        userService = new UserService(this);
    }

    @OnClick(R.id.getVerCodeBtn)
    public void clickGetVerCode(View view) {
        if (dupCommit(5000)) return;
        try {
            phone = CheckUtils.checkPhone(mPhoneEdit, "号码格式有误");
            addDisposableIoMain(userService.getVerCode(phone,"0"), new DefaultConsumer<Object>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<Object> baseBean) {
                    VerCodeTimer mVerCodeTimer = new VerCodeTimer(60, getVerCodeBtn);
                    mVerCodeTimer.setChangeView(mPhoneEdit, getVerCodeBtn);
                    mVerCodeTimer.start();
                }
            });
        } catch (MyNullException e) {
            showToast(e.getMessage());
            shake(e.getView());
        }
    }


    @OnClick({R.id.register_btn, R.id.onceLogin})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.register_btn:
                register();
                break;
            case R.id.onceLogin:
                getWxCode();
                break;
        }
    }

    private void register() {
        Intent intent = new Intent();
        intent.putExtra("appInfo", mAppInfo);
        startActivity(RegisterActivity.class, intent);
    }


    /**
     * 微信授权成功,回调
     *
     * @param authResultEvent
     */
    @Subscribe(priority = 99)//011sh1Pi03TPAn1aStPi0SiFOi0sh1P9
    public void authFinish(AuthResultEvent authResultEvent) {
        hideAlertDialog();
        EventBus.getDefault().cancelEventDelivery(authResultEvent);
        onceLogin(authResultEvent.getCode());
    }


    public void onceLogin(String code) {
        addDisposableIoMain(userService.wxLogin(code), new DefaultConsumer<LoginBean>(mApplication) {
            @Override
            public void operateError(String message) {
                DialogController.showConfirmDialog(mActivity, "提示", message, "立即注冊", (View v) -> {
                    register();
                });
            }

            @Override
            public void operateSuccess(BaseResult<LoginBean> baseBean) {
                handleLoginInfo(baseBean);
            }
        });
    }

    public void getWxCode() {
        try {
            showAlertDialog();
            WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
            build.getUserCode(mActivity, mAppInfo.getWxAppId());
        } catch (MyException e) {
            showToast(e.getMessage());
        }
    }


    @OnClick(R.id.login_btn)
    public void login(View view) {
        String phone = mPhoneEdit.getText().toString().trim();
        String verCode = mVerCodeEdit.getText().toString().trim();
        try {
            CheckUtils.checkData(!PhoneUtil.isMobileNO(phone), mPhoneEdit, "号码格式有误");
            CheckUtils.checkData(verCode.trim().length() != 4, mVerCodeEdit, "请输入验证码");
            CheckUtils.checkData(this.phone == phone, mPhoneEdit, "请重新获取验证码");
        } catch (MyNullException e) {
            showToast(e.getMessage());
            shake(e.getView());
            return;
        }
        showAlertDialog("正在登录中，请稍后。。。");
        addDisposableIoMain(userService.loginUser(phone, verCode), new DefaultConsumer<LoginBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<LoginBean> baseBean) {
                hideAlertDialog();
                handleLoginInfo(baseBean);
            }
        });
    }

    public void handleLoginInfo(BaseResult<LoginBean> baseBean) {
        if (baseBean.getData().getToken() != null) {
            mApplication.cleanUser();
            getMyShopList(baseBean.getData().getShopList());
            User user = baseBean.getData();//强转 去掉 店铺List
            mApplication.setUser(user);
        } else {
            showToast("登录失败");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void getMyShopList(List<Shop> shops) {
        if (shops != null && shops.size() > 0 && !TextUtils.isEmpty(mApplication.getCurrShopName())) {
            startActivity(MainActivity.class);
        } else {
            startActivity(MyShopManagerActivity.class);
        }
        this.finish();
    }
}
